﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" Async="true" AutoEventWireup="true" CodeBehind="VolunteerEvents.aspx.cs" Inherits="SGServe.VolunteerEvents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
        <script type="text/javascript">
        function generateVIAButton(id) {
            //var hdnfldVariable = document.getElementById("hdnfldVariable");
            //hdnfldVariable.setAttribute("Value", id);
            $('#<%=hdnfldVariable.ClientID %>').val(id);
            <%= generateVia() %>
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         &nbsp;<asp:HiddenField ID="hdnfldVariable" runat="server" />
    <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
    <h1 class="text-h2"></h1>
    <div class="container-fluid">

        <div class="media media-grid media-clearfix-xs">
            <div class="media-left">

               
            </div>
            <div class="media-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default" style="border-radius: 8px">
                            <div class="panel-heading panel-heading-gray">
                                <i class="fa fa-trophy"></i>
                                <asp:Label ID="lbltotalcompletedevents" runat="server" Text=""></asp:Label>
                                Completed Events
                            </div>
                            <div class="panel-body">
                                <center> <asp:Label ID="lblnocompletedevents" runat="server" Text="" Visible="false"></asp:Label></center> 
                                <ul class="notifications-timeline" runat="server" id="completedEvents">
                         
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default" style="border-radius: 8px">
                            <div class="panel-heading panel-heading-gray">
                                <i class="fa fa-calendar"></i> <asp:Label ID="lblupcomingevents" runat="server" Text=""></asp:Label>&nbsp;Upcoming Event
                            </div>
                            <div class="panel-body">
                                  <center> <asp:Label ID="lblnoupcomingevents" runat="server" Text="" Visible="false"></asp:Label></center> 
                                <ul class="notifications-timeline" runat="server" id="upcomingEvents">
                                
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default" style="border-radius: 8px">
                            <div class="panel-heading panel-heading-gray">
                            <i class="fa fa-clock-o"></i><asp:Label ID="lblongoingevents" runat="server" Text=""></asp:Label>&nbsp;Ongoing Events 
                            </div>

                            <div class="panel-body">
                                <center> <asp:Label ID="lblnoongoingevents" runat="server" Text="" Visible="false"></asp:Label></center> 
                             <ul class="notifications-timeline" runat="server" id="ongoingEvents">
                   
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>      
            </div>
        </div>
    </div>
</asp:Content>

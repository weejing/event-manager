﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" Async ="true" AutoEventWireup="true" CodeBehind="EventFeedBack.aspx.cs" Inherits="SGServe.EventFeedBack" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class ="page-section">
       <div class ="row">
           <div class ="col-lg-10 col-lg-offset-1">
                <h4 class ="page-section-heading"> Create FeedBack Form </h4>

               <div class ="panel panel-default">
               <div class ="panel-body">                      
                    <div id ="body" class ="body">                  
                        <asp:PlaceHolder ID="feedBackPlace" runat="server"></asp:PlaceHolder>
                    </div>                             
               </div>
           </div> 
               <asp:Button ID="save" runat="server" class ="btn btn-primary" Text="Submit feedback" OnClick="submitfeedBack" />          
           </div>                      
       </div>                
   </div>
    <asp:HiddenField ID="hiddenCount" runat="server" />
    <asp:HiddenField ID="hiddenType" runat="server" />

    <asp:HiddenField ID="hiddenResponse" runat="server" />

    <script type="text/javascript">
        $(document).ready(function () {

            var typeArray = $('#<%=hiddenType.ClientID%>').val().split(',');
            var numOfQuestion = $('#<%=hiddenCount.ClientID%>').val();

            var response = document.getElementById('<%= hiddenResponse.ClientID %>');

            console.log("num: " + numOfQuestion);

            $('#<%=save.ClientID%>').click(function () {
                for(var count =1; count <= numOfQuestion; count++ )
                {
                    if (typeArray[count -1] == 1)
                    {
                        var value = $('#text' + count).val();
                        response.value += value + ",";
                    }
                    else
                    {
                        var value = $('input[name=radio' + count + ']:checked').val();
                        response.value += value + ",";
                    }
                }
            });

        });
    </script>

</asp:Content>

﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Signup.aspx.cs" Inherits="SGServe.Signup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-section">
        <asp:Panel ID="fbPnl" runat="server">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <h4 class="page-section-heading">
                        <asp:Button ID="btnFB" class="btn btn-fb" runat="server" Text="Signup with Facebook" OnClick="btnFB_Click" /></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="text-center">
                        <div class="form-group">- OR -</div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <%--<h4 class="page-section-heading">Personal Particulars</h4>--%>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3 class="text-h2 ribbon-heading ribbon-primary bottom-left-right">Personal Particulars</h3>
                                <div class="form-group form-control-default required">
                                    <label>
                                        Full Name (as of NRIC)
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtFullName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="txtFullName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default required">
                                            <label>
                                                NRIC
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtNRIC" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ValidationGroup="signUpGrp" ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ID="RegularExpressionValidator1" ControlToValidate="txtNRIC" runat="server" ValidationExpression="^[STFG]\d{7}[A-Z]$" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                            </label>
                                            <asp:TextBox ID="txtNRIC" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default required">
                                            <label>
                                                Phone
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtPhone" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ValidationGroup="signUpGrp" ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ID="RegularExpressionValidator2" ControlToValidate="txtPhone" runat="server" ValidationExpression="^[689]\d{7}$" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                            </label>
                                            <asp:TextBox ID="txtPhone" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default required">
                                            <div class="ribbon-mark ribbon-primary absolute right">
                                                <span class="ribbon">
                                                    <span class="text">*</span>
                                                </span>
                                            </div>
                                            <label>Personality (MBTI) <a href="https://www.16personalities.com/personality-types" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                            <asp:DropDownList ID="ddlPersonality" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlSchLvl_SelectedIndexChanged">
                                                <asp:ListItem>ESTP</asp:ListItem>
                                                <asp:ListItem>ESTJ</asp:ListItem>
                                                <asp:ListItem>ESFP</asp:ListItem>
                                                <asp:ListItem>ESFJ</asp:ListItem>
                                                <asp:ListItem>ENFP</asp:ListItem>
                                                <asp:ListItem>ENFJ</asp:ListItem>
                                                <asp:ListItem>ENTP</asp:ListItem>
                                                <asp:ListItem>ENTJ</asp:ListItem>
                                                <asp:ListItem>ISTJ</asp:ListItem>
                                                <asp:ListItem>ISTP</asp:ListItem>
                                                <asp:ListItem>ISFJ</asp:ListItem>
                                                <asp:ListItem>ISFP</asp:ListItem>
                                                <asp:ListItem>INFJ</asp:ListItem>
                                                <asp:ListItem>INFP</asp:ListItem>
                                                <asp:ListItem>INTJ</asp:ListItem>
                                                <asp:ListItem>INTP</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default required">
                                            <label>
                                                Date of Birth
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="DOB" ErrorMessage="Please select" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </label>
                                            <asp:TextBox ID="DOB" runat="server" class="form-control datepicker" ReadOnly></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default">
                                            <label>Religion</label>
                                            <asp:DropDownList ID="ddlReligion" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" data-toggle="dropdown" Width="90%">
                                                <asp:ListItem>None</asp:ListItem>
                                                <asp:ListItem>Christianity</asp:ListItem>
                                                <asp:ListItem>Islam</asp:ListItem>
                                                <asp:ListItem>Buddhism</asp:ListItem>
                                                <asp:ListItem>Others</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default required">
                                            <label>Dietary</label>
                                            <asp:DropDownList ID="ddlDietary" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" data-toggle="dropdown" Width="90%">
                                                <asp:ListItem>None</asp:ListItem>
                                                <asp:ListItem>Halal</asp:ListItem>
                                                <asp:ListItem>Vegeterian</asp:ListItem>
                                                <asp:ListItem>G6PD Dificiency</asp:ListItem>
                                                <asp:ListItem>No Beef</asp:ListItem>
                                                <asp:ListItem>No Peanut</asp:ListItem>
                                                <asp:ListItem>Others</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                <div class="form-group form-control-default required">
                                    <label>Current School</label>
                                    <asp:DropDownList ID="ddlSchLvl" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlSchLvl_SelectedIndexChanged">
                                        <asp:ListItem>None</asp:ListItem>
                                        <asp:ListItem>Primary</asp:ListItem>
                                        <asp:ListItem>Secondary</asp:ListItem>
                                         <asp:ListItem>Mixed Level</asp:ListItem>
                                        <asp:ListItem>Junior College</asp:ListItem>
                                          <asp:ListItem>ITE/Poly</asp:ListItem>
                                          <asp:ListItem>Universities</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlSchool" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                                        </div>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>
                                        Email
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="signUpGrp" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="signUpGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                        <asp:Label ID="lblmessage" runat="server" Text="Email already exists" ForeColor="#FF3300"></asp:Label>
                                    </label>
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <asp:Panel ID="pwPnl" runat="server">
                                    <div class="form-group form-control-default required">
                                        <label>
                                            Password
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="signUpGrp" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPassword" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="signUpGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator4" ControlToValidate="txtPassword" runat="server" ValidationExpression="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})" ErrorMessage="Must contain digit,uppercase and lowercase. Min 8 characters"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox ID="txtPassword" type="password" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <div class="form-group form-control-default required">
                                        <label>
                                            Repeat Password
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="signUpGrp" ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPw2" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator Display="Dynamic" ValidationGroup="signUpGrp" SetFocusOnError="true" ID="CompareValidator1" runat="server" ErrorMessage="Password does not match" ControlToCompare="txtPassword" ControlToValidate="txtPw2" ForeColor="Red"></asp:CompareValidator>
                                        </label>
                                        <asp:TextBox ID="txtPw2" type="password" runat="server" class="form-control" ControlToValidate="txtPw2"></asp:TextBox>
                                    </div>
                                </asp:Panel>
                                <%--  </div>--%>
                                <%-- </div>
                        <h4 class="page-section-heading">Interest</h4>
                        <div class="panel panel-default">
                            <div class="panel-body">--%>
                                <h3 class="ribbon-heading ribbon-primary top-left inline">Interest Form</h3>
                                <asp:HiddenField ID="hiddenBeneVocabID" runat="server" />
                                <asp:HiddenField ID="hiddenRoleVocabID" runat="server" />
                                <asp:HiddenField ID="hiddenSkillVocabID" runat="server" />
                                 <br/><asp:Label ID="Label1" style="font-style:italic" runat="server" Text="Interest response will be used to facilitate events matching based on your interests and skills"></asp:Label><br/>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default">
                                            <div class="ribbon-mark ribbon-primary absolute right">
                                                <span class="ribbon">
                                                    <span class="text">*</span>
                                                </span>
                                            </div>
                                            <label>
                                                Cause
                            <br>
                                                <asp:CustomValidator ValidationGroup="signUpGrp" runat="server" SetFocusOnError="true" Display="Dynamic" ID="cv" ClientValidationFunction="ValidateBeneList" ErrorMessage="Please select" ForeColor="Red"></asp:CustomValidator>
                                            </label>
                                            <asp:CheckBoxList ID="cbBenefi" runat="server" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default">
                                            <div class="ribbon-mark ribbon-primary absolute right">
                                                <span class="ribbon">
                                                    <span class="text">*</span>
                                                </span>
                                            </div>
                                            <label>
                                                Roles
                        <br>
                                                <asp:CustomValidator ValidationGroup="signUpGrp" runat="server" SetFocusOnError="true" Display="Dynamic" ID="CustomValidator1" ClientValidationFunction="ValidateRoleList" ErrorMessage="Please select" ForeColor="Red"></asp:CustomValidator>
                                            </label>
                                            <asp:CheckBoxList ID="cbRoles" runat="server" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-control-default">
                                            <div class="ribbon-mark ribbon-primary absolute right">
                                                <span class="ribbon">
                                                    <span class="text"></span>
                                                </span>
                                            </div>
                                            <label>Skills</label>
                                            <asp:CheckBoxList ID="cbSkill" runat="server" SetFocusOnError="true" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:Button ID="btnReg" runat="server" class="btn btn-primary" ValidationGroup="signUpGrp" Text="Submit" OnClick="btnReg_Click" /><br />
                <br />
                <br />
            </div>
        </div>
    </div>



    <script language="javascript">
        function ValidateBeneList(source, args) {
            var chkListModules = document.getElementById('<%= cbBenefi.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
        function ValidateRoleList(source, args) {
            var chkListModules = document.getElementById('<%= cbRoles.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>

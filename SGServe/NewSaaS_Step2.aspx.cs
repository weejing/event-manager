﻿using Newtonsoft.Json;
using PayPal.PayPalAPIInterfaceService;
using PayPal.PayPalAPIInterfaceService.Model;
using SGServe.CommonClass;
using SGServe.Models.TransactionModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class NewSaaS_Step2 : System.Web.UI.Page
    {
        public static List<PayPlan> pay_plan_list = new List<PayPlan>();

        //flow for page load is as follows
        //1, load_this_first
        //2. load_plan_data
        //3. generate_page_ui

        protected void Page_Load(object sender, EventArgs e)
        {
            pay_plan_list = new List<PayPlan>();
            getAvaliablePaymentPlan();
        }

        //load data
        public async void getAvaliablePaymentPlan()
        {
            try
            {
                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                //getOrgTransaction
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Transaction/getAvaliablePaymentPlan");
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();

                //get reply to bind to gridview
                var recent_transactions = JsonConvert.DeserializeObject<DataTable>(reply);

                //reply to object
                JavaScriptSerializer jss = new JavaScriptSerializer();
                List<OrgPayment> plan_list = jss.Deserialize<List<OrgPayment>>(reply);

                for (int i = 0; i < plan_list.Count; i++)
                {
                    OrgPayment temp = plan_list[i];
                    PayPlan pay_plan = new PayPlan();
                    pay_plan.PLAN_TYPE_ID = temp.PLAN_TYPE_ID;
                    pay_plan.PLAN_NAME = temp.PLAN_TYPE;
                    pay_plan.PLAN_AMOUNT = temp.AMT;
                    pay_plan.PLAN_DESCRIPTION = temp.PLAN_DESCRIPTION;
                    pay_plan.PER = temp.PER;
                    pay_plan_list.Add(pay_plan);
                }

                generatePageUI();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }

        //design related elements
        protected void generatePageUI()
        {
            Panel2.Controls.Clear();
            Literal literal_1 = new Literal();
            literal_1.Text = "<div class='table-pricing-3'><ul class='list-unstyled row'>";
            Panel2.Controls.Add(literal_1);


            for (int i = 0; i < pay_plan_list.Count; i++)
            {
                PayPlan temp = pay_plan_list[i];
                generatePlan(temp, i);
            }

            Literal literal_2 = new Literal();
            literal_2.Text = "</ul></div> ";
            Panel2.Controls.Add(literal_2);
        }
        protected string generatePlan(PayPlan temp, int style)
        {
            //generating parameters
            string planName = temp.PLAN_NAME;

            bool bool_discount = false;
            double subtotalAmt = Convert.ToDouble(temp.PLAN_AMOUNT);
            double discount = 0;
            if (temp.DISCOUNT != null)
            {
                discount = Convert.ToDouble(temp.DISCOUNT);
                bool_discount = true;
            }
            double totalAmt = subtotalAmt - discount;

            //opening html tags
            string start = "<li class='col-md-4'>";
            if (style == 0)
            {
                start = "<li class='col-md-4 active'>";
            }

            //features html
            string per = "";
            string html_feature = temp.PER + " months plan<br>";
            if (temp.PLAN_DESCRIPTION != null)
            {
                if (bool_discount)
                {
                    html_feature = "Usual Price: SGD" + subtotalAmt + "<br>";
                }
                else
                {
                    html_feature = "<br>";
                }
                html_feature += temp.PLAN_DESCRIPTION;
            }


            start += "<div class='innerAll'><h3>" + planName + "</h3><div class='pricing-body'><div class='price'><span class='figure' style = 'line-height: 110%;'>SGD</br> " + totalAmt +
                "</span><span class='term'>" + per + "</span></div></div><div class='pricing-features'>" + html_feature + "</div><div class='pricing-footer'>";
            //+"<asp:ImageButton ID='ImageButton" + style + "' runat='server' OnClick='paypal_checkout' ImageUrl=' " + link + " ' AlternateText='Pay Now' data-paypal-button= 'true' /> ";

            string end = "</div>";

            //pricing html content
            Literal literal_1 = new Literal();
            literal_1.Text = start;
            Panel2.Controls.Add(literal_1);

            //asp button
            string link = "https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_paynow_107x26.png";
            ImageButton imagebutton = new ImageButton();
            imagebutton.ID = "ImageButton" + style;
            imagebutton.Click += new ImageClickEventHandler(paypal_checkout);
            imagebutton.ImageUrl = link;
            imagebutton.AlternateText = "Pay Now";
            this.Panel2.Controls.Add(imagebutton);

            //break link
            Literal literal_next = new Literal();
            literal_next.Text = "</br>";
            Panel2.Controls.Add(literal_next);

            //closing html tag
            Literal literal_2 = new Literal();
            literal_2.Text = end;
            Panel2.Controls.Add(literal_2);

            return start;
        }

        //event trigger
        protected void paypal_checkout(object sender, ImageClickEventArgs e)
        {
            try
            {
                //System.Diagnostics.Debug.WriteLine("hello");
                //get values of what plan is selected by user
                string id = ((ImageButton)sender).ID;
                PayPlan selected_plan = selectedPayPlan(id);
                //Boolean recurring = selectRecurring(id);

                string url_token = paypalSetExpressCheckoutRequestDetailsType(selected_plan);
                string url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + url_token;

                Response.Redirect(url, false);
            }
            catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }

        protected PayPlan selectedPayPlan(string id)
        {
            try
            {
                int index = 0;
                string str_index = id.Substring(11);
                int.TryParse(str_index, out index);

                return pay_plan_list[index];
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        protected string paypalSetExpressCheckoutRequestDetailsType(PayPlan selected_plan)
        {
            try
            {
                //prepare parameters
                Boolean recurring = true;
                string itemCurrency = "SGD";
                int itemQuantity = 1;
                string itemName = selected_plan.PLAN_NAME;
                string itemNumber = selected_plan.PLAN_TYPE_ID;
                string initialAmount = selected_plan.PLAN_AMOUNT;
                string crmID = selected_plan.CRM_ID;
                string per = selected_plan.PER;
                //check if discount is avaliable
                string str_discount = selected_plan.DISCOUNT;
                double discount = 0;
                string itemAmount = initialAmount;
                if (str_discount != null)
                {
                    discount = Convert.ToDouble(str_discount);
                    itemAmount = (Convert.ToDouble(itemAmount) - discount).ToString();
                }

                string itemCategory = "Digital";
                string paymentAction = "Sale";
                string itemTotal = (Convert.ToDouble(itemAmount) * Convert.ToDouble(itemQuantity)).ToString();


                //paypal methods used
                PaymentDetailsType paymentDetail = new PaymentDetailsType();
                CurrencyCodeType currency = (CurrencyCodeType)EnumUtils.GetValue(itemCurrency, typeof(CurrencyCodeType));

                //plan details
                PaymentDetailsItemType paymentItem1 = new PaymentDetailsItemType();
                paymentItem1.Name = itemName;
                paymentItem1.Number = itemNumber;
                paymentItem1.Amount = new BasicAmountType(currency, initialAmount);
                paymentItem1.Quantity = itemQuantity;
                paymentItem1.ItemCategory = (ItemCategoryType)EnumUtils.GetValue(itemCategory, typeof(ItemCategoryType));
                paymentItem1.Description = per;

                List<PaymentDetailsItemType> paymentItems = new List<PaymentDetailsItemType>();
                paymentItems.Add(paymentItem1);
                paymentDetail.PaymentDetailsItem = paymentItems;

                //discount, if any -- add crm details
                if (discount != 0)
                {
                    PaymentDetailsItemType paymentItem2 = new PaymentDetailsItemType();
                    paymentItem2.Name = "Discount Applied";
                    paymentItem2.Number = crmID;
                    paymentItem2.Amount = new BasicAmountType(currency, "-" + discount);
                    paymentItem2.Quantity = itemQuantity;
                    paymentItems.Add(paymentItem2);
                }


                paymentDetail.PaymentAction = (PaymentActionCodeType)EnumUtils.GetValue(paymentAction, typeof(PaymentActionCodeType));
                paymentDetail.OrderTotal = new BasicAmountType((CurrencyCodeType)EnumUtils.GetValue(itemCurrency, typeof(CurrencyCodeType)), itemTotal);
                List<PaymentDetailsType> paymentDetails = new List<PaymentDetailsType>();
                paymentDetails.Add(paymentDetail);

                //set re-direct page
                string baseurl = Request.Url.GetLeftPart(UriPartial.Authority) + "/";
                //string baseurl = "http://localhost:53197/";
                SetExpressCheckoutRequestDetailsType ecDetails = new SetExpressCheckoutRequestDetailsType();
                ecDetails.ReturnURL = baseurl + "/NewSaaS_Step3";
                ecDetails.CancelURL = baseurl + "/NewSaaS_Step2";
                ecDetails.PaymentDetails = paymentDetails;

                ecDetails.BrandName = "SGServe";
                //ecDetails.cppLogoImage = "https://dl.dropboxusercontent.com/u/73122179/sgserve_small.jpg";
                //ecDetails.CustomerServiceNumber = "+65 6784 934";
                ecDetails.OrderDescription = "Initial Price:" + initialAmount;


                //set billing agreement -- this is for recurring plan
                if (recurring)
                {
                    paypalSetBillingAgreement(ecDetails);
                }

                SetExpressCheckoutRequestType request = new SetExpressCheckoutRequestType();
                request.Version = "104.0";
                request.SetExpressCheckoutRequestDetails = ecDetails;

                SetExpressCheckoutReq wrapper = new SetExpressCheckoutReq();
                wrapper.SetExpressCheckoutRequest = request;
                Dictionary<string, string> sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                sdkConfig.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                sdkConfig.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(sdkConfig);
                SetExpressCheckoutResponseType setECResponse = service.SetExpressCheckout(wrapper);

                return setECResponse.Token.ToString();
            }
            catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
            return null;
        }
        protected void paypalSetBillingAgreement(SetExpressCheckoutRequestDetailsType ecDetails)
        {
            try
            {
                //set parameter
                string bill_type = "RecurringPayments";
                string bill_msg = "You have selected recurring payment";


                BillingCodeType billingCodeType = (BillingCodeType)EnumUtils.GetValue(bill_type, typeof(BillingCodeType));
                BillingAgreementDetailsType baType = new BillingAgreementDetailsType(billingCodeType);
                baType.BillingAgreementDescription = bill_msg;
                ecDetails.BillingAgreementDetails.Add(baType);
            }
            catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }
    }
}
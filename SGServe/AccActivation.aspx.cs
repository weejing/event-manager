﻿using SGServe.CommonClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class AccActivation : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Account/updateAccActivated?ActivationCode=" + Request.QueryString["ActivationCode"]);
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();

                if (Convert.ToInt32(reply) == 0)
                {
                    ltMessage.Text = "Invalid Activation code.";
                }
                else
                {
                    ltMessage.Text = "User account has been activated successfully. You can now log in.";
                }

            }
        }
    }
}
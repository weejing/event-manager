﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class Messages : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";
        static String navigateUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //await retrieveUserDetails();
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    navigateUserID = Request.QueryString["id"];
                    currentUserID = volunteer.VOL_ID;
                    generateContent();
                }
            }
        }

        /*
        private async Task retrieveUserDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            if (jwtToken != null)
            {
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                // add the jwt token into the authorization header for server validation
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                volunteer = GlobalModule.getUserDetails(reply);
            }
        }
        */

        protected async void generateContent()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage messagesReqTo = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getCommentsForUser?toUserID=" + currentUserID);
                HttpResponseMessage messagesRespTo = await client.SendAsync(messagesReqTo);
                String messagesTo = await messagesRespTo.Content.ReadAsStringAsync();

                List<UserCommentsModel> ucmTo = JsonConvert.DeserializeObject<List<UserCommentsModel>>(messagesTo);
                MessagesFromItemGrid.DataSource = ucmTo;
                MessagesFromItemGrid.DataBind();

                HttpRequestMessage messagesReqFrom = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getCommentsByUser?fromUserID=" + currentUserID);
                HttpResponseMessage messagesRespFrom = await client.SendAsync(messagesReqFrom);
                String messagesFrom = await messagesRespFrom.Content.ReadAsStringAsync();

                List<UserCommentsModel> ucmFrom = JsonConvert.DeserializeObject<List<UserCommentsModel>>(messagesFrom);
                MessagesToItemGrid.DataSource = ucmFrom;
                MessagesToItemGrid.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/UserPortfolio.aspx?id=" + currentUserID);
        }
    }
}
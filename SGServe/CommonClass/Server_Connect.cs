﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Json;
using SGServe.Models.AccountModels;

namespace SGServe.CommonClass
{
    public class Server_Connect
    {
        HttpClient client;
        public Server_Connect()
        {
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri("http://localhost:59637");
            //this.client.BaseAddress = new Uri("http://vmsserver.azurewebsites.net/");
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public HttpClient getHttpClient()
        {
            return this.client;
        }

    }
}
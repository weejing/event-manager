﻿using Newtonsoft.Json;
using SGServe.Models.AccountModels;
using SGServe.Models.NotificationModels;
using SGServe.SaaS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.CommonClass
{
    public class GlobalModule
    {
        /*Kaifu: OLD COPY, DO NOT USE THIS*/
        public static Volunteer getUserDetails(string reply)
        {
            dynamic data = JsonObject.Parse(reply);
            Volunteer volunteer = new Volunteer();
            volunteer.ACTIVATION_CODE = data.ACTIVATION_CODE;
            volunteer.DIETARY = data.DIETARY;
            string strDOB = data.DOB;
            volunteer.DOB = (DateTime)(data.DOB);
            volunteer.EMAIL = data.EMAIL;
            volunteer.FACEBOOK_ID = data.FACEBOOK_ID;
            volunteer.FULL_NAME = data.FULL_NAME;
            volunteer.NRIC = data.NRIC;
            volunteer.PASSWORD = data.PASSWORD;
            volunteer.PERSONALITY_CHAR = data.PERSONALITY_CHAR;
            volunteer.PHONE_NO = data.PHONE_NO;
            volunteer.VOL_ID = data.VOL_ID;
            volunteer.REGISTRATION_DATE = (DateTime)(data.REGISTRATION_DATE);
            volunteer.RELIGION = data.RELIGION;
            volunteer.SCHL_ID = data.SCHL_ID;
            volunteer.SCHL_NAME = data.SCHL_NAME;
            volunteer.SCH_LVL = data.SCH_LVL;
            volunteer.VIA_TARGET = data.VIA_TARGET;
            volunteer.RESETPW_CODE = data.RESETPW_CODE;
            return volunteer;
        }

        /*kaifu use this*/
        public async Task<Volunteer> getLoginVolDetails()
        {
            Volunteer volunteer = new Volunteer();
            if (System.Web.HttpContext.Current.Session["Token"] != null)
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = System.Web.HttpContext.Current.Session["Token"] as String;
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                dynamic data = JsonObject.Parse(reply);
                volunteer.ACTIVATION_CODE = data.ACTIVATION_CODE;
                volunteer.DIETARY = data.DIETARY;
                string strDOB = data.DOB;
                volunteer.DOB = (DateTime)(data.DOB);
                volunteer.EMAIL = data.EMAIL;
                volunteer.FACEBOOK_ID = data.FACEBOOK_ID;
                volunteer.FULL_NAME = data.FULL_NAME;
                volunteer.NRIC = data.NRIC;
                volunteer.PASSWORD = data.PASSWORD;
                volunteer.PERSONALITY_CHAR = data.PERSONALITY_CHAR;
                volunteer.PHONE_NO = data.PHONE_NO;
                volunteer.VOL_ID = data.VOL_ID;
                volunteer.REGISTRATION_DATE = (DateTime)(data.REGISTRATION_DATE);
                volunteer.RELIGION = data.RELIGION;
                volunteer.SCHL_ID = data.SCHL_ID;
                volunteer.SCHL_NAME = data.SCHL_NAME;
                volunteer.SCH_LVL = data.SCH_LVL;
                volunteer.VIA_TARGET = data.VIA_TARGET;
                volunteer.RESETPW_CODE = data.RESETPW_CODE;
                System.Web.HttpContext.Current.Session["vol_id"] = data.VOL_ID;
            }
            return volunteer;
        }

        public async Task<OrgEmployee> getLoginEmpDetails(string connection_string)
        {
            OrgEmployee employee = new OrgEmployee();

            if (System.Web.HttpContext.Current.Session["Token"] != null)
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = System.Web.HttpContext.Current.Session["Token"] as String;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/OrgAccount/getLoginEmpDetails?connection_string="+ connection_string);
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                OrgEmployee emp = JsonConvert.DeserializeObject<OrgEmployee>(reply);
                employee.EMAIL = emp.EMAIL;
                employee.NAME = emp.NAME;
                employee.PASSWORD = emp.PASSWORD;
                employee.USR_ID = emp.USR_ID;
                employee.REGISTRATION_DATE = (DateTime)(emp.REGISTRATION_DATE);
                employee.RESETPW_CODE = emp.RESETPW_CODE;
                employee.roleList = emp.roleList;
                employee.ORG_STATUS = emp.ORG_STATUS;
                //check organization status
                if (employee.ORG_STATUS.Equals("ACTIVE"))
                {
                    //store emp pages that can view        
                    HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "api/OrgAccount/getPageAccessRights?connection_string=" + connection_string + "&usr_id=" + emp.USR_ID);
                    HttpResponseMessage Resp2 = await client.SendAsync(request2);
                    String reply2 = await Resp2.Content.ReadAsStringAsync();
                    DataTable pagesTable = JsonConvert.DeserializeObject<DataTable>(reply2);
                    System.Web.HttpContext.Current.Session["EMP_PAGES"] = pagesTable;
                }
                else {
                    HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "api/OrgAccount/getBillPage" );
                    HttpResponseMessage Resp2 = await client.SendAsync(request2);
                    String reply2 = await Resp2.Content.ReadAsStringAsync();
                    DataTable pagesTable = JsonConvert.DeserializeObject<DataTable>(reply2);
                    System.Web.HttpContext.Current.Session["EMP_PAGES"] = pagesTable;
                }
    
            }
            return employee;
        }

        public OrgEmployee getOrgEmpDetails(string reply)
        {
            OrgEmployee employee = new OrgEmployee();
            OrgEmployee emp = JsonConvert.DeserializeObject<OrgEmployee>(reply);
            employee.EMAIL = emp.EMAIL;
            employee.NAME = emp.NAME;
            employee.PASSWORD = emp.PASSWORD;
            employee.USR_ID = emp.USR_ID;
            employee.REGISTRATION_DATE = (DateTime)(emp.REGISTRATION_DATE);
            employee.RESETPW_CODE = emp.RESETPW_CODE;
            employee.roleList = emp.roleList;
            return employee;
        }


        public async Task getOrgHeader(string curURL)
        {
         try { 
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                OrganizationDetails org = new OrganizationDetails();
                string url = curURL;
                string[] urlArr = url.Split('/');
                org.CONNECTION_STRING = urlArr[1].ToUpper();
                var response = await client.PostAsJsonAsync("api/OrgMgmt/getOrgInfo", org);
                String reply = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("reply ologins: " + reply);
                dynamic data = JsonObject.Parse(reply);
                string connection_str = data.ORG_NAME;
                string menu = "<a class='navbar-brand hidden-xs navbar-brand-primary' href='internalhome'>";
                menu = menu + connection_str.Replace("\"", "") + "</a>";
                System.Web.HttpContext.Current.Session["Orgheader"] = menu;
            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.Redirect("../home.aspx", false);
            }

        }

        public async Task getOrgDetail(string curURL, Label lbltitle)
        {
             try { 
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                OrganizationDetails org = new OrganizationDetails();
                string url = curURL;
                string[] urlArr = url.Split('/');
                org.CONNECTION_STRING = urlArr[1].ToUpper();
                var response = await client.PostAsJsonAsync("api/OrgMgmt/getOrgInfo", org);
                String reply = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("reply org detail: " + reply);
                dynamic data = JsonObject.Parse(reply);
                string connection_str = data.ORG_NAME;
                string menu = "<a class='navbar-brand hidden-xs navbar-brand-primary' href='home'>";
                lbltitle.Text = menu + connection_str.Replace("\"", "") + "</a>";
            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.Redirect("../home.aspx", false);
            }

        }

        public async Task getNotification(string connection_str,string tablename, Label lbl)
        {
            string display = "";
            if (System.Web.HttpContext.Current.Session["Token"] != null) {
                String jwtToken = System.Web.HttpContext.Current.Session["Token"] as String;
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Notification/getNotifList?connection_string="+ connection_str + "&tablename="+tablename);
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                List<Notification> notificationList = JsonConvert.DeserializeObject<List<Notification>>(reply);

                //here
                if (notificationList.Any())
                {
                    foreach (Notification notification in notificationList)
                    {
                        display = display + "<div class='row' style='padding-left:10px'>" + notification.NOTIFICATION;
                        display = display + "<br /><span class='text-caption text-muted'>" + notification.TIMESTAMP + "</span></div>";
                    }
                }
                else {
                    display = display + "<div class='row' style='padding-left:10px'> - </div>";
                }
                lbl.Text = display;
            } 
        }

        public void MessageBox(string message)
        {
            Page executingPage = HttpContext.Current.Handler as Page;
            if (executingPage != null)
            {
                executingPage.ClientScript.RegisterStartupScript(executingPage.GetType(), "MsgBox",
                    string.Format("alert('{0}');", message), true);
            }
        }

        public void displayEmpMenu(Label lblLeftMenu, Label lblTopMenu)
        {
            string topMenu = " <div class='navbar-collapse collapse' id='collapse'><ul class='nav navbar-nav navbar-right'>";
            topMenu = topMenu + "<li class='dropdown user'><a href='profile'>Profile</a></li>";
            topMenu = topMenu + "<li class='dropdown user'><a href='Logout'>Logout</a></li>";
            topMenu = topMenu + "</ul></div>";
            lblTopMenu.Text = topMenu;

            string LeftMenu = "<div data-scrollable>";
            LeftMenu = LeftMenu + "<ul class='sidebar-menu sm-icons-right sm-icons-block'><li><a href='index.html'><i class='fa fa-home'></i> <span>Dashboard</span></a></li> </ul>";
            LeftMenu = LeftMenu + "<ul class='sidebar-menu sm-icons-block sm-icons-right'>";

            DataTable table = (DataTable)System.Web.HttpContext.Current.Session["EMP_PAGES"];
            if (table.Rows.Count != 0)
            {
                DataRow[] result = table.Select("SHOW_ON_MENU = 1");
                foreach (DataRow row in result)
                {
                    LeftMenu = LeftMenu + "<li><a href='" + row["url"].ToString().Replace(".aspx","") + "'><i class='fa fa-" + row["icon"] + "'></i> <span>" + row["name"] + "</span></a></li>";
                }
            }
            LeftMenu = LeftMenu + "</ul></div>";
            lblLeftMenu.Text = LeftMenu;
        }

        //check authorize user
        public string checkAuthorizeUser(string currentURL)
        {
            string message = "";
            if (System.Web.HttpContext.Current.Session["Token"] == null)
            {
                message = "Please login";
            }
            else
            {
                DataTable table = (DataTable)System.Web.HttpContext.Current.Session["EMP_PAGES"];
                table.CaseSensitive = false;
                DataRow[] result;
                if (table.Rows.Count != 0)
                {
                    result = table.Select("url = '" + currentURL + "'");
                    if (result.Length == 0)
                    {
                        message = "You are not authroize to view this page";
                    }
                }
                else
                {
                    message = "You are not authroize to view this page";
                }

            }

            return message;
        }

   
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using iTextSharp.text;


namespace SGServe.CommonClass
{
    public class ResizeImage
    {
        float originalHeight;
        float originalWidth;
        float maxHeight; //250f
        float maxWidth; //550f

        int originalHeightInt;
        int originalWidthInt;
        int maxHeightInt; //250
        int maxWidthInt; //550

        public ResizeImage(float originalHeight, float originalWidth, float maxHeight, float maxWidth)
        {
            this.originalHeight = originalHeight;
            this.originalWidth = originalWidth;
            this.maxHeight = maxHeight;
            this.maxWidth = maxWidth;
            this.originalHeightInt = 0;
            this.originalWidthInt = 0;
            resizeFloat();
        }

        public ResizeImage(int originalHeight, int originalWidth, int maxHeight, int maxWidth)
        {
            this.originalHeightInt = originalHeight;
            this.originalWidthInt = originalWidth;
            this.maxHeightInt = maxHeight;
            this.maxWidthInt = maxWidth;
            this.originalHeight = 0f;
            this.originalWidth = 0f;
            resizeInt();
        }

        public void resizeFloat()
        {
            float strinkHeight = 0f;
            float strinkWidth = 0f;

            if (originalHeight > maxHeight)
            {
                strinkHeight = maxHeight * (originalWidth / originalHeight);
            }

            if (originalWidth > maxWidth)
            {
                strinkWidth = maxWidth * (originalHeight / originalWidth);
            }

            if (strinkHeight > 0f && strinkWidth > 0f)
            {
                originalHeight = strinkHeight;
                originalWidth = strinkWidth;
                //img.ScaleToFit(strinkHeight, strinkWidth);
            }
            else if (strinkHeight > 0f)
            {
                originalHeight = strinkHeight;
                //img.ScaleToFit(strinkHeight, imgWidth);
            }
            else if (strinkWidth > 0f)
            {
                originalWidth = strinkWidth;
                //img.ScaleToFit(imgHeight, strinkWidth);
            }
        }

        public void resizeInt()
        {
            int strinkHeight = 0;
            int strinkWidth = 0;

            if (originalHeightInt > maxHeightInt)
            {
                strinkHeight = maxHeightInt * (originalWidthInt / originalHeightInt);
            }

            if (originalWidth > maxWidth)
            {
                strinkWidth = maxWidthInt * (originalHeightInt / originalWidthInt);
            }

            if (strinkHeight > 0 && strinkWidth > 0)
            {
                originalHeightInt = strinkHeight;
                originalWidthInt = strinkWidth;
            }
            else if (strinkHeight > 0f)
                originalHeightInt = strinkHeight;

            else if (strinkWidth > 0)
                originalWidthInt = strinkWidth;
        }

        public float getImageHeightFloat()
        {
            return this.originalHeight;
        }

        public float getImageWidthFloat()
        {
            return this.originalWidth;
        }

        public int getImageHeightInt()
        {
            return this.originalHeightInt;
        }

        public int getImageWidthInt()
        {
            return this.originalWidthInt;
        }
    }
}
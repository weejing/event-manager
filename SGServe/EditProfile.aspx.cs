﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class EditProfile : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                DOB.Text = Request.Form[DOB.UniqueID].ToString();
            }
            else
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx",false);
                }else {
                    displayDetails();
                }                
            }
        }

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Account/updateAccDetail");
            string FULL_NAME = txtFullName.Text;
            string SCHL_ID = "";
            if (ddlSchLvl.SelectedValue.ToString() != "None")
            {
                SCHL_ID = ddlSchool.SelectedValue.ToString();
            }
            string NRIC = txtNRIC.Text;
            string PHONE_NO = txtPhone.Text;
            DateTime DateOB = DateTime.ParseExact(DOB.Text, "d MMM yyyy", null);
            string DIETARY = ddlDietary.SelectedValue.ToString();
            string PERSONALITY_CHAR = ddlPersonality.SelectedValue.ToString();
            string RELIGION = ddlReligion.SelectedValue.ToString();
            volunteer = await gm.getLoginVolDetails();
            if (volunteer.VOL_ID == null)
            {
                Response.Redirect("login.aspx", false);
            }
            else
            {
                string VOL_ID = volunteer.VOL_ID;
                Volunteer volunteer2 = new Volunteer { SCHL_ID = SCHL_ID, FULL_NAME = FULL_NAME, VOL_ID = VOL_ID, NRIC = NRIC, PHONE_NO = PHONE_NO, DOB = DateOB, DIETARY = DIETARY, PERSONALITY_CHAR = PERSONALITY_CHAR, RELIGION = RELIGION };
                var response = await client.PostAsJsonAsync("api/Account/updateAccDetail", volunteer2);
                Response.Redirect("Dashboard.aspx");
            }
        }

        //private async Task retrieveUserDetails()
        //{
        //    Server_Connect serverConnection = new Server_Connect();
        //    HttpClient client = serverConnection.getHttpClient();
        //    String jwtToken = Session["Token"] as String;
        //    if (jwtToken != null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
        //        // add the jwt token into the authorization header for server validation
        //        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        //        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
        //        HttpResponseMessage Resp = await client.SendAsync(request);
        //        String reply = await Resp.Content.ReadAsStringAsync();
        //        volunteer = GlobalModule.getUserDetails(reply);
        //    }
        //}

        protected async void displayDetails()
        {
            ddlDietary.SelectedValue = volunteer.DIETARY;
            DOB.Text = volunteer.DOB.ToString("d MMM yyyy");
            lblEmail.Text = volunteer.EMAIL;
            txtFullName.Text = volunteer.FULL_NAME;
            txtNRIC.Text = volunteer.NRIC;
            ddlPersonality.SelectedValue = volunteer.PERSONALITY_CHAR.ToString();
            txtPhone.Text = volunteer.PHONE_NO;
            ddlReligion.SelectedValue = volunteer.RELIGION;
            if (volunteer.SCHL_NAME == "")
            {
                ddlSchLvl.SelectedValue = "None";
                ddlSchool.Visible = false;
            }
            else
            {
                ddlSchLvl.SelectedValue = volunteer.SCH_LVL;
                ddlSchool.Visible = true;

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                HttpRequestMessage schReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getSchools?sch_lvl=" + volunteer.SCH_LVL);
                HttpResponseMessage schResp = await client.SendAsync(schReq);
                String schools = await schResp.Content.ReadAsStringAsync();
                var schooltable = JsonConvert.DeserializeObject<DataTable>(schools);
                ddlSchool.Visible = true;
                ddlSchool.DataSource = schooltable;
                ddlSchool.DataTextField = "SCHL_NAME";
                ddlSchool.DataValueField = "SCHL_ID";
                ddlSchool.DataBind();
                ddlSchool.SelectedValue = volunteer.SCHL_ID;
            }

        }



        protected async void ddlSchLvl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchLvl.SelectedValue.ToString() == "None")
            {
                ddlSchool.Visible = false;
            }
            else
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();


                HttpRequestMessage schReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getSchools?sch_lvl=" + ddlSchLvl.SelectedValue.ToString());
                HttpResponseMessage schResp = await client.SendAsync(schReq);
                String schools = await schResp.Content.ReadAsStringAsync();

                var schooltable = JsonConvert.DeserializeObject<DataTable>(schools);
                ddlSchool.Visible = true;
                ddlSchool.DataSource = schooltable;
                ddlSchool.DataTextField = "SCHL_NAME";
                ddlSchool.DataValueField = "SCHL_ID";
                ddlSchool.DataBind();
            }
        }
    }
}
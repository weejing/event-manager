﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Json;
using System.Text;

namespace SGServe.SaaS
{
    public partial class CreateVIATemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                if(checkTemplateExist() == true)
                {
                    btnVIALetterTemplate.Visible = true;
                    loadTemplate();
                }
                else
                {
                    btnVIALetterTemplate.Visible = false;
                }
            }
        }

        public bool checkTemplateExist()
        {
            bool isExist = false;
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            string connection = urlArr[1].ToUpper();
            string viatemplatefilename = connection + "_VIA_TEMPLATE.txt";
            String path = Server.MapPath("~/SaaS/VIATemplate/" + viatemplatefilename);
            if(File.Exists(path))
            {
                isExist = true;
            }
            return isExist;
        }

    

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (uploadLogo.HasFile)
            {
                string filename = uploadLogo.FileName;
                string[] nameArr = filename.Split('.');
                if (nameArr.Contains("png") || nameArr.Contains("jpg") || nameArr.Contains("PNG") || nameArr.Contains("JPG"))
                {
                    string imgFileName = Path.GetFileName(uploadLogo.PostedFile.FileName);
                    string Extension = Path.GetExtension(uploadLogo.PostedFile.FileName);
                    String imgpath = Server.MapPath("~/SaaS/VIATemplate/OrgLogo/" + imgFileName);
                    uploadLogo.SaveAs(imgpath);
                }
                else
                {
                    //prompt
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Sorry, an error has been detected. Please kindly ensure that you have uploaded a valid file format!" + "');", true);
                }
            }
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            string connection = urlArr[1].ToUpper();
            string viatemplatefilename = connection+"_VIA_TEMPLATE.txt";
            String path = Server.MapPath("~/SaaS/VIATemplate/" + viatemplatefilename);
            using (StreamWriter writer = new StreamWriter(path, false)) 
            {
                if (!uploadLogo.FileName.Equals(""))
                    writer.WriteLine("@" + uploadLogo.FileName + "@");
                else
                    writer.WriteLine(logoHidden.Value);
                writer.WriteLine();
                writer.WriteLine(tbBody.Text);
                writer.WriteLine();
                writer.WriteLine("<br />");
                writer.WriteLine("<br />");
                writer.WriteLine("<br />");
                writer.WriteLine(tbOrganiserName.Text);
                writer.WriteLine("<div>" + "</div>");
                writer.WriteLine(tbDepartmentName.Text);
                writer.WriteLine("<div>" + "</div>");
                writer.WriteLine(tbComittee.Text);
                writer.WriteLine("<div>" + "</div>");
                writer.WriteLine(tbOrganisationName.Text);
                writer.WriteLine("<br />");
                writer.WriteLine("<br />");
                writer.WriteLine("<br />");
                writer.WriteLine("<br />");
                writer.WriteLine("<div align=\"center\">This is computer generated copy. No signature is required.</div>");
            }

            //prompt
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "CIP Letter Template has been successfully created!" + "');", true);
            tbBody.Text = "";
            tbComittee.Text = "";
            tbDepartmentName.Text = "";
            tbOrganisationName.Text = "";
            tbOrganiserName.Text = "";
            lbllogo.Text = "";

        }

        public void loadTemplate()
        {
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            string connection = urlArr[1].ToUpper();
            string viatemplatefilename = connection + "_VIA_TEMPLATE.txt";
            String path = Server.MapPath("~/SaaS/VIATemplate/" + viatemplatefilename);
            StringBuilder sb = new StringBuilder();
            StringBuilder sbTextBox = new StringBuilder();
            int counter = 0;
            using (StreamReader sr = new StreamReader(path, true))
            {
                String line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    counter++;
                    sbTextBox.AppendLine(line);
                    if (counter == 1)
                    {
                        //lbllogo.Visible = true;
                        //lbllogo.Text = line;
                        logoHidden.Value = line;
                    }
                    if (counter == 3)
                    {
                        tbBody.Text = line;
                    }
                    else if (counter == 8)
                    {
                        tbOrganiserName.Text = line;
                    }
                    else if (counter == 10)
                    {
                        tbDepartmentName.Text = line;
                    }
                    else if (counter == 12)
                    {
                        tbComittee.Text = line;
                    }
                    else if (counter == 14)
                    {
                        tbOrganisationName.Text = line;
                    }
                }
            }
        }

        protected void btnVIALetterTemplate_Click(object sender, EventArgs e)
        {
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            string connection = urlArr[1].ToUpper();
            string viatemplatefilename = connection + "_VIA_TEMPLATE.txt";
            String path = Server.MapPath("~/SaaS/VIATemplate/" + viatemplatefilename);
            StringBuilder sb = new StringBuilder();
            StringBuilder sbTextBox = new StringBuilder();
            int counter = 0;
            using (StreamReader sr = new StreamReader(path, true))
            {
                String line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    counter++;
                    sbTextBox.AppendLine(line);
                    if(counter == 1)
                    {
                        //lbllogo.Visible = true;
                        //lbllogo.Text = line;
                        logoHidden.Value = line;
                    }
                    if(counter == 3)
                    {
                        tbBody.Text = line;
                    }
                    else if(counter == 8)
                    {
                        tbOrganiserName.Text = line;
                    }
                    else if(counter == 10)
                    {
                        tbDepartmentName.Text = line;
                    }
                    else if(counter == 12)
                    {
                        tbComittee.Text = line;
                    }
                    else if(counter == 14)
                    {
                        tbOrganisationName.Text = line;
                    }
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            mpHelpText.Show();
        }
    }
}
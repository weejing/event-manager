﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="ViewEventDetails.aspx.cs" Inherits="SGServe.SaaS.VIewEventDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <div class="property-meta">
        <div class="row">
            <h1 class="page-section-heading" style="color: #26355b;">
                <asp:Label ID="evtName" runat="server"></asp:Label>
                 <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete?" TargetControlID="Button1" />
                <asp:Button ID="Button1" runat="server" Text="Delete" class="btn btn-danger btn-sm" OnClick="delete_btn" Style="float: right" /></h1>
        </div>
        <div class="row">
            <div class="col-md-7">
                <asp:Image ID="evtImage" runat="server" ImageUrl="/main_css/images/photodune-378874-real-estate-xs.jpg" Style="width: 100%; height: 300px" />
            </div>
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <asp:Label ID="Date" runat="server" />
                        </h4>
                    </div>
                    <div class="panel-body">
                            <div class="expandable-content">
                                <p>
                                    <asp:Label ID="desc" runat="server"></asp:Label>
                                </p>
                                <asp:DataList ID="dlcause" runat="server" CellSpacing="20" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <span class="label label-customize-100"><i class="fa fa-tag" aria-hidden="true"></i>&nbsp;<%# DataBinder.Eval(Container.DataItem, "TERM_NAME") %></span>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <asp:PlaceHolder ID="sessionDetailsHolder" runat="server"></asp:PlaceHolder>

    <asp:HiddenField ID="hiddenRoleName" runat="server" />
    <asp:HiddenField ID="hiddenRoleQty" runat="server" />
    <asp:HiddenField ID="hiddenRoleDesc" runat="server" />
    <asp:HiddenField ID="hiddenRoleMaxQty" runat="server" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var hiddenRoleDescrip = $('#<%= hiddenRoleDesc.ClientID%>');
        var hiddenRoleQty = $('#<%= hiddenRoleQty.ClientID%>');
        var hiddenRoleName = $('#<%=hiddenRoleName.ClientID%>');
        var hiddenRoleMaxQty = $('#<%=hiddenRoleMaxQty.ClientID%>');
        var roleDecsArray = hiddenRoleDescrip.val().split('+');
        var roleQtyArray = hiddenRoleQty.val().split('+');
        var roleNameArray = hiddenRoleName.val().split('+');
        var maxRoleQtyArray = hiddenRoleMaxQty.val().split('+');
        console.log(hiddenRoleDescrip.val());

        hiddenRoleDescrip.val('');
        hiddenRoleQty.val('');
        hiddenRoleName.val('');
        hiddenRoleMaxQty.val('');

        console.log(hiddenRoleDescrip.val());



        function roleNameOnChange(id, arrayCount) {

            $('#table' + arrayCount + ' > tbody').empty();
            var table = $('#table' + arrayCount);

            var roleDescList = roleDecsArray[arrayCount].split('-');
            var roleQtyList = roleQtyArray[arrayCount].split('*');
            var roleNameList = roleNameArray[arrayCount].split('-');
            var maxRoleQtyList = maxRoleQtyArray[arrayCount].split('-');

            var description = roleDescList[id.selectedIndex - 1].split(',');
            var qty = roleQtyList[id.selectedIndex - 1].split(',');
            var name = roleNameList[id.selectedIndex - 1].split(',');
            var maxQty = maxRoleQtyList[id.selectedIndex - 1].split(',');

            console.log("role qty " + roleQtyList[id.selectedIndex - 1]);
            for (var count = 0; count < name.length; count++) {
                table.append("<tr><td>" + name[count] + "</td><td>" + qty[count] + " / " + maxQty[count] +
                   "</td> <td>" + description[count] + "</td></tr>");
            }
        }
    </script>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Net.Mail;
using SGServe.Models.HrModels;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Json;

namespace SGServe.SaaS
{
    public partial class CreateEmailInvites : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                string namelistid = (string)(Session["namelist_id"]);
                string name = (string)(Session["name"]);
                string email = (string)(Session["email"]);
               
                tbRecipent.Text = email;
                lblexternalvolname.Text = name;
                List<string> emaillist = (List<string>)(Session["emaillist"]);
                if(HttpContext.Current.Session["ManyRecipent"]!= null)
                {
                   
                    if (emaillist.Count != 0)
                    {
                        for (int i = 0; i < emaillist.Count; i++)
                        {
                            if (i < emaillist.Count - 1)
                            {
                                tbBcc.Text += emaillist[i] + ',';
                            }
                            else
                            {
                                tbBcc.Text += emaillist[i];
                            }
                        }
                    }
                    tbRecipent.Text = "";
                    lblexternalvolname.Text = "All Recipient";
                }
                if (HttpContext.Current.Session["OneRecipent"]!= null)
                {
                    tbRecipent.Text = email;
                    tbBcc.Text = "";
                }
            }
        }



        protected void btnSend_Click(object sender, EventArgs e)
        {
            bool isSend = false;
            //string from = "sgserve8mc@gmail.com";
            string from = "admin@sgserve.com";
            string pass = "sgserve1991_iAM";
            //string pass = "8mcdjjshwj";
            if (!tbBcc.Text.Trim().Equals(""))
            {
                using (MailMessage mm = new MailMessage(from, "NoReply@CompanyName"))
                {
                    string[] emailadds = tbBcc.Text.Split(',');
                    foreach (string emailadd in emailadds)
                    {
                        MailAddress bcc = new MailAddress(emailadd);
                        mm.Bcc.Add(bcc);
                    }

                    mm.Subject = tbSubject.Text;

                    if (!tbCc.Text.Trim().Equals(""))
                    {
                        MailAddress cc = new MailAddress(tbCc.Text);
                        mm.CC.Add(cc);
                    }

                    mm.Body = tbBody.Text;
                    if (fuAttachment.HasFile)
                    {
                        string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
                        mm.Attachments.Add(new Attachment(fuAttachment.PostedFile.InputStream, FileName));
                    }
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                  // smtp.Host = "smtp.gmail.com";
                     smtp.Host = "smtp.zoho.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(from, pass);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                   //smtp.Port = 465;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    isSend = true;

                }
            }
            else
            {
                using (MailMessage mm = new MailMessage(from, tbRecipent.Text))
                {

                    mm.Subject = tbSubject.Text;
                    if (!tbRecipent.Text.Trim().Equals(""))
                    {
                        MailAddress to = new MailAddress(tbRecipent.Text);
                        mm.To.Add(to);
                    }
                    if (!tbBcc.Text.Trim().Equals(""))
                    {
                        MailAddress bcc = new MailAddress(tbBcc.Text);
                        mm.Bcc.Add(bcc);
                    }

                    if (!tbCc.Text.Trim().Equals(""))
                    {
                        MailAddress cc = new MailAddress(tbCc.Text);
                        mm.CC.Add(cc);
                    }

                    mm.Body = tbBody.Text;
                    if (fuAttachment.HasFile)
                    {
                        string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
                        mm.Attachments.Add(new Attachment(fuAttachment.PostedFile.InputStream, FileName));
                    }
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    //smtp.Host = "smtp.gmail.com";
                    smtp.Host = "smtp.zoho.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(from, pass);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                   // smtp.Port = 465;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    isSend = true;
                   
                }
            }
            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');window.location ='ViewExternalVolunteer';", true);
            if (isSend == true)
            {
                tbBody.Text = "";
                tbSubject.Text = "";
                tbCc.Text = "";
                tbBcc.Text = "";
            }

        }

        protected void btnTemplate1_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbTextBox = new StringBuilder();


            List<OpenEvents> eventdetails = (List<OpenEvents>)Session["openeventdetails"];
            for (int i = 0; i < eventdetails.Count; i++)
            {

                sb.AppendLine(i + 1 + ") " + "<b>Event Name:</b> " + eventdetails[i].EVENTNAME + "    " + "<b>Event Date:</b> " + eventdetails[i].EVENTDATE.ToLongDateString() + "   " + "<b>Sign Up Link:</b> " + "<a href=\"" + eventdetails[i].EVENTLINK + "\">" + eventdetails[i].EVENTLINK + "</a>");
                //sb.AppendLine(Environment.NewLine);
            }

            using (StreamReader sr = new StreamReader(Server.MapPath("/SaaS/EmailTemplate/StandardTemplate.txt"), true))
            {
                String line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    sbTextBox.AppendLine(line);
                    //sbTextBox.AppendLine(Environment.NewLine);
                }
            }

            //var content = System.IO.File.ReadAllText(Server.MapPath("/SaaS/EmailTemplate/StandardTemplate.txt"));
            if (!tbBcc.Text.Trim().Equals(""))
            {
                sbTextBox.Replace("NAMEHERE", "All");
            }
            else
            {
                sbTextBox.Replace("NAMEHERE", lblexternalvolname.Text);
            }
            sbTextBox.Replace("EVENTSHERE", sb.ToString());
            //content.Replace(
            //content.Replace("EVENTSHERE", sb.ToString());
            tbBody.Text = sbTextBox.ToString().Replace(Environment.NewLine, "<br />");
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="CreateEmailInvites.aspx.cs" Inherits="SGServe.SaaS.CreateEmailInvites" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <h4 class="page-section-heading"> <i class="fa fa-envelope-o"></i> Email Invite
        To:
        <asp:Label ID="lblexternalvolname" runat="server" Text="" ForeColor="Red"></asp:Label></h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <!-- this is the wrapper for the content -->


            <!-- extra div for emulating position:fixed of the menu -->

      <%--      <div class="container-fluid split-vertical" >--%>
          
                <div class="panel panel-default table-responsive">
                 

                        <table style="width: 925px; height: 163px; margin-left: 18px">
                            <tr>
                                <td class="width-150" style="width: 165px">
                                    <h5 class="media-heading margin-v-5" style="text-align: left"><i class="fa fa-envelope"></i> <b>Subject:</b></h5>
                                </td>
                                <td class="width-550" style="width: 767px; text-align: left;">
                                    <asp:TextBox ID="tbSubject" runat="server" Height="19px" Width="739px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="width-150" style="width: 165px">
                                    <h5 class="media-heading margin-v-5" style="text-align: left"><i class="fa fa-user"></i> <b>To:</b></h5>
                                </td>
                                <td class="width-550" style="width: 767px">
                                    <asp:TextBox ID="tbRecipent" runat="server" Height="19px" Width="739px" ReadOnly="True"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="width-150" style="width: 165px">
                                    <h5 class="media-heading margin-v-5" style="text-align: left"><i class="fa fa-user-plus"></i> <b>Cc:</b></h5>
                                </td>
                                <td class="width-550" style="width: 767px">

                                    <asp:TextBox ID="tbCc" runat="server" Height="19px" Width="739px"></asp:TextBox>

                                    <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="signUpGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="tbCc" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 25px; width: 165px;">
                                    <h5 class="media-heading margin-v-5" style="text-align: left"><i class="fa fa-user-plus"></i> <b>Bcc:</b></h5>
                                </td>
                                <td style="height: 25px; width: 767px;">
                                    <asp:TextBox ID="tbBcc" runat="server" Height="19px" Width="739px"></asp:TextBox>
                                    </td>
                            </tr>
                            <tr>
                                <td class="width-150" style="width: 165px">
                                    <h5 class="media-heading margin-v-5" style="text-align: left"><i class="fa fa-folder-open"></i> <b>Attachment:</b></h5>
                                </td>
                                <td class="width-550" style="width: 767px">
                                    <asp:FileUpload ID="fuAttachment" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 25px; width: 165px;">
                                    <h5 class="media-heading margin-v-5"><i class="fa fa-file-text"></i> <b>Email Template:</b></h5>
                                </td>
                                <td class="width-550" style="width: 767px; height: 25px;">
                                <asp:LinkButton ID="btnTemplate1" runat="server" CssClass="btn btn-success btn-xs" OnClick="btnTemplate1_Click"><i class="fa fa-envelope-square"></i>
                                 Standard Template</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
       <%-- </div>--%>
     <div class="panel panel-default table-responsive">
        <div class="panel-body">
                <div class="split-vertical-cell">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <ajaxToolkit:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" EnableSanitization="false" TargetControlID="tbBody">
                            </ajaxToolkit:HtmlEditorExtender>
                            <asp:TextBox ID="tbBody" runat="server" TextMode="MultiLine" Height="425px" Width="100%" Wrap="False"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Button ID="btnSend" runat="server" Text="Send" CssClass="btn btn-success " OnClick="btnSend_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
 

</asp:Content>

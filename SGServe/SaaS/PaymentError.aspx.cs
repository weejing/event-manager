﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class PaymentError : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadErrorCode();
        }
        protected void loadErrorCode()
        {
            try
            {
                string error = Request.QueryString["error"];

                if (error.Equals(""))
                {

                }
                else if (error.Equals("1"))
                {
                    string request_id = Request.QueryString["request_id"];
                    string code = Request.QueryString["code"];
                    string code_long = Request.QueryString["code_long"];
                    string message = "Error Message: " + code_long;
                    Label1.Text = message;
                }
                else
                {
                    Label1.Text = "No error messages returned by Paypal";
                }
            } catch (Exception ex)
            {

            }
            
        }
    }
}
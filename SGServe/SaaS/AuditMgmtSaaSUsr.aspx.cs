﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class AuditMgmtSaaSUsr : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            //await generateForm();
            await getEmpParticular();
            //loadData();
        }

        protected async Task generateForm()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/getRoles?connection_string=" + urlArr[1].ToUpper());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var rolTable = JsonConvert.DeserializeObject<DataTable>(reply);
            //ddlRole.DataSource = rolTable;
            //ddlRole.DataTextField = "ROLE_NAME";
            //ddlRole.DataValueField = "ROLE_ID";
            //ddlRole.DataBind();
        }

        protected async Task getEmpParticular()
        {

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/getEmpParticular?connection_string=" + urlArr[1].ToUpper() + "&USR_ID=" + Session["CurrAuditUsrID"].ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            OrgEmployee emp = gm.getOrgEmpDetails(reply);
            txtFullName.Text = emp.NAME;
            txtEmail.Text = emp.EMAIL;

            string roles = "";
            List<Role> roleList = emp.roleList;
            for (int i = 0; i < roleList.Count; i++)
            {
                roles = roles + roleList[i].ROLE_NAME + ", ";
            }
            roles = roles.Substring(0, roles.Length - 2);
            TextBox1.Text = roles;
            // ddlRole.SelectedValue = emp.ROLE_ID;

        }
        public async void loadData()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1]));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                //Label1.Text = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
                //txtOrgShort.Text = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
                //txtEmail.Text = array["EMAIL"].ToString().Substring(1, array["EMAIL"].ToString().Length - 2);
                //txtLink.Text = array["LINK"].ToString().Substring(1, array["LINK"].ToString().Length - 2);
                ///txtUEN.Text = array["UEN"].ToString().Substring(1, array["UEN"].ToString().Length - 2);
                //txtAddress.Text = array["ADDRESS"].ToString().Substring(1, array["ADDRESS"].ToString().Length - 2);
                //LabelDescription.Text = array["DESCRIPTION"].ToString().Substring(1, array["DESCRIPTION"].ToString().Length - 2);
                //ddType.Text = array["ORG_TYPE"].ToString().Substring(1, array["ORG_TYPE"].ToString().Length - 2);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

    }
}
﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Globalization;
using SGServe.Models.AdminModels;
using SGServe.Models.TransactionModels;
using PayPal.PayPalAPIInterfaceService.Model;
using PayPal.PayPalAPIInterfaceService;
using System.Collections;
using SGServe.Models.AccountModels;

namespace SGServe.SaaS
{
    public partial class PaymentSuccess : System.Web.UI.Page
    {
        string org_id = "";
        public static OrgPayment newTransaction = new OrgPayment();
        Boolean paymentMade = false;
        GlobalModule gm = new GlobalModule();
        OrgEmployee employee = new OrgEmployee();
        string amount = "";
        public string connstring = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            loadOrgInfo();
            
            
            //update employee organization status so other employer can access otehr functions
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            employee = await gm.getLoginEmpDetails(urlArr[1].ToUpper());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('$"+ amount + " payment has been made!');window.location ='ManageBill';", true);
        }

        //load data
        public async void loadOrgInfo()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1]));
                connstring = urlArr[1];

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);
                //await Task.Delay(1000);
                String reply = await response.Content.ReadAsStringAsync();

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                this.org_id = array["ORG_ID"].ToString().Substring(1, array["ORG_ID"].ToString().Length - 2);
                newTransaction.ORG_ID = this.org_id;

                paypalGetExpressCheckoutDetails();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Response.Redirect("PaymentError", false);
            }
        }

        //paypal methods
        //methods are loaded sequentially according to paypal developer page and also retrieve neccessary details after each paypal call
        //refer to this page https://devtools-paypal.com/guide/expresscheckout/dotnet?success=true&token=EC-4BL359488S159110S&PayerID=3FLKVMYQCSHVW
        protected void paypalGetExpressCheckoutDetails()
        {
            try
            {
                string token = Request.QueryString["token"];
                string payerID = Request.QueryString["PayerID"];

                GetExpressCheckoutDetailsRequestType get = new GetExpressCheckoutDetailsRequestType();
                get.Token = Request.QueryString.Get("token");
                get.Version = "65.1";

                GetExpressCheckoutDetailsRequestType request = new GetExpressCheckoutDetailsRequestType();
                request.Version = "104.0";
                request.Token = get.Token;
                GetExpressCheckoutDetailsReq wrapper = new GetExpressCheckoutDetailsReq();
                wrapper.GetExpressCheckoutDetailsRequest = request;
                Dictionary<string, string> sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                sdkConfig.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                sdkConfig.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(sdkConfig);
                GetExpressCheckoutDetailsResponseType ecResponse = service.GetExpressCheckoutDetails(wrapper);

                string status = ecResponse.Ack.ToString();
                GetExpressCheckoutDetailsResponseDetailsType paypal_info = ecResponse.GetExpressCheckoutDetailsResponseDetails;
                
                if (status.Equals("SUCCESS"))
                {
                    //get payer information
                    string billing_info = paypal_info.BillingAgreementAcceptedStatus.Value.ToString();
                    getCustomerDetails(paypal_info);//see if got payment details type, PaymentInfoType
                    paypalDoExpressCheckoutPayment(token, payerID, billing_info);
                }
                else
                {
                    var query = HttpUtility.ParseQueryString(string.Empty);
                    var error_obj = ecResponse.GetExpressCheckoutDetailsResponseDetails.PaymentRequestInfo[0];
                    query["error"] = "1";
                    query["request_id"] = error_obj.PaymentRequestID;
                    query["code"] = error_obj.PaymentError.ErrorCode;
                    query["code_long"] = error_obj.PaymentError.LongMessage;
                    string queryString = query.ToString();
                    Response.Redirect("PaymentError?" + queryString, false);
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }
        protected void getCustomerDetails(GetExpressCheckoutDetailsResponseDetailsType paypal_info)
        {
            try
            {
                PayerInfoType payer_info = paypal_info.PayerInfo;
                string payer_email = payer_info.Payer; // jaslyn94-buyer@gmail.com
                string payer_name = payer_info.PayerName.FirstName + " " + payer_info.PayerName.LastName; //"test buyer"
                string payer_id = payer_info.PayerID; //"3FLKVMYQCSHVW"

                //Payment Overview
                PaymentDetailsType paymentDetails = paypal_info.PaymentDetails[0];
                newTransaction.AMT = paymentDetails.OrderTotal.value; //"7.00"

                //Item purchased
                PaymentDetailsItemType planPurchased = paymentDetails.PaymentDetailsItem[0];
                newTransaction.PLAN_TYPE_ID = planPurchased.Number;//PLAN_ID "1F013373-0F1C-4CD2-7893-1CC700F1F457"
                newTransaction.PLAN_NAME = planPurchased.Name; //"YEARLY PLAN"
                newTransaction.PER = planPurchased.Description.Split(' ')[0]; //per
                newTransaction.PLAN_AMOUNT = planPurchased.Amount.value;// "10.00"

                newTransaction.FREE_TRIAL = "0";
                //check for crm code

                if (paymentDetails.PaymentDetailsItem.Count > 1)
                {
                    PaymentDetailsItemType item = paymentDetails.PaymentDetailsItem[1];
                    if (item.Name.Equals("Discount Applied"))
                    {
                        newTransaction.CRM_ID = item.Number;//"fa229d93-7b76-4238-a0a5-1569bef4f984"
                    }
                }

                if (paypal_info.Custom != null)
                {
                    newTransaction.FREE_TRIAL = paypal_info.Custom.Split(':')[1];//"1"
                }
            } catch (Exception ex)
            {

            }
            
        }

        protected void paypalDoExpressCheckoutPayment(string token, string payerID, string billing_info)
        {
            try
            {
                string baseurl = Request.Url.GetLeftPart(UriPartial.Authority) +"/" +connstring+ "/PaymentSuccess";


                PaymentDetailsType paymentDetail = new PaymentDetailsType();
                paymentDetail.NotifyURL = "http://localhost:53197/TAMCC_VMS/PaymentSuccess";
                paymentDetail.NotifyURL = baseurl;
                paymentDetail.PaymentAction = (PaymentActionCodeType)EnumUtils.GetValue("Sale", typeof(PaymentActionCodeType));
                paymentDetail.OrderTotal = new BasicAmountType((CurrencyCodeType)EnumUtils.GetValue("SGD", typeof(CurrencyCodeType)), "1.00");
                List<PaymentDetailsType> paymentDetails = new List<PaymentDetailsType>();
                paymentDetails.Add(paymentDetail);

                DoExpressCheckoutPaymentRequestType request = new DoExpressCheckoutPaymentRequestType();
                request.Version = "104.0";
                DoExpressCheckoutPaymentRequestDetailsType requestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
                requestDetails.PaymentDetails = paymentDetails;
                requestDetails.Token = token;
                requestDetails.PayerID = payerID;
                request.DoExpressCheckoutPaymentRequestDetails = requestDetails;

                DoExpressCheckoutPaymentReq wrapper = new DoExpressCheckoutPaymentReq();
                wrapper.DoExpressCheckoutPaymentRequest = request;
                Dictionary<string, string> sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                sdkConfig.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                sdkConfig.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(sdkConfig);
                DoExpressCheckoutPaymentResponseType doECResponse = service.DoExpressCheckoutPayment(wrapper);

                string status = doECResponse.Ack.ToString();
                var payment_status = doECResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo;
                if (status.Equals("SUCCESS"))
                {
                   //getTransactionResult(doECResponse);
                    //var payment_status_refer = doECResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0];
                    //if recurring billing, get recurring payment profile
                    if (billing_info.Equals("True"))
                    {
                        string bill_msg = "You have selected recurring payment";
                        paypalCreateRecurringPaymentsProfile(token, bill_msg);
                        getTransactionResult(doECResponse);
                    }
                } else
                {
                    var query = HttpUtility.ParseQueryString(string.Empty);
                    var error_obj = doECResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0];
                    query["error"] = "1";
                    query["request_id"] = error_obj.PaymentRequestID;
                    query["code"] = error_obj.PaymentError.ErrorCode;
                    query["code_long"] = error_obj.PaymentError.LongMessage;
                    string queryString = query.ToString();
                    Response.Redirect("PaymentError?" + queryString, false);
                }
            } catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }
        protected void getTransactionResult(DoExpressCheckoutPaymentResponseType doECResponse)
        {
            try
            {
                DoExpressCheckoutPaymentResponseDetailsType response = doECResponse.DoExpressCheckoutPaymentResponseDetails;
                PaymentInfoType paymentInfo = response.PaymentInfo[0];
                string paymentStatus = paymentInfo.PaymentStatus.Value.ToString(); //"COMPLETED"
                if (paymentStatus.Equals("COMPLETED")|| paymentStatus.Equals("PENDING"))
                {
                    newTransaction.PAYPAL_FEE = paymentInfo.FeeAmount.value; //"0.53"
                    newTransaction.PAYMENT_TIME = DateTime.Parse(paymentInfo.PaymentDate).ToLocalTime().ToString(); //"9/10/2016 9:26:39 PM"
                    newTransaction.PAYPAL_ID = paymentInfo.TransactionID; //"94A11958BN919320V"
                    newTransaction.PAYMENT_TYPE = "ONE OFF";
                    this.paymentMade = true;
                    string paymentType = paymentInfo.PaymentType.Value.ToString(); //"INSTANT"
                    string grossAmount = paymentInfo.GrossAmount.value; //"1.00"
                    string taxAmount = paymentInfo.TaxAmount.value; //0.00

                    createTransaction();

                }
            } catch (Exception ex)
            {

            }
        }

        protected void paypalCreateRecurringPaymentsProfile(string token, string billingAgreementDescription)
        {
            try
            {
                //setting parameters
                string currency = "SGD";
                string amount = newTransaction.PLAN_AMOUNT;
                int frequency = 0;
                string frequencyPeriod;
                if (newTransaction.PER.Equals("12"))
                {
                    frequencyPeriod = "Year";
                    frequency = 1;
                }
                else
                {
                    frequencyPeriod = "Month";
                    frequency = Convert.ToInt32(newTransaction.PER);
                }
                newTransaction.PAYMENT_TYPE = "RECURRING";


                CreateRecurringPaymentsProfileRequestType createRPProfileRequest = new CreateRecurringPaymentsProfileRequestType();
                CreateRecurringPaymentsProfileRequestDetailsType createRPProfileRequestDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
                createRPProfileRequestDetails.Token = token;
                createRPProfileRequest.CreateRecurringPaymentsProfileRequestDetails = createRPProfileRequestDetails;

                //get today date to create profile
                DateTime date = DateTime.Now;
                string datetime = date.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                RecurringPaymentsProfileDetailsType profileDetails = new RecurringPaymentsProfileDetailsType(datetime);
                createRPProfileRequestDetails.RecurringPaymentsProfileDetails = profileDetails;

                BasicAmountType paymentAmount = new BasicAmountType((CurrencyCodeType)EnumUtils.GetValue(currency, typeof(CurrencyCodeType)), amount);
                BillingPeriodType period = (BillingPeriodType)EnumUtils.GetValue(frequencyPeriod, typeof(BillingPeriodType));
                BillingPeriodDetailsType paymentPeriod = new BillingPeriodDetailsType(period, frequency, paymentAmount);

                ScheduleDetailsType scheduleDetails = new ScheduleDetailsType();
                scheduleDetails.Description = billingAgreementDescription;
                scheduleDetails.PaymentPeriod = paymentPeriod;
                createRPProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails = scheduleDetails;

                CreateRecurringPaymentsProfileReq createRPProfileReq = new CreateRecurringPaymentsProfileReq();
                createRPProfileReq.CreateRecurringPaymentsProfileRequest = createRPProfileRequest;

                Dictionary<string, string> sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                sdkConfig.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                sdkConfig.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(sdkConfig);
                CreateRecurringPaymentsProfileResponseType createRPProfileResponse = service.CreateRecurringPaymentsProfile(createRPProfileReq);

                string status = createRPProfileResponse.Ack.ToString();
                if (status.Equals("SUCCESS"))
                {
                    getBuyerProfile(createRPProfileResponse);
                } else
                {
                    var query = HttpUtility.ParseQueryString(string.Empty);
                    string error_obj = createRPProfileResponse.CreateRecurringPaymentsProfileResponseDetails.TransactionID;
                    query["error"] = "1";
                    query["request_id"] = error_obj;
                    query["code_long"] = "Error calling Paypal server - Recurring Profile";
                    string queryString = query.ToString();
                    Response.Redirect("PaymentError?" + queryString, false);
                }
            } catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }
        protected void getBuyerProfile(CreateRecurringPaymentsProfileResponseType createRPProfileResponse)
        {
            try
            {
                var details = createRPProfileResponse.CreateRecurringPaymentsProfileResponseDetails;
                newTransaction.PROFILE_ID = details.ProfileID; //"I-SPXFEJARC8N8"
            }
            catch (Exception ex)
            {

            }
        }
        

        //create transactions over to vms
        public async void createTransaction()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Transaction/createNewTransaction");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("FIRST_PAYMENT", "0"));
                keyValues.Add(new KeyValuePair<string, string>("AMT", newTransaction.AMT));
                keyValues.Add(new KeyValuePair<string, string>("CRM_ID", newTransaction.CRM_ID));
                keyValues.Add(new KeyValuePair<string, string>("FREE_TRIAL", newTransaction.FREE_TRIAL));
                keyValues.Add(new KeyValuePair<string, string>("ORG_ID", newTransaction.ORG_ID));
                keyValues.Add(new KeyValuePair<string, string>("PAYMENT_TIME", newTransaction.PAYMENT_TIME));
                keyValues.Add(new KeyValuePair<string, string>("PAYMENT_TYPE", newTransaction.PAYMENT_TYPE));
                keyValues.Add(new KeyValuePair<string, string>("PAYPAL_FEE", newTransaction.PAYPAL_FEE));
                keyValues.Add(new KeyValuePair<string, string>("PAYPAL_ID", newTransaction.PAYPAL_ID));
                keyValues.Add(new KeyValuePair<string, string>("PER", newTransaction.PER));
                keyValues.Add(new KeyValuePair<string, string>("PLAN_AMOUNT", newTransaction.PLAN_AMOUNT));
                keyValues.Add(new KeyValuePair<string, string>("PLAN_TYPE_ID", newTransaction.PLAN_TYPE_ID));
                keyValues.Add(new KeyValuePair<string, string>("PROFILE_ID", newTransaction.PROFILE_ID));

                request.Content = new FormUrlEncodedContent(keyValues);
                HttpResponseMessage response = await client.SendAsync(request);
                //await Task.Delay(1000);
                String reply = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                OrgPayment plan_list = jss.Deserialize<OrgPayment>(reply);
                //OrgPayment confirmedTransaction = new OrgPayment();//newTransaction;

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                TextBox1.Text = array["ORG_PAYMENT_ID"].ToString().Substring(1, array["ORG_PAYMENT_ID"].ToString().Length - 2);
                TextBox2.Text = array["AMT"].ToString().Substring(1, array["AMT"].ToString().Length - 2);
                TextBox3.Text = array["PLAN_NAME"].ToString().Substring(1, array["PLAN_NAME"].ToString().Length - 2);
                amount = TextBox2.Text;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Response.Redirect("PaymentError", false);
            }
        }
    }
}

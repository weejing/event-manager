﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" CodeBehind="ManageExternalVolunteers.aspx.cs" Inherits="SGServe.SaaS.ManageExternalVolunteers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style type="text/css">
        .im-centered {
            margin-left: 300px;
            /*max-width: 700px;*/
        }
    </style>
    <link href="InventoryListCSS/InventoryCss.css" rel="stylesheet" />
    <link href="css/vendor/all.css" rel="stylesheet">
    <h4 class="page-section-heading">Manage External Volunteer</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="tabbable tabs-primary">
                    <!-- Tabs -->
                    <ul class="nav nav-tabs" runat="server">

                        <li class="active" id="firstTab" runat="server">
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click"><i class="fa fa-fw fa-plus"></i>Add A New Record</asp:LinkButton></li>

                        <li id="secondTab" runat="server">
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click"><i class="fa fa-fw fa-list-ol"></i>Add Multiple Records</asp:LinkButton></li>

<%--                        <li id="thirdTab" runat="server">
                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click"><i class="fa fa-pencil-square-o"></i>Create CIP Letter Template</asp:LinkButton></li>--%>
                    </ul>
                    <!-- // END Tabs -->

                    <!-- Panes -->
                    <div class="tab-content" runat="server">
                        <div class="tab-panel active" runat="server" id="insertItemPanel" visible="true">
                            <h4>Add a new record</h4>
                            <div class="panel panel-default">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group form-control-default required">
                                            <label>
                                                Name 
                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="mgmtvolGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbName"
                                                ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                            <asp:TextBox ID="tbName" runat="server" Width="80%" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default required">
                                            <label>
                                                Contact Number 
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="mgmtvolGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbContactNumber" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ValidationGroup="mgmtvolGrp" ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ID="RegularExpressionValidator2" ControlToValidate="tbContactNumber" runat="server" ValidationExpression="^[689]\d{7}$" ErrorMessage="Wrong format"></asp:RegularExpressionValidator></label>
                                            <asp:TextBox ID="tbContactNumber" runat="server" Width="80%" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default required">
                                            <label>
                                                Email Address  
                                                <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="mgmtvolGrp" ID="RequiredFieldValidator4" runat="server" ControlToValidate="tbEmail" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="mgmtvolGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="tbEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator></label>
                                            <asp:TextBox ID="tbEmail" runat="server" Width="80%" class="form-control"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-success" ValidationGroup="mgmtvolGrp"  Width="100px" OnClick="btnAdd_Click" />
                            </div>
                        </div>
                        <div class="tab-panel" runat="server" id="insertMultipleItem" visible="false">
                            <!---newly added-->
                            <div class="wizard">

                                <div class="tab-content">
                                    <div class="tab-panel" role="tabpanel" id="step1" runat="server">
                                         <div class="alert alert-info alert-dismissible fade in" role="alert">
                                            <h4 class="alert-heading">Step 1: Download the template provided</h4> 
                                                  To facilitate the process of importing multiple external volunteer records into SGServe, we encourage 
                                                  users to <strong> download our excel template </strong> and complete the necessary fields.
                                             </div>
                                        <br />
                                        
                                        <div class="row">
                                                 <div class="col-md-6 col-md-offset-3">
                                      <div class="form-group form-control-default">
                                     
                                            <label align="center" >* Click here to begin download! </label>
                                                        <br />
                                                    <center><asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-success btn-icon-stacked" OnClick="btnDownload_Click" ><i class=" fa fa-2x icon-download-1"></i><center>Click to download<br />VolunteerDetailTemplate</center></asp:LinkButton></center>
                                              <br />
                                                     </div>
                                                </div>
                                          
                                      </div>
                                            <ul class="list-inline pull-right">
                                                <li>
                                                    <a href="#step2" aria-controls="step2" role="tab" title="Step 2">
                                                        <asp:Button ID="btnNext" runat="server" Text="Next" class="btn btn-primary" OnClick="btnNext_Click" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                  
                                        <div class="tab-panel" role="tabpanel" id="step2" runat="server" visible="false">
                                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                            <h4 class="alert-heading">Step 2: Upload the completed template provided </h4> 
                                                  After filling in the necessary fields in the excel template, user is required to <strong>upload completed template</strong> for SGServe system
                                                  to create the new records.
                                             </div>
                                        <br />
                                            <div class="row">
                                                 <div class="col-md-6 col-md-offset-3">
                               
                                                <div class="form-group form-control-default">
                                                    <label align="center" >* Select a file to upload! </label>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-xs-6 col-md-offset-1">
                                                            <asp:FileUpload ID="FileUpload1" runat="server" height="30px"/>
                                                           
                                                        </div>
                                                        <div class="col-xs-4 col-md-offset-1">
                                                           <asp:LinkButton ID="btnUpload" runat="server" CssClass="btn btn-success " OnClick="btnUpload_Click"><i class="fa fa-upload"></i>Upload</asp:LinkButton> 
                                                        </div>
                                                    </div>
                                                </div>
                                                     </div>
                                            </div>
                                          
                                            <ul class="list-inline pull-right">
                                                <li>
                                                    <asp:Button ID="btnPrev" runat="server" Text="Prev" class="btn btn-primary" OnClick="btnPrev_Click" />
                                                </li>
                                                <li>
                                                    <asp:Button ID="btnNextNext" runat="server" Text="Done" class="btn btn-primary" OnClick="btnNextNext_Click" />
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

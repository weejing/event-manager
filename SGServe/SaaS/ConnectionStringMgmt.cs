﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace SGServe.SaaS
{
    public class ConnectionStringMgmt
    {
        public static void Add(String connect) {

            RouteTable.Routes.MapPageRoute("SaaSHome" + connect, connect, "~/SaaS/Home.aspx");
            //shihui
            RouteTable.Routes.MapPageRoute("Home" + connect, connect + "/home", "~/SaaS/home.aspx");
            RouteTable.Routes.MapPageRoute("InternalHome" + connect, connect + "/InternalHome", "~/SaaS/InternalHome.aspx");
            RouteTable.Routes.MapPageRoute("Login" + connect, connect + "/login", "~/SaaS/Login.aspx");
            RouteTable.Routes.MapPageRoute("Logout" + connect, connect + "/logout", "~/SaaS/Logout.aspx");
            RouteTable.Routes.MapPageRoute("Employee Login" + connect, connect + "/ologin", "~/SaaS/oLogin.aspx");
            RouteTable.Routes.MapPageRoute("Change Password" + connect, connect + "/changepassword", "~/SaaS/changepassword.aspx");
            RouteTable.Routes.MapPageRoute("Set Password" + connect, connect + "/setpassword", "~/SaaS/setpassword.aspx");
            RouteTable.Routes.MapPageRoute("Reset Password" + connect, connect + "/resetpassword", "~/SaaS/resetpassword.aspx");
            RouteTable.Routes.MapPageRoute("Profile" + connect, connect + "/profile", "~/SaaS/profile.aspx");
            RouteTable.Routes.MapPageRoute("Redirect" + connect, connect + "/redirectpage", "~/SaaS/redirectpage.aspx");
            RouteTable.Routes.MapPageRoute("Manage Account" + connect, connect + "/manageaccount", "~/SaaS/manageaccount.aspx");
            RouteTable.Routes.MapPageRoute("Create Account" + connect, connect + "/createaccount", "~/SaaS/createaccount.aspx");
            RouteTable.Routes.MapPageRoute("Page List" + connect, connect + "/pagelist", "~/SaaS/pagelist.aspx");
            RouteTable.Routes.MapPageRoute("Role List" + connect, connect + "/rolelist", "~/SaaS/rolelist.aspx");
            RouteTable.Routes.MapPageRoute("Notifications" + connect, connect + "/internalhome", "~/SaaS/internalhome.aspx");
            RouteTable.Routes.MapPageRoute("Event Roles" + connect, connect + "/eventroles", "~/SaaS/eventroles.aspx");
            RouteTable.Routes.MapPageRoute("Feedback Responses" + connect, connect + "/fdbackResponses", "~/SaaS/fdbackResponses.aspx");
            
            //debbie
            RouteTable.Routes.MapPageRoute("EditInventoryItems" + connect, connect + "/EditInventoryItems", "~/SaaS/EditInventoryItems.aspx");
            RouteTable.Routes.MapPageRoute("ManageInventory" + connect, connect + "/ManageInventory", "~/SaaS/ManageInventory.aspx");
            RouteTable.Routes.MapPageRoute("TenderStatusList" + connect, connect + "/TenderStatusList", "~/SaaS/TenderStatusList.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventLogisticList" + connect, connect + "/ViewEventLogisticList", "~/SaaS/ViewEventLogisticList.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventLogisticItems" + connect, connect + "/ViewEventLogisticItems", "~/SaaS/ViewEventLogisticItems.aspx");
            RouteTable.Routes.MapPageRoute("ManageExternalVolunteers" + connect, connect + "/ManageExternalVolunteers", "~/SaaS/ManageExternalVolunteers.aspx");
            RouteTable.Routes.MapPageRoute("ViewExternalVolunteer" + connect, connect + "/ViewExternalVolunteer", "~/SaaS/ViewExternalVolunteer.aspx");
            RouteTable.Routes.MapPageRoute("CreateEmailInvites" + connect, connect + "/CreateEmailInvites", "~/SaaS/CreateEmailInvites.aspx");
            RouteTable.Routes.MapPageRoute("CreateVIATemplate" + connect, connect + "/CreateVIATemplate", "~/SaaS/CreateVIATemplate.aspx");
            //junhau
            RouteTable.Routes.MapPageRoute("UpdateOrgInfo" + connect, connect + "/UpdateOrgInfo", "~/SaaS/UpdateOrgInfo.aspx");
            RouteTable.Routes.MapPageRoute("CreateTender" + connect, connect + "/CreateTender", "~/SaaS/CreateTender.aspx");
            RouteTable.Routes.MapPageRoute("CreateTenderItem" + connect, connect + "/CreateTenderItem", "~/SaaS/CreateTenderItem.aspx");
            RouteTable.Routes.MapPageRoute("TenderingMgmt" + connect, connect + "/TenderingMgmt", "~/SaaS/TenderingMgmt.aspx");
            RouteTable.Routes.MapPageRoute("TenderingItemMgmt" + connect, connect + "/TenderingItemMgmt", "~/SaaS/TenderingItemMgmt.aspx");
            RouteTable.Routes.MapPageRoute("ViewTender" + connect, connect + "/ViewTender", "~/SaaS/ViewTender.aspx");
            RouteTable.Routes.MapPageRoute("ViewBids" + connect, connect + "/ViewBids", "~/SaaS/ViewBids.aspx");
            RouteTable.Routes.MapPageRoute("AuditSaaS" + connect, connect + "/AuditMgmtSaaS", "~/SaaS/AuditMgmtSaaS.aspx");
            RouteTable.Routes.MapPageRoute("AuditUsr" + connect, connect + "/AuditMgmtSaaSUsr", "~/SaaS/AuditMgmtSaaSUsr.aspx");

            //weejing
            RouteTable.Routes.MapPageRoute("CreateEvent" + connect, connect + "/CreateEvent", "~/SaaS/CreateEvent.aspx");
            RouteTable.Routes.MapPageRoute("CreateEventSession" + connect, connect + "/CreateEventSession", "~/SaaS/CreateEventSession.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventList" + connect, connect + "/ViewEventList", "~/SaaS/ViewEventList.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventDetails" + connect, connect + "/ViewEventDetails", "~/SaaS/ViewEventDetails.aspx");
            RouteTable.Routes.MapPageRoute("CreateEventLogs" + connect, connect + "/CreateEventLogs", "~/SaaS/CreateEventLogs.aspx");
            RouteTable.Routes.MapPageRoute("CreateFeedBack" + connect, connect + "/CreateFeedBack", "~/SaaS/CreateFeedBack.aspx");
            RouteTable.Routes.MapPageRoute("SessionVol" + connect, connect + "/SessionVol", "~/SaaS/SessionVol.aspx");





            //kai fu
            RouteTable.Routes.MapPageRoute("UsersFollowed" + connect, connect + "/UsersFollowed", "~/SaaS/UsersFollowed.aspx");
            RouteTable.Routes.MapPageRoute("Portfolio" + connect, connect + "/Portfolio", "~/SaaS/Portfolio.aspx");

            //jaslyn
            RouteTable.Routes.MapPageRoute("ManageBill" + connect, connect + "/ManageBill", "~/SaaS/ManageBill.aspx");
            RouteTable.Routes.MapPageRoute("PaymentPlans" + connect, connect + "/PaymentPlans", "~/SaaS/PaymentPlans.aspx");
            RouteTable.Routes.MapPageRoute("PaymentSuccess" + connect, connect + "/PaymentSuccess", "~/SaaS/PaymentSuccess.aspx");
            RouteTable.Routes.MapPageRoute("PaymentError" + connect, connect + "/PaymentError", "~/SaaS/PaymentError.aspx");
            RouteTable.Routes.MapPageRoute("Reporting" + connect, connect + "/Reporting", "~/SaaS/Reporting.aspx");
        }
    }
}
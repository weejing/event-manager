﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationFront.Master" Async="true" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="SGServe.SaaS.Home" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
    <a class="navbar-brand">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </a>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%#Eval("startDate", "{0:d MMM yyyy}") + " - " + Eval("endDate", "{0:d MMM yyyy}") %>
 
    <div class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                   <asp:Image ID="orgImg" runat="server" Width="100%" height="300px"/>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default" style="border-radius: 8px">
                        <div class="panel-heading">
                            <h1>
                                <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>&nbsp;<asp:Image ID="ImageVerify" runat="server" Height="30px" ImageUrl="~/images/badges/verified.png" Width="30px" />
                            </h1>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <p><asp:Label ID="lblDesc" runat="server" Text="Label"></asp:Label></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:HyperLink ID="linkOrgPage" runat="server" target="_blank" NavigateUrl="http://www.google.com" Text="www.google.com" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <i class="fa fa-fw fa-building-o"></i>Address :
                                    <asp:Label ID="lblAddress" runat="server" Text="Label"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <i class="fa fa-fw fa-envelope"></i>Email :
                                    <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
          
            <div class="row">
                <h3 class="page-section-heading">
                    <asp:Label ID="lblEvent" runat="server" Text="Upcoming Event"></asp:Label></h3>
                <asp:DataList ID="DataList1" runat="server" RepeatDirection="Horizontal"  RepeatLayout="Flow">
                    <ItemTemplate>
                        <div class="col-sm-8 col-lg-4 col-md-7">
                            <div class="panel panel-default">
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("imagePath", "../{0}") %>' CssClass="width-100pc" />
                               <div class="panel-body ">
                                 <h4 class="margin-v-0-5" style="overflow: hidden; height: 40px;text-overflow: ellipsis;">
                                    <asp:Label ID="lblEventName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "eventName") %>'></asp:Label></h4>
                                <p>
                                  <span class="label label-customize-100" style="text-align:center"><%#Eval("startDate", "{0:d MMM yyyy}") + " - " + Eval("endDate", "{0:d MMM yyyy}") %></span>
                                 

                                <asp:HiddenField ID="HiddenField" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "eventId") %>' />                                
                                    <asp:Button ID="btnEvent" runat="server" class="btn btn-sm btn-info" Text="Join" PostBackUrl='<%# string.Format("~/EventDetail.aspx?e={0}", DataBinder.Eval(Container.DataItem, "eventId")) %>'/>
                                    </div>
                               </div>
                              </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>
    </div>
  
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
  
</asp:Content>

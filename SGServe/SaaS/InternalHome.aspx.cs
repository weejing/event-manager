﻿using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class InternalHome : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();
        OrgEmployee employee = new OrgEmployee();
        protected async void Page_Load(object sender, EventArgs e)
        {
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            employee = await gm.getLoginEmpDetails(urlArr[1].ToUpper());

            if (employee.ORG_STATUS.Equals("INACTIVE")) {
                Response.Redirect("ManageBill",false);
            } else {

                generateMenu();
            }
        }

        protected void generateMenu()
        {
            DataTable table = (DataTable)Session["EMP_PAGES"];
            dl.DataSource = table;
            dl.DataBind();

        }

    }
}
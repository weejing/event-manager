﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="AuditMgmtSaaSUsr.aspx.cs" Inherits="SGServe.SaaS.AuditMgmtSaaSUsr" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <div class="page-section">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h4 class="page-section-heading">
                    <asp:Label ID="lblHeader" runat="server" Text="View User Details"></asp:Label></h4>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group form-control-default required">
                            <label>
                                Full Name
                            </label>
                            <asp:TextBox ID="txtFullName" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group form-control-default required">
                            <label>
                                Email

                            </label>
                            <asp:TextBox ID="txtEmail" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group form-control-default required">
                            <label>
                                Roles

                            </label>
                            <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                        </div>
<%--                        <div class="form-group form-control-default required">
                            <label>Role</label>
                            <asp:DropDownList ID="ddlRole" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" Enabled="False">
                            </asp:DropDownList>
                        </div>--%>
                        <%-- <asp:Panel ID="pnlPW" runat="server">
                                    <div class="form-group form-control-default required">
                                        <label>
                                            Password
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="createGrp" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPassword" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="createGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator4" ControlToValidate="txtPassword" runat="server" ValidationExpression="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})" ErrorMessage="Must contain digit,uppercase and lowercase. Min 8 characters"></asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox ID="txtPassword" type="password" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <div class="form-group form-control-default required">
                                        <label>
                                            Repeat Password
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="createGrp" ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPw2" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator Display="Dynamic" ValidationGroup="createGrp" SetFocusOnError="true" ID="CompareValidator1" runat="server" ErrorMessage="Password does not match" ControlToCompare="txtPassword" ControlToValidate="txtPw2" ForeColor="Red"></asp:CompareValidator>
                                        </label>
                                        <asp:TextBox ID="txtPw2" type="password" runat="server" class="form-control" ControlToValidate="txtPw2"></asp:TextBox>
                                    </div>
                                </asp:Panel>--%>
                    </div>
                </div>


            </div>
        </div>
    </div>





</asp:Content>

﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class CreateEventLogs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                setUpLogsPage();
            }
        }

        private void setUpLogsPage()
        {
            EventDetails eventDetails = Session["EventDetails"] as EventDetails;
            int count = 1;
            hiddenSecondDropDown.Value = "dummy-";

            sessionPlaceHolder.Controls.Add(new LiteralControl("<select id = 'sessionRole' class ='selectpicker'>"));
            //sessionPlaceHolder.Controls.Add(new LiteralControl("<option value='0' disabled selected style='display: none;'>Select a role</option>"));

            foreach (SessionDetails sessionDetails in eventDetails.listOfSession)
            {
                sessionPlaceHolder.Controls.Add(new LiteralControl("<option value = '" +count + "'>" +sessionDetails.sessionName + " </option>"));

                String dates = "";
                foreach (SessionDays day in sessionDetails.listOfSessionDays)
                {
                    dates += day.evt_Date.ToString("d MMM yyyy") + ",";
                }

                hiddenSecondDropDown.Value += dates + "-";
                count++;
                System.Diagnostics.Debug.WriteLine("hiddenvalue: " + hiddenSecondDropDown.Value);
            }

            sessionPlaceHolder.Controls.Add(new LiteralControl("</select>"));
        }

        /*
        * This method shoots an ajax request to the database to request for a list of roles
        * No authentication token is needed hence there is not a need to go through the vms server         
       */
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] GetLogsList(string prefixText)
        {
            EventLogsName logsName = new EventLogsName { listOfLogsName = new List<string>() };
            CreateEventLogs eventLogs = new CreateEventLogs();
            System.Diagnostics.Debug.WriteLine("please get called");

           
            CreateEventLogs callLogs = new CreateEventLogs();
            callLogs.getListOfLogsName(logsName, prefixText);

            return logsName.listOfLogsName.ToArray();
        }

        /*
         * Method to grab the logistc items 
         */
        private void getListOfLogsName(EventLogsName logsName, string searchValue)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            String connectionString = Session["connectionString"] as string;
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/InventoryMgmt/getListOfLogsName?logsName=" + searchValue + "&connectionString=" + connectionString);
            HttpResponseMessage response = client.SendAsync(request).Result;           
            String reply = response.Content.ReadAsStringAsync().Result;

            List<String> listOfLogs = JsonConvert.DeserializeObject<List<String>>(reply);
            System.Diagnostics.Debug.WriteLine("reply: " + reply);
            logsName.listOfLogsName = listOfLogs;         
        }


        protected void createEvent_Click(object sender, EventArgs e)
        {
            EventDetails eventDetails = Session["EventDetails"] as EventDetails;
            String logisticsTable = hiddenTableValue.Value;
            System.Diagnostics.Debug.WriteLine("logs: " + logisticsTable);
            String[] logList = logisticsTable.Split('-');


            int logsCount = 0;

            foreach (String logistic in logList)
            {
                String[] logsDetail = logistic.Split('+');
                if (logsDetail.Length == 1)
                    continue;
                String[] dates = logsDetail[3].Split(',');
                int sessionValue = Int16.Parse(logsDetail[2]);
                foreach(String date in dates)
                {
                    EventLogs eventLogs = new EventLogs { logsName = logsDetail[0], qty = Int16.Parse(logsDetail[1]) };
                    List<SessionDays> listOfSessionDay = eventDetails.listOfSession[sessionValue - 1].listOfSessionDays;
                    foreach(SessionDays day in listOfSessionDay)
                    {
                        if(date.Equals(day.evt_Date.ToString("d MMM yyyy")))
                        {
                            if (day.listOfLogs == null)
                                day.listOfLogs = new List<EventLogs>();
                            day.listOfLogs.Add(eventLogs);
                            logsCount++;
                        }
                    }
                }               
            }
            eventDetails.logsNum = logsCount;
            createEventDetails(eventDetails);
        }

        private async void createEventDetails(EventDetails eventDetails)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String token = Session["Token"].ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = await client.PostAsJsonAsync("/api/EventMgmt/createEvent", eventDetails);
            String result = await response.Content.ReadAsStringAsync();
            Response.Redirect("ViewEventList", false);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System.Net.Http;
using Newtonsoft.Json;

namespace SGServe.SaaS
{
    public partial class CreateFeedBack : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String token = Session["Token"] as String;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/getSessionInfo?sessionId=" + Request.QueryString["s"]);
                HttpResponseMessage response = await client.SendAsync(request);
                String result = await response.Content.ReadAsStringAsync();
                SessionDetails session = JsonConvert.DeserializeObject<SessionDetails>(result);
                lblHeader.Text = session.sessionName + " Feedback Form";


                getFeedBackDesign();
            }
        }

        protected async void save_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(hiddenFeedBack.Value);
            String[] feedBackQuestion = hiddenFeedBack.Value.Split('+');
            List<FeedBackDesign> listOfFeedBackDetail = new List<FeedBackDesign>();
            String sessionId = Request.QueryString["s"];

            for(int feedBackCount =0; feedBackCount < feedBackQuestion.Length -1; feedBackCount++)
            {
                String[] feedBack = feedBackQuestion[feedBackCount].Split(',');
                FeedBackDesign feedBackDetail = new FeedBackDesign();

                feedBackDetail.sessionId = sessionId;
                feedBackDetail.question = feedBack[0];
                feedBackDetail.type = Int16.Parse(feedBack[1]);

                listOfFeedBackDetail.Add(feedBackDetail);
                                               
            }

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String token = Session["Token"].ToString();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            await client.PostAsJsonAsync("/api/EventMgmt/createFeedBack", listOfFeedBackDetail);

            Response.Redirect("CreateFeedBack?s="+ sessionId, false);
        }

        protected async void getFeedBackDesign()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            String token = Session["Token"] + "";
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            System.Diagnostics.Debug.WriteLine("wierd");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/getFeedBackDetail?sessionId=" + Request.QueryString["s"]);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("result: " + result);

            List<FeedBackDesign> listOfFeedBackDesign = JsonConvert.DeserializeObject<List<FeedBackDesign>>(result);

            if (listOfFeedBackDesign.Count == 0)
                return;
            hiddenFeedBackDesign.Value = "";

            int count = 0;
            foreach(FeedBackDesign feedbackDesign in listOfFeedBackDesign)
            {
                hiddenFeedBackDesign.Value += feedbackDesign.question + "," + feedbackDesign.type ;
                hiddenFeedBackDesign.Value += "+";
                count++;
            }

            System.Diagnostics.Debug.WriteLine("count " + count);
        }

        protected void btnViewResp_Click(object sender, EventArgs e)
        {
            Response.Redirect("fdbackResponses?s="+ Request.QueryString["s"], false);
        }
    }
}
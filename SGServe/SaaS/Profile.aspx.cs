﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FastMember;

namespace SGServe.SaaS
{
    public partial class Profile : System.Web.UI.Page
    {
        OrgEmployee employee = new OrgEmployee();
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                string[] urlArr = Request.RawUrl.ToString().Split('/');
                employee= await gm.getLoginEmpDetails(urlArr[1].ToUpper());

                if (employee.USR_ID == null)
                {
                    Response.Redirect("ologin", false);
                }
                else
                {
                    displayDetails();
                }
            }
        }


        private async void displayDetails()
        {

            lblEmail.Text = employee.EMAIL;
            lblName.Text = employee.NAME;
            DataTable table = new DataTable();
            using (var reader = ObjectReader.Create(employee.roleList))
            {
                table.Load(reader);
            }
            dlRole.DataSource = table;
            dlRole.DataBind();
        }

    }
}
﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="CreateAccount.aspx.cs" Inherits="SGServe.SaaS.CreateAccount" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
        
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="page-section">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h4 class="page-section-heading">
                    <asp:Label ID="lblHeader" runat="server" Text="Create Account"></asp:Label></h4>
                        <div class="row" style="margin-left:2px">
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete" />
                                <asp:Button ID="btnEdit" class="btn btn-success btn-sm" runat="server" Text="Edit" OnClick="btnEdit_Click" />&nbsp;&nbsp;<asp:Button ID="btnDelete" class="btn btn-danger btn-sm" runat="server" Text="Delete" OnClick="btnDelete_Click" />
                        </div><br>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group form-control-default required">
                                    <label>
                                        Full Name
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtFullName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="txtFullName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>
                                        Email
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="createGrp" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="createGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                        <asp:Label ID="lblmessage" runat="server" Text="Email already exists" ForeColor="#FF3300"></asp:Label>
                                    </label>
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>Role</label>
                                    <asp:CustomValidator ValidationGroup="createGrp" runat="server" SetFocusOnError="true" Display="Dynamic" ID="cv" ClientValidationFunction="ValidateRoleList" ErrorMessage="Please select" ForeColor="Red"></asp:CustomValidator>
                                      <asp:CheckBoxList ID="cbRole" runat="server" SetFocusOnError="true" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                                            </asp:CheckBoxList>
                                </div>
  
                            </div>
                        </div>

                <asp:Button ID="btnSave" runat="server" class="btn btn-success" ValidationGroup="createGrp" Text="Save" OnClick="btnSave_Click" /><br />
                <br />
                <br />
            </div>
        </div>
    </div>
     <script language="javascript">
        function ValidateRoleList(source, args) {
            var chkListModules = document.getElementById('<%= cbRole.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>

﻿using SGServe.CommonClass;
using SGServe.SaaS.Models;
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace SGServe.SaaS
{
    public partial class Home : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();

        static String connectionString = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                await loadData();
                loadEvents();
            }
        }


        public async Task loadData()
        {
            String status = "";
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo2");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1]));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("reply: " + reply);

                OrganizationDetails od = JsonConvert.DeserializeObject<OrganizationDetails>(reply);

                Label1.Text = od.ORG_NAME;
                lblTitle.Text = od.ORG_NAME;
                connectionString = od.CONNECTION_STRING;
                lblEmail.Text = od.EMAIL;
                linkOrgPage.Text = od.LINK;
                linkOrgPage.NavigateUrl = "http://" + od.LINK;
                lblAddress.Text = od.ADDRESS;
                lblDesc.Text = od.DESCRIPTION;
                orgImg.ImageUrl = od.IMAGE;
                status = od.ORG_STATUS;
                if (od.VERIFICATION.Equals("True"))
                {
                    ImageVerify.Visible = true;
                }
                else
                {
                    ImageVerify.Visible = false;
                }

                /*
                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                Label1.Text = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
                lblTitle.Text = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
                connectionString = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
                lblEmail.Text = array["EMAIL"].ToString().Substring(1, array["EMAIL"].ToString().Length - 2);
                String link = array["LINK"].ToString().Substring(1, array["LINK"].ToString().Length - 2);
                linkOrgPage.Text = link; //.Replace("http://w","w");
                linkOrgPage.NavigateUrl = "http://" + link;
                lblAddress.Text = array["ADDRESS"].ToString().Substring(1, array["ADDRESS"].ToString().Length - 2);
                lblDesc.Text = array["DESCRIPTION"].ToString().Substring(1, array["DESCRIPTION"].ToString().Length - 2);
                orgImg.ImageUrl = array["IMAGE"].ToString().Substring(1, array["IMAGE"].ToString().Length - 2); //.Replace('-', '+').Replace('_', '/');
                status = array["ORG_STATUS"].ToString().Substring(1, array["ORG_STATUS"].ToString().Length - 2);
                */
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            if (status == "PREPARING") {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('The system is still preparing the SaaS Account!');window.location ='../Home';", true);
            }
            
        }


        public async void loadEvents()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/EventMgmt/getRecentEventsListByOrg?connectionName=" + connectionString);
                HttpResponseMessage message = await client.SendAsync(request);
                String reply = await message.Content.ReadAsStringAsync();

                List<EventDetails> ed = JsonConvert.DeserializeObject<List<EventDetails>>(reply);
                if (ed.Any())
                {
                    lblEvent.Visible = true;
                    foreach (EventDetails detail in ed)
                    {
                        detail.eventId = detail.eventId + "&o=" + connectionString;
                    }

                    DataList1.DataSource = ed;
                    DataList1.DataBind();
                }
                else {
                    lblEvent.Visible = false;
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected void btnEvent_Click(object sender, EventArgs e)
        {
            int count = DataList1.Items.Count;
            for (int i = 0; i < count; i++)
            {
                HiddenField lbl = DataList1.Items[i].FindControl("eventId") as HiddenField;
                Session["eventID"] = lbl.Value;
            }
            Response.Redirect("../ViewEventDetail.aspx?e=" + Session["eventID"] + "&o=" + connectionString, false);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class Loading : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:59637");
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/launchNewOrgInstance");
                // form up the body data
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", Session["CONNECTION_STRING"].ToString()));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("reply: " + reply);
                //Label1.Text = reply;

                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), reply, "alert('Record Inserted Successfully')", true);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Insert New Tender Button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }


            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:59637");
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Transaction/insertNewPayment");
                // form up the body data
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("ORG_PAYMENT_ID", "-"));
                keyValues.Add(new KeyValuePair<string, string>("ORG_ID", Session["ORG_ID"].ToString()));
                keyValues.Add(new KeyValuePair<string, string>("AMT", "100"));
                keyValues.Add(new KeyValuePair<string, string>("PAYMENT_TIME", DateTime.Now.ToString()));
                keyValues.Add(new KeyValuePair<string, string>("PLAN_TYPE", "YEARLY"));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                //Label3.Text = "Done: " + reply;

               // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), reply, "alert('Record Inserted Successfully')", true);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Insert New Payment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            Response.Redirect("CreateOrgUser.aspx",false);

        }


    }
}
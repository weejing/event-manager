﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using Newtonsoft.Json;
using FastMember;
using System.Data;

namespace SGServe.SaaS
{
    public partial class VIewEventDetails : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                displayForm();
            
        }

        protected async void displayForm()
        {
            String token = Session["Token"].ToString();
            String eventId = Request.QueryString["e"];

            Session["eventId"] = eventId;

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveSessionDetails?eventId=" + eventId);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            EventDetails eventDetails = JsonConvert.DeserializeObject<EventDetails>(result);

            //display event cause
            DataTable tableCause = new DataTable();
            using (var reader = ObjectReader.Create(eventDetails.listofCauseName))
            {
                tableCause.Load(reader);
            }
            dlcause.DataSource = tableCause;
            dlcause.DataBind();

            evtName.Text = eventDetails.eventName;
            desc.Text = eventDetails.description;
            Date.Text = eventDetails.startDate.ToString("d MMM yyyy") + " - " + eventDetails.endDate.ToString("d MMM yyyy");
            evtImage.ImageUrl = "../" +eventDetails.imagePath;

            hiddenRoleQty.Value = "";
            hiddenRoleDesc.Value = "";

            int count = 0;
            foreach (SessionDetails sessionDetail in eventDetails.listOfSession)
            {
                addSessionDetail(sessionDetail, count);
                count++;
            }

        }

        protected void addSessionDetail(SessionDetails sessionDetail, int count)
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<h3 class='page-section-heading' style='color:#1A5276;margin-bottom:5px'>" + sessionDetail.sessionName + "</h4>"));
            addPanelOpenings();
            String time = sessionDetail.startTime.ToShortTimeString() + " - " + sessionDetail.endTime.ToShortTimeString();
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class ='fa fa-clock-o fa-fw'></i>" + time + "<br/>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class='fa fa-map-marker fa-fw'></i>" + sessionDetail.location + "<br/><br>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<p class='margin-none'>" + sessionDetail.sessionDescription + "</p><hr>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("Session Days "));
            defineSessionDaysDropDown(sessionDetail.listOfSessionDays, count, sessionDetail.listOfSessionRoles);

            sessionDetailsHolder.Controls.Add(new LiteralControl("<br>"));

            defineTable(count);

            sessionDetailsHolder.Controls.Add(new LiteralControl("<br> <a href ='CreateFeedBack?s="+sessionDetail.sessionId+"'>FeedBack</a>"));
                                       
            addPanelClosings();
        }

       
        protected void addPanelOpenings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel panel-default'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
            //sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media media-clearfix-xs media-clearfix-sm'>"));
            //sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media-body'>"));
        }

        protected void addPanelClosings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            //sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            //sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
        }



        protected void defineTable(int count)
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'table-responsive'>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<table class = 'table' id = 'table"+count+"'>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<thead><tr> <th>Event Roles</th> <th>Quantity</th> <th>Description</th></tr></thead>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<tbody>"));



            sessionDetailsHolder.Controls.Add(new LiteralControl("</tbody>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</table>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
        }



        protected void defineSessionDaysDropDown(List<SessionDays> listOfSessionDays, int count, List<SessionRoles> listOfSessionRole)
        {
            String selectId = "select" + count;
            sessionDetailsHolder.Controls.Add(new LiteralControl("<select id = '" + selectId + "' class ='btn btn-white dropdown-toggle' onchange = 'roleNameOnChange(" + selectId + ", " + count + ")'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<option value='' disabled selected style='display: none;'>Select a day</option>"));

            int dayCount = 1;
            foreach(SessionDays sessionDay in listOfSessionDays)
            {
                sessionDetailsHolder.Controls.Add(new LiteralControl("<option value = '" + (dayCount-1) + "'>" + sessionDay.evt_Date.ToString("d MMM yyyy") + " </option>"));

                int roleCount = 1;
                foreach(SessionRoles role in listOfSessionRole)
                {
                    hiddenRoleName.Value += role.roleName;
                    hiddenRoleQty.Value += "<a href='SessionVol?d="+sessionDay.sessionDayId+"&r="+role.evt_roleId+"'>" + sessionDay.listOfRolesQuantity[roleCount -1] +"</a>";
                    hiddenRoleDesc.Value += role.roleDesc;
                    hiddenRoleMaxQty.Value += role.quantity;
                    if (roleCount != listOfSessionRole.Count)
                    {
                        hiddenRoleName.Value += ",";
                        hiddenRoleQty.Value +=",";
                        hiddenRoleDesc.Value +=",";
                        hiddenRoleMaxQty.Value += ",";
                    }
                    roleCount++;
                }

                if(dayCount != listOfSessionDays.Count)
                {
                    hiddenRoleName.Value += "-";
                    hiddenRoleQty.Value += "*";
                    hiddenRoleDesc.Value += "-";
                    hiddenRoleMaxQty.Value += "-";
                }
                dayCount++;

            }

            sessionDetailsHolder.Controls.Add(new LiteralControl("</select>"));

            hiddenRoleName.Value += "+";
            hiddenRoleQty.Value += "+";
            hiddenRoleDesc.Value += "+";
            hiddenRoleMaxQty.Value += "+";
        }


       

        protected  async void delete_btn(Object sender, EventArgs e)
        {
            String token = Session["Token"].ToString();
            String eventId = Session["eventId"] + "";

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpResponseMessage response = await client.PostAsJsonAsync("/api/EventMgmt/deleteEvent", eventId);
            
            Response.Redirect("ViewEventList", false);

        }

    }
}
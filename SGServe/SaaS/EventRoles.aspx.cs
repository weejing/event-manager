﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class EventRoles : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();
        string[] urlArr;
        protected async void Page_Load(object sender, EventArgs e)
        {
            urlArr = Request.RawUrl.ToString().Split('/');
            if (!IsPostBack)
            {
                await generateGV();
            }
        }

        protected async Task generateGV()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/OrgGetEvtRoleList?connection_string="+ urlArr[1].ToUpper());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var acctable = JsonConvert.DeserializeObject<DataTable>(reply);
            gv.DataSource = acctable;
            gv.DataBind();
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;

        }

        protected async void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = (int)e.NewPageIndex;
            await generateGV();
        }
        //havent change below
        protected async void btnSave_Click(object sender, EventArgs e)
        { 
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request;
            EventRole evtRole = new EventRole();
            request = new HttpRequestMessage(HttpMethod.Post, "/api/Admin/OrgUpdateEvtRole");
            evtRole.ROLE_ID = Convert.ToInt32(hiddenRoleID.Value);
            evtRole.TRAITS = rbl1.SelectedValue + rbl2.SelectedValue + rbl3.SelectedValue + rbl4.SelectedValue;
            evtRole.CONNECTION_STRING = urlArr[1].ToUpper();
            List<Term> EVTROLESKILLLIST = new List<Term>();
            foreach (ListItem item in cbSkill.Items)
            {
                if (item.Selected)
                {
                    Term skill = new Term { TERM_ID = Convert.ToInt32(item.Value), TERM_NAME = item.Text };
                    EVTROLESKILLLIST.Add(skill);
                }
            }
            evtRole.EVTROLESKILLLIST = EVTROLESKILLLIST;
            var response = await client.PostAsJsonAsync("api/Admin/OrgUpdateEvtRole", evtRole);
            gm.MessageBox("Record updated!");
            await generateGV();
        
        }

        protected async void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage skillReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermListByVocabName?vocab_name=skill");
            HttpResponseMessage skillResp = await client.SendAsync(skillReq);
            String skills = await skillResp.Content.ReadAsStringAsync();
            var skillstable = JsonConvert.DeserializeObject<DataTable>(skills);
            cbSkill.DataSource = skillstable;
            cbSkill.DataTextField = "TERM_NAME";
            cbSkill.DataValueField = "TERM_ID";
            cbSkill.DataBind();

            //display role skills
            Server_Connect vrserverConnection = new Server_Connect();
            HttpRequestMessage vrrequest = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/OrgGetEvtRoleSkills?term_id=" + Convert.ToInt32(gv.DataKeys[gv.EditIndex].Value.ToString()) + "&connection_string="+ urlArr[1].ToUpper());
            HttpResponseMessage vrresponse = await client.SendAsync(vrrequest);
            String vrreply = await vrresponse.Content.ReadAsStringAsync();
            EventRole evtRole = JsonConvert.DeserializeObject<EventRole>(vrreply);

            if (evtRole.EVTROLESKILLLIST != null)
            {
                foreach (Term evtSkill in evtRole.EVTROLESKILLLIST)
                {
                    cbSkill.Items.FindByValue(evtSkill.TERM_ID.ToString()).Selected = true;
                }
            }
            Label labelName = gv.Rows[gv.EditIndex].FindControl("lblroleName") as Label;
            lblName.Text = labelName.Text;
            if (evtRole.TRAITS != null)
            {
                char[] charArr = evtRole.TRAITS.ToCharArray();
                rbl1.SelectedValue = charArr[0].ToString();
                rbl2.SelectedValue = charArr[1].ToString();
                rbl3.SelectedValue = charArr[2].ToString();
                rbl4.SelectedValue = charArr[3].ToString();
            }
            else
            {
                rbl1.SelectedValue = "X";
                rbl2.SelectedValue = "X";
                rbl3.SelectedValue = "X";
                rbl4.SelectedValue = "X";
            }
            mp.Show();
            lblMdTitle.Text = "Edit";
            hiddenRoleID.Value = gv.DataKeys[gv.EditIndex].Value.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            mpHelpText.Show();
        }
    }
}
﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        OrgEmployee emp = new OrgEmployee();
        GlobalModule gm = new GlobalModule();
        string[] urlArr;
        protected async void Page_Load(object sender, EventArgs e)
        {
            urlArr = Request.RawUrl.ToString().Split('/');
          
            lblmessage.Visible = false;
            if (!IsPostBack)
            {
                txtEmail.CssClass = "form-control";
                txtFullName.CssClass = "form-control";
                cbRole.CssClass = "checkbox checkbox-success";
                await generateCbRole();

                if (Session["emp_id"] == null)
                {
                    //add
                    lblHeader.Text = "Create Account";
                    btnSave.Visible = true;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                }
                else
                {
                    //view
                    lblHeader.Text = "View particular";
                    //display particulars
                    await getEmpParticular();
                    txtEmail.Text = emp.EMAIL;
                    txtFullName.Text = emp.NAME;
                    if (emp.roleList!=null) {
                        foreach (Role role in emp.roleList)
                        {
                            cbRole.Items.FindByValue(role.ROLE_ID.ToString()).Selected = true;
                        }
                    }
                    txtEmail.Enabled = false;
                    txtFullName.Enabled = false;
                    cbRole.Enabled = false;
                    btnSave.Visible = false;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                }
            }

        }
        protected async Task generateCbRole()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getRoleList?CONNECTION_STRING="+ urlArr[1].ToUpper());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var table = JsonConvert.DeserializeObject<DataTable>(reply);
            cbRole.DataSource = table;
            cbRole.DataTextField = "ROLE_NAME";
            cbRole.DataValueField = "ROLE_ID";
            cbRole.DataBind();
        }

        protected async void btnSave_Click(object sender, EventArgs e)
        {   //check for existing email
            Server_Connect serverConnection1 = new Server_Connect();
            HttpClient client1 = serverConnection1.getHttpClient();
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/checkEmailExist?connection_string=" + urlArr[1].ToUpper()+"&EMAIL=" + txtEmail.Text.ToString());
            HttpResponseMessage resp = await client1.SendAsync(req);
            String emailExist = await resp.Content.ReadAsStringAsync();
            //email exits
            if (Convert.ToBoolean(emailExist) == true)
            {
                //edit account
                if (lblHeader.Text == "Edit particular")
                {
                    await getEmpParticular();
                    if ((txtEmail.Text.ToString()) != emp.EMAIL)
                    {
                        lblmessage.Visible = true;
                    }
                    else
                    {
                        await doAction(urlArr);
                    }
                }
                else
                {
                    lblmessage.Visible = true;
                }
            }
            else
            {
                await doAction(urlArr);
            }
         
        }

        protected async Task doAction(string[] urlArr)
        {
            lblmessage.Visible = false;
            List<Role> roleList = new List<Role>();
            foreach (ListItem item in cbRole.Items)
            {
                if (item.Selected)
                {
                    Role role = new Role { ROLE_ID = item.Value.ToString() };
                    roleList.Add(role);
                }
            }
            if (lblHeader.Text == "Create Account")
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgAccount/createAccount");
                OrgEmployee employee = new OrgEmployee();
                employee.NAME = txtFullName.Text;
                employee.EMAIL = txtEmail.Text;
                employee.PASSWORD = "Heyhey12";
                employee.roleList = roleList;
                employee.CONNECTION_STRING = urlArr[1].ToUpper();
                employee.URL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/" + urlArr[1].ToUpper();
                var response = client.PostAsJsonAsync("api/OrgAccount/createAccount", employee);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Account created! An email is sent to the user to set his/her password.');window.location ='ManageAccount';", true);
            }
            else
            {
                if (Session["emp_id"] != null)
                {

                    Server_Connect serverConnection = new Server_Connect();
                    HttpClient client2 = serverConnection.getHttpClient();
                    String jwtToken = Session["Token"] as String;
                    client2.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgAccount/updateAccDetail");
                    OrgEmployee employee = new OrgEmployee();
                    employee.USR_ID = Session["emp_id"].ToString();
                    employee.NAME = txtFullName.Text;
                    employee.EMAIL = txtEmail.Text;
                    employee.roleList = roleList;
                    employee.CONNECTION_STRING = urlArr[1].ToUpper();
                    var response = await client2.PostAsJsonAsync("api/OrgAccount/updateAccDetail", employee);
                    Session.Remove("emp_id");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Account updated!');window.location ='ManageAccount';", true);
                }
                else
                {
                    Response.Redirect("manageaccount", false);
                }
            }
        }


        protected async Task getEmpParticular()
        {
            if (Session["emp_id"]!=null) {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                String jwtToken = Session["Token"] as String;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/getEmpParticular?connection_string=" + urlArr[1].ToUpper() + "&USR_ID=" + Session["emp_id"].ToString());
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                emp = JsonConvert.DeserializeObject<OrgEmployee>(reply);
                emp.EMAIL = emp.EMAIL;
                emp.NAME = emp.NAME;
                emp.PASSWORD = emp.PASSWORD;
                emp.USR_ID = emp.USR_ID;
                emp.REGISTRATION_DATE = (DateTime)(emp.REGISTRATION_DATE);
                emp.RESETPW_CODE = emp.RESETPW_CODE;
                emp.roleList = emp.roleList;
            } else {
                Response.Redirect("manageaccount", false);
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            txtEmail.Enabled = true;
            txtFullName.Enabled = true;
            cbRole.Enabled = true;
            txtEmail.CssClass = "form-control";
            txtFullName.CssClass = "form-control";
            cbRole.CssClass = "checkbox checkbox-success";
            btnSave.Visible = true;
            lblHeader.Text = "Edit particular";
        }

        protected async void btnDelete_Click(object sender, EventArgs e)
        {
            if (Session["emp_id"] != null)
            {
                Server_Connect serverConnection3 = new Server_Connect();
            HttpClient client3 = serverConnection3.getHttpClient();
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
                String jwtToken = Session["Token"] as String;
                client3.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request3 = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/deleteAcc?connection_string=" + urlArr[1].ToUpper() + "&USR_ID=" + Session["emp_id"].ToString());
            HttpResponseMessage response3 = await client3.SendAsync(request3);
            String reply3 = await response3.Content.ReadAsStringAsync();
            Session.Remove("emp_id");
            Response.Redirect("manageaccount", false);
            }
            else
            {
                Response.Redirect("manageaccount", false);
            }
        }
    }
}
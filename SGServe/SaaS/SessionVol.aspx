﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="SessionVol.aspx.cs" Inherits="SGServe.SaaS.SessionVol" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div class="page-section">
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <h4 class="page-section-heading"><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label> </h4>
                <br>
                <div class="panel panel-default">
                    <br>
                      <div class="table-responsive">
                         
                               <asp:GridView CellPadding="50" GridLines="None" Height="24px" Width="100%" CssClass="table"
                  AllowPaging="True" EmptyDataText="There is no record" ID="gv" DataKeyNames="VOL_ID" runat="server"  AutoGenerateColumns="False" PageSize="20" OnPageIndexChanging="gv_PageIndexChanging">
                       <Columns>
                            <asp:BoundField DataField="FULL_NAME" SortExpression="FULL_NAME" HeaderText="Name"></asp:BoundField>
                            <asp:BoundField DataField="EMAIL" SortExpression="EMAIL" HeaderText="Email"></asp:BoundField>
                             <asp:BoundField DataField="DIETARY" SortExpression="DIETARY" HeaderText="Dietary"></asp:BoundField>
                        <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink2" class="btn btn-default btn-xs" runat="server" NavigateUrl='<%# string.Format("~/UserPortfolioView.aspx?id={0}", DataBinder.Eval(Container.DataItem, "VOL_ID")) %>' Target="_blank">View</asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                </asp:TemplateField> 
                           </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                   <PagerStyle HorizontalAlign = "Right" CssClass = "pagination-ys" />
                    </asp:GridView>
                </div>

            </div>
            </div>
        </div>
 </div>
</asp:Content>

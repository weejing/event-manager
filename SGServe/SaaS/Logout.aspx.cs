﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["role"] != null)
            {
                Session.Abandon();
                //Session.Remove("Token");
                //Session.Remove("role");
                //Session.Remove("notificationList");
                //Session.Remove("usr_id");                
                Response.Redirect("ologin", false);
            }
            else {
                Session.Abandon();
                //Session.Remove("Token");
                //Session.Remove("role");
                Response.Redirect("Home", false);
        }
        }
    }
}
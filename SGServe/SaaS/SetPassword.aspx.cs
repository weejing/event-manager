﻿using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class SetPassword : System.Web.UI.Page
    {
        OrgEmployee employee = new OrgEmployee();
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            await gm.getOrgDetail(Request.RawUrl.ToString(), lbltitle1);
            if (!this.IsPostBack)
            {
                lblMessage.Visible = false;
                if (Session["Token"] == null)
                {
                    string a = Request.Url.AbsoluteUri;
                    if (Request.Url.AbsoluteUri.Contains(("ResetPwCode")) != true)
                    {
                        Response.Redirect("ologin", false);
                    }
                    
                }
            }
        }

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/resetPw?connection_string=" + urlArr[1].ToUpper() + "&resetpw_code=" + Request.QueryString["ResetPwCode"] + "&newpassword=" + txtPassword.Text);
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                if (reply == "false")
                {
                    lblMessage.Text = "Invalid Code";
                    lblMessage.Visible = true;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Password has been reset successfully!');window.location ='ologin';", true);
                }

        }




    }
}
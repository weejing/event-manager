﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="PaymentSuccess.aspx.cs" Inherits="SGServe.SaaS.PaymentSuccess" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="panel panel-default">
        <div class="page-section" style="padding: 1%; padding-left: 5%;">
            <h3>Payment has been successfully made!</h3>
            <p>Here are the details of your transaction</p>
            <asp:Panel ID="Panel1" runat="server">
                <div class="row" style="padding-top: 10px;">
                    <div class="col-lg-4">
                        <asp:Label ID="Label4" runat="server" Text="Payment ID"></asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server" class="form-control input-sm" ReadOnly="True"></asp:TextBox>
                    </div>
                </div>
                <div class="row" style="padding-top: 10px;">
                    <div class="col-lg-4">
                        <asp:Label ID="Label1" runat="server" Text="Amount"></asp:Label>
                        <asp:TextBox ID="TextBox2" runat="server" class="form-control input-sm" ReadOnly="True"></asp:TextBox>
                    </div>
                </div>
                <div class="row" style="padding-top: 10px;">
                    <div class="col-lg-4">
                        <asp:Label ID="Label2" runat="server" Text="Plan Name"></asp:Label>
                        <asp:TextBox ID="TextBox3" runat="server" class="form-control input-sm" ReadOnly="True"></asp:TextBox>
                    </div>
                </div>
                  </asp:Panel>
        </div>
    </div>
  
</asp:Content>

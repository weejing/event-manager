﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationFront.Master" AutoEventWireup="true" CodeBehind="SetPassword.aspx.cs" Inherits="SGServe.SaaS.SetPassword" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
    <a class="navbar-brand hidden-xs navbar-brand-primary">
        <asp:Label ID="lbltitle1" runat="server" Text="Label"></asp:Label>
    </a>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
    <h4 class="page-section-heading">
        <asp:Label ID="lblTitle" runat="server" Text="Reset Password"></asp:Label></h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group form-control-default required">
                <label>
                    New Password
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPassword" ErrorMessage="Please fill in" ForeColor="Red" ValidationGroup ="resetGrp"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator4" ControlToValidate="txtPassword" runat="server" ValidationExpression="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})" ValidationGroup ="resetGrp" ErrorMessage="Must contain digit,uppercase and lowercase. Min 8 characters"></asp:RegularExpressionValidator>
                </label>
                <asp:TextBox ID="txtPassword" type="password" runat="server" class="form-control"></asp:TextBox>
            </div>
            <div class="form-group form-control-default required">
                <label>
                    Repeat Password
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ID="RequiredFieldValidator6" runat="server" ValidationGroup ="resetGrp" ControlToValidate="txtPw2" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator Display="Dynamic" SetFocusOnError="true" ID="CompareValidator1" runat="server" ValidationGroup ="resetGrp" ErrorMessage="Password does not match" ControlToCompare="txtPassword" ControlToValidate="txtPw2" ForeColor="Red"></asp:CompareValidator>
                </label>
                <asp:TextBox ID="txtPw2" type="password" runat="server" class="form-control" ControlToValidate="txtPw2"></asp:TextBox>
                <asp:Label ID="lblMessage" runat="server" Text="Label" ForeColor="#FF3300"></asp:Label>
            </div>
        </div>
    </div>
     <asp:Button ID="btnSave" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSave_Click" ValidationGroup ="resetGrp"/><br />
    <br />
                 </div>
    </div>
</asp:Content>

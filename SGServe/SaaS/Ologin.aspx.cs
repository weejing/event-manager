﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using System.Json;
using System.Threading.Tasks;
using SGServe.Models.AccountModels;
using SGServe.Models.CommunityModels;
using SGServe.SaaS.Models;



namespace SGServe.SaaS
{
    public partial class Ologin : System.Web.UI.Page
    {
        OrgEmployee employee = new OrgEmployee();
        OrganizationDetails org = new OrganizationDetails();
        GlobalModule gm = new GlobalModule();
        Boolean canLogin;
        protected async void Page_Load(object sender, EventArgs e)
        {
            await gm.getOrgHeader(Request.RawUrl.ToString());
            if (!IsPostBack)
            {
                await gm.getOrgDetail(Request.RawUrl.ToString(), lbltitle);
                lblmessage.Visible = false;
            }
        }

        protected async void btnLogin_Click(object sender, EventArgs e)
        {
            await request_login();
            if (canLogin == true)
            {
                string[] urlArr = Request.RawUrl.ToString().Split('/');
                employee= await gm.getLoginEmpDetails(urlArr[1].ToUpper());
                Response.Redirect("internalhome", false);
            }
            else {
                lblmessage.Visible = true;
            }
        }


        private async Task request_login()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/Authorise/Token");

            // form up the body data
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("UserName", txtLoginEmail.Text));
            keyValues.Add(new KeyValuePair<string, string>("password", txtLoginPassword.Text));
            keyValues.Add(new KeyValuePair<string, string>("LoginType", "Organiser"));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));
            keyValues.Add(new KeyValuePair<string, string>("connectionString",urlArr[1].ToUpper()));

            request.Content = new FormUrlEncodedContent(keyValues);

            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            JsonObject content = (JsonObject)JsonValue.Parse(result);
            ICollection<String> listOfKeys = content.Keys;

            foreach (string key in listOfKeys)
            {
                if (key.Equals("error"))
                {
                    canLogin = false;
                }
                else
                {
                    String access = content["access_token"].ToString();
                    String token = access.Substring(1, access.Length - 2);
                    Session["Token"] = token;
                    Session.Add("LoginType", "Organiser");
                    canLogin = true;
                    break;
                }
            }

        }

        //private async Task getOrgEmpDetails()
        //{
        //    Server_Connect serverConnection = new Server_Connect();
        //    HttpClient client = serverConnection.getHttpClient();
        //    String jwtToken = Session["Token"] as String;
        //    if (jwtToken != null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
        //        // add the jwt token into the authorization header for server validation
        //        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        //        string url = Request.RawUrl.ToString();
        //        string[] urlArr = url.Split('/');
        //        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/OrgAccount/getOrgEmpDetails?connection_string=" + urlArr[1].ToUpper());
        //        HttpResponseMessage Resp = await client.SendAsync(request);
        //        String reply = await Resp.Content.ReadAsStringAsync();
        //        employee = GlobalModule.getOrgEmpDetails(reply);
        //        Session["role"] = employee.ROLEE;
        //    }
        //}    
        }
}
﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class PageList : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                await getRoleName();
                await generatTV();
            }
        }
        protected async Task getRoleName()
        {
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getRole?CONNECTION_STRING="+ urlArr[1].ToUpper()+"&role_id=" + Session["role_id"].ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            dynamic data = JsonObject.Parse(reply);
            string rolename = data.ROLE_NAME;
            lbltitle.Text = rolename + " Access rights";
        }

        protected async Task generatTV()
        {
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getModuleList?CONNECTION_STRING="+ urlArr[1].ToUpper());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var cattable = JsonConvert.DeserializeObject<DataTable>(reply);

            HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getPageList?CONNECTION_STRING=" + urlArr[1].ToUpper());
            HttpResponseMessage response2 = await client.SendAsync(request2);
            String reply2 = await response2.Content.ReadAsStringAsync();
            var childtable = JsonConvert.DeserializeObject<DataTable>(reply2);
            PopulateTreeView(cattable, "a", null, childtable);

            //checked role existing access rights
            HttpRequestMessage request3 = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getRoleModList?CONNECTION_STRING="+ urlArr[1].ToUpper() + "&role_id=" + Session["role_id"].ToString());
            HttpResponseMessage response3 = await client.SendAsync(request3);
            String reply3 = await response3.Content.ReadAsStringAsync();
            var rolModsTable = JsonConvert.DeserializeObject<DataTable>(reply3);
            foreach (DataRow row in rolModsTable.Rows)
            {
                foreach (TreeNode node in TreeView1.Nodes)
                {
                    if (node.Value == row["ID"].ToString())
                    {
                        node.Checked = true;
                    }
                }
            }
        }

        private void PopulateTreeView(DataTable dtParent, string parentId, TreeNode treeNode, DataTable Mainchildtable)
        {
            foreach (DataRow row in dtParent.Rows)
            {
                TreeNode child = new TreeNode
                {
                    Text = row["NAME"].ToString(),
                    Value = row["ID"].ToString()
                };
                if (parentId == "a")
                {
                    TreeView1.Nodes.Add(child);
                    DataTable dtChild = getData(Mainchildtable, child.Value);
                    PopulateTreeView(dtChild, child.Value, child, Mainchildtable);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }
            }
        }

        private DataTable getData(DataTable dtChild, string id)
        {
            DataTable dt = dtChild.Copy();
            dt.Rows.Clear();
            foreach (DataRow row in dtChild.Rows)
            {
                if (row["MODULE_ID"].ToString() == id)
                {
                    dt.ImportRow(row);
                }
            }
            return dt;
        }

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Admin/createRoleMod");
            Role role = new Role();
            List<Module> roleMods = new List<Module>();
            foreach (TreeNode node in TreeView1.Nodes)
            {
                if (node.Checked == true) // Checking whether a node is checked or not.
                {
                    Module mod = new Module();
                    mod.ID = node.Value;
                    roleMods.Add(mod);
                }
            }
            role.roleModList = roleMods;
            role.ROLE_ID = Session["role_id"].ToString();
            role.CONNECTION_STRING = urlArr[1].ToUpper();
            var response = client.PostAsJsonAsync("api/Admin/createRoleMod", role);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Role access rights updated!');window.location ='RoleList';", true);
            Session.Remove("role_id");
        }
    }
}
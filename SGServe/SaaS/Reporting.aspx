﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" Async="true" CodeBehind="Reporting.aspx.cs" Inherits="SGServe.SaaS.Reporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <div class="panel panel-default">
        <div class="page-section" style="padding: 2%; padding-top:0;">
            <%-- VOLUNTEER REPORTING --%>
            <div class="row" style="padding: 3%; padding-bottom: 0">
                <h1>Volunteer Reporting</h1>
            </div>
            <div class="row">
                <div class="col-lg-12" style="background-color: #fad0d0; padding: 2%;">
                    <asp:Label ID="Label3" runat="server" Text="0" Font-Size="XX-Large"></asp:Label>
                    <p>Total Number of Volunteer followed</p>
                </div>
                <div class="col-lg-12" style="min-height: 300px;">
                    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                    <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                    <div id="VolNumChart" style="height: 400px; width:100%;">
                    </div>
                </div>

                <div class="col-lg-6" style="background-color: #fad0d0; padding: 2%; margin-top: 4%;">
                    <asp:Label ID="Label4" runat="server" Text="0" Font-Size="XX-Large"></asp:Label>
                    <p>Skills most volunteers have</p>
                </div>
                <div class="col-lg-6" style="background-color: #fad0d0; padding: 2%; margin-top: 4%;">
                    <asp:Label ID="Label5" runat="server" Text="0" Font-Size="XX-Large"></asp:Label>
                    <p>Causes most volunteers are interested in</p>
                </div>
                <div class="col-lg-6" style="min-height: 300px;">
                    <script type="text/javascript"></script>
                    <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                    <div id="VolSkillInterest" style="width: 660px; height: 400px;">
                    </div>
                </div>
                <div class="col-lg-6" style="min-height: 300px;">
                    <script type="text/javascript"></script>
                    <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                    <div id="VolCauseInterest" style="width: 660px; height: 400px;">
                    </div>
                </div>
                
                <div class="col-lg-12" style="background-color: #fad0d0; padding: 2%; margin-top: 4%;">
                    <asp:Label ID="Label6" runat="server" Text="0" Font-Size="XX-Large"></asp:Label>
                    <p>Event Roles most volunteers are interested in</p>
                </div>
                
                <div class="col-lg-12" style="min-height: 300px;">
                    <script type="text/javascript"></script>
                    <asp:Literal ID="Literal5" runat="server"></asp:Literal>
                    <div id="VolEventRoleInterest" style="width: 660px; height: 400px;">
                    </div>
                </div>
            </div>


            <%-- Event Details --%>
            <div class="row" style="padding: 3%; padding-bottom: 0; margin-top:10%;">
                <h1>Event Reporting</h1>
            </div>
            <div class="row" style="padding: 3%; padding-top: 0">
                <div class="col-lg-12" style="background-color: #f3c592; padding: 2%;">
                    <asp:Label ID="Label9" runat="server" Text="0" Font-Size="XX-Large"></asp:Label>
                    <p>Numbers of upcoming events</p>
                </div>
                <div class="col-lg-12" style="min-height: 350px; margin-top:2%; padding-top:3%;">
                    <script type="text/javascript"></script>
                    <div id="EventNumChart">
                    </div>
                </div>
                <div class="col-lg-12" style="background-color: #f3c592; padding: 2%;  margin-top: 4%;">
                    <asp:Label ID="Label10" runat="server" Text="0" Font-Size="XX-Large"></asp:Label>
                    <p>Event with higest number of volunteers turn out in last 6 months</p>
                </div>
                <div class="col-lg-12" style="min-height: 300px;">
                    <script type="text/javascript"></script>
                    <asp:Literal ID="Literal9" runat="server"></asp:Literal>
                    <div id="EventAttendanceChart" style="width: 1100px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Literal ID="ltScripts" runat="server"></asp:Literal>
</asp:Content>

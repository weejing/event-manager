﻿using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class changepassword : System.Web.UI.Page
    {
        OrgEmployee employee = new OrgEmployee();
        GlobalModule gm=new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
          
            if (!this.IsPostBack)
            {
                    lblMessage.Visible = false;
           
                    //change password
                    lblTitle.Text = "Change Password";
                    RequiredFieldValidator1.Enabled = true;
                    RegularExpressionValidator1.Enabled = true;
                    pnlCurrentPw.Visible = true;
                string[] urlArr = Request.RawUrl.ToString().Split('/');
                employee = await gm.getLoginEmpDetails(urlArr[1].ToUpper());
                if (employee.USR_ID == null)
                    {
                        Response.Redirect("ologin", false);
                    }
            }
        }

        //private async Task getOrgEmpDetails()
        //{
        //    Server_Connect serverConnection = new Server_Connect();
        //    HttpClient client = serverConnection.getHttpClient();
        //    String jwtToken = Session["Token"] as String;
        //    if (jwtToken != null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
        //        // add the jwt token into the authorization header for server validation
        //        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        //        string url = Request.RawUrl.ToString();
        //        string[] urlArr = url.Split('/');
        //        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/OrgAccount/getOrgEmpDetails?connection_string=" + urlArr[1].ToUpper());
        //        HttpResponseMessage Resp = await client.SendAsync(request);
        //        String reply = await Resp.Content.ReadAsStringAsync();
        //        employee = GlobalModule.getOrgEmpDetails(reply);
        //        Session["role"] = employee.ROLEE;
        //    }
        //}

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            employee=await gm.getLoginEmpDetails(urlArr[1].ToUpper());
            if (employee.USR_ID == null)
                {
                    Response.Redirect("ologin", false);
                }
                else
                {
                    Server_Connect serverConnection = new Server_Connect();
                    HttpClient client = serverConnection.getHttpClient();
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/changePw?connection_string=" + urlArr[1].ToUpper() + "&email=" + employee.EMAIL + "&newpassword=" + txtPassword.Text + "&oldpassword=" + txtCurrentpw.Text);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();
                    if (reply == "true")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Password has been changed successfully!');window.location ='profile';", true);
                    }
                    else
                    {
                        lblMessage.Text = "Wrong current password";
                        lblMessage.Visible = true;
                    }
                }
            
        }



     
    }
}
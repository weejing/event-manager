﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="AuditMgmtSaaS.aspx.cs" Inherits="SGServe.SaaS.AuditMgmt" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="panel panel-default">
        <div class="page-section" style="padding: 1%; padding-left: 5%;">
            <h4 class="page-section-heading">Audit Trail List</h4>
            <div class="table-responsive">
                <asp:GridView CellPadding="50" GridLines="None" Height="24px" Width="100%" CssClass="table" AllowPaging="True" EmptyDataText="No record!" ID="AuditGrid" DataKeyNames="USR_ID" runat="server" AutoGenerateColumns="False" OnDataBound="AuditItemGrid_DataBound" OnPageIndexChanging="AuditGrid_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="TIMESTAMP" HeaderText="Timestamp"></asp:BoundField>
                        <asp:BoundField DataField="MODULE_NAME" HeaderText="Module Name"></asp:BoundField>
                        <asp:BoundField DataField="MODULE_FEATURE" HeaderText="Module Feature"></asp:BoundField>
                        <asp:BoundField DataField="NAME" HeaderText="System User"></asp:BoundField>
                        <asp:BoundField DataField="ACTION" HeaderText="Action"></asp:BoundField>
                        <asp:BoundField DataField="DESCRIPTION" HeaderText="Description"></asp:BoundField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkView" OnClick="lnkView_Click">View User</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle ForeColor="#0066CC" />
                    <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using Newtonsoft.Json;
using SGServe.Models.AccountModels;

namespace SGServe.SaaS
{
    public partial class ViewJoinedVolunteer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

            }
        }


        protected async void displayJoinedVolunteer()
        {
            String token = Session["Token"].ToString();

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveVolunteerList?sessionDayId=" + Request.QueryString["d"] + "&evtRoleId="+Request.QueryString["r"]);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            List<Volunteer> volunteerList = JsonConvert.DeserializeObject<List<Volunteer>>(result);

        }
    }
}
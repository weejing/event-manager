﻿<%@ Page Title="" Language="C#" Async ="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="ViewEventList.aspx.cs" Inherits="SGServe.SaaS.ViewEventList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server"> 
    <h1 class="text-h2">Events</h1>
    <div class="row gridalicious" data-toggle="gridalicious" data-width="300">
          <asp:placeholder id ="eventListPanel" runat="server" ></asp:placeholder>
    </div>      
</asp:Content>

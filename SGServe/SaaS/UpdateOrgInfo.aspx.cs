﻿using SGServe.SaaS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class UpdateOrgInfo : System.Web.UI.Page
    {
        public static bool result = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                loadData();
            }
        }

        public async void loadData()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://vmsserver.azurewebsites.net/");
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1].ToUpper()));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("reply: " + reply);

                JsonObject array = (JsonObject)JsonValue.Parse(reply);

                txtOrgName.Text = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
                //txtOrgShort.Text = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
                txtEmail.Text = array["EMAIL"].ToString().Substring(1, array["EMAIL"].ToString().Length - 2);
                txtLink.Text = array["LINK"].ToString().Substring(1, array["LINK"].ToString().Length - 2);
                txtAddress.Text = array["ADDRESS"].ToString().Substring(1, array["ADDRESS"].ToString().Length - 2);
                txtDesc.Text = array["DESCRIPTION"].ToString().Substring(1, array["DESCRIPTION"].ToString().Length - 2);
                ddType.Text = array["ORG_TYPE"].ToString().Substring(1, array["ORG_TYPE"].ToString().Length - 2);
                imgBadge.ImageUrl = array["IMAGE"].ToString().Substring(1, array["IMAGE"].ToString().Length - 2).Replace('-', '+').Replace('_', '/'); ;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }




        }


        protected async void btnReg_Click(object sender, EventArgs e)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://vmsserver.azurewebsites.net/");
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/updateOrganization");
                // form up the body data

                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("ORG_NAME", txtOrgName.Text));
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1].ToUpper()));
                keyValues.Add(new KeyValuePair<string, string>("EMAIL", txtEmail.Text));
                keyValues.Add(new KeyValuePair<string, string>("LINK", txtLink.Text));
                keyValues.Add(new KeyValuePair<string, string>("ADDRESS", txtAddress.Text));
                keyValues.Add(new KeyValuePair<string, string>("DESCRIPTION", txtDesc.Text));
                keyValues.Add(new KeyValuePair<string, string>("ORG_TYPE", ddType.Text));

                if (FileUploadImage.HasFile)
                {
                    if ((Path.GetExtension(FileUploadImage.FileName).ToLower() != ".jpg") && Path.GetExtension(FileUploadImage.FileName).ToLower() != ".png")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please upload an image');", true);
                    }

                    string guidResult = System.Guid.NewGuid().ToString("N");
                    string fn = System.IO.Path.GetFileName(FileUploadImage.PostedFile.FileName);
                    //to specify the filename
                    string filename = fn.ToLower().ToString();
                    string[] exts = filename.Split('.');
                    string ext = exts[1].ToString();
                    string finexts = guidResult + "." + ext;
                    String storedPath = Server.MapPath("../SaaS/images/orgs/" + finexts);
                    String path = "images/orgs/" + finexts;

                    keyValues.Add(new KeyValuePair<string, string>("IMAGE_PATH", path));
                    FileUploadImage.SaveAs(storedPath);
                }
                request.Content = new FormUrlEncodedContent(keyValues);
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();

                JavaScriptSerializer jss = new JavaScriptSerializer();
                string message = jss.Deserialize<string>(reply);

                if (message.Equals("Success"))
                {
                    MessageBox.Show("Organisation update is successful!");
                } else
                {
                    //MessageBox.Show("Organisation update is unsuccessful!");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Insert New Organization Button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        /*
        protected void validateWebsite(object sender, ServerValidateEventArgs e)
        {
            string source = txtLink.Text;
            Uri uriResult;
            result = Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;

            e.IsValid = result;
        }
        */

    }
}
﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class RoleList : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();
        string[] urlArr;
        protected async void Page_Load(object sender, EventArgs e)
        {
            urlArr = Request.RawUrl.ToString().Split('/');
            await gm.getLoginEmpDetails(urlArr[1].ToUpper());
            if (!IsPostBack)
            {
                await generateGV();
            }
        }

        protected async Task generateGV()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getRoleList?CONNECTION_STRING="+ urlArr[1].ToUpper());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var acctable = JsonConvert.DeserializeObject<DataTable>(reply);
            gv.DataSource = acctable;
            gv.DataBind();
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;

        }


        protected async void btnAdd_Click(object sender, EventArgs e)
        {
            mp.Show();
            lblMdTitle.Text = "Add";
            txtName.Text = "";
        }

        protected async void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = (int)e.NewPageIndex;
            await generateGV();
        }


        protected async void btnSave_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request;
            Role role = new Role();

            if (lblMdTitle.Text == "Add")
            {
                request = new HttpRequestMessage(HttpMethod.Post, "/api/Admin/createRole");
                role.ROLE_NAME = txtName.Text.Trim();
                role.CONNECTION_STRING = urlArr[1].ToUpper();
                var response = await client.PostAsJsonAsync("api/Admin/createRole", role);
                gm.MessageBox("Record created!");
            }
            else
            {
                request = new HttpRequestMessage(HttpMethod.Post, "/api/Admin/updateRole");
                role.ROLE_ID = hiddenID.Value.ToString();
                role.ROLE_NAME = txtName.Text.Trim();
                role.CONNECTION_STRING = urlArr[1].ToUpper();
                var response = await client.PostAsJsonAsync("api/Admin/updateRole", role);
                gm.MessageBox("Record updated!");
            }
            await generateGV();
        }

        protected async void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getRole?CONNECTION_STRING="+ urlArr[1].ToUpper() + "&role_id=" + gv.DataKeys[gv.EditIndex].Values["ROLE_ID"].ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            dynamic data = JsonObject.Parse(reply);
            txtName.Text = data.ROLE_NAME;
            mp.Show();
            lblMdTitle.Text = "Edit";
            hiddenID.Value = data.ROLE_ID;
        }

        protected async void btnDelete_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Server_Connect serverConnection3 = new Server_Connect();
            HttpClient client3 = serverConnection3.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client3.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request3 = new HttpRequestMessage(HttpMethod.Post, "/api/Admin/deleteRole");
            Role role = new Role();
            role.ROLE_ID = gv.DataKeys[row.RowIndex].Value.ToString();
            role.ROLE_NAME = txtName.Text.Trim();
            role.CONNECTION_STRING = urlArr[1].ToUpper();
            var response = await client3.PostAsJsonAsync("/api/Admin/deleteRole", role);
            await generateGV();

        }

        protected void lbtnView_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["role_id"] = gv.DataKeys[row.RowIndex].Value.ToString();
            Response.Redirect("pagelist", false);

        }
    }
}

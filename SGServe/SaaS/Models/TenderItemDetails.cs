﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.SaaS.Models
{
    public class TenderItemDetails
    {
        public TenderItemDetails()
        {

        }
        public string TENDER_ITEM_ID
        {
            get;


            set;

        }
        public string TENDER_DESCRIPTION
        {
            get;


            set;

        }

        public string CONNECTION_STRING
        {
            get;



            set;

        }

        public int QTY
        {
            get;


            set;

        }

        public string TENDER_STATUS
        {
            get;


            set;

        }
        public string BID_STATUS
        {
            get;


            set;

        }
        public string TENDER_HEADER_ID
        {
            get;


            set;

        }

        public string WINNER_SOLO_FIRM_ID
        {
            get;


            set;

        }
        public string WINNER_GROUP_FIRM_ID
        {
            get;


            set;

        }

        public string ORG_NAME
        {
            get;


            set;

        }
    }
}
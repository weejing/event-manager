﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.SaaS.Models
{
    public class AuditTrail
    {
        public AuditTrail()
        {

        }
        public string AUDIT_TRAIL_ID
        {
            get;
            set;
        }
        public string MODULE_NAME
        {
            get;
            set;
        }
        public string MODULE_FEATURE
        {
            get;
            set;
        }
        public string ACTION
        {
            get;
            set;
        }
        public string DESCRIPTION
        {
            get;
            set;
        }
        public DateTime TIMESTAMP
        {
            get;
            set;
        }
        public string SYSTEM_USR
        {
            get;
            set;
        }
        public string USR_ID
        {
            get;
            set;
        }
        public string NAME
        {
            get;
            set;
        }
        public string CONNECTION_STRING
        {
            get;
            set;
        }
    }
}
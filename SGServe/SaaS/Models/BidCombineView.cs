﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.SaaS.Models
{
    public class BidCombineView
    {
        public BidCombineView()
        {

        }
        public string VDR_BIDS_ID
        {
            get;
            set;
        }
        public string TENDER_ID
        {
            get;
            set;
        }
        public double BID_PRICE
        {
            get;
            set;
        }
        public DateTime BID_DATE
        {
            get;
            set;
        }
        public string STATUS
        {
            get;
            set;
        }
        public string PRICING_BREAKDOWN
        {
            get;
            set;
        }
        public string VENDOR_ID
        {
            get;
            set;
        }
        public string PRODUCT_NAME
        {
            get;
            set;
        }
        public string PRODUCT_DESC
        {
            get;
            set;
        }
        public DateTime DELIVERY_TIME
        {
            get;
            set;
        }

        public string PRODUCT_BRAND
        {
            get;
            set;
        }

        public string COMPANY_NAME
        {
            get;
            set;
        }

        public string ADDRESS
        {
            get;
            set;
        }

        public string CITY
        {
            get;
            set;
        }

        public string POSTAL_CODE
        {
            get;
            set;
        }

        public string STATE
        {
            get;
            set;
        }

        public string PHONE
        {
            get;
            set;
        }

        public string FAX
        {
            get;
            set;
        }

        public string COM_DESCRIPTION
        {
            get;
            set;
        }
        public double RATING
        {
            get;
            set;
        }
    }
}
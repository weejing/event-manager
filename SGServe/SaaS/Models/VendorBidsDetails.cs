﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.SaaS.Models
{
    public class VendorBidsDetails
    {
        public VendorBidsDetails()
        {

        }
        public string VDR_BIDS_ITEM_ID
        {
            get;
            set;
        }
        public string TENDER_ITEM_ID
        {
            get;
            set;
        }
        public double BID_PRICE
        {
            get;
            set;
        }
        public double BID_PRICE_SOLO
        {
            get;
            set;
        }
        public string PRODUCT_NAME
        {
            get;
            set;
        }
        public string PRODUCT_DESC
        {
            get;
            set;
        }
        public string PRODUCT_BRAND
        {
            get;
            set;
        }
        public int RATING
        {
            get;
            set;
        }
        public string STATUS
        {
            get;
            set;
        }

        public string FIRM_ID
        {
            get;
            set;
        }
        public string FIRM_NAME
        {
            get;
            set;
        }
        public DateTime BID_DATE
        {
            get;
            set;
        }
        public DateTime VALID_TO
        {
            get;


            set;
        }
        public DateTime DELIVERY_TIME
        {
            get;
            set;
        }
        public DateTime PACKING_TIME
        {
            get;
            set;
        }
        public string ADDRESS
        {
            get;
            set;
        }
        public string PHONE
        {
            get;
            set;
        }
        public string FAX
        {
            get;
            set;
        }
        public string FIRM_DESCRIPTION
        {
            get;
            set;
        }
    }
}
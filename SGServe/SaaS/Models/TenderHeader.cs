﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.SaaS.Models
{
    public class TenderHeader
    {
        public TenderHeader()
        {

        }
        public string TENDER_HEADER_ID
        {
            get;


            set;

        }
        public DateTime START_DATE
        {
            get;


            set;

        }

        public DateTime END_DATE
        {
            get;



            set;

        }

        public string STATUS
        {
            get;


            set;

        }

        public string CATEGORY
        {
            get;


            set;

        }
        public string CONNECTION_STRING
        {
            get;


            set;

        }
        public string ORG_NAME
        {
            get;


            set;

        }
        public string NAME
        {
            get;


            set;

        }
    }
}
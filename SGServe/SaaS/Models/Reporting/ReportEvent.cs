﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.SaaS.Models.Reporting
{
    public class ReportEvent
    {
        public ReportEvent()
        {

        }
        public string EVENT_ID
        { get; set; }
        public string EVENT_NAME
        { get; set; }
        public string SESSION_NAME
        { get; set; }
        public DateTime DATE
        { get; set; }
        public string EVENT_DATE
        { get; set; }
        public int EVENT_CREATED
        { get; set; }
        public int TIME_IN
        { get; set; }
        public int EXCEED
        { get; set; }
        public int EXAM
        { get; set; }
        public string EVENT_DESC
        { get; set; }
        public string START_DATE
        { get; set; }
        public string END_DATE
        { get; set; }
        public int REG
        { get; set; }
        public int TURN_UP
        { get; set; }
    }
}
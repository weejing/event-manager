﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace SGServe.SaaS.Models
{
    public class MessageBox
    {
        /// &lt;summary&gt;
        /// Shows messagebox with the specified message.
        /// </summary>
        /// <param name="message"></param>
        public static void Show(string message)
        {
            Page executingPage = HttpContext.Current.Handler as Page;
            if (executingPage != null)
            {
                executingPage.ClientScript.RegisterStartupScript(executingPage.GetType(), "MsgBox",
                    string.Format("alert('{0}');", message), true);
            }
        }
    }
}
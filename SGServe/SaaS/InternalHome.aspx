﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="InternalHome.aspx.cs" Inherits="SGServe.SaaS.InternalHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<asp:DataList ID="dl" runat="server" RepeatDirection="Horizontal"  RepeatLayout="Flow">
                    <ItemTemplate>
                        <div class="col-sm-4 col-lg-4 col-md-4 col-xs-8">
                            <div class="panel panel-default text-center">
                  <div class="panel-body">
                    <div data-scale-color="false" data-track-color="#efefef" data-line-width="6" data-toggle="tooltip" title="View" onclick="location.href='<%# DataBinder.Eval(Container.DataItem, "url").ToString().Replace(".aspx","") %>'">
                      <div class="value text-center">
                        <span class="strong"><i class='<%# string.Format("fa fa-{0} fa-3x", DataBinder.Eval(Container.DataItem, "icon")) %>'></i><h2 class="text-display-2 text-success margin-none">  <asp:Label ID="lblcompleted" runat="server" Text=""></asp:Label> </h2><h4 class="margin-none"><br /><%# DataBinder.Eval(Container.DataItem, "name") %></h4></span>
                     <br />
                           </div>
                    </div>  
                  </div>
                </div>
                            <%--<div class="cover hover overlay margin-none">
                                <asp:Image ID="imgBadge" runat="server"  Height="80" Width="80" src='<%# DataBinder.Eval(Container.DataItem, "PHOTO_PATH") %>' />
                            </div>
                            <div class="panel-body" style="height:180px;width:100%">
                                <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "STATUS") %>' Font-Bold="True"></asp:Label><br />
                                <div style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; line-height: 16px; max-height: 46px; -webkit-line-clamp: 3; -webkit-box-orient: vertical;"><%# DataBinder.Eval(Container.DataItem, "DESCRIPTION") %></div>
<%--                                <asp:Label ID="lblDesc" style="white-space: nowrap; width: 12em; overflow: hidden; text-overflow: ellipsis;" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DESCRIPTION") %>'></asp:Label><br /><br />--%>
                            <%--    <asp:Label ID="Label1" runat="server" Text="Criteria:" Font-Bold="True"></asp:Label><br />
                                <asp:Label ID="lbl" runat="server"></asp:Label><br />
                                <asp:Button ID="btnEdit" runat="server" class="btn btn-info btn-xs" Text="Edit" PostBackUrl='<%# string.Format("Viewbadge.aspx?id={0}", DataBinder.Eval(Container.DataItem, "BADGE_ID")) %>' />
                                <asp:Button ID="btnPublish" runat="server" class="btn btn-success btn-xs" Text="Publish" CommandName="Publish" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BADGE_ID") %>' />
                                <asp:Button ID="btnDelete" runat="server" class="btn btn-danger btn-xs" Text="Delete" CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BADGE_ID")+","+ DataBinder.Eval(Container.DataItem, "PHOTO_PATH")%>' />
                             <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete" />
                            </div>--%>
                                </div>
                    </ItemTemplate>
                </asp:DataList>
</asp:Content>

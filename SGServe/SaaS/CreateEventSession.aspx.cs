﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using SGServe.Models.EventModels;
using AjaxControlToolkit;
using SGServe.CommonClass;
using System.Net.Http;
using System.Globalization;
using Newtonsoft.Json;
using System.Data;

namespace SGServe.SaaS
{
    public partial class CreateEventSession : System.Web.UI.Page
    {
        // create a list to store the ID of a sessionRole text row
        private List<String> controlList
        {
            get
            {
                if (ViewState["controls"] == null)
                {
                    ViewState["controls"] = new List<String>();
                }

                return (List<String>)ViewState["controls"];
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Validate();
            EventDetails eventdetails = (EventDetails)Session["eventDetails"];
            int numOfEventSession = eventdetails.numOfSessions;
            generateForm(numOfEventSession);
        }

        protected void generateForm(int numOfSession)
        {

            for (int count = 1; count <= numOfSession; count++)
            {
                // starting div tags
                eventSessionPanel.Controls.Add(new LiteralControl("<br/>"));
                eventSessionPanel.Controls.Add(new LiteralControl("<div class='text-center bg-transparent margin-none'>"));
                eventSessionPanel.Controls.Add(new LiteralControl("<h4 class='text-headline'> Session" + count + "</h4>"));
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'panel panel-default'>"));
                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'panel-body'>"));


                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'row'>"));
                setUpNameRow(count);
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'row'>"));
                setUpLocationRow(count);
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'row'>"));
                setUpDescriptionRow(count);
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'row'>"));
                setUpDateRow(count);
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'row'>"));
                setUpStartTime(count);
                setUpEndTime(count);
                setUpComplusoryDayRow(count);
                eventSessionPanel.Controls.Add(new LiteralControl("</div><hr>"));


                //eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'row'>"));
                //setUpComplusoryDayRow(count);
                //eventSessionPanel.Controls.Add(new LiteralControl("</div><hr>"));

                PlaceHolder sessionRole = setUpSessionRolesPlaceHolder(count);

                createRoleTxtBox(sessionRole, count);


                // add button for event roles               
                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'row'>"));

                eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
                Button roleButton = defineButton();
                roleButton.ID = count.ToString();
                roleButton.Click += new EventHandler(addSessionRole);
                eventSessionPanel.Controls.Add(roleButton);
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));


                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

                // closing div tags
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
                eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

            }
        }

        /*
         * First role is sesison name
         */
        protected void setUpNameRow(int count)
        {
            // session Name text box
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-6'>"));
            Label label = new Label();
            label.Text = "Session Name   ";
            eventSessionPanel.Controls.Add(label);
            TextBox textBox = defineTextBox();
            textBox.ID = "sessionName" + count;

            RequiredFieldValidator validator = defineRequiredFieldValidator();
            validator.ID = "validateName" + count;
            validator.ControlToValidate = textBox.ID;
            eventSessionPanel.Controls.Add(validator);
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            eventSessionPanel.Controls.Add(textBox);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));


        }

        // second row is event location
        protected void setUpLocationRow(int count)
        {
            // location textbox
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-5'>"));
            Label label = new Label();
            label.Text = "Location ";
            eventSessionPanel.Controls.Add(label);
            TextBox textBox = defineTextBox();
            textBox.ID = "location" + count;
            RequiredFieldValidator validator = defineRequiredFieldValidator();
            validator.ID = "validateLocation" + count;
            validator.ControlToValidate = textBox.ID;
            eventSessionPanel.Controls.Add(validator);
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            eventSessionPanel.Controls.Add(textBox);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));


            // postal code textbox
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-2'>"));
            label = new Label();
            label.Text = "Postal code";
            eventSessionPanel.Controls.Add(label);
            textBox = defineTextBox();
            textBox.ID = "postal" + count;
            validator = defineRequiredFieldValidator();
            validator.ID = "validatePostal" + count;
            validator.ControlToValidate = textBox.ID;
            RegularExpressionValidator regValidator = defineRegularExpressionValidator();
            regValidator.ID = "validatePostalRegex" + count;
            regValidator.ControlToValidate = textBox.ID;
            regValidator.ValidationExpression = "^[0-9]{6}";
            eventSessionPanel.Controls.Add(validator);
            eventSessionPanel.Controls.Add(regValidator);
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            eventSessionPanel.Controls.Add(textBox);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
        }

        /*                  
         * third row is start and end date
         */
        protected void setUpDateRow(int count)
        {
            //rate of occurrence drop down list
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
            Label label = new Label();
            label.Text = "Occurence ";
            eventSessionPanel.Controls.Add(label);
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            DropDownList dropDownList = defineDropDownList();
            dropDownList.ID = "occurrence" + count;
            dropDownList.Items.Add(new ListItem("Daily"));
            dropDownList.Items.Add(new ListItem("Weekly"));
            eventSessionPanel.Controls.Add(dropDownList);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

        }

        // start time
        protected void setUpStartTime(int count)
        {
            // eventDate text box
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            Label label = new Label();
            label.Text = "Period ";
            eventSessionPanel.Controls.Add(label);
            TextBox textBox = defineDate();
            textBox.ID = "sessionDate" + count;
            RequiredFieldValidator validator = defineRequiredFieldValidator();
            validator.ID = "validateDate" + count;
            validator.ControlToValidate = textBox.ID;
            eventSessionPanel.Controls.Add(validator);
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            eventSessionPanel.Controls.Add(textBox);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-2'>"));
            label = new Label();
            label.Text = "Start time ";
            eventSessionPanel.Controls.Add(label);
            textBox = defineTime();
            textBox.ID = "startTime" + count;
            RequiredFieldValidator validatorST = defineRequiredFieldValidator();
            validatorST.ID = "validateST" + count;
            validatorST.ControlToValidate = textBox.ID;
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            eventSessionPanel.Controls.Add(validatorST);
            eventSessionPanel.Controls.Add(textBox);

            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));


        }

        // end time
        protected void setUpEndTime(int count)
        {
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-2'>"));
            Label label = new Label();
            label.Text = "End time ";
            eventSessionPanel.Controls.Add(label);
            TextBox textBox = defineTime();
            textBox.ID = "endTime" + count;
            RequiredFieldValidator validateET = defineRequiredFieldValidator();
            validateET.ID = "validateET" + count;
            validateET.ControlToValidate = textBox.ID;
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            eventSessionPanel.Controls.Add(validateET);
            eventSessionPanel.Controls.Add(textBox);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
        }



        /*
         * number of compulsory days for needed to join 
         */
        protected void setUpComplusoryDayRow(int count)
        {
            // compulsory days textbox
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            Label label = new Label();
            label.Text = "Compulsory days to attend ";
            eventSessionPanel.Controls.Add(label);
            TextBox textBox = defineTextBox();
            textBox.ID = "compulsory" + count;
            RequiredFieldValidator validator = defineRequiredFieldValidator();
            validator.ID = "validateCompulsory" + count;
            validator.ControlToValidate = "compulsory" + count;
            eventSessionPanel.Controls.Add(validator);
            //regex validator
            RegularExpressionValidator regValidator = defineRegularExpressionValidator();
            regValidator.ID = "validateReg" + count;
            regValidator.ControlToValidate = "compulsory" + count;
            regValidator.ValidationExpression = "[1-9]*";
            eventSessionPanel.Controls.Add(regValidator);
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            eventSessionPanel.Controls.Add(textBox);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
        }

        /*
         * Tdescription
         */
        protected void setUpDescriptionRow(int count)
        {
            // description text field
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'col-md-12'>"));
            Label label = new Label();
            label.Text = "Description ";
            eventSessionPanel.Controls.Add(label);
            eventSessionPanel.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));
            TextBox textBox = defineTextArea();
            textBox.ID = "sessionDesc" + count;
            eventSessionPanel.Controls.Add(textBox);
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));
            eventSessionPanel.Controls.Add(new LiteralControl("</div>"));

        }

        /*
         * This method sets up a placeholder for session roles to be added in 
         */
        protected PlaceHolder setUpSessionRolesPlaceHolder(int count)
        {
            // placeholders for event roles
            PlaceHolder sessionRole = new PlaceHolder();
            sessionRole.ID = "sessionRoles" + count;
            eventSessionPanel.Controls.Add(sessionRole);

            sessionRole.Controls.Add(new LiteralControl("<div class = 'row'>"));

            sessionRole.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
            Label label = new Label();
            label.Text = "Role";
            sessionRole.Controls.Add(label);
            sessionRole.Controls.Add(new LiteralControl("</div>"));


            sessionRole.Controls.Add(new LiteralControl("<div class = 'col-md-1'>"));
            label = new Label();
            label.Text = "Quantity";
            sessionRole.Controls.Add(label);
            sessionRole.Controls.Add(new LiteralControl("</div>"));


            sessionRole.Controls.Add(new LiteralControl("<div class = 'col-md-7'>"));
            label = new Label();
            label.Text = "Description";
            sessionRole.Controls.Add(label);
            sessionRole.Controls.Add(new LiteralControl("</div>"));



            sessionRole.Controls.Add(new LiteralControl("</div>"));

            addSessionRoleFrmViewState(sessionRole, count);

            return sessionRole;


        }

        /*
         *This method adds back the textbox back from the view state 
         */
        protected void addSessionRoleFrmViewState(PlaceHolder sessionRoles, int sessionNum)
        {
            Session["sessionRole" + sessionNum] = 0;

            foreach (string sessionRoleTextRow in controlList)
            {
                if (sessionRoleTextRow.Equals("sessionRoles" + sessionNum))
                {

                    createRoleTxtBox(sessionRoles, sessionNum);
                }
            }
        }

        /*
         *  This method adds session roles to the particular session in wich it is tagged to 
         */
        protected void addSessionRole(Object sender, EventArgs e)
        {
            Button button = sender as Button;
            int sessionNum = Int32.Parse(button.ID);
            PlaceHolder sessionRoles = eventSessionPanel.FindControl("sessionRoles" + sessionNum) as PlaceHolder;

            //System.Diagnostics.Debug.WriteLine("sessionRoles" + sessionNum);
            createRoleTxtBox(sessionRoles, sessionNum);
            controlList.Add("sessionRoles" + sessionNum);
        }


        /*
         * This methods dymanically adds the roles field to the session role placeholer 
         **/
        protected void createRoleTxtBox(PlaceHolder sessionRoles, int sessionNum)
        {

            if (Session["sessionRole" + sessionNum] == null)
            {
                System.Diagnostics.Debug.WriteLine("should only appear once");
                Session["sessionRole" + sessionNum] = 1;
            }
            else
            {
                Session["sessionRole" + sessionNum] = ((int)Session["sessionRole" + sessionNum]) + 1;
            }


            int numOfroles = sessionRoles.Controls.Count;
            sessionRoles.Controls.Add(new LiteralControl("<div class = 'row'>"));


            // role name
            sessionRoles.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
            sessionRoles.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));

            DropDownList roleText = defineRoleDropDownList();
            roleText.ID = "Role" + sessionNum + Session["sessionRole" + sessionNum];
            sessionRoles.Controls.Add(roleText);


            sessionRoles.Controls.Add(new LiteralControl("</div>"));
            sessionRoles.Controls.Add(new LiteralControl("</div>"));


            // role quantity
            sessionRoles.Controls.Add(new LiteralControl("<div class = 'col-md-1'>"));
            sessionRoles.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));

            TextBox roleQuantity = defineTextBox();
            roleQuantity.ID = "Quantity" + sessionNum + Session["sessionRole" + sessionNum];
            RequiredFieldValidator validator = defineRequiredFieldValidator();
            validator.ID = "validateRoleReq" + sessionNum + Session["sessionRole" + sessionNum];
            validator.ControlToValidate = roleQuantity.ID;
            sessionRoles.Controls.Add(validator);
            //regex validator
            RegularExpressionValidator regValidator = defineRegularExpressionValidator();
            regValidator.ID = "validateRole" + sessionNum + Session["sessionRole" + sessionNum];
            regValidator.ControlToValidate = roleQuantity.ID;
            regValidator.ValidationExpression = "^[1-9]+[0-9]*$";
            sessionRoles.Controls.Add(regValidator);
            sessionRoles.Controls.Add(roleQuantity);


            sessionRoles.Controls.Add(new LiteralControl("</div>"));
            sessionRoles.Controls.Add(new LiteralControl("</div>"));


            // role desc
            sessionRoles.Controls.Add(new LiteralControl("<div class = 'col-md-7'>"));
            sessionRoles.Controls.Add(new LiteralControl("<div class = 'form-group required'>"));

            TextBox roleDesc = defineTextBox();
            roleDesc.ID = "Desc" + sessionNum + Session["sessionRole" + sessionNum];
            sessionRoles.Controls.Add(roleDesc);


            sessionRoles.Controls.Add(new LiteralControl("</div>"));
            sessionRoles.Controls.Add(new LiteralControl("</div>"));



            sessionRoles.Controls.Add(new LiteralControl("</div>"));
        }


        /*
         *Define the css structre for a textbox object 
         *
         */
        private TextBox defineTextBox()
        {
            TextBox textBox = new TextBox();
            textBox.CssClass = "form-control";

            return textBox;
        }

        private TextBox defineDate()
        {
            TextBox textBox = new TextBox();
            textBox.CssClass = "form-control daterangepicker-report active";
            //textBox.ReadOnly = true;
            return textBox;
        }

        private TextBox defineTime()
        {

            TextBox textBox = new TextBox();
            textBox.CssClass = "form-control";
            textBox.Attributes.Add("Type", "time");
            return textBox;
        }


        private AutoCompleteExtender defineAutoCompleteExtender(String id)
        {
            AutoCompleteExtender ace = new AutoCompleteExtender();

            ace.ServicePath = "~/SaaS/CreateEventSession.aspx";
            ace.ServiceMethod = "GetCompletionList";
            ace.MinimumPrefixLength = 2;
            ace.CompletionInterval = 100;
            ace.TargetControlID = id;
            return ace;
        }

        private RequiredFieldValidator defineRequiredFieldValidator()
        {
            RequiredFieldValidator validator = new RequiredFieldValidator();
            validator.ValidationGroup = "eventSession";
            validator.Display = ValidatorDisplay.Dynamic;
            validator.ErrorMessage = "Please fill up";
            validator.ForeColor = Color.Red;
            return validator;
        }

        private RegularExpressionValidator defineRegularExpressionValidator()
        {
            RegularExpressionValidator validator = new RegularExpressionValidator();
            validator.ValidationGroup = "eventSession";
            validator.Display = ValidatorDisplay.Dynamic;
            validator.ErrorMessage = "Wrong format";
            validator.ForeColor = Color.Red;
            return validator;
        }

        private DropDownList defineDropDownList()
        {
            DropDownList dropDown = new DropDownList();
            dropDown.CssClass = "selectpicker";
            dropDown.ForeColor = Color.Black;
            return dropDown;
        }

        private DropDownList defineRoleDropDownList()
        {
            DropDownList dropDown = new DropDownList();
            dropDown.CssClass = "selectpicker";
            dropDown.ForeColor = Color.Black;

            String reply = Session["listOfEventRoles"] as string;
            var listOfEventRoles = JsonConvert.DeserializeObject<DataTable>(reply);
            System.Diagnostics.Debug.WriteLine(reply);

            dropDown.DataSource = listOfEventRoles;
            dropDown.DataTextField = "roleName";
            dropDown.DataValueField = "concatenateRoleId";
            dropDown.DataBind();
            return dropDown;
        }


        private Button defineButton()
        {
            Button button = new Button();
            button.CssClass = "btn btn-success btn-xs";
            button.Text = "Add Role";
            return button;
        }

        private TextBox defineTextArea()
        {
            TextBox textBox = new TextBox();
            textBox.CssClass = "form-control";
            textBox.Columns = 50;
            textBox.TextMode = TextBoxMode.MultiLine;
            textBox.Rows = 4;

            return textBox;
        }

        protected void createSession_Click(object sender, EventArgs e)
        {
            EventDetails eventDetails = (EventDetails)Session["eventDetails"];
            int numOfSession = eventDetails.numOfSessions;
            eventDetails.listOfSession = new List<SessionDetails>();

            for (int count = 1; count <= numOfSession; count++)
            {

                SessionDetails sessionDetails = new SessionDetails();

                TextBox sessionName = eventSessionPanel.FindControl("sessionName" + count) as TextBox;
                sessionDetails.sessionName = sessionName.Text;

                TextBox sessionDesc = eventSessionPanel.FindControl("sessionDesc" + count) as TextBox;
                sessionDetails.sessionDescription = sessionDesc.Text;

                TextBox location = eventSessionPanel.FindControl("location" + count) as TextBox;
                sessionDetails.location = location.Text;

                TextBox postal = eventSessionPanel.FindControl("postal" + count) as TextBox;
                sessionDetails.location += " " + postal.Text;
                sessionDetails.postal = postal.Text;

                TextBox compulsoryDays = eventSessionPanel.FindControl("compulsory" + count) as TextBox;
                sessionDetails.compulsoryDays = Int32.Parse(compulsoryDays.Text);

                TextBox startTime = eventSessionPanel.FindControl("startTime" + count) as TextBox;
                sessionDetails.startTime = DateTime.Parse(startTime.Text);

                TextBox endTime = eventSessionPanel.FindControl("endTime" + count) as TextBox;
                sessionDetails.endTime = DateTime.Parse(endTime.Text);


                PlaceHolder sessionRoles = eventSessionPanel.FindControl("sessionRoles" + count) as PlaceHolder;
                retrieveSessionRoleDetails(sessionRoles, sessionDetails, count);

                createSessionDays(count, sessionDetails, eventDetails);
                eventDetails.listOfSession.Add(sessionDetails);
            }
            redirectCreateEventLogs(eventDetails);
        }

        /*
         *  This method helps to generate a number of event session, depending on the number of days calculated
         */
        protected void createSessionDays(int count, SessionDetails sessionDetails, EventDetails eventDetails)
        {
            TextBox sessionDate = eventSessionPanel.FindControl("sessionDate" + count) as TextBox;
            DropDownList eventSessionOccurence = eventSessionPanel.FindControl("occurrence" + count) as DropDownList;

            String[] dateSegement = sessionDate.Text.Split('-');
            // specify the number of days a sesssion have 
            DateTime startDate = DateTime.ParseExact(dateSegement[0].Substring(0, dateSegement[0].Length - 1), "MM/dd/yyyy", null);
            DateTime endDate = DateTime.ParseExact(dateSegement[1].Substring(1, dateSegement[1].Length - 1), "MM/dd/yyyy", null);


            TimeSpan timeSpan = endDate - startDate;
            int numOfDays = timeSpan.Days;

            if (eventSessionOccurence.SelectedIndex == 0)
                numOfDays++;
            if (eventSessionOccurence.SelectedIndex == 1)
                numOfDays = numOfDays / 7 + 1;



            // check if start date is smaller then current eventDetails startDate 
            if (count == 1)
                eventDetails.startDate = startDate;
            else if (eventDetails.startDate > startDate)
                eventDetails.startDate = startDate;



            sessionDetails.listOfSessionDays = new List<SessionDays>();

            for (int dayCount = 0; dayCount < numOfDays; dayCount++)
            {
                SessionDays sessionDays = new SessionDays();
                sessionDays.evt_Date = startDate;

                if (eventSessionOccurence.SelectedIndex == 0) { }
                startDate = startDate.AddDays(1);
                if (eventSessionOccurence.SelectedIndex == 1)
                    startDate = startDate.AddDays(7);

                sessionDetails.listOfSessionDays.Add(sessionDays);
            }

            // check if end date is greater then event current date
            if (count == 1)
                eventDetails.endDate = sessionDetails.listOfSessionDays[sessionDetails.listOfSessionDays.Count - 1].evt_Date;
            else if (eventDetails.endDate < startDate)
                eventDetails.endDate = sessionDetails.listOfSessionDays[sessionDetails.listOfSessionDays.Count - 1].evt_Date;
        }

        /*
         *  This method iteratiate through a particular sessionRole place holder to retrieve all role details
         * */
        protected void retrieveSessionRoleDetails(PlaceHolder sessionRoles, SessionDetails sessionDetails, int sessionNum)
        {
            int numOfRoles = (int)Session["sessionRole" + sessionNum];
            int numOfVolunteers = 0;
            sessionDetails.listOfSessionRoles = new List<SessionRoles>();
            for (int roleCount = 1; roleCount <= numOfRoles; roleCount++)
            {
                SessionRoles sessionRole = new SessionRoles();
                DropDownList roleName = sessionRoles.FindControl("Role" + sessionNum + roleCount) as DropDownList;

                // split the concatenated values 
                System.Diagnostics.Debug.WriteLine("role selected value: " + roleName.SelectedValue);
                String[] roleDetails = roleName.SelectedValue.Split('-');
                sessionRole.roleId = Int16.Parse(roleDetails[0]);
                sessionRole.personalityTraits = roleDetails[1];
                if (roleDetails.Length == 3)
                    sessionRole.skillSets = roleDetails[2];

                sessionRole.roleName = roleName.SelectedItem.Text;


                TextBox quantity = sessionRoles.FindControl("Quantity" + sessionNum + roleCount) as TextBox;
                sessionRole.quantity = Int16.Parse(quantity.Text);
                numOfVolunteers += sessionRole.quantity;

                TextBox desc = sessionRoles.FindControl("Desc" + sessionNum + roleCount) as TextBox;
                sessionRole.roleDesc = desc.Text;

                sessionDetails.listOfSessionRoles.Add(sessionRole);
            }

            sessionDetails.sessionSize = numOfVolunteers;
        }

        /*
         * this method send a request to the server to create a new evnet 
         */
        protected async void redirectCreateEventLogs(EventDetails eventDetails)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String token = Session["Token"].ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = await client.PostAsJsonAsync("/api/EventMgmt/createEvent", eventDetails);
            String result = await response.Content.ReadAsStringAsync();
            Response.Redirect("ViewEventList", false);
        }

    }
}
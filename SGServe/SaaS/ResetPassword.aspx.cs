﻿using SGServe.CommonClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            await gm.getOrgDetail(Request.RawUrl.ToString(), lbltitle);
            lblmessage.Visible = false;

        }

        protected async void btnLookup_Click(object sender, EventArgs e)
        {
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/"+urlArr[1].ToUpper()+ "/setPassword";
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/reqResetPw?connection_string=" + urlArr[1].ToUpper() + "&url=" + baseUrl + "&email=" + txtEmail.Text.ToString());
            HttpResponseMessage resp = await client.SendAsync(req);
            String response = await resp.Content.ReadAsStringAsync();

            if (Convert.ToBoolean(response) == true)
            {
                lblmessage.Visible = false;
                //show successmsg
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Reset password request success! Please check your email to reset password');window.location ='ologin';", true);
            }
            else
            {

                lblmessage.Visible = true;
                lblmessage.Text = "Invalid email";
            }
        }
    }
    }

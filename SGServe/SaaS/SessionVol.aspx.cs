﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System.Net.Http;
using Newtonsoft.Json;
using System.Data;
using SGServe.Models.EventModels;

namespace SGServe.SaaS
{
    public partial class SessionVol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                displayVolunteerList();
            }
        }


        protected async void displayVolunteerList()
        {
            String token = Session["Token"].ToString();

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveVolunteerList?sessionDayId=" +Request.QueryString["d"] + "&evtRoleId=" + Request.QueryString["r"] );
            HttpResponseMessage response = await client.SendAsync(request);

            String reply = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("reply:" + reply);
            var volTable = JsonConvert.DeserializeObject<DataTable>(reply);
            gv.DataSource = volTable;
            gv.DataBind();

            //display header
            HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/getSessionBySessDay?sessionDayId=" + Request.QueryString["d"]);
            HttpResponseMessage response2 = await client.SendAsync(request2);
            String result2 = await response2.Content.ReadAsStringAsync();
            SessionDetails session = JsonConvert.DeserializeObject<SessionDetails>(result2);
            lblHeader.Text = session.sessionName + " "+session.listOfSessionDays[0].evt_Date.ToString("d MMM yyyy")+" Volunteers";
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = (int)e.NewPageIndex;
            displayVolunteerList();
        }
    }

}
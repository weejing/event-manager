﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using SGServe.Models.CommunityModels;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using SGServe.SaaS.Models;
using SGServe.SaaS.Models.Reporting;
using System.Data;

namespace SGServe.SaaS
{
    public partial class UsersFollowed : System.Web.UI.Page
    {
        OrgEmployee employee = new OrgEmployee();
        OrganizationDetailsModel org = new OrganizationDetailsModel();
        String role = "";
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            employee = await gm.getLoginEmpDetails(urlArr[1].ToUpper());

            if (employee.USR_ID == null)
            {
                Response.Redirect("login", false);
            }
            else
            {
                 //role = employee.ROLEE;
                 generateContent();
            }
        }

        protected async void generateContent()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                url = urlArr[1];
                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUsersFollowedForOrganisation?orgID=" + url + "&userID=" + employee.USR_ID);
                //userID, String connectionString
                HttpResponseMessage resp = await client.SendAsync(req);
                String usersDetails = await resp.Content.ReadAsStringAsync();

                List<UserPortfolioModel> upm = JsonConvert.DeserializeObject<List<UserPortfolioModel>>(usersDetails);

                Label3.Text = upm.Count.ToString();
                FollowersItemGrid.DataSource = upm;
                FollowersItemGrid.DataBind();
                
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

    }
}
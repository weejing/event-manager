﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="CreateEvent.aspx.cs" Inherits="SGServe.SaaS.CreateEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style>
        .chkChoice input {
            margin-left: -20px;
        }

        .chkChoice td {
            padding-left: 20px;
        }
    </style>


    <div class="page-section">
        <div class=" row">
            <div class="col-lg-10 col-lg-offset-1">
                <h4 class="page-section-heading">Event </h4>
                <div class="wizard-container wizard-1" id="wizard-demo-1">
                    <div class=" data-scrollable-h">
                        <ul class="wiz-progress">
                            <li class="active">Create Event</li>
                            <li>Event Session</li>
                           
                        </ul>
                    </div>
                </div>
                <div id="ContentPlaceHolder1_UpdatePanel1">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="text-h2 ribbon-heading ribbon-primary bottom-left-right">Event Details</h3>
                            <label>
                                Event Name
                                       <asp:RequiredFieldValidator runat="server" ValidationGroup="createEvent" Display="Dynamic" ControlToValidate="eventName" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                            </label>
                            <div class=" form-group required">
                                <asp:TextBox ID="eventName" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <label>
                                Image
                            </label>
                            <div class=" form-group required">
                                <asp:Image ID="imgBadge" runat="server" Height="200" Width="300" />
                                <asp:FileUpload ID="imageUpload" runat="server" />
                            </div>
                            <label>
                                Event Description
                                        <asp:RequiredFieldValidator runat="server" placeholder ="Enter your event description" ValidationGroup="createEvent" Display="Dynamic" ControlToValidate="eventName" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                            </label>
                            <div class="form-group required">
                                <asp:TextBox runat="server" ID="eventDesc" class="form-control" TextMode="multiline" Columns="50" Rows="4"></asp:TextBox>
                            </div>

                            <label>
                                Num of sessions
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Must be more than 0" ValidationGroup="createEvent" ControlToValidate="numOfSession" ForeColor="Red" ValidationExpression="[1-9]*" Display="Dynamic"></asp:RegularExpressionValidator>
                            </label>
                            <div class="form-group required" style="width: 20%">
                                <asp:TextBox ID="numOfSession" class="form-control" data-toggle="touch-spin" data-verticalbuttons="true" runat="server"></asp:TextBox>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <label>
                                        Event cause 
                                      <asp:CustomValidator ValidationGroup="createEvent" runat="server" SetFocusOnError="true" Display="Dynamic" ID="cv" ClientValidationFunction="ValidateBeneList" ErrorMessage="Please select" ForeColor="Red"></asp:CustomValidator>
                                    </label>
                                    <div class="form-group form-control-default required">
                                        <asp:CheckBoxList ID="beneChkBox" RepeatLayout="Flow" CssClass="checkbox checkbox-inline" RepeatDirection="Horizontal" runat="server"></asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                            <h3 class="text-h2 ribbon-heading ribbon-primary bottom-left-right">Matching Weightage</h3>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>
                                        Traits (%)
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic" ControlToValidate="traits" ValidationGroup="createEvent" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage="Numbers only" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                    </label>
                                    <asp:TextBox ID="traits" runat="server" class="form-control" Style="width: 40%"></asp:TextBox>
                                    <div class="maxTraits">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label>
                                        Event Roles Interest (%)
                                                                  <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator3" ControlToValidate="role" ValidationGroup="createEvent" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage="Numbers only" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                    </label>
                                    <asp:TextBox ID="role" runat="server" class="form-control" Style="width: 40%"></asp:TextBox>
                                    <div class="maxRole">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label>
                                        Cause Interest (%)
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="Dynamic" ControlToValidate="cause" ValidationGroup="createEvent" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage="Numbers only" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                    </label>
                                    <asp:TextBox ID="cause" runat="server" class="form-control" Style="width: 40%"></asp:TextBox>
                                    <div class="maxCause">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label>
                                        Skills Needed (%)
                                                                                <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator5" ControlToValidate="skill" ValidationGroup="createEvent" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage="Numbers only" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                    </label>
                                    <asp:TextBox ID="skill" runat="server" class="form-control" Style="width: 40%"></asp:TextBox>
                                    <div class="maxSkill">
                                    </div>
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <div class="col-md-3">
                                    <label>
                                       Passing Weightage (%)                                         
                                    </label>
                                    <br />
                                    <asp:Label ID="Label1" runat="server" Text="Total weightage is 100%" Font-Italic="True" Font-Size="Smaller"></asp:Label>
                                </div>
                                <div class="col-md-3">
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="Dynamic" ControlToValidate="totalWeightage" ValidationGroup="createEvent" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage="Numbers only" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="totalWeightage" runat="server" class="form-control" Style="width: 40%"></asp:TextBox>
                                    <div class="maxWeight">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnCreateEvent" class="btn btn-primary" ValidationGroup="createEvent" runat="server" Text="create" OnClick="createEvent_Click" />
            </div>
        </div>
    </div>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            var value = 100;

            $('.maxTraits').hide();
            $('.maxRole').hide();
            $('.maxCause').hide();
            $('.maxSkill').hide();
            $('.maxWeight').hide();


                document.getElementById("<%= traits.ClientID %>").value = 25;
                document.getElementById("<%= role.ClientID %>").value = 25;
                document.getElementById("<%= cause.ClientID %>").value = 25;
                document.getElementById("<%= skill.ClientID %>").value = 25;
                document.getElementById("<%=totalWeightage.ClientID%>").value = 60;



                $('#<%=btnCreateEvent.ClientID%>').click(function (e) {
                    var traitsValue = parseInt($('#<%= traits.ClientID %>').val());
                    var roleValue = parseInt($('#<%= role.ClientID %>').val());
                    var causevalue = parseInt($('#<%= cause.ClientID %>').val());
                    var skillValue = parseInt($('#<%= skill.ClientID %>').val());

                    var totalWeight = parseInt($('#<%= totalWeightage.ClientID %>').val());


                    if ((traitsValue > 0 || roleValue > 0 || causevalue > 0 || skillValue > 0) && totalWeight <= 0) {
                        alert("You have specified some event matching citeria, but your passing weight is only 0");
                        event.preventDefault();
                    }

                    if ((traitsValue + roleValue + causevalue + skillValue) > 100) {
                        console.log((traitsValue + roleValue + causevalue + skillValue) + "");
                        alert("Total weightage value for traits, skills, roles and cause interests cannot be greater than 100");
                        event.preventDefault();

                    }
                });


                // traits textbox
                $('#<%= traits.ClientID %>').on('keydown keyup', function (e) {
                    if ($(this).val() > 100 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(100);
                        $('.maxTraits').show();
                        $('.maxTraits').append("<p class = 'text' style='color:red'> max value is 100 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else if ($(this).val() < 0 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(0);

                        $('.maxTraits').show();
                        $('.maxTraits').append("<p class = 'text' style='color:red'> max value is 0 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else {
                        $('.maxTraits').hide();
                        $('.maxTraits').find('.text').remove();
                        $(this).removeClass("maxQuantity");
                    }
                });



                $('#<%= role.ClientID %>').on('keydown keyup', function (e) {
                    if ($(this).val() > 100 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(100);
                        $('.maxRole').show();
                        $('.maxRole').append("<p class = 'text' style='color:red'> max value is 100 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else if ($(this).val() < 0 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(0);

                        $('.maxRole').show();
                        $('.maxRole').append("<p class = 'text' style='color:red'> max value is 0 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else {
                        $('.maxRole').hide();
                        $('.maxRole').find('.text').remove();
                        $(this).removeClass("maxQuantity");
                    }
                });





                $('#<%= skill.ClientID %>').on('keydown keyup', function (e) {
                    if ($(this).val() > 100 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(100);
                        $('.maxSkill').show();
                        $('.maxSkill').append("<p class = 'text' style='color:red'> max value is 100 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else if ($(this).val() < 0 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(0);

                        $('.maxSkill').show();
                        $('.maxSkill').append("<p class = 'text' style='color:red'> max value is 0 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else {
                        $('.maxSkill').hide();
                        $('.maxSkill').find('.text').remove();
                        $(this).removeClass("maxQuantity");
                    }
                });



                $('#<%= cause.ClientID %>').on('keydown keyup', function (e) {
                    if ($(this).val() > 100 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(value);
                        $('.maxCause').show();
                        $('.maxCause').append("<p class = 'text' style='color:red'> max value is 100 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else if ($(this).val() < 0 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(0);

                        $('.maxCause').show();
                        $('.maxCause').append("<p class = 'text' style='color:red'> max value is 0 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else {
                        $('.maxCause').hide();
                        $('.maxCause').find('.text').remove();
                        $(this).removeClass("maxQuantity");
                    }
                });



                $('#<%= totalWeightage.ClientID %>').on('keydown keyup', function (e) {
                    if ($(this).val() > 100 && e.keyCode != 46 && e.keyCode != 8) {
                        e.preventDefault();
                        $(this).val(100);
                        $('.maxWeight').show();
                        $('.maxWeight').append("<p class = 'text' style='color:red'> max value is 100 </p>");
                        $(this).addClass('maxQuantity').focus();
                    }
                    else {
                        $('.maxWeight').hide();
                        $('.maxWeight').find('.text').remove();
                        $(this).removeClass("maxQuantity");
                    }

                });

            });

            function ValidateBeneList(source, args) {
                var chkListModules = document.getElementById('<%= beneChkBox.ClientID %>');

                var chkListinputs = chkListModules.getElementsByTagName("input");
                for (var i = 0; i < chkListinputs.length; i++) {
                    if (chkListinputs[i].checked) {
                        args.IsValid = true;
                        return;
                    }
                }
                args.IsValid = false;
            }

    </script>
</asp:Content>

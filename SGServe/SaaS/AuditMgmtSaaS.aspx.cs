﻿using SGServe.CommonClass;
using SGServe.Models.AuditModels;
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class AuditMgmt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                viewAll();
                loadData();
            }
        }

        public async void viewAll()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Audit/getAuditSaaSList");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1].ToUpper()));

                request.Content = new FormUrlEncodedContent(keyValues);
                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                //System.Diagnostics.Debug.WriteLine("reply: " + reply);

                JsonArray array = (JsonArray)JsonValue.Parse(reply);

                List<AuditTrail> items = new List<AuditTrail>();

                foreach (var arrayResult in array)
                {
                    AuditTrail newItem = new AuditTrail();
                    newItem.AUDIT_TRAIL_ID = arrayResult["AUDIT_TRAIL_ID"].ToString();
                    newItem.MODULE_NAME = arrayResult["MODULE_NAME"].ToString().Substring(1, arrayResult["MODULE_NAME"].ToString().Length - 2);
                    newItem.MODULE_FEATURE = arrayResult["MODULE_FEATURE"].ToString().Substring(1, arrayResult["MODULE_FEATURE"].ToString().Length - 2);
                    newItem.ACTION = arrayResult["ACTION"].ToString().Substring(1, arrayResult["ACTION"].ToString().Length - 2);
                    newItem.DESCRIPTION = arrayResult["DESCRIPTION"].ToString().Substring(1, arrayResult["DESCRIPTION"].ToString().Length - 2);
                    newItem.TIMESTAMP = (DateTime)arrayResult["TIMESTAMP"];
                    newItem.SYSTEM_USR = arrayResult["SYSTEM_USR"].ToString().Substring(1, arrayResult["SYSTEM_USR"].ToString().Length - 2);
                    newItem.USR_ID = arrayResult["USR_ID"].ToString().Substring(1, arrayResult["USR_ID"].ToString().Length - 2);
                    newItem.NAME = arrayResult["NAME"].ToString().Substring(1, arrayResult["NAME"].ToString().Length - 2);
                    items.Add(newItem);
                }

                AuditGrid.DataSource = items;
                AuditGrid.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

        }
        protected void lnkView_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            String final = AuditGrid.DataKeys[row.RowIndex].Value.ToString();
            
            Session["CurrAuditUsrID"] = final;
            Response.Redirect("AuditMgmtSaaSUsr", false);

            Context.ApplicationInstance.CompleteRequest();
        }

        protected void AuditItemGrid_DataBound(object sender, EventArgs e)
        {
            //TenderItemGrid.Columns[0].Visible = false;
        }
        public async void loadData()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1]));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                string orgName = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
                //txtOrgShort.Text = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
                //txtEmail.Text = array["EMAIL"].ToString().Substring(1, array["EMAIL"].ToString().Length - 2);
                //txtLink.Text = array["LINK"].ToString().Substring(1, array["LINK"].ToString().Length - 2);
                ///txtUEN.Text = array["UEN"].ToString().Substring(1, array["UEN"].ToString().Length - 2);
                //txtAddress.Text = array["ADDRESS"].ToString().Substring(1, array["ADDRESS"].ToString().Length - 2);
                //LabelDescription.Text = array["DESCRIPTION"].ToString().Substring(1, array["DESCRIPTION"].ToString().Length - 2);
                //ddType.Text = array["ORG_TYPE"].ToString().Substring(1, array["ORG_TYPE"].ToString().Length - 2);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected void AuditGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AuditGrid.PageIndex = (int)e.NewPageIndex;
            viewAll();
        }
    }
}
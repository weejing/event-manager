﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="EventRoles.aspx.cs" Inherits="SGServe.SaaS.EventRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="page-section">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lnkHelpText" runat="server"></asp:LinkButton>
                <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
                <div class="row">
                    <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                        <h4 class="page-section-heading">Event Roles 
                            <asp:Button ID="Button1" class="btn btn-circle btn-primary btn-sm" runat="server" Text="?" Style="float: right" OnClick="Button1_Click" /></h4>
                        <br>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <asp:Label ID="Label1" Style="float: right; font-style: italic" runat="server" Text="* X in MBTI means both value. Submit new event roles to sgserve8mc@gmail.com"></asp:Label>
                                    <asp:GridView CellPadding="50" GridLines="None" Height="24px" CssClass="table"
                                        AllowPaging="True" EmptyDataText="There is no record!" ID="gv" DataKeyNames="ROLE_ID" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gv_PageIndexChanging" PageSize="20" OnRowEditing="gv_RowEditing">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblroleName" runat="server" Text='<%#Eval("ROLE_NAME") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MBTI">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTraits" runat="server" Text='<%#Eval("TRAITS") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Skills">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSkills" runat="server" Text='<%#Eval("CONCAT_SKILL_NAME") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEdit" CommandName="edit" runat="server" class="btn btn-info btn-xs">Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle ForeColor="#0066CC" />
                                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <ajaxToolkit:ModalPopupExtender ID="mp" BackgroundCssClass="modalBackground" runat="server" PopupControlID="PopupPanel" TargetControlID="lnkDummy" CancelControlID="btnClose"></ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="PopupPanel" runat="server" CssClass="modal-lg" Style="display: none;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <asp:Button ID="btnClose" class="btn btn-stroke btn-circle btn-default btn-sm" runat="server" Text="X" Style="float: right" /><h4 class="modal-title">
                                <asp:Label ID="lblMdTitle" runat="server" Text=""></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:HiddenField ID="hiddenRoleID" runat="server" />
                            <div class="row">
                                <div class="col-md-2">
                                    Name:
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    MBTI:
                                </div>
                                <div class="col-md-2">
                                    <asp:RadioButtonList ID="rbl1" runat="server" class="radio radio-primary">
                                        <asp:ListItem Value="X" Selected="True">Both</asp:ListItem>
                                        <asp:ListItem Value="E">E</asp:ListItem>
                                        <asp:ListItem Value="I">I</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-md-2">
                                    <asp:RadioButtonList ID="rbl2" runat="server" class="radio radio-primary">
                                        <asp:ListItem Value="X" Selected="True">Both</asp:ListItem>
                                        <asp:ListItem Value="N">N</asp:ListItem>
                                        <asp:ListItem Value="S">S</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-md-2">
                                    <asp:RadioButtonList ID="rbl3" runat="server" class="radio radio-primary">
                                        <asp:ListItem Value="X" Selected="True">Both</asp:ListItem>
                                        <asp:ListItem Value="F">F</asp:ListItem>
                                        <asp:ListItem Value="T">T</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-md-2">
                                    <asp:RadioButtonList ID="rbl4" runat="server" class="radio radio-primary">
                                        <asp:ListItem Value="X" Selected="True">Both</asp:ListItem>
                                        <asp:ListItem Value="P">P</asp:ListItem>
                                        <asp:ListItem Value="J">J</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Skills:
                                </div>
                                <div class="col-md-8">
                                    <asp:CheckBoxList ID="cbSkill" runat="server" SetFocusOnError="true" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave" class="btn btn-success btn-sm" ValidationGroup="valiGroup" runat="server" Text="Save" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </asp:Panel>

                <ajaxToolkit:ModalPopupExtender ID="mpHelpText" BackgroundCssClass="modalBackground" runat="server" PopupControlID="HelpTextPanel" TargetControlID="lnkHelpText" CancelControlID="btnHelpClose"></ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="HelpTextPanel" runat="server" CssClass="modal-lg" Style="display: none;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <asp:Button ID="btnHelpClose" class="btn btn-stroke btn-circle btn-default btn-sm" runat="server" Text="X" Style="float: right" /><h4 class="modal-title">
                                <asp:Label ID="Label2" runat="server" Text="Event Roles"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                                    Tag traits and skills needed for an event role to facilitate event roles to volunteer matching. Traits are based on Myers–Briggs Type Indicator (MBTI). MBTI consists of 4 characters as the following:</br></br>
                                    1. <b>[I]ntroversion:</b> Individuals prefer solitary activities / <b>[E]xtraversion:</b> Prefer group activities and get energized by social interaction<br />
                                    2. <b>[I]ntuition:</b> Imaginative, open-minded and curious / <b>[S]ensing:</b> Highly practical, pragmatic and down-to-earth<br />
                                    3. <b>[T]hinking:</b> Focus on objectivity and rationality, prioritizing logic over emotions. / <b>[F]eeling:</b> Sensitive and emotionally expressive<br />
                                    4. <b>[J]udging:</b> Decisive, thorough and highly organized / <b>[P]erceiving:</b> Improvising and spotting opportunities<br />
                                   <br/> Click <a href="https://www.16personalities.com/personality-types" target="_blank">here</a> to find out more
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using Newtonsoft.Json;
using System.Data;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace SGServe.SaaS
{
    public partial class CreateEvent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                 System.Diagnostics.Debug.WriteLine("Session: " + Session["Token"]);
                 getEventCause();
                 numOfSession.Text="1";
            }          
        }

       
        /*
         * Generate a checkbox list of the existing beneficiary 
         */
        private async void getEventCause()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermList?vocab_id=" + "2");
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();

            var listOfBeneficiary = JsonConvert.DeserializeObject<DataTable>(reply);

            beneChkBox.DataSource = listOfBeneficiary;
            beneChkBox.DataTextField = "TERM_NAME";
            beneChkBox.DataValueField = "TERM_ID";
            beneChkBox.DataBind();

            System.Diagnostics.Debug.WriteLine("cause: " + listOfBeneficiary);
        }




        protected void createEvent_Click(object sender, EventArgs e)
        {
            EventDetails eventDetails = new EventDetails();
            eventDetails.eventName = eventName.Text;
            eventDetails.description = eventDesc.Text;
          
            eventDetails.numOfSessions = Int32.Parse(numOfSession.Text);
           
            string guidResult = System.Guid.NewGuid().ToString("N");
            String fileName = Path.GetFileName(imageUpload.PostedFile.FileName).ToLower();
            string fileExtention = imageUpload.PostedFile.ContentType.ToLower();
            if (fileExtention == "image/png" || fileExtention == "image/jpeg" || fileExtention == "image/jpg")
            {
                string[] exts = fileName.Split('.');
                string finexts = guidResult + "." + exts[1];
                String path = Server.MapPath("../images/event/" + finexts);

                System.Drawing.Bitmap bmpPostedImage = new System.Drawing.Bitmap(imageUpload.PostedFile.InputStream);
                System.Drawing.Image objImage = ScaleImage(bmpPostedImage);
                objImage.Save(path, ImageFormat.Jpeg);
                
                //imageUpload.SaveAs(path);
                //resizeImage(path);
                System.Diagnostics.Debug.WriteLine("path: " + path);

                eventDetails.imagePath = "images/event/" + finexts;


                eventDetails.listOfWeights = new List<int>();

                eventDetails.listOfWeights.Add(Int16.Parse(traits.Text));
                eventDetails.listOfWeights.Add(Int16.Parse(cause.Text));
                eventDetails.listOfWeights.Add(Int16.Parse(role.Text));
                eventDetails.listOfWeights.Add(Int16.Parse(skill.Text));

               

                eventDetails.passingWeight = Int16.Parse(totalWeightage.Text);
                eventDetails.listOfEventCause = new List<int>();
                eventDetails.listOfEventCauseName = new List<String>();

                foreach (ListItem item in beneChkBox.Items)
                {
                    if (item.Selected == true)
                    {
                        eventDetails.listOfEventCause.Add(Int16.Parse(item.Value));
                        eventDetails.listOfEventCauseName.Add(item.Text);
                    }
                }
                Session["eventDetails"] = eventDetails;
                getListOfEventRoles();
                Response.Redirect("CreateEventSession", false);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Please upload an appropriate image file!" + "');", true);
            }
        }

        public static System.Drawing.Image ScaleImage(System.Drawing.Image image)
        { 
            var newImage = new Bitmap(370, 190);
            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(image, 0, 0, 370, 190);
            }
            return newImage;
        }
        
        protected async void getListOfEventRoles()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');

            String token = Session["Token"] + "";

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/getListOfEventRoles");
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            Session["listOfEventRoles"] = reply;
            Session["connectionString"] = urlArr[1];

        }


    }
}
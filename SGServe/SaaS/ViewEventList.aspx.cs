﻿using SGServe.CommonClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.Models.EventModels;
using Newtonsoft.Json;


namespace SGServe.SaaS
{
    public partial class ViewEventList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getEventList();
        }


        protected async void getEventList()
        {
            String token = Session["Token"].ToString();
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveListOfEvents");
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            List<EventDetails> listOfEventDetails = JsonConvert.DeserializeObject<List<EventDetails>>(result);
            createEventList(listOfEventDetails);         
        }


        protected void createEventList(List<EventDetails> listofEventDetails)
        {
            foreach (EventDetails eventDetails in listofEventDetails)
            {
                String date = eventDetails.startDate.ToString("d MMM yyyy") + " - " + eventDetails.endDate.ToString("d MMM yyyy");

                eventListPanel.Controls.Add(new LiteralControl("<div class ='item' data-width='50'>"));
                eventListPanel.Controls.Add(new LiteralControl("<div class ='panel panel-default relative'>"));

                // event image
                eventListPanel.Controls.Add(new LiteralControl("<div class='cover hover overlay margin-none'>"));
                eventListPanel.Controls.Add(new LiteralControl("<div class='media'>"));
                //eventListPanel.Controls.Add(new LiteralControl("<img src='/main_css/images/photodune-378874-real-estate-xs.jpg' alt='location' class='img-responsive' />"));

                eventListPanel.Controls.Add(new LiteralControl("<img src='../" + eventDetails.imagePath + "' alt='location' class='img-responsive' />"));
                eventListPanel.Controls.Add(new LiteralControl("<a href='ViewEventDetails?e=" + eventDetails.eventId + "' class='overlay overlay-full overlay-bg-black overlay-hover'>"));
                eventListPanel.Controls.Add(new LiteralControl("<span class='v-center'><span class='btn btn-circle btn-white'><i class='fa fa-eye'></i></span></span></a>"));
                eventListPanel.Controls.Add(new LiteralControl("</div>"));
                eventListPanel.Controls.Add(new LiteralControl("</div>"));
                // event Details
                eventListPanel.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
                eventListPanel.Controls.Add(new LiteralControl("<h4 class='margin-v-0-5' style='overflow: hidden; height: 40px;text-overflow: ellipsis;'>" + eventDetails.eventName + "</h4>"));
                eventListPanel.Controls.Add(new LiteralControl("<span class = 'label label-customize-100' style='text-align:center'>" + date + "</span>"));
                eventListPanel.Controls.Add(new LiteralControl("</div>"));



                eventListPanel.Controls.Add(new LiteralControl("</div>"));
                eventListPanel.Controls.Add(new LiteralControl("</div>"));

            }


        }

    }
}
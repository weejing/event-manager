﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class OrganizationFront : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            generateMenu();
            
            string url = Request.RawUrl.ToString();
           string[] urlArr = url.Split('/');
            //Label1.Text = urlArr[1].ToUpper();

          //  loadData();
        }

        public async void loadData()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://vmsserver.azurewebsites.net/");
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1].ToUpper()));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("reply: " + reply);

                JsonObject array = (JsonObject)JsonValue.Parse(reply);

               // txtOrgName.Text = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
                //txtOrgShort.Text = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
                //txtEmail.Text = array["EMAIL"].ToString().Substring(1, array["EMAIL"].ToString().Length - 2);
                //txtLink.Text = array["LINK"].ToString().Substring(1, array["LINK"].ToString().Length - 2);
                //txtUEN.Text = array["UEN"].ToString().Substring(1, array["UEN"].ToString().Length - 2);
                //txtAddress.Text = array["ADDRESS"].ToString().Substring(1, array["ADDRESS"].ToString().Length - 2);
                //txtDesc.Text = array["DESCRIPTION"].ToString().Substring(1, array["DESCRIPTION"].ToString().Length - 2);
                //ddType.Text = array["ORG_TYPE"].ToString().Substring(1, array["ORG_TYPE"].ToString().Length - 2);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }


        private void generateMenu()
        {
            string leftMenu = "<ul class='nav navbar-nav'>";
            string EndTag = "</ul>";
            string rightMenu = "<ul class='nav navbar-nav navbar-right'>";
            if (Session["Token"] == null)
            {
                //leftMenu = leftMenu + "<li> <a href='EventList'>Events</a> </li>";
                //right menu
                //rightMenu = rightMenu + "<li class='dropdown'><a href='Login'><i class='fa fa-fw fa-lock'></i>Login</a></li>";
            }
            else
            {
                //leftMenu = leftMenu + "<li> <a href='EventList'>Events</a> </li>";
                //right menu
                rightMenu = rightMenu + "<li class='dropdown'><a href ='Logout'><i class='fa fa-fw fa-sign-out'></i>Signout</a></li>";
            }
            leftMenu = leftMenu + EndTag;
            rightMenu = rightMenu + EndTag;
            lblleftMenu.Text = leftMenu;
            lblRightmenu.Text = rightMenu;
        }
    }
}
﻿<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="fdbackResponses.aspx.cs" Inherits="SGServe.SaaS.fdbackResponses" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 
        <div class="page-section">
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <h4 class="page-section-heading"><asp:Label ID="lblHeader" runat="server" Text="Feedback Response"></asp:Label></h4>
                <div class="panel panel-default">
                           <div class="panel-body">                                                              
              <asp:DataList ID="dl" runat="server" RepeatDirection="Vertical" CellSpacing="20" OnItemDataBound="dl_ItemDataBound" CellPadding="10">
                    <ItemTemplate>
                     Q<%# DataBinder.Eval(Container.DataItem, "number") %>&nbsp;<asp:Label ID="lblQues" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "question") %>' Font-Bold="True"></asp:Label><br />
                       <%-- <ajaxToolkit:PieChart ID="PieChart1" ChartType="Column" ChartTitleColor="#0E426C" Height="400px" ChartHeight="400" width="500" ChartWidth = "500" runat="server" ></ajaxToolkit:PieChart>--%>
                        <asp:Chart ID="PieChart1" runat="server" Palette="Pastel">
        <Series>
            <asp:Series Name="Series1" ChartType="Pie"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
        </ChartAreas>
     <Legends>
        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
    </Legends>
    </asp:Chart>
                      <asp:Label ID="lblAns" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "listOfTextAns") %>' ></asp:Label><br/>
                    </ItemTemplate>
                </asp:DataList>
 </div>
            </div>
            </div>
        </div>
 </div>
</asp:Content>

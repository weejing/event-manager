﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class ManageAccount : System.Web.UI.Page
    {
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
         
            if (!IsPostBack)
            {
                generateGV();
            }

        }

        protected async void generateGV()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/getEmployees?connection_string=" + urlArr[1].ToUpper()+"&role_id=" + "ALL");
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var acctable = JsonConvert.DeserializeObject<DataTable>(reply);
            gvAccounts.DataSource = acctable;
            gvAccounts.DataBind();
            //gvAccounts.HeaderRow.TableSection = TableRowSection.TableHeader;

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Session.Remove("emp_id");
            Response.Redirect("CreateAccount",false);
        }

        protected async void gvAccounts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAccounts.PageIndex = (int)e.NewPageIndex;
            generateGV();
        }

        protected async void btnSearch_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/searchEmployees?connection_string=" + urlArr[1].ToUpper() + "&name=" + txtName.Text);
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var acctable = JsonConvert.DeserializeObject<DataTable>(reply);
            gvAccounts.DataSource = acctable;
            gvAccounts.DataBind();
            //gvAccounts.HeaderRow.TableSection = TableRowSection.TableHeader;
        }



        //protected async void lbtnDelete_Click(object sender, EventArgs e)
        //{
        //    LinkButton btn = (LinkButton)sender;
        //    GridViewRow row = (GridViewRow)btn.NamingContainer;

        //    Server_Connect serverConnection3 = new Server_Connect();
        //    HttpClient client3 = serverConnection3.getHttpClient();
        //    HttpRequestMessage request3 = new HttpRequestMessage(HttpMethod.Get, "/api/OrgAccount/deleteAcc?USR_ID=" + gvAccounts.DataKeys[row.RowIndex].Value.ToString());
        //    HttpResponseMessage response3 = await client3.SendAsync(request3);
        //    String reply3 = await response3.Content.ReadAsStringAsync();
        //    Response.Redirect("CreateAccount.aspx");

        //}

        protected void lbtnView_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["emp_id"] = gvAccounts.DataKeys[row.RowIndex].Value.ToString();
            Response.Redirect("CreateAccount",false);

        }


    }
}
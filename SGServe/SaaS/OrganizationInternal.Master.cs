﻿using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using SGServe.CommonClass;
using System.Net.Http;

namespace SGServe.SaaS
{
    public partial class OrganizationInternal : System.Web.UI.MasterPage
    {
        GlobalModule gm = new GlobalModule();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Orgheader"] != null) {
                lbltitle.Text = Session["Orgheader"].ToString();
            }
            if (Session["Token"] != null)
            {
                gm.displayEmpMenu(lblLeftMenu, lbltopMenu);
            }
            else
            {
                Response.Redirect("ologin", false);
            }         
        }



    }
}
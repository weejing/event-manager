﻿using AjaxControlToolkit;
using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class fdbackResponses : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                displayData();
            }
        }

        protected async void displayData()
        {
            //display title
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String token = Session["Token"] + "";
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/getSessionInfo?sessionId=" + Request.QueryString["s"]);
            HttpResponseMessage response = await client.SendAsync(request);
            String result = await response.Content.ReadAsStringAsync();
            SessionDetails session = JsonConvert.DeserializeObject<SessionDetails>(result);
            lblHeader.Text = session.sessionName+" Responses";

            //display response
            HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/getFdbackOverallResp?sessionId=" + Request.QueryString["s"]);
            HttpResponseMessage response2 = await client.SendAsync(request2);
            String result2 = await response2.Content.ReadAsStringAsync();
            var table = JsonConvert.DeserializeObject<List<FeedBackDesign>>(result2);
            dl.DataSource = table;
            dl.DataBind();
        }

        protected void dl_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            //DataRowView row = (DataRowView)e.Item.DataItem;
            FeedBackDesign fb= (FeedBackDesign)e.Item.DataItem;
            Label lblAns = e.Item.FindControl("lblAns") as Label;
            Chart piechart = (Chart)e.Item.FindControl("PieChart1");

            if (fb.listOfTextAns ==null)
            {
                lblAns.Visible = false;

                //DataTable dt = GetData(query);
                string[] x = new string[fb.ratingCountList.Count];
                int[] y = new int[fb.ratingCountList.Count];
                for (int i = 0; i < fb.ratingCountList.Count; i++)
                {
                    x[i] = fb.ratingCountList[i].RATING.ToString();
                    y[i] = Convert.ToInt32(fb.ratingCountList[i].COUNT);
                }
                piechart.Series[0].Points.DataBindXY(x, y);
                piechart.Series[0].ChartType = SeriesChartType.Pie;
                piechart.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                piechart.Series[0].IsValueShownAsLabel = true;
                piechart.Legends[0].Enabled = true;
                piechart.Visible = true;
            }
            else
            {
                lblAns.Visible = true;
                piechart.Visible = false;
            }

        }
    }
}
﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System;
using System.Web.Script.Serialization;
using SGServe.Models.TransactionModels;
using System.Threading;
using SGServe.SaaS.Models;
using PayPal.PayPalAPIInterfaceService;
using PayPal.PayPalAPIInterfaceService.Model;
using System.Threading.Tasks;
using SGServe.Models.AuditModels;
using SGServe.Models.AccountModels;

namespace SGServe
{
    public partial class ManageBill : System.Web.UI.Page
    {
        //public static string org_id = "";
        public static string connection = "";
        public static List<OrgPayment> plan_list = new List<OrgPayment>();
        public static List<CRM> crm_list = new List<CRM>();
        DateTime date = new DateTime();
        OrgEmployee employee = new OrgEmployee();
        GlobalModule gm = new GlobalModule();
        public static OrganizationDetails orgInfo = new OrganizationDetails();

        //page load
        protected async void Page_Load(object sender, EventArgs e)
        {
            string[] urlArr = Request.RawUrl.ToString().Split('/');
            employee = await gm.getLoginEmpDetails(urlArr[1].ToUpper());
            if (employee.ORG_STATUS.Equals("INACTIVE"))
            {
                lblPayMoney.Visible = true;
            }
            else {
                lblPayMoney.Visible = false;
            }
            getOrgInfo();
        }
        public async void getOrgInfo()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1]));

                request.Content = new FormUrlEncodedContent(keyValues);
                HttpResponseMessage response = await client.SendAsync(request);
                //await Task.Delay(1000);
                String reply = await response.Content.ReadAsStringAsync();

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                orgInfo.ORG_ID = array["ORG_ID"].ToString().Substring(1, array["ORG_ID"].ToString().Length - 2);
                orgInfo.CONNECTION_STRING = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
                displayOrgTransaction();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
        protected async void displayOrgTransaction()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["org_id"] = orgInfo.ORG_ID;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    //getOrgTransaction
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Transaction/getOrgTransaction?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //get reply to bind to gridview
                    var recent_transactions = JsonConvert.DeserializeObject<DataTable>(reply);

                    if (recent_transactions != null)
                    {
                        GridView1.DataSource = recent_transactions;
                        GridView1.DataBind();
                        GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }

                    //reply to object
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    plan_list = jss.Deserialize<List<OrgPayment>>(reply);
                    OrgPayment item = plan_list.First();
                    //DateTime dateValue;
                    //string formats = "M/d/yyyy";
                    DateTime.TryParse(item.TO_DATE, out date);
                    TextBox1.Text = item.TO_DATE;
                    TextBox3.Text = item.PLAN_TYPE;
                    TextBox4.Text = item.PER.Split(' ')[0] + " Months";
                    getOrgPlanInfo();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }
        protected async void getOrgPlanInfo()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["org_id"] = orgInfo.ORG_ID;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    //getOrgTransaction
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Transaction/getOrgPlanInfo?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //reply to object
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    List<string> ans = jss.Deserialize<List<string>>(reply);
                    string recurr = ans[0];
                    string crm = ans[1];

                    infoPanel.Visible = false;
                    if (recurr.Equals("False"))
                    {
                        TextBox3.Text = "-";
                        TextBox4.Text = "-";
                        TextBox3.BackColor = System.Drawing.Color.LightGray;
                        TextBox4.BackColor = System.Drawing.Color.LightGray;
                        TextBox2.Text = "No Payment Made";
                        Button_cancel.Visible = false;

                        if (crm != null && !crm.Equals("") && !crm.Equals("False"))
                        {
                            infoPanel.Visible = true;
                        }
                    } else
                    {
                        TextBox2.Text = "Recurring";
                        Button_payment.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }

        //actions
        protected void makePayment(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("PaymentPlans", false);
            }
            catch (ThreadAbortException ex)
            {
            }
        }

        protected async void cancelPayment(object sender, EventArgs e)
        {
            try
            {
                string profileId = await getProfileID();
                if (profileId.Length != 0)
                {
                    paypalManageRecurringPaymentsProfile(profileId.Split(' ')[0]);
                }
                if (cancelOrgPaymentCall().Equals("success"))
                {
                    Button_payment.Visible = true;
                    Button_cancel.Visible = false;
                    SGServe.Models.TransactionModels.MessageBox.Show("Recurring Payment Successfully cancelled.");
                }
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }

        protected async Task<string> getProfileID()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["org_id"] = orgInfo.ORG_ID;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    //getOrgTransaction
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Transaction/getProfileID?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //reply to object
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    return jss.Deserialize<string>(reply);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
                return null;
            }
            return null;
        }
        protected void paypalManageRecurringPaymentsProfile(string profileId)
        {
            try
            {
                // Create request object
                ManageRecurringPaymentsProfileStatusRequestType request = new ManageRecurringPaymentsProfileStatusRequestType();
                ManageRecurringPaymentsProfileStatusRequestDetailsType details = new ManageRecurringPaymentsProfileStatusRequestDetailsType();
                request.ManageRecurringPaymentsProfileStatusRequestDetails = details;

                // (Required) Recurring payments profile ID returned in the CreateRecurringPaymentsProfile response.
                details.ProfileID = profileId;

                // (Required) The action to be performed to the recurring payments profile. 
                details.Action = (StatusChangeActionType)Enum.Parse(typeof(StatusChangeActionType), "SUSPEND");

                // (Optional) The reason for the change in status. For profiles created using Express Checkout, this message is included in the email notification to the buyer when the status of the profile is successfully changed, and can also be seen by both you and the buyer on the Status History page of the PayPal account.
                details.Note = "[SGServe] We are sorry to see you go. Thank you for your support!";

                // Invoke the API
                ManageRecurringPaymentsProfileStatusReq wrapper = new ManageRecurringPaymentsProfileStatusReq();
                wrapper.ManageRecurringPaymentsProfileStatusRequest = request;

                // Configuration map containing signature credentials and other required configuration.
                Dictionary<string, string> configurationMap = new Dictionary<string, string>();
                configurationMap.Add("mode", "sandbox");
                configurationMap.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                configurationMap.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                configurationMap.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");

                // Create the PayPalAPIInterfaceServiceService service object to make the API call
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(configurationMap);

                // # API call 
                // Invoke the ManageRecurringPaymentsProfileStatus method in service wrapper object  
                ManageRecurringPaymentsProfileStatusResponseType manageProfileStatusResponse = service.ManageRecurringPaymentsProfileStatus(wrapper);

                // Check for API return status
                setKeyResponseObjects(service, manageProfileStatusResponse);
            } catch (Exception ex)
            {

            }
        }
        private void setKeyResponseObjects(PayPalAPIInterfaceServiceService service, ManageRecurringPaymentsProfileStatusResponseType response)
        {
            Dictionary<string, string> responseParams = new Dictionary<string, string>();
            responseParams.Add("API Status", response.Ack.ToString());
            HttpContext CurrContext = HttpContext.Current;
            CurrContext.Items.Add("Response_redirectURL", null);
            if (response.Ack.Equals(AckCodeType.FAILURE) ||
                (response.Errors != null && response.Errors.Count > 0))
            {
                CurrContext.Items.Add("Response_error", response.Errors);
            }
            else
            {
                CurrContext.Items.Add("Response_error", null);
                responseParams.Add("Profile Id", response.ManageRecurringPaymentsProfileStatusResponseDetails.ProfileID);
            }
            CurrContext.Items.Add("Response_keyResponseObject", responseParams);
            CurrContext.Items.Add("Response_apiName", "ManageRecurringPaymentsProfileStatus");
            CurrContext.Items.Add("Response_requestPayload", service.getLastRequest());
            CurrContext.Items.Add("Response_responsePayload", service.getLastResponse());
        }
        protected async Task<string> cancelOrgPaymentCall()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["org_id"] = orgInfo.ORG_ID;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    //getOrgTransaction
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Transaction/cancelPayment?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //reply to object
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    return jss.Deserialize<string>(reply);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
                return null;
            }
            return null;
        }
    }
}
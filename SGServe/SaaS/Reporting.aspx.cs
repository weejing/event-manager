﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.SaaS.Models;
using SGServe.SaaS.Models.Reporting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class Reporting : System.Web.UI.Page
    {
        public static OrganizationDetails orgInfo = new OrganizationDetails();

        protected void Page_Load(object sender, EventArgs e)
        {
            getOrgInfo();
        }
        public async void getOrgInfo()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1]));

                request.Content = new FormUrlEncodedContent(keyValues);
                HttpResponseMessage response = await client.SendAsync(request);
                //await Task.Delay(1000);
                String reply = await response.Content.ReadAsStringAsync();

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                orgInfo.ORG_ID = array["ORG_ID"].ToString().Substring(1, array["ORG_ID"].ToString().Length - 2);
                orgInfo.CONNECTION_STRING = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
                orgInfo.ORG_NAME = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);

                getReportingData();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        public void getReportingData()
        {
            getVolNumData();
            getVolInterestData();
            getEventData();
        }

        private async void getVolNumData()
        {
            DataTable chart = await getVolNumDataSql();
            BindVolNumChart(chart);
        }
        protected async Task<DataTable> getVolNumDataSql()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["orgID"] = orgInfo.ORG_ID;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Reporting/getVolNumData?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();


                    //reply from object in class form
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    List<ReportVolunteers> list = jss.Deserialize<List<ReportVolunteers>>(reply);

                    DataTable report = new DataTable();
                    report.Columns.Add("Date", typeof(string));
                    report.Columns.Add("Follower", typeof(int));

                    int totalCount = 0;
                    DateTime dateEnd = DateTime.Now;
                    DateTime dateStart = dateEnd.AddDays(-30);
                    DateTime dateNow = new DateTime();

                    for (int i = 0; i < list.Count; i++)
                    {
                        ReportVolunteers dataset = list[i];

                        //insert in days with missing number of volunteers
                        DateTime.TryParse(dataset.VOLUNTEER_DATE, out dateNow);
                        while (dateNow.Date != dateStart.Date)
                        {
                            if (dateStart > dateEnd)
                            {
                                break;
                            }
                            report.Rows.Add(dateStart.ToString("dd'/'MM "), 0); // add empty row
                            dateStart = dateStart.AddDays(1);
                        }

                        string[] dateArray = dataset.VOLUNTEER_DATE.Split('/');
                        dataset.VOLUNTEER_DATE = dateArray[0] + "/" + dateArray[1];

                        int count = 0;
                        Int32.TryParse(dataset.VOLUNTEER_COUNT, out count);

                        totalCount += count;
                        report.Rows.Add(dataset.VOLUNTEER_DATE, count);
                    }

                    Label3.Text = totalCount + "";

                    return report;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
                return null;
            }
            return null;
        }
        private void BindVolNumChart(DataTable chart)
        {
            StringBuilder strScript = new StringBuilder();
            try
            {
                strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']});</script>  
  
                    <script type='text/javascript'>  
                    function drawVisualization() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['Date','Follower'],");

                foreach (DataRow row in chart.Rows)
                {
                    strScript.Append("['" + row["Date"] + "'," + row["Follower"] + "],");
                }
                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");

                strScript.Append("var options = { title : 'Number of volunteers followed over last 30 days', vAxis: {title: 'Transactions' , viewWindow: {min: 0 }},  hAxis: {title: 'Date', showTextEvery: 5}, curveType: 'function', legend: 'none' };");
                strScript.Append(" var chart = new google.visualization.LineChart(document.getElementById('VolNumChart'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization);");
                strScript.Append(" </script>");

                ltScripts.Text += strScript.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //chart.Dispose();
                //strScript.Clear();
            }
        }

        //preferred interest
        private async void getVolInterestData()
        {
            List<ReportVolunteers> volunteerData = await getVolInterestDataSql();
            DataTable skill = getVolInterestDataTable("Skill", volunteerData);
            DataTable cause = getVolInterestDataTable("Cause", volunteerData);
            DataTable role = getVolInterestDataTable("Event role", volunteerData);
            BindVolInterestChart(skill, "VolSkillInterest", "Breakdown of skills that volunteers have", "Skills");
            BindVolInterestChart(cause, "VolCauseInterest", "Breakdown of Causes that volunteers are interested in", "Cause");
            BindVolInterestChart(role, "VolEventRoleInterest", "Breakdown of roles volunteers are interested in", "Event Roles");
        }
        protected async Task<List<ReportVolunteers>> getVolInterestDataSql()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["orgID"] = orgInfo.ORG_ID;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Reporting/getVolInterestData?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();


                    //reply from object in class form
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    List<ReportVolunteers> list = jss.Deserialize<List<ReportVolunteers>>(reply);
                    return list;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
                return null;
            }
            return null;
        }
        protected DataTable getVolInterestDataTable(string category, List<ReportVolunteers> list)
        {
            try
            {
                DataTable report = new DataTable();
                report.Columns.Add("Category", typeof(string));
                report.Columns.Add("Number", typeof(int));

                int compareCount = 0;
                List<string> listStr = new List<string>();
                string highestCat = "";
                for (int i = 0; i < list.Count; i++)
                {
                    ReportVolunteers data = list[i];
                    if (data.VOLUNTEER_INTEREST_CAT.Equals(category))
                    {
                        int count = 0;
                        Int32.TryParse(data.VOLUNTEER_COUNT, out count);
                        report.Rows.Add(data.VOLUNTEER_INTEREST, count);

                        if (count > compareCount)
                        {
                            listStr.Clear();
                            compareCount = count;
                            listStr.Add(data.VOLUNTEER_INTEREST);
                        }
                        else if (count == compareCount)
                        {
                            listStr.Add(data.VOLUNTEER_INTEREST);
                        }
                    }
                }

                highestCat = string.Join(", ", listStr.ToArray());

                if (highestCat == null || highestCat.Equals(""))
                {
                    highestCat = "(None)";
                }

                if (category.Equals("Skill"))
                {
                    Label4.Text = highestCat;
                }
                else if (category.Equals("Cause"))
                {
                    Label5.Text = highestCat;
                }
                else
                {
                    Label6.Text = highestCat;
                }
                return report;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        private void BindVolInterestChart(DataTable chart, string div, string title, string hAxis)
        {
            StringBuilder strScript = new StringBuilder();
            try
            {
                strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']});</script>  
  
                    <script type='text/javascript'>  
                    function drawVisualization() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['Category', 'Number'],");

                foreach (DataRow row in chart.Rows)
                {
                    strScript.Append("['" + row["Category"] + "'," + row["Number"] + "],");
                }
                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");

                strScript.Append(@"var options = { title : '" + title + "', vAxis: {title: 'Number of volunteers'},  hAxis: {title: '" + hAxis + "'}, seriesType: 'bars', legend: 'none' };");
                strScript.Append(" var chart = new google.visualization.ColumnChart(document.getElementById('" + div + "'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization);");
                strScript.Append(" </script>");

                ltScripts.Text += strScript.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //chart.Dispose();
                //strScript.Clear();
            }
        }

        //EVENT REPORTING
        private async void getEventData()
        {
            DataTable chartEvent = await getEventChartDataSql();
            DataTable chartEventTurnUp = await getEventTurnUpChartDataSql();
            BindEventChart(chartEvent);
            BindEventTurnUpChart(chartEventTurnUp);
        }
        protected async Task<DataTable> getEventChartDataSql()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["connectionStr"] = orgInfo.CONNECTION_STRING;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Reporting/getNPOEventData?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //reply from object in class form
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    List<ReportEvent> list = jss.Deserialize<List<ReportEvent>>(reply);

                    DataTable report = new DataTable();
                    report.Columns.Add("Event Type", typeof(string));
                    report.Columns.Add("Event Name", typeof(string));

                    report.Columns.Add("Event Start Year", typeof(int));
                    report.Columns.Add("Event Start Month", typeof(int));
                    report.Columns.Add("Event Start Day", typeof(int));

                    report.Columns.Add("Event End Year", typeof(int));
                    report.Columns.Add("Event End Month", typeof(int));
                    report.Columns.Add("Event End Day", typeof(int));

                    int count = 0;

                    for (int i = 0; i < list.Count; i++)
                    {
                        ReportEvent item = list[i];
                        string event_type = "";
                        string event_name = item.EVENT_NAME;

                        DateTime event_start = new DateTime();
                        DateTime event_end = new DateTime();
                        DateTime.TryParse(item.START_DATE, out event_start);
                        DateTime.TryParse(item.END_DATE, out event_end);

                        int event_start_day = event_start.Day;
                        int event_start_month = event_start.Month;
                        int event_start_year = event_start.Year;

                        int event_end_day = event_end.Day;
                        int event_end_month = event_end.Month;
                        int event_end_year = event_end.Year;
                        if (item.EXAM == 1)
                        {
                            event_type = "Exam Period";
                        }
                        else
                        {
                            event_type = item.EVENT_DESC;
                            if (event_start.Date > DateTime.Now.Date)
                            {
                                count++;
                            }
                        }
                        report.Rows.Add(event_type, event_name, event_start_year, event_start_month, event_start_day, event_end_year, event_end_month, event_end_day);
                    }

                    Label9.Text = count + "";
                    return report;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
                return null;
            }
            return null;
        }
        protected async Task<DataTable> getEventTurnUpChartDataSql()
        {
            try
            {
                //generating query parameters
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["orgID"] = orgInfo.ORG_ID;
                query["connectionStr"] = orgInfo.CONNECTION_STRING;
                string queryString = query.ToString();

                //connecting to server for results
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Reporting/getNPOEventTurnUp?" + queryString);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //reply from object in class form
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    List<ReportEvent> list = jss.Deserialize<List<ReportEvent>>(reply);

                    DataTable report = new DataTable();
                    report.Columns.Add("Event Name", typeof(string));
                    report.Columns.Add("Registered", typeof(int));
                    report.Columns.Add("Turn Up", typeof(int));

                    int count = 0;
                    string text = "";
                    for (int i = 0; i < list.Count; i++)
                    {
                        ReportEvent item = list[i];
                        if (item.TURN_UP > count)
                        {
                            count = item.TURN_UP;
                            text = item.EVENT_NAME;
                        }
                        report.Rows.Add(item.EVENT_NAME, item.REG, item.TURN_UP);
                    }

                    if (count == 0)
                    {
                        text = "(none)";
                    }
                    Label10.Text = text;
                    return report;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
                return null;
            }
            return null;
        }
        private void BindEventChart(DataTable chart)
        {
            StringBuilder strScript = new StringBuilder();
            try
            {
                strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['timeline']});</script>  
  
                    <script type='text/javascript'>  
                    function drawVisualization() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['Event Type', 'Event Name', 'Event Start', 'Event End'],");

                foreach (DataRow row in chart.Rows)
                {
                    strScript.Append("['" + row["Event Type"] + "','" + row["Event Name"] + "'"+
                        ",new Date(" + row["Event Start Year"] + ","+ row["Event Start Month"] + "," + row["Event Start Day"] + ")" +
                        ",new Date(" + row["Event End Year"] + "," + row["Event End Month"] + "," + row["Event End Day"] + ")" + "],");
                }
                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");

                strScript.Append(@" var options = { title : 'Events over last 30 days' , width: 1100, tooltip: {isHtml: true}};
var chart = new google.visualization.Timeline(document.getElementById('EventNumChart'));  
chart.draw(data, options); } 
google.setOnLoadCallback(drawVisualization);
</script>");

                ltScripts.Text += strScript.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //chart.Dispose();
                //strScript.Clear();
            }
        }
        private void BindEventTurnUpChart(DataTable chart)
        {
            StringBuilder strScript = new StringBuilder();
            try
            {
                strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']});</script>  
  
                    <script type='text/javascript'>  
                    function drawVisualization() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['Event Name', 'Registered', 'Turn Up'],");

                foreach (DataRow row in chart.Rows)
                {
                    strScript.Append("['" + row["Event Name"] + "'," + row["Registered"] + "," + row["Turn Up"] + "],");
                }
                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");

                strScript.Append("var options = { title : 'Events over last 6 months', width: 1100, vAxis: {title: 'Number of Volunteers' , viewWindow: {min: 0 }},  hAxis: {title: 'Events'}};");
                strScript.Append(" var chart = new google.visualization.ColumnChart(document.getElementById('EventAttendanceChart'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization);");
                strScript.Append(" </script>");

                ltScripts.Text += strScript.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //chart.Dispose();
                //strScript.Clear();
            }
        }
    }
}
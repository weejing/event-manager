﻿using Newtonsoft.Json;
using SGServe.Models.HrModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System.Threading;
using System.Threading.Tasks;

namespace SGServe.SaaS
{
    public partial class ViewExternalVolunteer : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                BindGridview();
            }
        }
      
        /// <summary>HR: This method is called during page load to bind gridview data</summary>
        private async void BindGridview()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/HR/getExternalVolList?connectionstring=" + urlArr[1].ToUpper());
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var acctable = JsonConvert.DeserializeObject<DataTable>(reply);
            grdExternalVolunteer.DataSource = acctable;
            grdExternalVolunteer.DataBind();
        }

        protected void grdExternalVolunteer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdExternalVolunteer.PageIndex = (int)e.NewPageIndex;
            BindGridview();
        }

        protected async void btnSendInvite_Click(object sender, EventArgs e)
        {

            List<OpenEvents> eventlist = new List<OpenEvents>();
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["namelist_id"] = grdExternalVolunteer.DataKeys[row.RowIndex].Value.ToString();
            Session["name"] = grdExternalVolunteer.DataKeys[row.RowIndex].Values["NAME"].ToString();
            Session["email"] = grdExternalVolunteer.DataKeys[row.RowIndex].Values["EMAIL"].ToString();
            //retrieve the list of available events
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            //var eventlist = (List<OpenEvents>)Session["openeventdetails"];


            HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "/api/HR/getUpcomingEvents?connectionstring=" + urlArr[1].ToUpper());
            HttpResponseMessage response2 = await client.SendAsync(request2);
            String reply2 = await response2.Content.ReadAsStringAsync();
            JsonArray array = (JsonArray)JsonValue.Parse(reply2);

            // List<OpenEvents> items = new List<OpenEvents>();
            foreach (var arrayResult in array)
            {
                OpenEvents newItem = new OpenEvents();
                newItem.EVENTNAME = arrayResult["EVENTNAME"].ToString().Replace("\"", "");
                DateTime dt = DateTime.Parse(arrayResult["EVENTDATE"].ToString().Replace("\"", ""));
                newItem.EVENTDATE = dt.Date;
                newItem.EVENTLINK = arrayResult["EVENTLINK"].ToString().Replace("\"", "");
                eventlist.Add(newItem);
            }
            Session["openeventdetails"] = eventlist;
            //Session["emaillist"] = listofemail;
            if (Session["ManyRecipent"] != null)
            {
                Session["ManyRecipent"] = null;
            }
            Session["OneRecipent"] = "one";
            Response.Redirect("CreateEmailInvites", false);
        }

        protected async void btnDelete_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            await deleteVolunteer(row);
            BindGridview();
        }

        private async Task<string> deleteVolunteer(GridViewRow row)
        {
            string connection = Request.RawUrl.ToString().Substring(1, Request.RawUrl.ToString().Length - 23).ToUpper();
            Server_Connect serverConnection3 = new Server_Connect();
            HttpClient client3 = serverConnection3.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client3.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request3 = new HttpRequestMessage(HttpMethod.Get, "/api/HR/deleteExternalVol?NAMELIST_ID=" + grdExternalVolunteer.DataKeys[row.RowIndex].Value.ToString() + "&connectionstring=" + connection);
            HttpResponseMessage response3 = await client3.SendAsync(request3);
            return await response3.Content.ReadAsStringAsync();
        }



        protected async void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            List<string> listofemail = new List<string>();
            //retrieve the list of available events
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            //var eventlist = (List<OpenEvents>)Session["openeventdetails"];
            List<OpenEvents> eventlist = new List<OpenEvents>();

            HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "/api/HR/getUpcomingEvents?connectionstring=" + urlArr[1].ToUpper());
            HttpResponseMessage response2 = await client.SendAsync(request2);
            String reply2 = await response2.Content.ReadAsStringAsync();
            JsonArray array = (JsonArray)JsonValue.Parse(reply2);

            //List<OpenEvents> items = new List<OpenEvents>();
            foreach (var arrayResult in array)
            {
                OpenEvents newItem = new OpenEvents();
                newItem.EVENTNAME = arrayResult["EVENTNAME"].ToString().Replace("\"", "");
                DateTime dt = DateTime.Parse(arrayResult["EVENTDATE"].ToString().Replace("\"", ""));
                newItem.EVENTDATE = dt.Date;
                newItem.EVENTLINK = arrayResult["EVENTLINK"].ToString().Replace("\"", "");
                eventlist.Add(newItem);
            }
            Session["openeventdetails"] = eventlist;
            CheckBox ChkBoxHeader = (CheckBox)grdExternalVolunteer.HeaderRow.FindControl("chkboxSelectAll");
            foreach (GridViewRow row in grdExternalVolunteer.Rows)
            {
                CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkVol");
                if (ChkBoxHeader.Checked == true)
                {
                    ChkBoxRows.Checked = true;
                    listofemail.Add(grdExternalVolunteer.DataKeys[row.RowIndex].Values["EMAIL"].ToString());

                }
                else
                {
                    ChkBoxRows.Checked = false;
                }
                Session["emaillist"] = listofemail;
            }
        }

        protected void btnSendAll_Click(object sender, EventArgs e)
        {

            if(Session["OneRecipent"] != null)
            {
                Session["OneRecipent"] = null;
            }
            Session["ManyRecipent"] = "many";
            Response.Redirect("CreateEmailInvites", false);
        }
    }
}
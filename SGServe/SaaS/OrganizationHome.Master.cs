﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe.SaaS
{
    public partial class OrganizationHome : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            generateMenu();

            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            //Label1.Text = urlArr[1].ToUpper();
        }

        private void generateMenu()
        {
            string leftMenu = "<ul class='nav navbar-nav'>";
            string EndTag = "</ul>";
            string rightMenu = "<ul class='nav navbar-nav navbar-right'>";
            if (Session["Token"] == null)
            {
                //leftMenu = leftMenu + "<li> <a href='EventList'>Events</a> </li>";
                //right menu
                //rightMenu = rightMenu + "<li class='dropdown'><a href ='Login'><i class='fa fa-fw fa-lock'></i>Login</a></li>";
            }
            else
            {
               // leftMenu = leftMenu + "<li> <a href='EventList'>Events</a> </li>";
                //right menu
                rightMenu = rightMenu + "<li class='dropdown'><a href ='Logout'><i class='fa fa-fw fa-sign-out'></i>Signout</a></li>";
            }
            leftMenu = leftMenu + EndTag;
            rightMenu = rightMenu + EndTag;
            lblleftMenu.Text = leftMenu;
            lblRightmenu.Text = rightMenu;
        }
    }
}
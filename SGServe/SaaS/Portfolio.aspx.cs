﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System.Net.Http;
using System.Threading.Tasks;
using SGServe.SaaS.Models;
using System.Json;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;

namespace SGServe.SaaS
{
    public partial class Portfolio : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        static String currentUserID = "";
        static String orgID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                await getOrgDetail();
                await retrieveUserDetails();
                if (volunteer.VOL_ID == null)
                {
                    btnFollow.Visible = false;
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    btnFollow.Visible = true;
                    generateContent();
                }
            }
        }


        protected async Task getOrgDetail()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
            OrganizationDetails org = new OrganizationDetails();
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            org.CONNECTION_STRING = urlArr[1];
            var response = await client.PostAsJsonAsync("api/OrgMgmt/getOrgInfo", org);
            String reply = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("reply: " + reply);
            JsonObject array = (JsonObject)JsonValue.Parse(reply);
            lblOrgName.Text = array["ORG_NAME"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
            orgID = array["ORG_ID"].ToString().Substring(1, array["ORG_NAME"].ToString().Length - 2);
        }

        private async Task retrieveUserDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            if (jwtToken != null)
            {
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                // add the jwt token into the authorization header for server validation
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                volunteer = GlobalModule.getUserDetails(reply);
            }
        }

        protected async void generateContent()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getOrganisationDetailsWithFollow?userID=" + currentUserID + "&orgID=" + orgID);
                HttpResponseMessage resp = await client.SendAsync(req);
                String details = await resp.Content.ReadAsStringAsync();

                OrganizationDetailsModel lod = JsonConvert.DeserializeObject<OrganizationDetailsModel>(details);

                btnFollow.Text = lod.UNFOLLOW_STATUS;
                btnFollow.PostBackUrl = "/Follow.aspx?id=" + orgID;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }// end of generateForm()
    }
}
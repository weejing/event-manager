﻿using Newtonsoft.Json;
using SGServe.Models.HrModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System.IO;
using System.Drawing;
using System.Data.OleDb;
using System.Collections;
using AjaxControlToolkit;
using System.Threading.Tasks;

namespace SGServe.SaaS
{
    public partial class ManageExternalVolunteers : System.Web.UI.Page
    {
    
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        public async void addNewRecord()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            try
            {
               
                string NAME = tbName.Text;
                string PHONENO= tbContactNumber.Text;
                string EMAIL = tbEmail.Text;
                //check if external volunteer already exist
                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "/api/HR/checkVolunteerExist?name=" + NAME +"&contact="+ PHONENO +"&email="+ EMAIL + "&connectionstring=" + urlArr[1].ToUpper());
                HttpResponseMessage resp = await client.SendAsync(req);
                String response = await resp.Content.ReadAsStringAsync();
                ExternalVolunteerDetails details = new ExternalVolunteerDetails { NAME = NAME, EMAIL = EMAIL, PHONENO = PHONENO, CONNECTION_STRING = urlArr[1].ToUpper() };
                //volunteer record already exist
                if (Convert.ToBoolean(response) == true)
                {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Volunteer record already exist!" + "');", true);
                }
                //volunteer record don't exist
                else
                {
                  
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/HR/addOneRecord");
                        await client.PostAsJsonAsync("api/HR/addOneRecord", details);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "New External Volunteer Record has been successfully added!" + "');", true);
                }
              
                tbName.Text = "";
                tbEmail.Text = "";
                tbContactNumber.Text = "";
            }
            catch (Exception e)
            {
                //prompt error
               // ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Sorry, an error has been detected! Please kindly ensure that you have entered valid input fields!" + "');", true);
                tbName.Text = "";
                tbEmail.Text = "";
                tbContactNumber.Text = "";
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            firstTab.Attributes.Add("class", "active");
            secondTab.Attributes.Remove("class");
            insertMultipleItem.Visible = false;
            insertItemPanel.Visible = true;
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            secondTab.Attributes.Add("class", "active");
            firstTab.Attributes.Remove("class");
            insertMultipleItem.Visible = true;
            insertItemPanel.Visible = false;
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            secondTab.Attributes.Add("class", "active");
            firstTab.Attributes.Remove("class");
            step2.Visible = true;
            step1.Visible = false;
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            secondTab.Attributes.Add("class", "active");
            firstTab.Attributes.Remove("class");
            step1.Visible = true;
            step2.Visible = false;
        }

        protected void btnNextNext_Click(object sender, EventArgs e)
        {

            secondTab.Attributes.Add("class", "active");
            firstTab.Attributes.Remove("class");
            Response.Redirect("ViewExternalVolunteer");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            addNewRecord();
            tbName.Text = "";
            tbEmail.Text = "";
            tbContactNumber.Text = "";
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            String filepath = Server.MapPath("~/SaaS/InventoryListTemplate/VolunteerDetailsTemplate.xlsx");
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "application / vnd.openxmlformats - officedocument.spreadsheetml.sheet";
            response.AddHeader("Content-Disposition", "attachment; filename=" + "VolunteerDetailsTemplate.xlsx" + ";");
            response.TransmitFile(filepath);
            response.Flush();
            response.End();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string Extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                //remember to make file path dynamic
                String path = Server.MapPath("~/SaaS/UploadInventoryListTemplate/" + FileName);
                FileUpload1.SaveAs(path);
                GetExcelSheets(path, Extension, "Yes");
                System.IO.File.Delete(path);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Please select a valid excel file!" + "');", true);
            }
        }


        private async void GetExcelSheets(string FilePath, string Extension, string isHDR)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            string url = Request.RawUrl.ToString();
            string[] urlArr = url.Split('/');
            try
            {
                string conStr = "";
                conStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
                //Get the Sheets in Excel WorkBoo
                conStr = String.Format(conStr, FilePath, isHDR);
                OleDbConnection connExcel = new OleDbConnection(conStr);
                string query = "SELECT * FROM [Sheet1$]";
                OleDbCommand cmdExcel = new OleDbCommand(query, connExcel);
                OleDbDataAdapter oda = new OleDbDataAdapter(cmdExcel);
                cmdExcel.Connection = connExcel;
                connExcel.Open();
                DataSet ds = new DataSet();
                List<ExternalVolunteerDetails> uploadedDetails = new List<ExternalVolunteerDetails>();
                oda.Fill(ds);

                bool[] isEmpty = new bool[2];

                for (int row = 0; row < ds.Tables[0].Rows.Count; row++)
                {
                    ExternalVolunteerDetails obj = new ExternalVolunteerDetails(ds.Tables[0].Rows[row][0].ToString(), ds.Tables[0].Rows[row][2].ToString(), ds.Tables[0].Rows[row][1].ToString());
                    bool isNotNegative = obj.checkField();

                    if (isNotNegative)
                        uploadedDetails.Add(obj);
                    else
                    {
                        //prompt
                        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Sorry, an error has been detected in your uploaded file. Please kindly ensure that you have entered valid input field!" + "');", true);
                        break;
                    }
                }

                connExcel.Close();
                for (int i = 0; i < uploadedDetails.Count; i++)
                {
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "/api/HR/checkVolunteerExist?name=" + uploadedDetails[i].NAME.ToString() + "&contact=" + uploadedDetails[i].PHONENO.ToString() + "&email=" + uploadedDetails[i].EMAIL.ToString() + "&connectionstring=" + urlArr[1].ToUpper());
                    HttpResponseMessage resp = await client.SendAsync(req);
                    String response = await resp.Content.ReadAsStringAsync();
                    if (Convert.ToBoolean(response) == true)
                    {
                        //notify that volunteer already exist
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Volunteer record already exist!" + "');", true);

                    }
                    else
                    {
                        string NAME = uploadedDetails[i].NAME.ToLower();
                        string PHONENO = uploadedDetails[i].PHONENO.ToLower();
                        string EMAIL = uploadedDetails[i].EMAIL.ToLower();
                        ExternalVolunteerDetails item2 = new ExternalVolunteerDetails { NAME = NAME, EMAIL = EMAIL, PHONENO = PHONENO, CONNECTION_STRING = urlArr[1].ToUpper() };
                        //add volunteer record
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/HR/addOneRecord");
                        await client.PostAsJsonAsync("api/HR/addOneRecord", item2);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "New External Volunteer Record has been successfully added!" + "');", true);

                    }
                }
            }
            catch (Exception e)
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + "Sorry, an error has been detected in your uploaded file. Please kindly ensure that you have entered valid input field!" + "');", true);
            }
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
           
            secondTab.Attributes.Remove("class");
            firstTab.Attributes.Remove("class");
            Response.Redirect("CreateVIATemplate", false);
        }
    }
}
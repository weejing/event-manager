﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Http.Headers;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using SGServe.Models.TransactionModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class UserPortfolio : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";
        static String navigateUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //await retrieveUserDetails();
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    navigateUserID = Request.QueryString["id"];
                    currentUserID = volunteer.VOL_ID;
                    lblFullName.Text = volunteer.FULL_NAME;
                    if (navigateUserID == currentUserID)
                    {
                        btnAddFriend.Visible = false;
                        PanelF.Visible = false;
                        btnRemoveFriend.Visible = false;
                        lblPendingFriends.Visible = false;
                        panelWriteMessage.Visible = false;
                        btnViewDelete.Visible = true;
                        btnEditProfile.Visible = true;
                    }
                    else
                    {
                        btnViewDelete.Visible = false;
                        btnEditProfile.Visible = false;
                        checkFriend();
                    }

                    generateForm();
                    // retrievePortfolio();
                    // retrieveMessages();
                    // retrieveActivities();
                    linkAbout.NavigateUrl = "UserPortfolio.aspx?id=" + navigateUserID;
                    linkFriends.NavigateUrl = "UserFriends.aspx?id=" + navigateUserID;
                    linkFollows.NavigateUrl = "UserOrgFollows.aspx?id=" + navigateUserID;
                }
            }
            else
            {
                if (!(Request.QueryString["id"].Equals(navigateUserID)))
                {
                    volunteer = await gm.getLoginVolDetails();
                    if (volunteer.VOL_ID == null)
                    {
                        Response.Redirect("login.aspx", false);
                    }
                    else
                    {
                        navigateUserID = Request.QueryString["id"];
                        currentUserID = volunteer.VOL_ID;
                        lblFullName.Text = volunteer.FULL_NAME;
                        if (navigateUserID == currentUserID)
                        {
                            btnAddFriend.Visible = false;
                            PanelF.Visible = false;
                            btnRemoveFriend.Visible = false;
                            lblPendingFriends.Visible = false;
                            panelWriteMessage.Visible = false;
                            btnViewDelete.Visible = true;
                            btnEditProfile.Visible = true;
                        }
                        else
                        {
                            btnViewDelete.Visible = false;
                            btnEditProfile.Visible = false;
                            checkFriend();
                        }

                        generateForm();
                        linkAbout.NavigateUrl = "UserPortfolio.aspx?id=" + navigateUserID;
                        linkFriends.NavigateUrl = "UserFriends.aspx?id=" + navigateUserID;
                        linkFollows.NavigateUrl = "UserOrgFollows.aspx?id=" + navigateUserID;
                    }
                }
            }
        }


        protected async void checkFriend()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage checkFriendsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/checkfriends?userID1=" + navigateUserID + "&userID2=" + currentUserID);
                HttpResponseMessage checkFriendsResp = await client.SendAsync(checkFriendsReq);
                String checkFriends = await checkFriendsResp.Content.ReadAsStringAsync();

                String friendStatus = JsonConvert.DeserializeObject<String>(checkFriends);
                if (friendStatus == "not friends" || friendStatus == "deleted")
                {
                    btnAddFriend.Visible = true;
                    PanelF.Visible = false;
                    btnRemoveFriend.Visible = false;
                    lblPendingFriends.Visible = false;
                    panelWriteMessage.Visible = false;
                }
                else if (friendStatus == "friend")
                {
                    btnAddFriend.Visible = false;
                    PanelF.Visible = true;
                    btnRemoveFriend.Visible = true;
                    lblPendingFriends.Visible = false;
                    panelWriteMessage.Visible = true;
                }
                else if (friendStatus == "pending")
                {
                    btnAddFriend.Visible = false;
                    PanelF.Visible = false;
                    btnRemoveFriend.Visible = false;
                    lblPendingFriends.Text = "Pending friend request";
                    lblPendingFriends.Visible = true;
                    panelWriteMessage.Visible = false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }


        protected async void generateForm()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserDetails?userID=" + navigateUserID);
                HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
                String userDetails = await userDetailsResp.Content.ReadAsStringAsync();

                UserPortfolioModel upm = JsonConvert.DeserializeObject<UserPortfolioModel>(userDetails);

                lblFullName.Text = upm.FULL_NAME;
                lblToName.Text = upm.FULL_NAME;
                String school = upm.SCHL_NAME;
                lblSchool.Text = school;
                lblPersonalityMBTI.Text = upm.PERSONALITY_CHAR;
                lblDescription.Text = upm.DESCRIPTION;
                //ProfilePicImg.ImageUrl = "data:image/jpeg;base64," + upm.IMAGE;
                ProfilePicImg.ImageUrl = upm.IMAGE;

                if (school.Trim().Length == 0 || school.Equals(null))
                {
                    PanelSchool.Visible = false;
                }
                else
                {
                    PanelSchool.Visible = true;
                }
                
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + navigateUserID + "&vocab_name=cause");
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                var Benetable = JsonConvert.DeserializeObject<DataTable>(reply);
                dlBene.DataSource = Benetable;
                dlBene.DataBind();
                if (reply.Length == 0 || reply.Equals(null))
                {
                    PanelInterestsCause.Visible = false;
                }
                else
                {
                    PanelInterestsCause.Visible = true;
                }

                //get roles
                //Server_Connect roleserverConnection = new Server_Connect();
                //HttpClient roleclient = roleserverConnection.getHttpClient();
                HttpRequestMessage rolerequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + navigateUserID + "&vocab_name=" + "event role");
                HttpResponseMessage roleresponse = await client.SendAsync(rolerequest);
                String rolereply = await roleresponse.Content.ReadAsStringAsync();
                var Roletable = JsonConvert.DeserializeObject<DataTable>(rolereply);
                dlRole.DataSource = Roletable;
                dlRole.DataBind();
                if (rolereply.Length == 0 || rolereply.Equals(null))
                {
                    PanelInterestsRole.Visible = false;
                }
                else
                {
                    PanelInterestsRole.Visible = true;
                }
                //get skill
                //Server_Connect skillerverConnection = new Server_Connect();
                //HttpClient skillclient = skillerverConnection.getHttpClient();
                HttpRequestMessage skillrequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + navigateUserID + "&vocab_name=" + "skill");
                HttpResponseMessage skillresponse = await client.SendAsync(skillrequest);
                String skillgreply = await skillresponse.Content.ReadAsStringAsync();
                var skilltable = JsonConvert.DeserializeObject<DataTable>(skillgreply);
                dlskill.DataSource = skilltable;
                dlskill.DataBind();
                if (skillgreply.Length == 0 || skillgreply.Equals(null))
                {
                    PanelSkills.Visible = false;
                }
                else
                {
                    PanelSkills.Visible = true;
                }
                /*
                for (int i = 0; i < upmi.Count(); i++)
                {
                    if (i == 0)
                    {
                        lblInterests.Text = upmi[i];
                    }
                    else if (i == upmi.Count() - 1)
                    {
                        lblInterests.Text = lblInterests.Text + " " + upmi[i];
                    }
                    else
                    {
                        lblInterests.Text = lblInterests.Text + ", " + upmi[i];
                    }
                }
                for (int i = 0; i < upm.userSkill.Count(); i++)
                {
                    if (i == 0)
                    {
                        lblSkills.Text = upms[i];
                    }
                    else if (i == upmi.Count() - 1)
                    {
                        lblSkills.Text = lblSkills.Text + " " + upms[i];
                    }
                    else
                    {
                        lblSkills.Text = lblSkills.Text + ", " + upms[i];
                    }
                }*/

                HttpRequestMessage messagesReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getCommentsForUser?toUserID=" + navigateUserID);
                HttpResponseMessage messagesResp = await client.SendAsync(messagesReq);
                String messages = await messagesResp.Content.ReadAsStringAsync();

                List<UserCommentsModel> ucm = JsonConvert.DeserializeObject<List<UserCommentsModel>>(messages);

                foreach (UserCommentsModel comment in ucm)
                {
                    DateTime dtDisplay = Convert.ToDateTime(comment.COMMENT_DATE);
                    comment.COMMENT_DATE = dtDisplay.ToString("d MMM yyyy h:mm tt");
                }

                DataList2.DataSource = ucm;
                DataList2.DataBind();

                HttpRequestMessage userActivitiesReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserActivities?userID=" + navigateUserID);
                HttpResponseMessage userActivitiesResp = await client.SendAsync(userActivitiesReq);
                String userActivities = await userActivitiesResp.Content.ReadAsStringAsync();

                List<UserNewsfeedModel> uam = JsonConvert.DeserializeObject<List<UserNewsfeedModel>>(userActivities);

                foreach (UserNewsfeedModel feed in uam)
                {
                    DateTime dtDisplay = Convert.ToDateTime(feed.ACTIVITY_DATETIME);
                    feed.ACTIVITY_DATETIME = dtDisplay.ToString("d MMM yyyy h:mm tt");
                }

                DataList3.DataSource = uam;
                DataList3.DataBind();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

        }

        protected async void btnSend_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request1 = new HttpRequestMessage(HttpMethod.Get, "/api/CommunityMgmt/getCommentDatetimeByUserForUser?fromUserID=" + currentUserID + "&toUserID=" + navigateUserID);
            HttpResponseMessage resp1 = await client.SendAsync(request1);
            var response1 = await resp1.Content.ReadAsStringAsync();

            String commentDatetimeInString = JsonConvert.DeserializeObject<String>(response1);
            DateTime commentDatetime = System.DateTime.Parse(commentDatetimeInString);

            if (System.DateTime.Now < commentDatetime.AddHours(1))
            {
                MessageBox.Show("You have commented less than 1 hour ago. Please wait before you can comment.");
            }
            else
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/addComments?toUserID=" + navigateUserID + "&fromUserID=" + currentUserID + "&comment=" + tbWriteMessage.Text);
                HttpResponseMessage resp = await client.SendAsync(request);
                String response = await resp.Content.ReadAsStringAsync();

                Response.Redirect("UserPortfolio.aspx?id=" + Request.QueryString["id"]);
            }
        }

        protected async void btnAddFriend_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/addUserFriend?userID1=" + currentUserID + "&userID2=" + navigateUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserPortfolio.aspx?id=" + navigateUserID);
        }

        protected async void btnRemoveFriend_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/removeFriend?userID1=" + currentUserID + "&userID2=" + navigateUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserPortfolio.aspx?id=" + navigateUserID);
        }

        protected void DataList2_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Button btn = e.Item.FindControl("btnDelete") as Button;
            if (btn != null)
            {
                if (currentUserID == navigateUserID)
                {
                    btn.Visible = true;
                }
                else if (e.Item.FindControl("From_USR_NAME").ToString() == currentUserID)
                {
                    btn.Visible = true;
                }
                else
                {
                    btn.Visible = false;
                }
            }

        }
    }
}
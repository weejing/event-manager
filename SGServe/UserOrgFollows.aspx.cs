﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;
namespace SGServe
{
    public partial class UserOrgFollows : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";
        static String navigateUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //await retrieveUserDetails();
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    navigateUserID = Request.QueryString["id"];
                    currentUserID = volunteer.VOL_ID;
                    lblFullName.Text = volunteer.FULL_NAME;
                    if (navigateUserID == currentUserID)
                    {
                        btnAddFriend.Visible = false;
                        PanelF.Visible = false;
                        btnRemoveFriend.Visible = false;
                        lblPendingFriends.Visible = false;
                    }
                    else
                    {
                        checkFriend();
                    }
                    generateContent();
                    generateUserDetails();
                    linkAbout.NavigateUrl = "UserPortfolio.aspx?id=" + navigateUserID;
                    linkFriends.NavigateUrl = "UserFriends.aspx?id=" + navigateUserID;
                    linkFollows.NavigateUrl = "UserOrgFollows.aspx?id=" + navigateUserID;
                }
            }
        }

        protected async void checkFriend()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage checkFriendsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/checkfriends?userID1=" + navigateUserID + "&userID2=" + currentUserID);
                HttpResponseMessage checkFriendsResp = await client.SendAsync(checkFriendsReq);
                String checkFriends = await checkFriendsResp.Content.ReadAsStringAsync();

                String friendStatus = JsonConvert.DeserializeObject<String>(checkFriends);
                if (friendStatus == "not friends" || friendStatus == "deleted")
                {
                    btnAddFriend.Visible = true;
                    PanelF.Visible = false;
                    btnRemoveFriend.Visible = false;
                    lblPendingFriends.Visible = false;
                }
                else if (friendStatus == "friend")
                {
                    btnAddFriend.Visible = false;
                    PanelF.Visible = true;
                    btnRemoveFriend.Visible = true;
                    lblPendingFriends.Visible = false;
                }
                else if (friendStatus == "pending")
                {
                    btnAddFriend.Visible = false;
                    PanelF.Visible = false;
                    btnRemoveFriend.Visible = false;
                    lblPendingFriends.Text = "Pending friend request";
                    lblPendingFriends.Visible = true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected async void generateContent()
        {
            try
            {
                String navigateUserID = Request.QueryString["id"].ToString();

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserOrganisations?userID=" + navigateUserID);
                HttpResponseMessage resp = await client.SendAsync(req);
                String orgDetails = await resp.Content.ReadAsStringAsync();

                List<OrganizationDetailsModel> upm = JsonConvert.DeserializeObject<List<OrganizationDetailsModel>>(orgDetails);

                if (upm.Count == 0 || upm.Equals(null))
                {
                    if (navigateUserID == currentUserID)
                    {
                        lblNone.Text = "You have not followed organisations yet. Kick start your SGServe volunterring journey by following some organisations !";
                    }
                    else
                    {
                        lblNone.Text = "The volunteer has not followed any organisations on SGServe.";
                    }

                    lblNone.Visible = true;
                }
                else
                {
                    lblNone.Visible = false;
                }

                DataList1.DataSource = upm;
                DataList1.DataBind();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
        {
            /*
            if (e.CommandName == "navigatePortfolio")
            {
                Session["navigateUserID"] = e.Item.FindControl("USR_ID");
                Response.Redirect("/UserPortfolio.aspx?id=" + e.Item.FindControl("USR_ID"));
            }
            */
        }

        protected async void generateUserDetails()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserDetails?userID=" + navigateUserID);
                HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
                String userDetails = await userDetailsResp.Content.ReadAsStringAsync();

                UserPortfolioModel upm = JsonConvert.DeserializeObject<UserPortfolioModel>(userDetails);

                lblFullName.Text = volunteer.FULL_NAME;
                ProfilePicImg.ImageUrl = upm.IMAGE;
                lblDescription.Text = upm.DESCRIPTION;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected async void btnAddFriend_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/addUserFriend?userID1=" + currentUserID + "&userID2=" + navigateUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserPortfolio.aspx?id=" + navigateUserID);
        }

        protected async void btnRemoveFriend_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/removeFriend?userID1=" + currentUserID + "&userID2=" + navigateUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserPortfolio.aspx?id=" + navigateUserID);
        }

        protected void btnProfile_Click(object sender, EventArgs e)
        {

        }
    }
}
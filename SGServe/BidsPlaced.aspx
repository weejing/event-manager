﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="BidsPlaced.aspx.cs" Inherits="SGServe.BidsPlaced" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <h1>Bids you have placed </h1>
    <asp:Button ID="btnCurrent" runat="server" Text="Current" CssClass="btn btn-inverse" OnClick="btnCurrent_Click"/>&nbsp;
    <asp:Button ID="btnWon" runat="server" Text="Won" CssClass="btn btn-default" OnClick="btnWon_Click"/>&nbsp;
    <asp:Button ID="btnAll" runat="server" Text="All" CssClass="btn btn-default" OnClick="btnAll_Click"/>

    <div class="row">
        <br>
        <asp:Panel ID="Panel1" runat="server">
            <div class="panel panel-default" style="border-radius: 8px" >
                <div class="table-responsive" style="padding-left: 3%; padding-right: 3%;">
                    <br />
                    <asp:GridView ID="RecordGridView" CellPadding="50" GridLines="None" Width="100%" CssClass="table"
                        AllowPaging="True" EmptyDataText="You have not placed any bids." runat="server" AutoGenerateColumns="False" PageSize="20" 
                        DataKeyNames = "BID_ITEM_ID">
                        <Columns>
                            <asp:BoundField DataField="ITEM_NAME" HeaderText="Item Name"/>
                            <asp:BoundField DataField="BID_POINTS_AMOUNT" HeaderText="Points Placed" />
                            <asp:BoundField DataField="DATE_TIME" HeaderText="Date / Time" />
                            <asp:BoundField DataField="STATUS" HeaderText="Status" />
                            <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="btnViewBid" CssClass="btn btn-primary btn-xs" OnClick="btnViewBid_Click">
                                        View / Bid item</asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </div>

</asp:Content>

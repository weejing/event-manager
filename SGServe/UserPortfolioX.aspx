﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="UserPortfolio.aspx.cs" Inherits="SGServe.UserPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="community_css/css/vendor/all.css" rel="stylesheet">
    <link href="community_css/css/app/app.css" rel="stylesheet">
    <script src="community_css/js/vendor/all.js"></script>
    <script src="community_css/js/app/app.js"></script>

    <!--
    <script type="text/javascript">
      window.onload = function() {
          document.getElementById("btnRemoveFriend").onmouseover = function ()
          {
              this.style.display = "Really ?! Remove ?!";
          }
 
          document.getElementById("btnRemoveFriend").onmouseout = function ()
          {
              this.style.display = "Remove Friend";
          }
      }
  </script>-->
          <div class="container">

              <br/>
            <div class="cover profile">
                
              <div class="wrapper">
                <div class="image" style="height:58px;">
                  <!--<img src="main_css/images/place1-wide.jpg" alt="people" />-->
                </div>
              </div>
                    
              <div class="cover-info">
                <div class="avatar">
                  <asp:Image ID="ProfilePicImg" runat="server" />
                </div>
                <div class="name">
                    <h2>
                        <asp:Label ID="lblFullName" runat="server" Text="Bill Damien" ForeColor="Black"></asp:Label>
                    </h2>
                            </div>
                <ul class="cover-nav">

                  <li class="active"><!--<a href="UserPortfolio.aspx" id="linkAbout"><i class="fa fa-fw icon-user-1"></i> About</a>-->
                    <asp:HyperLink ID="linkAbout" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> About </asp:HyperLink></li>
                  <!--<li><a href="UserFriends.aspx" id="linkFriends"><i class="fa fa-fw fa-users"></i> Friends</a></li>
                  <li><a href="UserOrgFollows.aspx" id="linkFollows"><i class="fa fa-fw fa-building-o"></i> Follows</a></li>-->
                  <li><asp:HyperLink ID="linkFriends" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Friends </asp:HyperLink> </li>
                  <li><asp:HyperLink ID="linkFollows" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Follows </asp:HyperLink>

                      <li>
                    <asp:Button ID="btnAddFriend" runat="server" class="btn btn-info" Text="Add Friend" OnClick="btnAddFriend_Click" />
                    <asp:Label ID="lblAlreadyFriends" runat="server" Text="You are friends with this volunteer"></asp:Label>
                    <asp:Button ID="btnRemoveFriend" runat="server" class="btn btn-warning" Text="Remove Friend" OnClick="btnRemoveFriend_Click" />
                      </li>
                </ul>
              </div>
            </div>
           </div>

        <div class="container">
            <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> About
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">School</span></div>
                          <div class="col-sm-8">
                              <asp:Label ID="lblSchool" runat="server" Text="Label">my school name</asp:Label>
                            </div>
                        </div>
                      </li>
                        <!--
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Gender</span></div>
                          <div class="col-sm-8">
                              <asp:Label ID="lblGender" runat="server" Text="Label">Male</asp:Label>
                          </div>
                        </div>
                      </li>
                        -->
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Interests</span></div>
                          <div class="col-sm-8">
                              <asp:Label ID="lblInterests" runat="server"></asp:Label>
                          </div>
                        </div>
                      </li>
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Skills</span></div>
                          <div class="col-sm-8">
                              <asp:Label ID="lblSkills" runat="server"></asp:Label>
                          </div>
                        </div>
                      </li>
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Personality MBTI</span>
                              </div>
                              <div class="col-sm-8">
                              <asp:Label ID="lblPersonalityMBTI" runat="server"></asp:Label>
                                </div>
                         </div>
                      </li>
                    <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Description</span>
                              </div><br/>
                              <div class="col-sm-8">
                              <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                </div>
                         </div>
                      </li>
                    </ul>
                  </div>
                  <asp:Button ID="btnEditProfile" class="btn btn-warning btn-xs" runat="server" Text="Edit Profile" PostBackUrl="~/UserPortfolioEdit.aspx" />
                <br/>
            </div>

            <div class="row">
              <!-- LEFT COLUMN -->

              <div class="col-md-5">
                  <asp:Panel ID="panelWriteMessage" runat="server">
                      <div class="panel panel-default share">
                    <h5>   Write some words of encouragement for
                        <asp:Label ID="lblToName" runat="server" Text=""></asp:Label>
                    </h5>
                        <br/>
                  <div class="input-group">
                      <asp:TextBox ID="tbWriteMessage" class="form-control share-text" runat="server"></asp:TextBox>
                    <div class="input-group-btn">
                      <asp:Button ID="btnSend" class="btn btn-info" runat="server" Text="Send" OnClick="btnSend_Click" />
                    </div>
                  </div>
                </div>

                  </asp:Panel>

                <br/>
                  
                  <div class="panel panel-default">
                  <asp:Button ID="btnViewDelete" class="btn btn-warning btn-xs" runat="server" Text="View / Delete Messages" PostBackUrl="~/Messages.aspx" />
                  <asp:DataList ID="DataList2" runat="server">
                      <ItemTemplate>
                          <div class="panel-heading">
                              <table>
                      <div class="media">
                          <tr>
                              <td>
                        <div class="media-left">
                          <a href="">
                            <img src="main_css/images/people/50/guy-6.jpg" class="media-object">
                          </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                  </td>
                              <td>
                        <div class="media-body">
                           <asp:Label ID="lblCommentorName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "From_USR_NAME") %>></asp:Label>
                            &nbsp; on &nbsp;
                            <asp:Label ID="lblCommentDate" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "COMMENT_DATE") %>></asp:Label>
                            <br />
                            <asp:Label ID="lblMessage" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "COMMENT") %>></asp:Label>
                        </div>
                          </td>
                        </tr>
                       </div> 
                      </table>
                                  
                      </div>        
                      </ItemTemplate>
                  </asp:DataList>
                  </div>
                <br/>
                  </div>

                <!-- RIGHT COLUMN -->

              <div class="col-md-7">

                  <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <h5> Activities </h5>
                  
                      <asp:DataList ID="DataList3" runat="server">
                      <ItemTemplate>
                          <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="media">
                        <div class="media-left">
                          <a href="">
                            <img src="main_css/images/people/50/guy-6.jpg" class="media-object">
                          </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="media-body">
                            <asp:Label ID="lblActivityName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MESSAGE_DISPLAY") %>'></asp:Label>
                            <br/>
                            <asp:Label ID="lblDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DATETIME") %>'></asp:Label>
                            &nbsp;</div>
                      </div>
                    </div>
                              <!--
                              &nbsp;<ul class="comments">
                                  <li clas="media">
                                      <div class="media-left">
                                          &nbsp;&nbsp;<a></a>&nbsp;&nbsp;<asp:Label ID="lblNumHearts" runat="server" Text="2 hearts"></asp:Label>
                                      </div>
                                  </li>
                                  <li class="comment-form">
                                      <div class="input-group">
                                          &nbsp;<span class="input-group-btn"><asp:Button ID="btnHeart" runat="server" Text="Button" />
                                          </span>
                                      </div>
                                  </li>
                              </ul>
                              -->
                  </div>
                          </ItemTemplate>
                  </asp:DataList>

                        </div>
                      </div>
                  </div>
                </div>
            </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpinWheel.aspx.cs" Inherits="SGServe.SpinWheels" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>HTML5 Canvas Winning Wheel</title>
    <link href="spinwheelcontent/main.css" rel="stylesheet" />
    <script src="spinwheelcontent/Winwheel.js"></script>
        
    <script src='<%=ResolveClientUrl("~/scripts/jquery-1.6.4.min.js") %>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/scripts/jquery.signalR-2.2.1.min.js") %>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/signalr/hubs") %>' type="text/javascript"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <link href="main_css/css/vendor/all.css" rel="stylesheet">
    <link href="main_css/css/app/app.css" rel="stylesheet">
    <link href="main_css/css/app/timeline.css" rel="stylesheet">
    <link href="main_css/css/app/cover.css" rel="stylesheet">
    <link href="main_css/css/app/essentials.css" rel="stylesheet">
    <link href="main_css/css/app/layout.css" rel="stylesheet">
    <link href="main_css/css/app/media.css" rel="stylesheet">
     
</head>

<body>
    <form id="form2" runat="server">
                <asp:scriptmanager runat="server">
                </asp:scriptmanager>
    <!-- Fixed navbar -->
    <div class="navbar navbar-main navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.aspx">SGServe</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-nav">
                <asp:Label ID="lblleftMenu" runat="server" Text=""></asp:Label>
                       <asp:Label ID="lblRightmenu" runat="server" Text=""></asp:Label> 
            </div>
            <!-- /.navbar-collapse -->

        </div>
    </div>
      <div class="container">
          <asp:HiddenField ID="hdnfldVariable" runat="server" />
		<div align="center">
			<!--<h1>Spin for bonus points</h1>-->
			<table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
                    <div class="power_controls">
                        <h3>Spin for bonus points</h3>
                        <img id="spin_button" src="spinwheelcontent/spin_off.png" alt="Spin" onClick="startSpin();" />
                    </div>
                </td>
                <td width="438" height="582" class="the_wheel" align="center" valign="center">
                    <canvas id="canvas" width="434" height="434">
                        <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please try another.</p>
                    </canvas>
                </td>
            </tr>
        </table>
		<script>
		    // Create new wheel object specifying the parameters at creation time.
            // call the getContent method in the code behind to pass the values from code behind to javascript array object
		    var publicationTable = <%= getContent() %>;
            //call the getCount from the code behind to determine the number of records you want to display
		    var segmentCount = <%= getCount() %>;
            //initialise the wheel object
			var theWheel = new Winwheel({
				'numSegments'  : segmentCount,
				'outerRadius'  : 212,
				'textFontSize' : 20,
				'segments'     : publicationTable,
				'animation' :
				{
					'type'     : 'spinToStop',
					'duration' : 5,
					'spins'    : 8,
					'callbackFinished' : 'alertPrize()'
				}
			});
			
			// Vars used by the code in this page to do power controls.
			var wheelPower    = 0;
			var wheelSpinning = false;
			
			// -------------------------------------------------------
			// Function to handle the onClick on the power buttons.
			// -------------------------------------------------------
			function powerSelected(powerLevel)
			{
				// Ensure that power can't be changed while wheel is spinning.
				if (wheelSpinning == false)
				{
					// Reset all to grey incase this is not the first time the user has selected the power.
					document.getElementById('pw1').className = "";
					document.getElementById('pw2').className = "";
					document.getElementById('pw3').className = "";
					
					// Now light up all cells below-and-including the one selected by changing the class.
					if (powerLevel >= 1)
					{
						document.getElementById('pw1').className = "pw1";
					}
						
					if (powerLevel >= 2)
					{
						document.getElementById('pw2').className = "pw2";
					}
						
					if (powerLevel >= 3)
					{
						document.getElementById('pw3').className = "pw3";
					}
					
					// Set wheelPower var used when spin button is clicked.
					wheelPower = powerLevel;
					
					// Light up the spin button by changing it's source image and adding a clickable class to it.
					document.getElementById('spin_button').src = "spinwheelcontent/spin_off.png";
					document.getElementById('spin_button').className = "clickable";
				}
			}
			
			// -------------------------------------------------------
			// Click handler for spin button.
			// -------------------------------------------------------
			function startSpin()
			{
				// Ensure that spinning can't be clicked again while already running.
				if (wheelSpinning == false)
				{
					// Based on the power level selected adjust the number of spins for the wheel, the more times is has
					// to rotate with the duration of the animation the quicker the wheel spins.

						theWheel.animation.spins = 3;
					
					// Disable the spin button so can't click again while wheel is spinning.
					document.getElementById('spin_button').src       = "spinwheelcontent/spin_off.png";
					document.getElementById('spin_button').className = "";
					
					// Begin the spin animation by calling startAnimation on the wheel object.
					theWheel.startAnimation();
					
					// Set to true so that power can't be changed and spin button re-enabled during
					// the current animation. The user will have to reset before spinning again.
					wheelSpinning = true;

                    /*
					var hdnfldVariable = document.getElementById('hdnfldVariable');
					hdnfldVariable.value = winningSegment.text;

					alert("You have won " + hdnfldVariable.value);

			        window.location.assign("About.aspx");
                    */

				}
			}
			
			// -------------------------------------------------------
			// Function for reset button.
			// -------------------------------------------------------
			function resetWheel()
			{
				theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
				theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
				theWheel.draw();                // Call draw to render changes to the wheel.
				
				document.getElementById('pw1').className = "";  // Remove all colours from the power level indicators.
				document.getElementById('pw2').className = "";
				document.getElementById('pw3').className = "";
				
				wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
			}
			
			// -------------------------------------------------------
			// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
			// -------------------------------------------------------
			function alertPrize()
			{
				// Get the segment indicated by the pointer on the wheel background which is at 0 degrees.
			    var winningSegment = theWheel.getIndicatedSegment();

                //the part where you assign the selected text to the hidden field so that you can retrieve from code behind
				var hdnfldVariable = document.getElementById('hdnfldVariable');
				hdnfldVariable.value = winningSegment.text;

				// Do basic alert of the segment text. You would probably want to do something more interesting with this information.
				alert("You have won " + winningSegment.text);

				var points = winningSegment.text;

				window.location.assign("PointsSpinned.aspx?points=" + points);
            }
		</script>
    </div>
          
          
        </div>
    <section class="footer-section">
      <div class="container">
          <strong>SGServe</strong> Copyright 2015
        </div>
    
    </section>




  <!-- Inline Script for colors and config objects; used by various external scripts; -->
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "real-estate",
      skins: {
        "default": {
          "primary-color": "#e74c3c"
        }
      }
    };
  </script>

  <script src="main_css/js/vendor/all.js"></script>

<%--  <script src="main_css/js/app/app.js"></script>--%>
         <script language="javascript">
             function pageLoad(sender, args) {

                 $.getScript("main_css/js/app/app.js");
             }
          </script>
</form>

</body>
</html>

﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class MyEvents : System.Web.UI.Page
    {

        private List<String> controlList
        {
            get
            {
                if (ViewState["controls"] == null)
                {
                    ViewState["controls"] = new List<String>();
                }

                return (List<String>)ViewState["controls"];
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                getEventList();
            else
            {
                foreach (string button in controlList.ToList())
                {
                    Button view = defineButton(button);
                    eventListPanel.Controls.Add(view);
                }
            }
        }


        protected async void getEventList()
        {
            String token = Session["Token"].ToString();
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveVolJoinEvent");
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            List<List<EventDetails>> listOfEventDetails = JsonConvert.DeserializeObject<List<List<EventDetails>>>(result);
            createEventList(listOfEventDetails);
        }


        protected void createEventList(List<List<EventDetails>> listofEventCategory)
        {

            foreach (List<EventDetails> listofEventDetails in listofEventCategory)
            {
                String NPO = listofEventDetails[0].connectionString;
                eventListPanel.Controls.Add(new LiteralControl("<H4>" + NPO + "</H4>"));
                eventListPanel.Controls.Add(new LiteralControl("<br/>"));

                eventListPanel.Controls.Add(new LiteralControl("<div class='row gridalicious' data-toggle='gridalicious' data-width='200' >"));
                System.Diagnostics.Debug.WriteLine(NPO + ":" + listofEventDetails.Count);
                foreach (EventDetails eventDetails in listofEventDetails)
                {
                    formEventPanel(eventDetails);
                }

                eventListPanel.Controls.Add(new LiteralControl("</div>"));
                eventListPanel.Controls.Add(new LiteralControl("<br/>"));

            }          
        }

        protected void formEventPanel(EventDetails eventDetails)
        {
            String date = eventDetails.startDate.ToShortDateString() + "-" + eventDetails.endDate.ToShortDateString();

            eventListPanel.Controls.Add(new LiteralControl("<div class ='item' data-width='50'>"));
            eventListPanel.Controls.Add(new LiteralControl("<div class ='panel panel-default relative'>"));

            // event image
            eventListPanel.Controls.Add(new LiteralControl("<div class='cover hover overlay margin - none'>"));
            eventListPanel.Controls.Add(new LiteralControl("<img src='/main_css/images/photodune-378874-real-estate-xs.jpg' alt='location' class='img-responsive' />"));
            eventListPanel.Controls.Add(new LiteralControl("</div>"));

            // event Details
            eventListPanel.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
            eventListPanel.Controls.Add(new LiteralControl("<h4 class ='margin-v-0-5'>" + eventDetails.eventName + "</h4>"));
            eventListPanel.Controls.Add(new LiteralControl("<p>" + eventDetails.description + " and it is very interesting and thers salot of aparties</p>"));
            eventListPanel.Controls.Add(new LiteralControl("<span class = 'label label-grey-300'>" + date + "</span>"));
            eventListPanel.Controls.Add(new LiteralControl("<br/>"));
            Button eventDetailsBtn = defineButton(eventDetails.eventId + "," + eventDetails.connectionString);
            eventListPanel.Controls.Add(eventDetailsBtn);
            eventListPanel.Controls.Add(new LiteralControl("</div>"));


            eventListPanel.Controls.Add(new LiteralControl("</div>"));
            eventListPanel.Controls.Add(new LiteralControl("</div>"));

        }

        protected Button defineButton(String eventId)
        {
            Button button = new Button();
            button.ID = eventId;
            button.CssClass = "btn btn-primary lightred-color";
            button.Text = "view";
            button.Click += new EventHandler(goToEventDetails);
            controlList.Add(eventId);

            return button;
        }

        protected void goToEventDetails(Object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("button does not come here");
            Button button = sender as Button;
            String[] buttonId = button.ID.Split(',');
            Session["eventId"] = buttonId[0];
            Session["connectionString"] = buttonId[1];

            Response.Redirect("EventDetail.aspx", false);
        }


    }
}
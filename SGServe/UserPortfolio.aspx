﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="UserPortfolio.aspx.cs" Inherits="SGServe.UserPortfolio" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="community_css/css/app/cover.css" rel="stylesheet">
    <!--
    <link href="community_css/css/vendor/all.css" rel="stylesheet">
    <link href="community_css/css/app/app.css" rel="stylesheet">
    <link href="community_css/css/app/timeline.css" rel="stylesheet">
    <link href="community_css/css/app/essentials.css" rel="stylesheet">
    <link href="community_css/css/app/layout.css" rel="stylesheet">
    <link href="community_css/css/app/media.css" rel="stylesheet">
    <link href="community_css/css/app/colors-text.css" rel="stylesheet">
    <link href="community_css/css/app/colors-buttons.css" rel="stylesheet">
    <script src="community_css/js/vendor/all.js"></script>
    <script src="community_css/js/app/app.js"></script>
    -->

          <div class="container">
              <br/>
            <div class="cover profile">

              <div class="wrapper">
                <div class="image" style="height:58px;">
                </div>
              </div>
                   
              <div class="cover-info">
                <div class="avatar">
                  <asp:Image ID="ProfilePicImg" runat="server" />
                </div>
                <div class="name">
                    <h2>
                        <asp:Label ID="lblFullName" runat="server" Text="Name" ForeColor="Black"></asp:Label>
                    </h2>
                            </div>
                <ul class="cover-nav">
                  <li class="active"><asp:HyperLink ID="linkAbout" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> About </asp:HyperLink></li>
                  <li><asp:HyperLink ID="linkFriends" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Friends </asp:HyperLink> </li>
                  <li><asp:HyperLink ID="linkFollows" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Follows </asp:HyperLink></li>

                    <li><asp:Button ID="btnAddFriend" runat="server" class="btn btn-info" Text="Add Friend" OnClick="btnAddFriend_Click" /></li>
                    <li><asp:Label ID="lblPendingFriends" runat="server" Text="Pending friend request"></asp:Label></li>
                    <li><asp:Panel ID="PanelF" runat="server">
                    <button class="btn btn-success disabled" type="button" data-toggle="dropdown">Friend</button></asp:Panel></li>
                    <li><asp:Button ID="btnRemoveFriend" runat="server" class="btn btn-danger" Text="Remove friend" OnClick="btnRemoveFriend_Click" /></li>
                    </ul>
              </div>
                <div class="panel panel-default">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                  <div class="panel-body" >
                        <h5><asp:Label ID="lblDescription" runat="server">description</asp:Label></h5>
                  </div>
                </div>
              </div>
            </div>

              <!-- start of row-->
              <div class="row">
              <div class="col-sm-5 col-md-5">

                 <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> About
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                        <asp:Panel ID="PanelSchool" runat="server">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">School</span></div>
                          <div class="col-sm-8">
                              <asp:Label ID="lblSchool" runat="server" Text="Label">my school name</asp:Label>
                            </div>
                        </div>
                      </li></asp:Panel>
                        <asp:Panel ID="PanelInterestsCause" runat="server">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Interests (Cause)</span></div>
                          <div class="col-sm-8">
                                    <asp:DataList ID="dlBene" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li></asp:Panel>
                        <asp:Panel ID="PanelInterestsRole" runat="server">
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Interests (Role)</span></div>
                          <div class="col-sm-8">
                              
                                    <asp:DataList ID="dlRole" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li></asp:Panel>
                        <asp:Panel ID="PanelSkills" runat="server">
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Skills</span></div>
                          <div class="col-sm-8">
                                    <asp:DataList ID="dlskill" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li></asp:Panel>
                        <asp:Panel ID="PanelMBTI" runat="server">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Personality MBTI</span>
                              </div>
                              <div class="col-sm-8">
                              <asp:Label ID="lblPersonalityMBTI" runat="server"></asp:Label>
                                </div>
                         </div>
                      </li></asp:Panel>
                        <!--
                    <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Description</span>
                              </div><br/>
                              <div class="col-sm-8">
                              <asp:Label ID="lblDescriptionx" runat="server"></asp:Label>
                                </div>
                         </div>
                      </li>
                        -->
                    </ul>
                  </div>
                  <asp:Button ID="btnEditProfile" class="btn btn-warning btn-xs" runat="server" Text="Edit Profile" PostBackUrl="~/UserPortfolioEdit.aspx" />
                <br/>
            </div>
              <!-- end of user info panel -->

                  <asp:Panel ID="panelWriteMessage" runat="server">
                <div class="timeline-block">
                  <div class="panel panel-default share clearfix-xs">
                    <div class="panel-heading panel-heading-gray title">
                      Write a message for <asp:Label ID="lblToName" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="panel-body">
                      <asp:TextBox ID="tbWriteMessage" class="form-control share-text" runat="server"></asp:TextBox>
                    </div>
                    <div class="panel-body">
                      <asp:Button ID="btnSend" class="btn btn-info btn-m pull-right" runat="server" Text="Send" OnClick="btnSend_Click" />
                    </div>
                  </div>
                </div>
                    </asp:Panel>
                  <!-- end of message input -->

             <div class="panel panel-default">
              <asp:Button ID="btnViewDelete" class="btn btn-warning btn-xs" runat="server" Text="View / Delete Messages" PostBackUrl="~/Messages.aspx" />

                 <asp:DataList ID="DataList2" runat="server">
                      <ItemTemplate>
                          <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="media">
                        <div class="media-left">
                          <a href="">
                            <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "FROM_USR_IMAGE") %> Width="60" />
                          </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="media-body">
                          <asp:Label ID="lblCommentorName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "From_USR_NAME") %>></asp:Label>
                          <span>on <asp:Label ID="lblCommentDate" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "COMMENT_DATE") %>></asp:Label></span>
                        </div>
                      </div>
                    </div>
                    <div class="panel-body">
                        <asp:Label ID="lblMessage" runat="server" style="word-wrap: break-word;" Text=<%# DataBinder.Eval(Container.DataItem, "COMMENT") %>></asp:Label>
                    </div>
                  </div>
                      </ItemTemplate>
                 </asp:DataList>

             </div>

            </div>
            <!-- end of col 5 -->

              <!-- start of user timeline -->
              <div class="col-sm-7 col-md-7">
                  
           <div class="container">
            <div class="row">
              <div class="col-md-6">
                <ul class="timeline-list">

                    <asp:DataList ID="DataList3" runat="server">
                      <ItemTemplate>
                          <li class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="user-wrapper">
                        <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "USR_IMAGE") %> Width="80" />
                        <div class="date"><asp:Label ID="lblDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ACTIVITY_DATETIME") %>'></asp:Label></div>
                      </div>
                    </div>
                    <div class="media-body">
                      <div class="media-body-wrapper">
                        <div class="panel panel-default">

                          <div class="panel-body">
                            <div class="boxed">
                              <div class="media">
                                
                                <div class="media-body">
                                    <h5>
                                        <asp:LinkButton ID="lbUserFriend" runat="server" ForeColor="Black" Text=<%# DataBinder.Eval(Container.DataItem, "USR_FULL_NAME") %>></asp:LinkButton>
                                        <asp:Label ID="lblActivityName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "MESSAGE_DISPLAY") %>></asp:Label>
                                        <asp:LinkButton ID="lblTargetName" runat="server" ForeColor="Black" Text='<%# DataBinder.Eval(Container.DataItem, "TARGET_NAME") %>' PostBackUrl='<%# string.Format("{0}", DataBinder.Eval(Container.DataItem, "EVENT_ID")) %>'></asp:LinkButton>
                                    </h5>
                                </div>
                                  <div class="media-right">
                                  <a href="#">
                                    <asp:Image ID="Image2" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "TARGET_IMAGE") %> Width="70" />
                                  </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                              </div>
                            </div>

                          </div>

                        </div>
                      </div>
                    </div>
                  </li>
                       </ItemTemplate>
                  </asp:DataList>

                </ul>
               </div>
            </div>
           </div>
         </div>

                    <!-- end of user timeline -->

             </div>
            <!-- end of row-->
           </div> 
            <!-- end of container -->
</asp:Content>

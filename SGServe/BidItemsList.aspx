﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="BidItemsList.aspx.cs" Inherits="SGServe.BidItemsList" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- content push wrapper -->
    <div class="st-pusher" id="content">

      <!-- sidebar effects INSIDE of st-pusher: -->
      <!-- st-effect-3, st-effect-6, st-effect-7, st-effect-8, st-effect-14 -->

      <!-- this is the wrapper for the content -->
      <div class="st-content">

        <!-- extra div for emulating position:fixed of the menu -->
        <div class="st-content-inner">

          <div class="cover overlay tutors height-200 height-250-xs">
            <img src="images/misc/display02.jpg" width="1600"> 
            <div class="overlay overlay-full overlay-bg-grey">
              <div class="container">
                <h1 class="text-display-1 text-overlay">Earn rewards while you volunteer </h1>
                <p class="text-subhead text-overlay">At SGServe, we apprecate effort by volunteers. <span class="hidden-sm hidden-xs">For every hour you have volunteered on an event in SGServe, you will gain 5 bid points , which can be use to bid for items listed here.</span></p>
              </div>
            </div>
          </div>
          <div class="container">
              <h2>Current Sponsored Items on Bidding </h2>

    <div class="row">
        <br>
        <asp:Panel ID="Panel1" runat="server">
            <div class="panel panel-default"style="border-radius: 3px">

                <div class="table-responsive" style="padding-left: 1%; padding-right: 1%;">
                    <br />
                    <asp:GridView ID="ItemsGridView" CellPadding="50" GridLines="None" Width="98%" CssClass="table"
                        AllowPaging="True" EmptyDataText="There are currently no items avaliable for bidding" runat="server" AutoGenerateColumns="False" PageSize="20" 
                        DataKeyNames = "BID_ITEM_ID">
                        <Columns>
                            <asp:TemplateField>
                                    <ItemTemplate>
                                       <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> Width="80" />
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ITEM_NAME" HeaderText="Item Name"/>
                            <asp:BoundField DataField="ITEM_CATEGORY_NAME" HeaderText="Category"/>
                            <asp:BoundField DataField="ITEM_DESCRIPTION" HeaderText="Item Description" />
                            <asp:BoundField DataField="QTY" HeaderText="Quantity Avaliable" />
                            <asp:BoundField DataField="MIN_POINT" HeaderText="Minimum Bidding Points" />
                            <asp:BoundField DataField="START_DATE" HeaderText="Start Date" />
                            <asp:BoundField DataField="END_DATE" HeaderText="End Date" />
                            <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="btnViewBid" CssClass="btn btn-primary btn-xs" OnClick="btnViewBid_Click">
                                        View / Bid item</asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </div>

          </div>

        </div>
        <!-- /st-content-inner -->

      </div>
      <!-- /st-content -->

    </div>
    <!-- /st-pusher -->

</asp:Content>

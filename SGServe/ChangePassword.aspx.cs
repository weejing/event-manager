﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                lblMessage.Visible = false;
                if (Session["Token"] == null)
                {
                    string a = Request.Url.AbsoluteUri;
                    if (Request.Url.AbsoluteUri.Contains(("ResetPwCode")) == true)
                    {
                        //reset password
                        lblTitle.Text = "Reset Password";
                        RequiredFieldValidator1.Enabled = false;
                        RegularExpressionValidator1.Enabled = false;
                        pnlCurrentPw.Visible = false;                   
                    }
                    else
                    {
                        Response.Redirect("login.aspx",false);
                    }
                }
                else
                {
                    //change password
                    lblTitle.Text = "Change Password";
                    RequiredFieldValidator1.Enabled = true;
                    RegularExpressionValidator1.Enabled = true;
                    pnlCurrentPw.Visible = true;
                    //get volunteer details
                    volunteer = await gm.getLoginVolDetails();
                    if (volunteer.VOL_ID == null)
                    {
                        Response.Redirect("login.aspx", false);
                    }
                }


            }

        }

        //private async Task retrieveUserDetails()
        //{
        //    Server_Connect serverConnection = new Server_Connect();
        //    HttpClient client = serverConnection.getHttpClient();
        //    String jwtToken = Session["Token"] as String;
        //    if (jwtToken != null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
        //        // add the jwt token into the authorization header for server validation
        //        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        //        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
        //        HttpResponseMessage Resp = await client.SendAsync(request);
        //        String reply = await Resp.Content.ReadAsStringAsync();
        //        volunteer = GlobalModule.getUserDetails(reply);
        //    }
        //}

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            if (lblTitle.Text == "Reset Password")
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Account/resetPw?resetpw_code=" + Request.QueryString["ResetPwCode"] + "&newpassword=" + txtPassword.Text);
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                if (reply == "false")
                {
                    lblMessage.Text = "Invalid Code";
                    lblMessage.Visible = true;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Password has been reset successfully!');window.location ='home.aspx';", true);
                }
            }
            else
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    Server_Connect serverConnection = new Server_Connect();
                    HttpClient client = serverConnection.getHttpClient();
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Account/changePw?email=" + volunteer.EMAIL + "&newpassword=" + txtPassword.Text + "&oldpassword=" + txtCurrentpw.Text);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();
                    if (reply == "true")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Password has been changed successfully!');window.location ='home.aspx';", true);
                    }
                    else
                    {
                        lblMessage.Text = "Wrong current password";
                        lblMessage.Visible = true;
                    }
                }
            }
        }

        //protected async void Button1_Click(object sender, EventArgs e)
        //{
        //    Server_Connect serverConnection = new Server_Connect();
        //    HttpClient client = serverConnection.getHttpClient();
        //    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/issueBadges?userId=055F6632-8A41-4A45-A169-69B2EC60B27F");
        //    HttpResponseMessage response = await client.SendAsync(request);
        //    String reply = await response.Content.ReadAsStringAsync();
        //    var Benetable = JsonConvert.DeserializeObject<DataTable>(reply);
        //}
    }
}
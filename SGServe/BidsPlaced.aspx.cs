﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using SGServe.Models.UserBiddingModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class BidsPlaced : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateFirstRecordList();
                }
            }
        }

        protected async void generateFirstRecordList()
        {
            try
            {

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getVolCurrentBidItemRecord?userID=" + currentUserID);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //get reply to bind to gridview
                    List<BidRecord> items = JsonConvert.DeserializeObject<List<BidRecord>>(reply);

                    if (items != null)
                    {
                        RecordGridView.DataSource = items;
                        RecordGridView.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }

        protected async void generateRecordList(String type)
        {
            try
            {

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request;
                    if (type.Equals("current"))
                    {
                        request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getVolCurrentBidItemRecord?userID=" + currentUserID);
                    }
                    else if (type.Equals("won"))
                    {
                        request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getVolBidItemRecordWon?userID=" + currentUserID);
                    }
                    else
                    {
                        request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getVolAllBidItemRecord?userID=" + currentUserID);
                    }
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //get reply to bind to gridview
                    List<BidRecord> items = JsonConvert.DeserializeObject<List<BidRecord>>(reply);

                    if (items != null)
                    {
                        RecordGridView.DataSource = items;
                        RecordGridView.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }

        protected void btnCurrent_Click(object sender, EventArgs e)
        {
            btnCurrent.CssClass = "btn btn-inverse";
            btnWon.CssClass = "btn btn-default";
            btnAll.CssClass = "btn btn-default";
            generateRecordList("current");
        }

        protected void btnWon_Click(object sender, EventArgs e)
        {
            btnCurrent.CssClass = "btn btn-default";
            btnWon.CssClass = "btn btn-inverse";
            btnAll.CssClass = "btn btn-default";
            generateRecordList("won");
        }

        protected void btnAll_Click(object sender, EventArgs e)
        {
            btnCurrent.CssClass = "btn btn-default";
            btnWon.CssClass = "btn btn-default";
            btnAll.CssClass = "btn btn-inverse";
            generateRecordList("all");
        }

        protected void btnViewBid_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            String bidItemID = RecordGridView.DataKeys[row.RowIndex].Value.ToString();

            Response.Redirect("BidItemInfo.aspx?bidItemID=" + bidItemID);

        }
    }
}
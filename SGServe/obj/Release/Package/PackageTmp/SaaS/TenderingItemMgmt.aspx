﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="TenderingItemMgmt.aspx.cs" Inherits="SGServe.SaaS.TenderingItemMgmt" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h4 class="page-section-heading">Tender Head Details</h4>
      
    </div>
      <div class="panel panel-default">
                
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Product</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblCat" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Tender Host</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblHost" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Start Date</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblStartDate" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>    
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">End Date</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblEndDate" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>   
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Status</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>  
                            <asp:Button ID="Button1" runat="server" class="btn btn-success" Text="Create New Tender Item" OnClick="Button1_Click" />
   <asp:Button ID="Button2" runat="server" class="btn btn-danger btn-sm" Text="Close Tender" OnClick="Button2_Click" />
    <asp:Button ID="Button3" runat="server" class="btn btn-success" Text="Start Tender" OnClick="Button3_Click" Visible="False" />                          
                    </ul>
                    
                  </div>
             
                </div>
      <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h4 class="page-section-heading">Manage Tender Items</h4>
      
    </div>
    
      <div class="panel panel-default">
                

            <asp:GridView CellPadding="50"
                    GridLines="None" Height="24px" Width="100%" CssClass="table"
                    EmptyDataText="No NPO participated, please participate!" ID="TenderItemGrid" DataKeyNames="TENDER_ITEM_ID" runat="server" AutoGenerateColumns="False" OnDataBound="TenderItemGrid_DataBound">
                    <Columns>

                        <asp:BoundField DataField="TENDER_DESCRIPTION" HeaderText="Description"></asp:BoundField>
                        <asp:BoundField DataField="QTY" HeaderText="Quantity"></asp:BoundField>
                        <asp:BoundField DataField="ORG_NAME" HeaderText="Requested By"></asp:BoundField>
                        <asp:BoundField DataField="TENDER_STATUS" HeaderText="Status"></asp:BoundField>
                        <asp:BoundField DataField="WINNER_GROUP_FIRM_ID" HeaderText="Winner (Group)"></asp:BoundField>
                        <asp:BoundField DataField="WINNER_SOLO_FIRM_ID" HeaderText="Winner (Individual)"></asp:BoundField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkView" OnClick="lnkView_Click">View Tender Item</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle ForeColor="#0066CC" />
                    <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                </asp:GridView>

      </div>


   
</asp:Content>

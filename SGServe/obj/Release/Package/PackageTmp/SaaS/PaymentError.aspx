﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="PaymentError.aspx.cs" Inherits="SGServe.SaaS.PaymentError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="panel panel-default">
        <div class="page-section" style="padding: 1%; padding-left: 5%;">
            <h2>Opps, there seems to be an error.</h2>
            <p>No transaction has been made with Paypal</p>
            <asp:Label ID ="Label1" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>

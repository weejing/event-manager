﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="ViewEventLogisticList.aspx.cs" Inherits="SGServe.SaaS.ViewEventLogisticList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
      <h4 class="page-section-heading">Event Logistic Listing</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <table align="left" class="auto-style1">

                    <tr>
                        <td>Event Name:&nbsp;&nbsp;</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="tbEventName" runat="server" Width="250px" Height="30px"></asp:TextBox> &nbsp;&nbsp;&nbsp;</td>
                        <td> Event Session Name: &nbsp;&nbsp;</td>
                        <td><asp:TextBox ID="tbEventSessionName" runat="server" Width="250px" Height="30px" ></asp:TextBox> </td>
                        <td>
                           &nbsp;&nbsp;&nbsp;  <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-success" OnClick="btnSearch_Click" > <i class="fa fa-search"></i>Search </asp:LinkButton></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <br />

                <div class="table-responsive">
                    <asp:GridView ID="grdEventLogistic" runat="server" AutoGenerateColumns="False" CellPadding="50" ForeColor="#333333" GridLines="None"
                        Height="24px" Width="100%"
                        CssClass="table" AllowPaging="True" DataKeyNames="EVENT_LOG_ID,EVENT_NAME" EmptyDataText="There is current no event logistic item request!" ShowHeaderWhenEmpty="True" OnPageIndexChanging="grdEventLogistic_PageIndexChanging">

                        <Columns>
                            <asp:BoundField DataField="EVENT_LOG_ID" HeaderText="Event Logistic ID&nbsp;&nbsp;" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="EVENT_NAME" HeaderText="Event Name&nbsp;&nbsp;" ReadOnly="True" />
                             <asp:BoundField DataField="SESSION_NAME" HeaderText="Event Session Name&nbsp;&nbsp;" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Action&nbsp;&nbsp;&nbsp;&nbsp;">
                                <ItemTemplate>
                              <asp:LinkButton ID="btnView" CommandName="View" runat="server" class="btn btn-default btn-xs" OnClick="btnView_Click" >View</asp:LinkButton>
                                    </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
                
            </div>
        </div>
    </div>
</asp:Content>

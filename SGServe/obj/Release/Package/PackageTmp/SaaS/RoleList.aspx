﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="RoleList.aspx.cs" Inherits="SGServe.SaaS.RoleList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="page-section">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
                <div class="row">
                    <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                        <h4 class="page-section-heading">Roles</h4>
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-success" OnClick="btnAdd_Click" /><br>
                        <br>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <asp:GridView CellPadding="50" GridLines="None" Height="24px" CssClass="table"
                                        AllowPaging="True" EmptyDataText="There is no record!" ID="gv" DataKeyNames="ROLE_ID" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gv_PageIndexChanging" PageSize="20" OnRowEditing="gv_RowEditing">
                                        <Columns>
                                             <asp:TemplateField HeaderText="Role">
                                            <ItemTemplate>
                                                <asp:Label ID="lblroleName" runat="server" Text='<%#Eval("ROLE_NAME") %>' />
                                            </ItemTemplate>     
                                                    </asp:TemplateField>                                      
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                     <asp:LinkButton ID="lbtnView" CommandName="View" runat="server" class="btn btn-default btn-xs" OnClick="lbtnView_Click">Access rights</asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnEdit" CommandName="edit" runat="server" class="btn btn-info btn-xs">Edit</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure you want to delete?"></ajaxToolkit:ConfirmButtonExtender>
                                                <asp:LinkButton runat="server" ID="btnDelete" CssClass="btn btn-danger btn-xs" OnClick="btnDelete_Click">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle ForeColor="#0066CC" />
                                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <ajaxToolkit:ModalPopupExtender ID="mp" BackgroundCssClass="modalBackground" runat="server" PopupControlID="PopupPanel" TargetControlID="lnkDummy" CancelControlID="btnClose"></ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="PopupPanel" runat="server" CssClass="modal-content" Style="display: none">
                    <div class="modal-header">
                        <asp:Button ID="btnClose" class="btn btn-stroke btn-circle btn-default btn-sm" runat="server" Text="X" Style="float: right" /><h4 class="modal-title">
                            <asp:Label ID="lblMdTitle" runat="server" Text=""></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:HiddenField ID="hiddenID" runat="server" />
                        <div class="row">
                            <div class="col-md-3">
                                Role:
                            </div>
                            <div class="col-md-9">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="Please fill up" ValidationGroup="valiGroup" ForeColor="Red" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:TextBox class="form-control" ID="txtName" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnSave" class="btn btn-success btn-sm" ValidationGroup="valiGroup" runat="server" Text="Save" OnClick="btnSave_Click" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
                        </div>
</asp:Content>

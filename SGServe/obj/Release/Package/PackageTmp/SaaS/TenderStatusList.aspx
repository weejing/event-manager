﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async ="true" AutoEventWireup="true" CodeBehind="TenderStatusList.aspx.cs" Inherits="SGServe.SaaS.TenderStatusList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .modalPopup {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
            padding: 0;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
                margin-bottom: 5px;
            }

        .center {
            margin-left: 35%;
            width: 100%;
        }
    </style>
    <h4 class="page-section-heading">Tender Request Status Listing</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <table align="left" class="auto-style1" style="width: 460px">

                    <tr>
                        <td>Tender Status:</td>
                        <td>
                            <asp:DropDownList ID="ddlTenderStatus" runat="server" Height="30px" Width="275px" AutoPostBack="True">
                                <asp:ListItem>- Select a Status -</asp:ListItem>
                                <asp:ListItem>Delivery Confirmed</asp:ListItem>
                                <asp:ListItem>Goods Accepted</asp:ListItem>

                            </asp:DropDownList></td>
                        <td>
                            <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-success" OnClick="btnSearch_Click">
                                <i class="fa fa-search"></i>
                                 Search
                            </asp:LinkButton></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <br />

                <div class="table-responsive">
                    <asp:GridView ID="grdTenderStatus" runat="server" AutoGenerateColumns="False" CellPadding="50" ForeColor="#333333" GridLines="None"
                        Height="24px" Width="100%"
                        CssClass="table" AllowPaging="True" DataKeyNames="TENDER_ITEM_ID,NAME,PRODUCT_NAME,PRODUCT_DESC,PRODUCT_BRAND" EmptyDataText="There is currently no tender requests in progress!" ShowHeaderWhenEmpty="True" OnPageIndexChanging="grdTenderStatus_PageIndexChanging" OnRowCancelingEdit="grdTenderStatus_RowCancelingEdit" OnRowEditing="grdTenderStatus_RowEditing" OnRowUpdating="grdTenderStatus_RowUpdating" OnRowDataBound="grdTenderStatus_RowDataBound" OnRowCommand="grdTenderStatus_RowCommand">

                        <Columns>
                            <asp:BoundField DataField="TENDER_ITEM_ID" HeaderText="Tender Item ID&nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="BID_PRICE" HeaderText="Bid Price&nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="BID_DATE" HeaderText="Bid Date&nbsp;&nbsp;" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" runat="server" Text='<%#Eval("STATUS") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblstatus2" runat="server" Visible="false" />
                                    <asp:DropDownList ID="ddlStatus" runat="server" Width="100px">
                                        <asp:ListItem>Goods Accepted</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NAME" HeaderText="Item Name &nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="PRODUCT_NAME" HeaderText="Product Name &nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="PRODUCT_DESC" HeaderText="Product Description &nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="PRODUCT_BRAND" HeaderText="Product Brand &nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="DELIVERY_TIME" HeaderText="Delivery Time &nbsp;&nbsp;" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Action&nbsp;&nbsp;&nbsp;&nbsp;">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="btnEdit" CssClass="btn btn-success btn-xs" CommandName="edit">
                                <i class="fa fa-edit"></i>
                                 Edit
                                    </asp:LinkButton>
                                    <itemstyle width="50px" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" ID="btnUpdate" CssClass="btn btn-success btn-xs " CommandName="Update">
                                <i class="fa fa-check-circle"></i>
                                Update
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnCancel" CssClass="btn btn-warning btn-xs " CommandName="Cancel">
                                <i class="fa fa-times-circle"></i>
                                Cancel
                                    </asp:LinkButton>
                                    <itemstyle width="50px" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
                <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BehaviorID="mpe" runat="server"
                    PopupControlID="pnlPopup" TargetControlID="lnkDummy" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">
                    </div>
                    <div class="body">
                        <asp:Label ID="lblrate" runat="server" Text=" Rate your vendor!"></asp:Label>
                        <p align="center">
                            <ajaxToolkit:Rating ID="Rating1" runat="server" CurrentRating="5" OnChanged="Rating1_Changed"
                                StarCssClass="fa fa-fw fa-star-o text-white" WaitingStarCssClass="fa fa-fw fa-star-o text-white" EmptyStarCssClass="fa fa-fw fa-star-o text-white"
                                FilledStarCssClass="fa fa-fw fa-star text-yellow-800" Direction="NotSet" HorizontalAlign="Center" CssClass="center">
                            </ajaxToolkit:Rating>
                            <p>
                            </p>
                            <br />
                            <%--<asp:Button ID="btnHide" runat="server" Text="Hide Modal Popup" />--%>
                            <asp:Button ID="btnSendRating" runat="server" Text="Rate" OnClick="btnSendRating_Click" />
                        </p>
                    </div>
                </asp:Panel>
                
            </div>
        </div>
    </div>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="ManageBill.aspx.cs" Inherits="SGServe.ManageBill" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="panel panel-default">
        <div class="page-section" style="padding: 1%; padding-left: 5%;">
            <asp:Panel ID="infoPanel" runat="server">
                <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">Welcome back!</h4> 
                    Miss SGServe? We have some fabuluous offers for you. Click on <strong>Resubscribe</strong> to know more!
                </div>
            </asp:Panel>
            <asp:Label ID="lblPayMoney" runat="server" Text="Please pay money in order to access other functions" ForeColor="Red" Font-Bold="True"></asp:Label>
            <div class="row">
                <div class="col-lg-2">
                    <h2>Manage Billings</h2>
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-4">
                    <span class="pull-right">
                        <asp:Button ID="Button_payment" runat="server" Text="Resubscribe" CssClass="btn btn-primary" OnClick="makePayment" />
                        <asp:Button ID="Button_cancel" runat="server" Text="Cancel Payment" CssClass="btn btn-primary" OnClick="cancelPayment" />
                        <%--<a href ="PaymentPlans.aspx" class = "btn btn-default">Make Payment</a>--%>
                    </span>
                </div>
            </div>

            <h4>Payment Status</h4>
            <div class="row" style="padding-top: 10px;">
                <div class="col-lg-4">
                    <asp:Label ID="Label1" runat="server" Text="Payment due on "></asp:Label>
                    <asp:TextBox ID="TextBox1" runat="server" class="form-control input-sm" ReadOnly="True"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="padding-top: 10px;">
                <div class="col-lg-4">
                    <asp:Label ID="Label2" runat="server" Text="Payment type "></asp:Label>
                    <asp:TextBox ID="TextBox2" runat="server" class="form-control input-sm" ReadOnly="True"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="padding-top: 10px;">
                <div class="col-lg-4">
                    <asp:Label ID="Label3" runat="server" Text="Plan Name"></asp:Label>
                    <asp:TextBox ID="TextBox3" runat="server" class="form-control input-sm" ReadOnly="True"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="padding-top: 10px;">
                <div class="col-lg-4">
                    <asp:Label ID="Label4" runat="server" Text="Plan Duration"></asp:Label>
                    <asp:TextBox ID="TextBox4" runat="server" class="form-control input-sm" ReadOnly="True"></asp:TextBox>
                </div>
            </div>
        </div>
        <div style="height: 10px;"></div>
        <div style="padding: 5%;">
            <h4>Past Payment</h4>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table">
                <Columns>
                    <asp:BoundField DataField="PAYMENT_TIME" HeaderText="Payment Time" SortExpression="PAYMENT_TIME" />
                    <asp:BoundField DataField="PLAN_TYPE" HeaderText="Plan Name" SortExpression="PLAN_TYPE" />
                    <asp:BoundField DataField="AMT" HeaderText="Amount" SortExpression="AMT" />
                    <asp:BoundField DataField="FROM_DATE" HeaderText="From" SortExpression="FROM_DATE" />
                    <asp:BoundField DataField="TO_DATE" HeaderText="To" SortExpression="TO_DATE" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="ManageAccount.aspx.cs" Inherits="SGServe.SaaS.ManageAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
   
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 
    <div class="page-section">
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <h4 class="page-section-heading">Manage Account</h4>
                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-success" OnClick="btnAdd_Click" /><br>
                <br>
                  <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                <div class="panel panel-default">
                    <br>
                     <div class="row">
                                 <div class="col-lg-1" style="margin-left:10px">                     
                              Name: </div>
                 <div class="col-lg-6"><asp:TextBox ID="txtName" runat="server" class="form-control input-sm" ></asp:TextBox> 
                      </div>
                         <div class="col-lg-1">  
                             <asp:LinkButton ID="btnSearch" CssClass="btn btn-circle btn-primary btn-sm btn-stroke" runat="server" OnClick="btnSearch_Click"> <i class="fa fa-search"></i></asp:LinkButton>                    
                         </div>
                     </div> <br/>
                      <div class="table-responsive">
                         
                               <asp:GridView CellPadding="50" GridLines="None" Height="24px" Width="100%" CssClass="table"
                  AllowPaging="True" EmptyDataText="There is not account!" ID="gvAccounts" DataKeyNames="USR_ID" runat="server"  AutoGenerateColumns="False" OnPageIndexChanging="gvAccounts_PageIndexChanging" PageSize="20">
                       <Columns>
                            <asp:BoundField DataField="NAME" SortExpression="NAME" HeaderText="Name"></asp:BoundField>
                            <asp:BoundField DataField="EMAIL" SortExpression="EMAIL" HeaderText="Email"></asp:BoundField>
                             <asp:BoundField DataField="roleNames" SortExpression="roleNames" HeaderText="Roles"></asp:BoundField>
                        <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnView" CommandName="View" runat="server" class="btn btn-default btn-xs" OnClick="lbtnView_Click">View</asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                </asp:TemplateField> 
                           </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                   <PagerStyle HorizontalAlign = "Right" CssClass = "pagination-ys" />
                    </asp:GridView>
                </div>

            </div>
                      </asp:Panel>
            </div>
        </div>
 </div>
     
</asp:Content>

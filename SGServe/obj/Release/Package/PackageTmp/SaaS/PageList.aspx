﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="PageList.aspx.cs" Inherits="SGServe.SaaS.PageList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style type="text/css">
    .rootNode
    {
        Font:Sans-Serif;
         color:Black;
          font-weight: bold;
           background-color:#FAFAFA;
            border-color:#FAFAFA;
             border-radius:5px;
              padding-right:10%;
              table-layout:fixed;
    }

    .leafNode
    {
      Font:Sans-Serif;
         color:Black;
           background-color:#FAFAFA;
            border-color:#FAFAFA;
             border-radius:5px;
             width:100px;
              padding-right:10%;
               table-layout:fixed;
    }

    .selectNode
    {
        /*font-weight: bold;*/
        background-color:#b4eee9;
        color:Black;
        border-radius:5px;
         table-layout:fixed;
      
    }

</style>
    <div class="page-section">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
                <div class="row">
                    <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                        <h4 class="page-section-heading">
                            <asp:Label ID="lbltitle" runat="server" Text=""></asp:Label></h4>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="Root" width="100%">
                                    <HoverNodeStyle cssclass="selectNode" />
                                    <RootNodeStyle cssclass="rootNode"></RootNodeStyle>
                                    <LeafNodeStyle cssclass="leafNode"/>
                                </asp:TreeView>
                            </div>
                        </div>
                        <asp:Button ID="btnSave" runat="server" class="btn btn-success" ValidationGroup="createGrp" Text="Save" OnClick="btnSave_Click" /><br />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

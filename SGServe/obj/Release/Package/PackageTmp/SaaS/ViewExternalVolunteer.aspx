﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="ViewExternalVolunteer.aspx.cs" Inherits="SGServe.SaaS.ViewExternalVolunteer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
        <h4 class="page-section-heading">External Volunteers Particulars</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="table-responsive">
                    <asp:GridView ID="grdExternalVolunteer" runat="server" AutoGenerateColumns="False" CellPadding="50" ForeColor="#333333" GridLines="None"
                        Height="24px" Width="100%"
                        CssClass="table" AllowPaging="True" DataKeyNames="NAMELIST_ID,EMAIL,NAME" EmptyDataText="There is no records!" ShowHeaderWhenEmpty="True" OnPageIndexChanging="grdExternalVolunteer_PageIndexChanging">

                        <Columns>
                            <asp:TemplateField ItemStyle-Width="40px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkVol" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
       <asp:BoundField DataField="NAMELIST_ID" HeaderText="ID&nbsp;&nbsp;" ReadOnly="True" Visible="false"/>
                            <asp:BoundField DataField="NAME" HeaderText="Name&nbsp;&nbsp;" ReadOnly="True" />
                             <asp:BoundField DataField="PHONENO" HeaderText="Contact Number&nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="EMAIL" HeaderText="Email&nbsp;&nbsp;" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Action&nbsp;&nbsp;&nbsp;&nbsp;">
                                <ItemTemplate>
                        <asp:LinkButton ID="btnSendInvite" CommandName="Send" runat="server" class="btn btn-default btn-xs" OnClick="btnSendInvite_Click"  >Send Invite</asp:LinkButton>
                                  
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure you want to delete?"></ajaxToolkit:ConfirmButtonExtender>
                                    <asp:LinkButton runat="server" ID="btnDelete" CssClass="btn btn-danger btn-xs" OnClick="btnDelete_Click">
                                <i class="fa fa-remove"></i>
                                 Delete
                                    </asp:LinkButton>

                                </ItemTemplate>
                              
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                    <asp:Button ID="btnSendAll" runat="server" Text="Send All" CssClass="btn btn-default" OnClick="btnSendAll_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

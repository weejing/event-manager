﻿<%@ Page Title="" Language="C#" Async ="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="CreateEventSession.aspx.cs" Inherits="SGServe.SaaS.CreateEventSession" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">


    <div class ="page-section">

        <div class =" row">
            <div class ="col-lg-10 col-lg-offset-1">
                  <h4 class="page-section-heading">Event Session</h4>
                    <div class="wizard-container wizard-1" id="wizard-demo-1">
                  <div data-scrollable-h>
                        <ul class="wiz-progress">
                            <li>Create Event</li>
                            <li class="active">Event Session</li>
                            <li>Event Logistics</li>
                        </ul>
                    </div>
                        </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:placeholder id ="eventSessionPanel" runat="server"></asp:placeholder>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                <br />
                <asp:Button ID="createSession" runat="server" Text="Next" class="btn btn-primary" OnClick="createSession_Click" />
            </div>
        </div>
    </div>  

    <script>
        function checkRoles()
        {
            // grab the id that calls this function 
            var id = $(this).parent().attr('id');

            return false;
        }
    </script>
    
    
</asp:Content>



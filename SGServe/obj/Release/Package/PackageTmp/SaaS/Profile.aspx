﻿<%@ Page Title="" Language="C#" Async ="true"  MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="SGServe.SaaS.Profile" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
     
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
       <div class="panel panel-default panel-primary">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> Particulars
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Name</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Email</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Role</span></div>
                          <div class="col-sm-8">
                                 <asp:DataList ID="dlRole" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("ROLE_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li>                        
                    </ul>
                      <div class="row">
                       <div class="col-md-6">
                           <a href="ChangePassword">Change Password</a>
 </div>
                </div>
                  </div>
                </div>
</asp:Content>

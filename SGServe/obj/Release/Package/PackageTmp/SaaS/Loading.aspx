﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Loading.aspx.cs" Async="true" Inherits="SGServe.SaaS.Loading" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:600,300" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="Loading/demo.css"/>
  <link rel="stylesheet" type="text/css" href="loaders.css.js"/>
</head>
<body>
   <main>
    <header>
      <div class="left">
        <h1>Collecting Firewood...Building Your House<span>...</span></h1>
        <h2>Creating your personalized instance</h2>
      </div>
   
    </header>
    <div class="loaders">
      <div class="loader">
        <div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div class="loader-inner ball-rotate">
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div class="loader-inner ball-rotate">
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div class="loader-inner ball-rotate">
          <div></div>

        </div>
      </div>
      <div class="loader">
        <div class="loader-inner ball-rotate">
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div class="loader">
        <div >
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  </main>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      document.querySelector('main').className += 'loaded';
    });
  </script>
</body>
</html>

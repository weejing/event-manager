﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="UsersFollowed.aspx.cs" Inherits="SGServe.SaaS.UsersFollowed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

     <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <h4 class="page-section-heading">Followers</h4>
     <div class="panel panel-default">
         <div class="row">
                <div class="col-lg-12" style="background-color: #fad0d0; padding: 2%;">
                    <asp:Label ID="Label3" runat="server" Text="0" Font-Size="X-Large"></asp:Label>
                    <p>Total Number of Volunteer followed</p>
                </div>
             </div>
         <div class="table-responsive">
    <asp:GridView CellPadding="50" GridLines="None" Height="24px" Width="100%" CssClass="table" AllowPaging="True" EmptyDataText="There are currently no followers for your organisation." ID="FollowersItemGrid" DataKeyNames="USR_ID" runat="server" AutoGenerateColumns="False" >
                            <Columns>
                                <asp:BoundField DataField="FULL_NAME" HeaderText="Full Name"></asp:BoundField>
                                <asp:BoundField DataField="PERSONALITY_CHAR" HeaderText="MBTI"></asp:BoundField>

                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                                                <asp:HyperLink ID="HyperLink2" class="btn btn-default btn-xs" runat="server" NavigateUrl='<%# string.Format("~/UserPortfolioView.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>' Target="_blank">View</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle ForeColor="#0066CC" />
                            <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                        </asp:GridView></div></div></div>
</asp:Content>

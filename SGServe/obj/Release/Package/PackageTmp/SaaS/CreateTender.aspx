﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true"  AutoEventWireup="true" CodeBehind="CreateTender.aspx.cs" Inherits="SGServe.SaaS.CreateTender" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
  <%--  <a class="navbar-brand hidden-xs navbar-brand-primary">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </a>--%>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="page-section">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h4 class="page-section-heading">
                    <asp:Label ID="lblHeader" runat="server" Text="Create New Tender"></asp:Label></h4>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="form-group form-control-default required">
                                    <label>Type</label>

                                  
                                    <asp:DropDownList ID="ddlCategoryLvl1" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryLvl1_SelectedIndexChanged">
                                    </asp:DropDownList><br />
                                    
                                   
                                </div>

                                 <div class="form-group form-control-default required">
                                    <label>Category
                                        </label>
                                 
                                    <asp:DropDownList ID="ddlCategoryLvl2" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryLvl2_SelectedIndexChanged">
                                    </asp:DropDownList><br />
                                </div>

                                 <div class="form-group form-control-default required">
                                    <label>Item</label>

                              
                                    <asp:DropDownList ID="ddlCategoryLvl3" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryLvl3_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group form-control-default required">
                            <label>
                                Description<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="DescTxt"
                                    ErrorMessage=" Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                            </label>

                            <asp:TextBox ID="DescTxt" class="form-control" TextMode="multiline" runat="server" />

                        </div>

                        <div class="form-group form-control-default required">
                            <label>
                                Start Date<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="StartDateTxt"
                                    ErrorMessage=" Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                            <div>
                                <asp:TextBox ID="StartDateTxt" class="form-control datepicker" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group form-control-default required">
                            <label>End Date<asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="EndDateTxt"
                                    ErrorMessage=" Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                            <div>
                                <asp:TextBox ID="EndDateTxt" class="form-control datepicker" runat="server"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group form-control-default required">
                            <label>
                                Quantity Needed<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="QtyTxt"
                                    ErrorMessage=" Please fill in" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="QtyTxt" ValidationGroup="createGrp" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage=" Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator></label>
                            <div>
                                <asp:TextBox ID="QtyTxt" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>


  <asp:Button ID="Button1" runat="server" Text="Create Tender" class="btn btn-success" ValidationGroup="createGrp" OnClick="Button1_Click" />
                    </div>
                  
                </div>


            </div>
        </div>
    </div>





















</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" Async="true" CodeBehind="CreateVIATemplate.aspx.cs" Inherits="SGServe.SaaS.CreateVIATemplate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <asp:LinkButton ID="lnkHelpText" runat="server"></asp:LinkButton>
    <h4 class="page-section-heading">Create Organisation CIP Letter Template 
        <asp:Button ID="Button1" class="btn btn-circle btn-primary btn-sm" runat="server" Text="?" Style="float: right" OnClick="Button1_Click" /></h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <table>
                <tr>
                    <td>
                        <h3 class="media-heading margin-v-5">Letter Header:</h3>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="media-heading margin-v-5">Organisation Logo:&nbsp;&nbsp; </h4>
                    </td>
                    <td>
                        <asp:FileUpload ID="uploadLogo" runat="server" />
                        <asp:Label ID="lbllogo" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:HiddenField ID="logoHidden" runat="server" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="height: 33px">
                        <h3 class="media-heading margin-v-5">Letter Body:</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnVIALetterTemplate" CssClass="btn btn-primary btn-xs" Visible="false" runat="server" OnClick="btnVIALetterTemplate_Click">VIA Letter Template</asp:LinkButton>
                        <br />
                    </td>

                </tr>
            </table>

            <ajaxToolkit:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" EnableSanitization="false" TargetControlID="tbBody">
            </ajaxToolkit:HtmlEditorExtender>
            <asp:TextBox ID="tbBody" runat="server" TextMode="MultiLine" Height="425px" Width="100%" Wrap="False"></asp:TextBox>
            <br />
            <table>
                <tr>
                    <td class="width-140" style="width: 145px">
                        <h3 class="media-heading margin-v-5">Letter Footer:</h3>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="width-140" style="width: 145px">
                        <h5 class="media-heading margin-v-5">Organiser Name:</h5>
                        <br />
                    </td>
                    <td>
                        <asp:TextBox ID="tbOrganiserName" runat="server" Width="317px"></asp:TextBox>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="width-140" style="width: 145px">
                        <h5 class="media-heading margin-v-5">Department Name:</h5>
                        <br />
                    </td>
                    <td>
                        <asp:TextBox ID="tbDepartmentName" runat="server" Width="317px"></asp:TextBox>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="width-140" style="width: 145px">
                        <h5 class="media-heading margin-v-5">Committee:</h5>
                        <br />
                    </td>
                    <td>
                        <asp:TextBox ID="tbComittee" runat="server" Width="317px"></asp:TextBox>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="width-140" style="width: 145px">
                        <h5 class="media-heading margin-v-5">Organisation Name:</h5>
                        <br />
                    </td>
                    <td>
                        <asp:TextBox ID="tbOrganisationName" runat="server" Width="317px"></asp:TextBox>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnCreate" runat="server" Text="Create" CssClass="btn btn-success" OnClick="btnCreate_Click" />
                    </td>
                </tr>
            </table>
            <ajaxToolkit:ModalPopupExtender ID="mpHelpText" BackgroundCssClass="modalBackground" runat="server" PopupControlID="HelpTextPanel" TargetControlID="lnkHelpText" CancelControlID="btnHelpClose"></ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="HelpTextPanel" runat="server" CssClass="modal-lg" Style="display: none;">
                <div class="modal-content">
                    <div class="modal-header">
                        <asp:Button ID="btnHelpClose" class="btn btn-stroke btn-circle btn-default btn-sm" runat="server" Text="X" Style="float: right" /><h4 class="modal-title">
                            <asp:Label ID="Label2" runat="server" Text="CIP Letter Creation Guide"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <b>CIP Letter Template Creation Guide</b>
                        <br />
                        Organisation are required to create/define their CIP Letter Template to use to generate user CIP event hours.<br />
                        1) Dynamic text fields should be enclosed within the '@' alias. E.g @volunteername@<br />
                        2) Any new updates done will replace the old template upon saving.<br />
                        <br />
                        <div>
                            <b>Example CIP Letter Template</b><br /><hr />
                            
                            @date@
                            <br />
                            <br />
                            To Whom It May Concern<br />
                            <br />
                            CERTIFICATION OF HOURS VOLUNTEERED IN @eventname@<br />

                            We would like to extend our sincere thanks to the following volunteer at the recent @eventname@. It is individuals like him/her which had made a big difference to the community we are living time.
                            <br />
                            <br />
                            Volunteer Information<br />
                            <br />
                            Name: @volunteername@<br />
                            NRIC: @volunteernric@<br />
                            Date: @eventdate@<br />
                            Total Duration: @viahours@<br />

                            <hr />
                        </div>

                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="EditInventoryItems.aspx.cs" Inherits="SGServe.SaaS.EditInventoryItems" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style type="text/css">
        .BottomBorder td {
            border-bottom: solid 1px black;
            padding: 5px;
            vertical-align: middle;
        }

        .auto-style1 {
            width: 795px;
        }

        .auto-style2 {
            width: 293px;
        }
    </style>
    <h4 class="page-section-heading">Inventory Items Listing</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <table align="left">
                    <tr>
                        <td>

                            <asp:LinkButton runat="server" ID="btnAddItem" CssClass="btn btn-success" OnClick="btnAddItem_Click">
   <i class="fa fa-plus"></i>
    Add Item
                            </asp:LinkButton>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton runat="server" ID="btnViewRequest" CssClass="btn btn-warning" OnClick="btnViewRequest_Click">
   <i class="fa fa-list-ol"></i>
    View Tender Request
                            </asp:LinkButton></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />


                <table align="left" class="auto-style1">

                    <tr>
                        <td>Name: </td>
                        <td class="auto-style2">
                            <asp:TextBox ID="tbItemName" runat="server" Width="275px" Height="30px"></asp:TextBox></td>
                        <td>Category:</td>
                        <td>
                            <asp:DropDownList ID="ddlItemType" runat="server" Height="30px" Width="275px" AutoPostBack="True">
                                <asp:ListItem>- Select an Item Type -</asp:ListItem>
                                <asp:ListItem>Consumable</asp:ListItem>
                                <asp:ListItem>Non-consumable</asp:ListItem>
                            </asp:DropDownList></td>
                        <td>
                            <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-success" OnClick="btnSearch_Click">
                                <i class="fa fa-search"></i>
                                 Search
                            </asp:LinkButton></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <br />

                <div class="table-responsive">
                    <asp:GridView ID="grdInventory" runat="server" AutoGenerateColumns="False" CellPadding="50" ForeColor="#333333" GridLines="None"
                        Height="24px" Width="100%"
                        CssClass="table" AllowPaging="True" DataKeyNames="IVT_NAME" EmptyDataText="Inventory is empty!" ShowHeaderWhenEmpty="True" OnPageIndexChanging="grdInventory_PageIndexChanging" OnRowCancelingEdit="grdInventory_RowCancelingEdit" OnRowEditing="grdInventory_RowEditing" OnRowUpdating="grdInventory_RowUpdating" PageSize="5">

                        <Columns>
                            <asp:BoundField DataField="IVT_NAME" HeaderText="Name&nbsp;&nbsp;" ReadOnly="True" />
                            <asp:BoundField DataField="DESCRIPTION" HeaderText="Description&nbsp;&nbsp;" ReadOnly="True" />

                            <asp:TemplateField HeaderText="Quantity">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" runat="server" Text='<%#Eval("QTY") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="tbqty" runat="server" Width="40px" Text='<%#Eval("QTY") %>' />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemtype" runat="server" Text='<%#Eval("ITEM_TYPE") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlItemtype2" runat="server" Width="100px">
                                        <asp:ListItem>Consumable</asp:ListItem>
                                        <asp:ListItem>Non-Consumable</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Threshold">
                                <ItemTemplate>
                                    <asp:Label ID="lblthreshold" runat="server" Text='<%#Eval("THRESHOLD") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="tbthreshold" runat="server" Width="40px" Text='<%#Eval("THRESHOLD") %>' />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action&nbsp;&nbsp;&nbsp;&nbsp;">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="btnEdit" CssClass="btn btn-success btn-xs" CommandName="edit">
                                <i class="fa fa-edit"></i>
                                 Edit
                                    </asp:LinkButton>
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure you want to delete?"></ajaxToolkit:ConfirmButtonExtender>
                                    <asp:LinkButton runat="server" ID="btnDelete" CssClass="btn btn-danger btn-xs" OnClick="btnDelete_Click">
                                <i class="fa fa-remove"></i>
                                 Delete
                                    </asp:LinkButton>

                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" ID="btnUpdate" CssClass="btn btn-success btn-xs" CommandName="Update">
                                <i class="fa fa-check-circle"></i>
                                Update
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnCancel" CssClass="btn btn-warning btn-xs" CommandName="Cancel">
                                <i class="fa fa-times-circle"></i>
                                Cancel
                                    </asp:LinkButton>

                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>


            </div>
        </div>
    </div>
</asp:Content>

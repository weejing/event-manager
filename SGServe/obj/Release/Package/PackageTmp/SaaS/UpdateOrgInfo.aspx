﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" Async="true" CodeBehind="UpdateOrgInfo.aspx.cs" Inherits="SGServe.SaaS.UpdateOrgInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="page-section">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h4 class="page-section-heading">Organisation Particulars</h4>
                    
      
                        <div class="panel panel-default">                                 
                            <div class="panel-body">
                                
                                <div class="row">
                                  
                                    
                                  
                                     
                                    <div class="col-md-12">
                                         <div class="form-group form-control-default required">
                                            <label>
                                                Organisation Name
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtOrgName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>    
                                            </label>
                                            <asp:TextBox ID="txtOrgName" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                             
                                        <div class="form-group form-control-default">
                                            <label>Organisation Email  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtEmail"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>    </label>
                                            <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                         <div class="form-group form-control-default">
                                            <label>Website Link
                                             <%--   <asp:CustomValidator ID="CustomValidator1" runat="server" OnServerValidate="validateWebsite" ErrorMessage="Wrong Website format" ForeColor="Red" ControlToValidate="txtLink" ValidationGroup="createGrp"></asp:CustomValidator> --%>  </label>
                                            <asp:TextBox ID="txtLink" runat="server"  class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default">
                                            <label>Address<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtAddress"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                            <asp:TextBox ID="txtAddress" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default">
                                            <label>Organisation Description<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtDesc"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                            <asp:TextBox ID="txtDesc" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default required">
                                            <label>Organisation Type</label>
                                            <asp:DropDownList ID="ddType" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" data-toggle="dropdown" Width="90%">
                                                <asp:ListItem>Community Club</asp:ListItem>
                                                <asp:ListItem>Charity Organisation</asp:ListItem>
                                                <asp:ListItem>More</asp:ListItem>
                                                <asp:ListItem>Type D</asp:ListItem>
                                                <asp:ListItem>Type E</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group form-control-default">
                                            <label>Organisation Image</label>
                                            <asp:FileUpload ID="FileUploadImage" runat="server" />
                                        </div>
                                    </div>
                                </div>
                        </div>       
                    </div>
                    <asp:Button ID="btnReg" runat="server" ValidationGroup="createGrp" class="btn btn-success" Text="Update" OnClick="btnReg_Click" />
                </div>
            </div>
         </div>
</asp:Content>

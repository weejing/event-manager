﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="InternalHome.aspx.cs" Inherits="SGServe.SaaS.InternalHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <asp:Timer runat="server" ID="UpdateTimer" Interval="10000" OnTick="UpdateTimer_Tick"></asp:Timer>
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default panel-default-info">
                <div class="panel-heading panel-heading-gray">
                    Notification <i class="fa fa-fw fa-bell-o" style="color: red"></i>
                </div>

                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="TimedPanel" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="padding-left: 2%">
                                <asp:Label runat="server" ID="lblNotif" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" Async ="true" MasterPageFile="~/SaaS/OrganizationInternal.Master" AutoEventWireup="true" CodeBehind="CreateFeedBack.aspx.cs" Inherits="SGServe.SaaS.CreateFeedBack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class ="page-section">
       <div class ="row">
           <div class ="col-lg-10 col-lg-offset-1">
                <h4 class ="page-section-heading"><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h4>
                 <asp:Button ID="btnViewResp" runat="server" class ="btn btn-success btn-xs" Text="View Responses" OnClick="btnViewResp_Click"  /><br/><br/>
               <div class ="panel panel-default">
               <div class ="panel-body">                      
                    <div id ="body" class ="body">                  

                    </div>                             
               </div>
           </div>

           <input type ="button" class ="btn btn-primary" value ="Add Question" id ="Add" />&nbsp;&nbsp;
           <asp:Button ID="save" runat="server" class ="btn btn-success" Text="Save" OnClick="save_Click" />
 
           </div>                      
       </div>                
   </div>
  

    <asp:HiddenField ID="hiddenFeedBack" runat="server" />
    <asp:HiddenField ID="hiddenFeedBackDesign" runat="server" />

      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type = "text/javascript">
        $(document).ready(function () {
           
            var counter = 0;
            var feedBackId = [];

            var feedBackDesign = $('#<%=hiddenFeedBackDesign.ClientID%>').val().split('+');
           
            console.log(feedBackDesign.length);

            if(feedBackDesign.length >1)
            {
                for(var count =0; count < feedBackDesign.length -1; count++)
                {
                    feedBackId.push(counter);

                    var feedBack = feedBackDesign[count].split(',');

                    console.log(feedBack[0]);
                    var question = ' <div class ="form-group"> <input id ="question' + counter + '" type ="text" class = "form-control" value ="'+feedBack[0]+'"/></div> ';

                    var answerArea;
                    var select;
                    if (feedBack[1] == 1)
                    {
                        answerArea = ' <div class = "ans"><div class = "form-group">  <textarea class="form-control" id ="ans' + counter + '"  rows="4"></textarea></div></div> ';
                        select = '<div class ="form-group"> <div class = "col-md-2">Question Type</div><div class = "col-md-4"> <select id ="' + counter + '" class = "type" > <option value = "1" selected>Text</option> <option value ="2">Choice</option></select> </div></div>';

                    }
                    else
                    {
                        var radio1 = ' <div class="radio radio-info radio-inline"><input type ="radio" value ="1" checked="" name = "radio' + counter + '" id = "radio1' + counter + '"/><label for = "radio1' + counter + '">1</label></div> ';
                        var radio2 = ' <div class="radio radio-inline"><input type ="radio" value ="2" name = "radio' + counter + '" id ="radio2' + counter + '"/><label for = "radio2' + counter + '">2</label></div> ';

                        var radio3 = ' <div class="radio radio-inline"><input type ="radio" value ="3" name = "radio' + counter + '"  id ="radio3' + counter + '"/><label for = "radio3' + counter + '">3</label></div> ';
                        var radio4 = ' <div class="radio radio-inline"><input type ="radio" value ="4" name = "radio' + counter + '" id = "radio4' + counter + '"/><label for = "radio4' + counter + '">4</label></div> ';

                        var radio5 = ' <div class="radio radio-inline"><input type ="radio" value ="5" name = "radio' + counter + '" id ="radio5' + counter + '"/><label for = "radio5' + counter + '">5</label></div> ';
                        answerArea = ' <div class = "ans"> <div class ="form-group"> <div class = "col-md-2"><p>Lowest<p></div>  <div class ="col-md-4">' + radio1 + radio2 + radio3 + radio4 + radio5 + '</div>  <div class ="col-md-2"><p>Highest</p></div> </div>  </div>';

                        select = '<div class ="form-group"> <div class = "col-md-2">Question Type</div><div class = "col-md-4"> <select id ="' + counter + '" class = "type" > <option value = "1" >Text</option> <option value ="2" selected >Choice</option></select> </div></div>';
                    }
                   
                                                                      
                    var del = '<div class="col-md-6 text-right"><input type = "button" id ="del' + counter + '" class ="btn btn-danger btn-sm" value = "X"/></div> <br/> <br/> ';
                    var elements = '<div class = "fdBackTag">' + select + del + question + answerArea + ' <br/> </div> <br/>';

                    $('.body').append(elements);
                    counter++;
                }
            }
            
                     
            // add a new feedback question
            $('#Add').click(function () {
                feedBackId.push(counter);

                var select = '<div class ="form-group"> <div class = "col-md-2">Question Type</div><div class = "col-md-4"> <select id ="' + counter + '" class = "type" > <option value = "1" selected>Text</option> <option value ="2">Choice</option></select> </div></div>';

                var del = '<div class="col-md-6 text-right"><input type = "button" id ="del' + counter + '" class ="btn btn-danger btn-sm" value = "X"/></div> <br/> <br/> ';


                var question = ' <div class ="form-group"> <input id ="question' + counter + '" type ="text" class = "form-control" placeholder ="Question"/></div> ';
                var textArea = ' <div class = "ans"><div class = "form-group">  <textarea class="form-control" id ="ans' + counter+ '"  rows="4"></textarea></div></div> ';

                
                var elements = '<div class = "fdBackTag">'+ select + del + question + textArea +' <br/> </div>';

                $('.body').append(elements);
                counter++;
                
            });
             
            // delete a feedback question
            $('div').on('click', '.btn-danger', function () {
                $(this).parent().parent().remove();
                feedBackId.pop();
            });

            // change feedback repsonse type
            $('div').on('change', '.type', function () {

                var ans = $(this).parent().parent().parent().find(".ans");
                ans.find('.form-group').remove();
                var id = $(this).attr('id');

                if ($(this).val() == 1)
                {
                    ans.append('<div class = "form-group"> <textarea class="form-control" id ="' + id+ '"  rows="3"></textarea></div>');
                }
                else
                {
                    var radio1 = ' <div class="radio radio-info radio-inline"><input type ="radio" value ="1" checked="" name = "radio' + id + '" id = "radio1' + id + '"/><label for = "radio1'+id+'">1</label></div> ';
                    var radio2 = ' <div class="radio radio-inline"><input type ="radio" value ="2" name = "radio' + id + '" id ="radio2'+id+'"/><label for = "radio2'+id+'">2</label></div> ';

                    var radio3 = ' <div class="radio radio-inline"><input type ="radio" value ="3" name = "radio' + id + '"  id ="radio3' + id + '"/><label for = "radio3' + id + '">3</label></div> ';
                    var radio4 = ' <div class="radio radio-inline"><input type ="radio" value ="4" name = "radio' + id + '" id = "radio4'+id+'"/><label for = "radio4'+id+'">4</label></div> ';

                    var radio5 = ' <div class="radio radio-inline"><input type ="radio" value ="5" name = "radio' + id + '" id ="radio5' + id + '"/><label for = "radio5' + id + '">5</label></div> ';
                    ans.append(' <div class ="form-group"> <div class = "col-md-2"><p>Lowest<p></div>  <div class ="col-md-4">' + radio1 + radio2 + radio3 + radio4 + radio5 + '</div>  <div class ="col-md-2"><p>Highest</p></div> </div> ');
                }
            });
           
            // save the feedback form design in the database
            $('#<%=save.ClientID%>').click(function () {

                if(feedBackId.length == 0)
                {
                    alert("you have not created any questions");
                    event.preventDefault();
                    return false;
                }

                var hiddenFeedBack = document.getElementById('<%=hiddenFeedBack.ClientID%>');
                //hiddenFeedBack.value = "";
               
                $('.fdBackTag').each(function () {
                    var question = $(this).find('.form-group').find('.form-control');
                    var type = $(this).find('.col-md-4').find('.type');

                    hiddenFeedBack.value += question.val() + "," + type.val() + "+";
                    console.log("feedback: " + hiddenFeedBack.value);
                });
            });
            
        });
    </script>

</asp:Content>

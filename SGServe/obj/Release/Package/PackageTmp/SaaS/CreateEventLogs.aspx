﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="CreateEventLogs.aspx.cs" Inherits="SGServe.SaaS.CreateEventLogs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">


    <h4 class="page-section-heading">Event logistics</h4>
    <div class="wizard-container wizard-1" id="wizard-demo-1">
        <div class ="data-scrollable-h">
            <ul class="wiz-progress">
                <li>Create Event</li>
                <li>Event Session</li>
                <li class="active">Event Logistics</li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default ">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    Session
                <asp:PlaceHolder ID="sessionPlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
                <div class="col-md-4">
                    Session Day
                <input type="hidden" id="datesMultiSelect" style="width: 100%" />
                </div>
                <div class="col-md-2">
                    Item
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="insertTable" Display="Dynamic" ControlToValidate="logText" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:TextBox ID="logText" class="form-control" runat="server"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" MinimumPrefixLength="3" CompletionInterval="100" EnableCaching="false" CompletionSetCount="10"
                        FirstRowSelected="false" ServiceMethod="GetLogsList" ServicePath="~/SaaS/CreateEventLogs.aspx" TargetControlID="logText" UseContextKey="True">
                    </asp:AutoCompleteExtender>
                </div>
                <div class="col-md-1">
                    Quantity
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="insertTable" Display="Dynamic" ControlToValidate="qty" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[1-9]+[0-9]*$" ErrorMessage="Wrong Format" ValidationGroup="insertTable" Display="Dynamic" ControlToValidate="qty" ForeColor="Red"></asp:RegularExpressionValidator>--%>
                <input id="qty" type="number" min="1" class="form-control" />
                </div>
                <div class="col-md-2">
                    <br />
                    <input type="button" id="insertTable" class=" btn btn-primary btn-sm" value="Add" />
                </div>
            </div>


            <div class="row">
                <div class="table-responsive">
                    <table id="logsTable" class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Session</th>
                                <th>Dates</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <asp:HiddenField ID="hiddenSecondDropDown" runat="server" />
                    <asp:HiddenField ID="hiddenTableValue" runat="server" />
                    <br />
                </div>
            </div>
        </div>
    </div>

   <asp:Button ID="createEvent" runat="server" class="btn btn-primary" Text="Create Event" OnClick="createEvent_Click" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var hiddenSecondDropDown = $("#<%= hiddenSecondDropDown.ClientID%>");
            var array = hiddenSecondDropDown.val().split('-');
            console.log(hiddenSecondDropDown.text());
            console.log(array[0]);

            var dropDown1 = $("#sessionRole");
            var dropDown2 = $("#datesMultiSelect");
            var insertTable = $("#insertTable");
            var hiddenTableValue = document.getElementById("<%= hiddenTableValue.ClientID %>");
            //dropDown2.parent().hide();


            dropDown1.change(function (e) {
                dropDown2.val('');
                var selectedValue = $(this).val();
                console.log("selectedValue:" + selectedValue);
                if (selectedValue != null) {
                    var dates = array[selectedValue].split(',');
                    dropDown2.select2({
                        tags: dates,
                        placeholder: 'Add your tags'
                    });
                    dropDown2.trigger("chosen:updated");
                    dropDown2.parent().show();

                }

            });

            dropDown1.trigger('change');


            insertTable.click(function (e) {
                var logText = $("#<%= logText.ClientID%>");


                if ((logText.val() == "") && ($("#qty").val() == ""))
                {
                    alert("Quantity & Item cannot be empty!");
                }
                else if ($("#qty").val() == "")
                {
                    alert("Quantity cannot be empty!");
                }
                else if (logText.val() == "")
                {
                    alert("Item cannot be empty!");
                }
                else if ($("#qty").val() <=0)
                {
                    alert("Quantity cannot be 0 and below");
                }
                else
                {
                    var table = $("#logsTable");
                    var dropDownOneText = $("#sessionRole option:selected");

                    table.append("<tr><td>" + logText.val() + "</td><td>" + $("#qty").val() +
                        "</td> <td>" + dropDownOneText.text() + "</td><td>" + dropDown2.val() + "</td></tr>");

                    hiddenTableValue.value += logText.val() + "+" + $("#qty").val() + "+" + dropDown1.val() + "+" + dropDown2.val() + "-";

                    logText.val("");
                    $("#qty").val("");
                    dropDown1.trigger('change');
                    //dropDown1.val('0').trigger('change');
                    //dropDown2.parent().hide();
                }

            });
        });
    </script>

</asp:Content>


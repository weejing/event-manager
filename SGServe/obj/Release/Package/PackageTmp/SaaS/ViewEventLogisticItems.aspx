﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="ViewEventLogisticItems.aspx.cs" Inherits="SGServe.SaaS.ViewEventLogisticItems" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="page-section">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">

                    <h4 class="page-section-heading">
                        <asp:Label ID="lbleventname" runat="server" Text=""></asp:Label>&nbsp;<asp:Label ID="lblHeader" runat="server" Text="Logistic Item Details"></asp:Label></h4>
                    <div class="table-responsive">
                        <asp:GridView ID="grdEventLogisticItems" runat="server" AutoGenerateColumns="False" CellPadding="50" ForeColor="#333333" GridLines="None"
                            Height="24px" Width="100%"
                            CssClass="table" AllowPaging="True" DataKeyNames="EVENT_LOG_ID,ITEM_NAME,QTY,ITEMSTATUS" EmptyDataText="There is currently no event logistic item request!" ShowHeaderWhenEmpty="True" OnPageIndexChanging="grdEventLogisticItems_PageIndexChanging" HorizontalAlign="Center" OnRowDataBound="grdEventLogisticItems_RowDataBound">


                            <Columns>
                                 <asp:BoundField DataField="EVENT_LOG_ID" HeaderText="Event Logistic ID&nbsp;&nbsp;" ReadOnly="True" />
                                <asp:BoundField DataField="ITEM_NAME" HeaderText="Item Name&nbsp;&nbsp;" ReadOnly="True" />
                                <asp:BoundField DataField="QTY" HeaderText="Quantity&nbsp;&nbsp;" ReadOnly="True" />
                                  <asp:TemplateField HeaderText="Item Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("ITEMSTATUS") %>' />
                                </ItemTemplate>
                                      </asp:TemplateField>
<%--                                <asp:BoundField DataField="ITEMSTATUS" HeaderText="Item Status&nbsp;&nbsp;" ReadOnly="True" />--%>
                                <asp:TemplateField  HeaderText="Action&nbsp;&nbsp;&nbsp;&nbsp;">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnTender" CommandName="Send" runat="server" class="btn btn-default btn-xs" OnClick="btnTender_Click" Visible="False">Tender</asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>

                            <HeaderStyle HorizontalAlign="Center" ForeColor="#0066CC" />
                            <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                        </asp:GridView>
                    </div>
                    <ul class="list-inline pull-right">
                        <li>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-success btn-xs" OnClick="btnBack_Click" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</asp:Content>

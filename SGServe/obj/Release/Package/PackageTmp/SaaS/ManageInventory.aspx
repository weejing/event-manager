﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="ManageInventory.aspx.cs" Inherits="SGServe.SaaS.ManageInventory" %>
    <asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
  
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="InventoryListCSS/InventoryCss.css" rel="stylesheet" />
    <link href="css/vendor/all.css" rel="stylesheet">
    <asp:LinkButton ID="lnkHelpText" CssClass="btn btn-circle btn-primary btn-sm " Style="float: right" Text="?" runat="server"></asp:LinkButton>
    <h4 class="page-section-heading">Manage Inventory</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="tabbable tabs-primary">
                    <!-- Tabs -->
                    <ul class="nav nav-tabs" runat="server">

                        <li class="active" id="firstTab" runat="server">
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click"><i class="fa fa-fw fa-plus"></i>Add Item</asp:LinkButton></li>

                        <li id="secondTab" runat="server">
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click"><i class="fa fa-fw fa-list-ol"></i>Add Multiple Items</asp:LinkButton></li>

                    </ul>
                    <!-- // END Tabs -->

                    <!-- Panes -->
                                    <div class="tab-content" runat="server">
                        <div class="tab-panel active" runat="server" id="insertItemPanel" visible="true">
                            <h4>Add a new inventory item</h4>
                            <div class="panel panel-default">
                                <div class="form-group form-control-default required">
                                    <label>Category <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="inventoryGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbCategory"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="tbCategory" runat="server" Width="80%" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>Category Description <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="inventoryGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbCategoryDescription"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="tbCategoryDescription" runat="server" Width="80%" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>Item Name <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="inventoryGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbItemName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="tbItemName" runat="server" Width="80%" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default">
                                    <label>Item Type <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="inventoryGrp" SetFocusOnError="true" ControlToValidate="ddlItemType" InitialValue="- Select an Item Type -" Display="Dynamic" ErrorMessage="Please select an item type" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:DropDownList ID="ddlItemType" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" data-toggle="dropdown" Width="40%" >
                                        <asp:ListItem>- Select an Item Type -</asp:ListItem>
                                        <asp:ListItem>Consumable</asp:ListItem>
                                        <asp:ListItem>Non-Consumable</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>Quantity  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="inventoryGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbQty" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                              </label>
                                    <asp:TextBox ID="tbQty" runat="server" Width="15%" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>Item Description <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="inventoryGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbDescription"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="tbDescription" runat="server" TextMode="MultiLine" Width="80%" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>Item Threshold  <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="inventoryGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbThreshold" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                          </label>
                                    <asp:TextBox ID="tbThreshold" runat="server" Width="15%" class="form-control"></asp:TextBox>
                                </div>
                                <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-success" ValidationGroup="inventoryGrp" Width="100px" OnClick="btnAdd_Click" />
                            </div>
                        </div>
                        <div class="tab-panel" runat="server" id="insertMultipleItem" visible="false">
                            <!---newly added-->
                            <div class="wizard">

                                <div class="tab-content">
                                    <div class="tab-panel" role="tabpanel" id="step1" runat="server">
                                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                            <h4 class="alert-heading">Step 1: Download the template provided</h4> 
                                                  To facilitate the process of importing multiple inventory item records into SGServe, we encourage 
                                                  users to <strong> download our excel template </strong> and complete the necessary fields.
                                             </div>
                                        <br />
                                        <div class="row">
                                             <div class="col-md-6 col-md-offset-3">
                                      <div class="form-group form-control-default">
                                     
                                            <label align="center" >* Click here to begin download! </label>
                                                        <br />
                                                <center><asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-success" Height="30" Width="250" Font-Size="Large" OnClick="btnDownload_Click"><i class="fa fa-download"></i>Download</asp:LinkButton></center>
                                                  </div>
                                                </div>
                                      </div>
                                        <ul class="list-inline pull-right">
                                            <li>
                                                <a href="#step2" aria-controls="step2" role="tab" title="Step 2">
                                                    <asp:Button ID="btnNext" runat="server" Text="Next" class="btn btn-primary" OnClick="btnNext_Click" />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-panel" role="tabpanel" id="step2" runat="server" visible="false">
                                             <div class="alert alert-info alert-dismissible fade in" role="alert">
                                            <h4 class="alert-heading">Step 2: Upload the completed template provided </h4> 
                                                  After filling in the necessary fields in the excel template, user is required to <strong>upload completed template</strong> for SGServe system
                                                  to create the new records.
                                             </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group form-control-default">
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-offset-1">
                                                        <asp:FileUpload ID="FileUpload1" runat="server" Height="30px" />
                                                    </div>
                                                       <div class="col-xs-4 col-md-offset-1">
                                                        <asp:LinkButton ID="btnUpload" runat="server" CssClass="btn btn-success" OnClick="btnUpload_Click"><i class="fa fa-upload"></i>Upload</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                             </div>
                                           </div>

                                        <ul class="list-inline pull-right">
                                            <li>
                                                <asp:Button ID="btnPrev" runat="server" Text="Prev" class="btn btn-primary" OnClick="btnPrev_Click" />
                                            </li>
                                            <li>
                                                <asp:Button ID="btnNextNext" runat="server" Text="Done" class="btn btn-primary" OnClick="btnNextNext_Click" />
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                                        <ajaxToolkit:ModalPopupExtender ID="mpHelpText" BackgroundCssClass="modalBackground" runat="server" PopupControlID="HelpTextPanel" TargetControlID="lnkHelpText" CancelControlID="btnHelpClose"></ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="HelpTextPanel" runat="server" CssClass="modal-lg" Style="display: none;">
                <div class="modal-content">
                    <div class="modal-header">
                        <asp:Button ID="btnHelpClose" class="btn btn-stroke btn-circle btn-default btn-sm" runat="server" Text="X" Style="float: right" OnClick="btnHelpClose_Click" /><h4 class="modal-title">
                            <asp:Label ID="Label2" runat="server" Text="Create Inventory Item Guide"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                       1.<b> Category</b> (E.g Logistic,Food,Stationary)<br /><br />
                       2.<b> Category Description</b>  refer to a short description of the category.<br /><br />
                       3.<b> Item Name</b>  (E.g Chairs,Apple)<br /><br />
                       4.<b> Item Type</b>  (<b>Consumable</b>: Perishable items E.g Food items(apple,bread) <b>Non-Consumable</b>: Non-Perishable items E.g Logistic items(chairs,table))<br /><br />
                       5.<b> Quantity</b>  refer to the number of inventory items to create.<br /><br />
                       6.<b> Item Description</b> refers to a short description of the inventory item.<br /><br />
                       7.<b> Item Threshold</b> refers to the minimum stock level of an inventory item. The Item Threshold should be more than 0.<br /><br />

                    </div>
                </div>
            </asp:Panel>
            </div>

        </div>
    </div>
</asp:Content>

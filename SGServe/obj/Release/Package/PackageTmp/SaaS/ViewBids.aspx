﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="ViewBids.aspx.cs" Inherits="SGServe.SaaS.ViewBids" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
   
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> View Bid Details
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Supplier Name</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_supplier" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Supplier Description</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_description" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Address</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_address" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>    
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Phone</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_phone" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Fax</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_fax" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Bid Date</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_biddate" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>    
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Bid Price (Group)</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_bidPriceGroup" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Bid Price (Individual)</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_bidPriceSolo" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Product Name</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_productname" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>    
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Product Description</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_productdescription" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>   
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Product Brand</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_productbrand" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>  
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Bid Status</span></div>
                          <div class="col-sm-8"><asp:Label ID="lbl_status" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>  
                              <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Company Rating</span></div>
                          <div class="col-sm-8"><asp:Label ID="LabelRating" runat="server" Text="0"></asp:Label></div>
                        </div>
                      </li>                                            
                    </ul>
                  
                  </div>
                </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="CreateTenderItem.aspx.cs" Inherits="SGServe.SaaS.CreateTenderItem" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div class="page-section">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h4 class="page-section-heading">
                    <asp:Label ID="lblHeader" runat="server" Text="Create New Tender"></asp:Label></h4>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="form-group form-control-default required">
                                    <label>Category</label>
                                    <asp:Label ID="LabelCat" runat="server" Text="Label"></asp:Label>
                                   
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group form-control-default required">
                            <label>
                                Description<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="DescTxt"
                                    ErrorMessage=" Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                            </label>

                            <asp:TextBox ID="DescTxt" class="form-control" TextMode="multiline" runat="server" />

                        </div>

                        

                        <div class="form-group form-control-default required">
                            <label>
                                Quantity Needed<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="QtyTxt"
                                    ErrorMessage=" Please fill in" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="QtyTxt" SetFocusOnError="true" runat="server" ForeColor="Red" ValidationGroup="createGrp" ErrorMessage=" Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator></label>
                            <div>
                                <asp:TextBox ID="QtyTxt" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>


                           <asp:Button ID="Button1" runat="server" Text="Create Tender"  class="btn btn-success"  ValidationGroup="createGrp" OnClick="Button1_Click" />
                    </div>
                 
                </div>


            </div>
        </div>
</asp:Content>

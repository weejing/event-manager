﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="TenderingMgmt.aspx.cs" Inherits="SGServe.SaaS.TenderingMgmt" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
  <%--  <a class="navbar-brand hidden-xs navbar-brand-primary">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </a>--%>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 
   
    <asp:Panel ID="PanelShow" runat="server">
 <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> Search by Inserting all Parameters Provided
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                                <div class="form-group">
                    <label class="col-sm-3 control-label">Type</label>
                    <div class="col-sm-9">
                   <asp:DropDownList ID="ddlCategoryLvl1" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryLvl1_SelectedIndexChanged">
                                    </asp:DropDownList>
                    </div>
                </div>
                        <div class="form-group">
                    <label class="col-sm-3 control-label">Category</label>
                    <div class="col-sm-9">
                       <asp:DropDownList ID="ddlCategoryLvl2" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryLvl2_SelectedIndexChanged">
                                    </asp:DropDownList>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Item</label>
                    <div class="col-sm-9">
                    <asp:DropDownList ID="ddlCategoryLvl3" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryLvl3_SelectedIndexChanged">
                                    </asp:DropDownList>
                    </div>
                </div>
                    </ul>
                      <asp:Button ID="ButtonSearch" runat="server" class="btn btn-success" Text="Search" OnClick="ButtonSearch_Click" />
                  </div>
                </div>
    </asp:Panel>
       <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h4 class="page-section-heading">Manage Tender</h4>
      
    </div>
      <div class="panel panel-default">
              

           <asp:GridView CellPadding="50"
                    GridLines="None" Height="24px" Width="100%" CssClass="table"
                    EmptyDataText="No tender participated, please participate!" ID="TenderItemGrid" DataKeyNames="TENDER_HEADER_ID" runat="server" AutoGenerateColumns="False" OnDataBound="TenderItemGrid_DataBound">
                    <Columns>

                        <asp:BoundField DataField="NAME" HeaderText="Product"></asp:BoundField>
                        <asp:BoundField DataField="ORG_NAME" HeaderText="Tender Host"></asp:BoundField>
                         <asp:TemplateField HeaderText="Start Date">
                            <ItemTemplate>
                               
                                <asp:Label ID="lblstartdate" runat="server" Text='<%#Eval("START_DATE", "{0:d MMM yyyy}") %>'></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                      
                         <asp:TemplateField HeaderText="Start Date">
                            <ItemTemplate>
                              
                                <asp:Label ID="lblenddate" runat="server" Text='<%#Eval("END_DATE", "{0:d MMM yyyy}") %>'></asp:Label>
                                 </ItemTemplate>
                        </asp:TemplateField>
                         <asp:BoundField DataField="STATUS" HeaderText="Status"></asp:BoundField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkView" OnClick="lnkView_Click">View Tender Item</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle ForeColor="#0066CC" />
                    <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                </asp:GridView>



      </div>
    
  
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SaaS/OrganizationInternal.Master" Async="true" AutoEventWireup="true" CodeBehind="ViewTender.aspx.cs" Inherits="SGServe.SaaS.ViewTender" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitle" runat="server">
   
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
      <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h4 class="page-section-heading">Tender Head Details</h4>
      
    </div>
     <div class="panel panel-default">
                
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Product</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblCat" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Tender Host</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblHost" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Start Date</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblStartDate" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>    
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">End Date</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblEndDate" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>   
                         <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Status</span></div>
                          <div class="col-sm-8"><asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label></div>
                        </div>
                      </li>                               
                    </ul>
                    
                  </div>
                </div>
      <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h4 class="page-section-heading">View Tender Item Details</h4>
      
    </div>
  
    <div class="panel panel-default">
        <div class="panel-body">


                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                    <div class="col-sm-9">
                    <asp:Label ID="StatusLabel" runat="server" class="form-control" Text="Label"></asp:Label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Description
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="DescTxt"
                                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                    </label>
                    <div class="col-sm-9">
                       <asp:TextBox id="DescTxt"  class="form-control"  runat="server" />
                    </div>
                </div>

               

                

                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Quantity Needed<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="QtyTxt"
                                    ErrorMessage=" Please fill in" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="QtyTxt" ValidationGroup="createGrp" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage=" Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator></label>
                    <div class="col-sm-9">
                      <asp:TextBox ID="QtyTxt" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>

                 

          

              
       
        </div>  <asp:Button ID="UpdateTenderButton" runat="server" class="btn btn-success" ValidationGroup="createGrp" Text="Update Tender" OnClick="Button1_Click" />
                 <asp:Button ID="DeleteTenderButton" runat="server" class="btn btn-danger btn-sm" Text="Delete Tender" OnClick="Button2_Click" />
    </div>
     <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> Bid Summary (Lowest Group Price)
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                                 <asp:GridView CellPadding="50" 
        
        
        GridLines="None" Height="24px" Width="100%" CssClass="table"
                            AllowPaging="True" EmptyDataText="No bidders participated, ask them participate!" ID="GridViewLowestBid" DataKeyNames="FIRM_ID" runat="server" AutoGenerateColumns="False" OnDataBound="TenderItemGrid_DataBound">
                            <Columns>

                                <asp:BoundField DataField="FIRM_NAME" HeaderText="Comapny Name"></asp:BoundField>
                                <asp:BoundField DataField="BID_PRICE" HeaderText="Bid Price (SGD)"></asp:BoundField>
                                

                               
                            </Columns>
                            <HeaderStyle ForeColor="#0066CC" />
                            <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                        </asp:GridView> 
                    </ul>
                    
                  </div>
                </div>
    
     <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> Bid Summary (Lowest Individual Price)
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                                 <asp:GridView CellPadding="50" 
        
        
        GridLines="None" Height="24px" Width="100%" CssClass="table"
                            AllowPaging="True" EmptyDataText="No bidders participated, ask them participate!" ID="GridViewLowestBidSolo" DataKeyNames="FIRM_ID" runat="server" AutoGenerateColumns="False" OnDataBound="TenderItemGrid_DataBound">
                            <Columns>

                                <asp:BoundField DataField="FIRM_NAME" HeaderText="Comapny Name"></asp:BoundField>
                                <asp:BoundField DataField="BID_PRICE_SOLO" HeaderText="Bid Price (SGD)"></asp:BoundField>
                               

                                
                            </Columns>
                            <HeaderStyle ForeColor="#0066CC" />
                            <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                        </asp:GridView> 
                    </ul>
                    
                  </div>
                </div>
    
    
    
    
    
    
       <asp:Panel ID="PanelShow" runat="server">
 <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> Choose Winner
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                                <div class="form-group">
                    <label class="col-sm-3 control-label">Winner for Group Price</label>
                    <div class="col-sm-9">
                     <asp:DropDownList ID="DropDownListGroup" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="False">
                                    </asp:DropDownList>
                    </div>
                </div>

               

                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Winner for Individual Price</label>
                    <div class="col-sm-9">
                     <asp:DropDownList ID="DropDownListSolo" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="False">
                                    </asp:DropDownList>
                    </div>
                </div>
                    </ul>
                      <asp:Button ID="ButtonWinner" class="btn btn-success" runat="server" Text="Submit Winner" OnClick="ButtonWinner_Click" />
                  </div>
                </div>
    </asp:Panel>

     <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> View Participated Bidders
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                          <asp:GridView CellPadding="50" 
        
        
        GridLines="None" Height="24px" Width="100%" CssClass="table"
                            AllowPaging="True" EmptyDataText="No bidders participated, ask them participate!" ID="TenderItemGrid" DataKeyNames="VDR_BIDS_ITEM_ID" runat="server" AutoGenerateColumns="False" OnDataBound="TenderItemGrid_DataBound">
                            <Columns>

                                <asp:BoundField DataField="FIRM_NAME" HeaderText="Comapny Name"></asp:BoundField>
                           
                                  <asp:TemplateField HeaderText="Bid Date">
                                    <ItemTemplate>
                                      <asp:Label ID="lblbiddate" runat="server" Text='<%#Eval("BID_DATE", "{0:d MMM yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PRODUCT_NAME" HeaderText="Product Name"></asp:BoundField>
                                <asp:BoundField DataField="PRODUCT_BRAND" HeaderText="Product Brand"></asp:BoundField>
                                <asp:BoundField DataField="STATUS" HeaderText="Status"></asp:BoundField>

                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lnkView" OnClick="lnkView_Click">View Deal</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle ForeColor="#0066CC" />
                            <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                        </asp:GridView>     
                    </ul>
                    
                  </div>
                </div>
    
    
    
    
    
   


</asp:Content>

﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="UserOrgFollows.aspx.cs" Inherits="SGServe.UserOrgFollows" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <link href="community_css/css/vendor/all.css" rel="stylesheet">
    <link href="community_css/css/app/app.css" rel="stylesheet">
    <script src="community_css/js/vendor/all.js"></script>
    <script src="community_css/js/app/app.js"></script>

          <div class="container">
            <div class="cover profile">

              <div class="wrapper">
                <div class="image" style="height:58px;">
                  <!--<img src="main_css/images/place1-wide.jpg" alt="people" />-->
                </div>
              </div>
                    
              <div class="cover-info">
                <div class="avatar">
                  <asp:Image ID="ProfilePicImg" runat="server" />
                </div>
                <div class="name">
                    <h2>
                        <asp:Label ID="lblFullName" runat="server" Text="Name" ForeColor="Black"></asp:Label>
                    </h2>
                            </div>
                <ul class="cover-nav">
                  <li><asp:HyperLink ID="linkAbout" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> About </asp:HyperLink></li>
                  <li><asp:HyperLink ID="linkFriends" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Friends </asp:HyperLink> </li>
                  <li class="active"><asp:HyperLink ID="linkFollows" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Follows </asp:HyperLink></li>

                    <li><asp:Button ID="btnAddFriend" runat="server" class="btn btn-info" Text="Add Friend" OnClick="btnAddFriend_Click" /></li>
                    <li><asp:Label ID="lblPendingFriends" runat="server" Text="Pending friend request"></asp:Label></li>
                    <li><asp:Panel ID="PanelF" runat="server">
                    <button class="btn btn-success disabled" type="button" data-toggle="dropdown">Friend</button></asp:Panel></li>
                    <li><asp:Button ID="btnRemoveFriend" runat="server" class="btn btn-danger" Text="Remove friend" OnClick="btnRemoveFriend_Click" /></li>
                    </ul>
              </div>
                <div class="panel panel-default">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                  <div class="panel-body" >
                        <h5><asp:Label ID="lblDescription" runat="server">description</asp:Label></h5>
                  </div>
                </div>
              </div>
            </div>
           
               <br/>
              
              <asp:DataList ID="DataList1" runat="server" onitemcommand="DataList1_ItemCommand" RepeatColumns="3">
                    <ItemTemplate>
                        <asp:Panel ID="Panel1" runat="server" Width="350">

                        <div class="row" data-toggle="isotope">
                      <div class="col-md-10 col-lg-8 item">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="media">
                      <div class="pull-left">
                        <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> Width="300" />
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading margin-v-5">
                            <a href="#"></a>
                            <asp:Label ID="lblName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "ORG_NAME") %>></asp:Label>
                          </h4>
                      </div>
                    </div>
                  </div>
                  <div class="panel-footer">
                       <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ORG_ID") %>' />
                       <asp:Button ID="btnPortfolio" runat="server" class="btn btn-default btn-info btn-l" Text="View" PostBackUrl='<%# string.Format("/{0}/Home", DataBinder.Eval(Container.DataItem, "CONNECTION_STRING")) %>' /> 
                      <br/>
                      <br/>
                      <br/>
                  </div>
                </div>
              </div>

             </div>
                      </asp:Panel>
                    </ItemTemplate>
                </asp:DataList>
                                <asp:Label ID="lblNone" runat="server" Text="The volunteer has not followed any organisations on SGServe."></asp:Label>

              <br/>

                </div>


</asp:Content>

﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SGServe.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-section">
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <h4 class="page-section-heading">Login</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnLogin">
                        <div class="text-center">
                            <div class="form-group">
                                <asp:Button ID="btnFB" class="btn btn-fb" runat="server" Text="Login with Facebook" OnClick="btnFB_Click" />
                            </div>
                        </div>

                        <div class="text-center">
                            <div class="form-group">- OR -</div>
                        </div>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLoginEmail" ErrorMessage="Please fill in" ForeColor="Red"  ValidationGroup ="loginGrp"></asp:RequiredFieldValidator><br>
                        <asp:RegularExpressionValidator ForeColor="Red" Display="Dynamic" ID="RegularExpressionValidator3" ControlToValidate="txtLoginEmail" runat="server"  ValidationGroup ="loginGrp" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <asp:TextBox ID="txtLoginEmail" runat="server" class="form-control" placeholder="Email"></asp:TextBox>
                            </div>

                        </div>
                        <div class="form-group">
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLoginPassword" ErrorMessage="Please fill in" ForeColor="Red" ValidationGroup ="loginGrp"></asp:RequiredFieldValidator><br>
                            <asp:RegularExpressionValidator Display="Dynamic" ForeColor="Red" ID="RegularExpressionValidator4"  ControlToValidate="txtLoginPassword" runat="server"  ValidationGroup ="loginGrp" ValidationExpression="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})" ErrorMessage="Must contain digit,uppercase and lowercase. Min 8 characters"></asp:RegularExpressionValidator>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-shield"></i></span>
                                <asp:TextBox ID="txtLoginPassword" type="password" runat="server" class="form-control" placeholder="Password"></asp:TextBox>    
                            </div>
                                                   <asp:Label ID="lblmessage" runat="server" Text="Wrong password or email" ForeColor="#FF3300"></asp:Label>
                        </div>
                        <div class="text-center">
                            <asp:Button ID="btnLogin" class="btn btn-primary" runat="server" Text="Login" OnClick="btnLogin_Click"  ValidationGroup ="loginGrp" />
                        </div>
                        <div class="text-center">
                            <a href="ResetPassword.aspx">Forgot password</a>
                        </div>
                            </asp:Panel>
                    </div>
                  
                </div>
                </div>
            </div>
        </div>
</asp:Content>

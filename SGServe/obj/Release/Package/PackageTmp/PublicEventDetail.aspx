﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" Async ="true" AutoEventWireup="true" CodeBehind="PublicEventDetail.aspx.cs" Inherits="SGServe.PublicEventDetail" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class ="property-meta">
        <h1 class="page-section-heading" style="color: #26355b;"><asp:Label ID="evtName" runat="server"></asp:Label></h1>
        <div class ="row">
            <div class ="col-md-7">
                <asp:Image ID="evtImage" runat="server" ImageUrl="/main_css/images/photodune-378874-real-estate-xs.jpg" Style="width: 100%; height: 300px" />
            </div>
            <div class ="col-md-5">
                <div class ="panel panel-default">
                    <div class ="panel-heading">
                      <h4 class="panel-title">
                            <asp:Label ID="Date" runat="server" /><div style="float:right"><asp:PlaceHolder ID="PlaceHolderFBlink" runat="server"></asp:PlaceHolder> </div> 
                        </h4>
                    </div>
                    <div class ="panel-body">
                        <div class ="expandable expandable-indicator-white expandable-trigger">
                            <div class ="expandable-content">
                                <p><asp:Label ID="desc" runat="server"></asp:Label></p>   
                                 <asp:DataList ID="dlcause" runat="server" CellSpacing="20" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <span class="label label-customize-100"><i class="fa fa-tag" aria-hidden="true"></i>&nbsp;<%# DataBinder.Eval(Container.DataItem, "TERM_NAME") %></span>
                                    </ItemTemplate>
                                </asp:DataList>
                                    <asp:Label ID="miniLink" runat="server" Font-Size="Smaller"></asp:Label><br>     
                               <asp:Label ID="lblOrg" runat="server" Text="Label" Font-Italic="True" Font-Size="Smaller"></asp:Label>                              
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
    </div>

    <asp:PlaceHolder ID="sessionDetailsHolder" runat="server"></asp:PlaceHolder> 

    <asp:HiddenField ID="hiddenRoleDescriptor" runat="server" />
    <asp:HiddenField ID="hiddenDate" runat="server" />

    <asp:HiddenField ID="datesValuePlaceHolder" runat="server" />
    <asp:HiddenField ID="roleValuePlaceHolder" runat="server" />


<%--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>--%>
    <script>      
        function roleNameOnChange(id, arrayCount)
        {
            var dateDropDown = $('#date' + arrayCount);
            dateDropDown.val('');

            var hiddenRoleDescrip = $('#<%= hiddenRoleDescriptor.ClientID%>');
            var hiddenDateList = $('#<%= hiddenDate.ClientID%>');

            var roleArray = hiddenRoleDescrip.val().split('-');
            var descriptionList = roleArray[arrayCount].split(',');
            var description = descriptionList[id.selectedIndex - 1];
            $('#p' + arrayCount).text(description);

            var dateArray = hiddenDateList.val().split('+');
            var dateList = dateArray[arrayCount].split('-');
            var date = dateList[id.selectedIndex - 1].split(',');

            dateDropDown.select2({
                tags: date,
                placeholder: 'Select your desired time slot'
            });

            dateDropDown.trigger("chosen:updated");
            dateDropDown.parent().show();
        }

    </script>


</asp:Content>

﻿<%@ Page Title="" Language="C#"  Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="Organization.aspx.cs" Inherits="SGServe.Organization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <h1 class="text-h2">Organisations</h1>
    <div class="panel panel-default" style="border-radius: 8px">
         <div class="panel-body">
      <div class="panel panel-overlay" >
          <div class="panel-heading">
                <div class="row">
               <div class="col-md-2">
                  <h4>Organisation Name:</h4> 
                </div>
              <div class="col-md-5">
                <div class="input-group">
                  <asp:TextBox ID="tbSearch" class="form-control" runat="server"></asp:TextBox>
                  <span class="input-group-btn">
                     <asp:Button ID="btnSearch" class="btn btn-info" runat="server" Text="Search" OnClick="btnSearch_Click" />
                  </span>
                </div><!-- /input-group -->
              </div><!-- /.col-md-5 -->
            </div><!-- /.row -->
          </div> 
      </div>
    <br/>
                
       <asp:DataList ID="DataList1" runat="server" RepeatDirection="Horizontal"  RepeatLayout="Flow">
           <ItemTemplate>
               <div class="col-sm-8 col-lg-4 col-md-7">
                   <div class="panel panel-default relative">
                       <div class="cover hover overlay margin-none">
                           <asp:Image ID="Image1" runat="server" style="height:200px;width:100%"   ImageUrl='<%# "Saas/" + DataBinder.Eval(Container.DataItem, "IMAGE") %>' />
                     <a class="overlay overlay-bg-black overlay-full overlay-hover img-responsive"  href='<%# string.Format("/{0}/Home", DataBinder.Eval(Container.DataItem, "CONNECTION_STRING")) %>'><span class="v-center"><span class="btn btn-circle btn-white"><i class="fa fa-eye"></i></span></span></a>
                       </div>
                       <div class="panel-body">
                          <h4 class="margin-v-0-5" style="overflow: hidden; height: 40px;text-overflow: ellipsis;">
                               <asp:Label ID="lblOrg" runat="server" NavigateUrl='<%# string.Format("/{0}/Portfolio", DataBinder.Eval(Container.DataItem, "CONNECTION_STRING")) %>' ><%# DataBinder.Eval(Container.DataItem, "ORG_NAME") %></asp:Label>
                           </h4>
                           <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ORG_ID") %>' />
                          <%-- <asp:Button ID="btnPortfolio" runat="server" class="btn btn-default btn-info btn-l" Text="View" PostBackUrl='<%# string.Format("/{0}/Home", DataBinder.Eval(Container.DataItem, "CONNECTION_STRING")) %>' />--%> 
                           <asp:Button ID="btnFollow" runat="server" class='<%#DataBinder.Eval(Container.DataItem, "UEN") %>' Text='<%#DataBinder.Eval(Container.DataItem, "UNFOLLOW_STATUS") %>' PostBackUrl='<%# string.Format("Follow.aspx?id={0}", DataBinder.Eval(Container.DataItem, "ORG_ID")) %>' />
                       </div>
                   </div>
               </div>
           </ItemTemplate>
       </asp:DataList>

          <asp:DataList ID="DataList2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
           <ItemTemplate>
               <div class="col-sm-8 col-lg-4 col-md-7">
                   <div class="panel panel-default relative">
                       <div class="cover hover overlay margin-none">
                           <asp:Image ID="Image1" runat="server" style="height:200px;width:100%"   ImageUrl='<%# "Saas/" + DataBinder.Eval(Container.DataItem, "IMAGE") %>'  />
                         <a class="overlay overlay-bg-black overlay-hover overlay-full" href='<%# string.Format("/{0}/Home", DataBinder.Eval(Container.DataItem, "CONNECTION_STRING")) %>'><span class="v-center"><span class="btn btn-circle btn-white"><i class="fa fa-eye"></i></span></span></a>
                       </div>
                       <div class="panel-body ">
                        <h4 class="margin-v-0-5" style="overflow: hidden; height: 40px;text-overflow: ellipsis;">
                               <asp:Label ID="lblOrg" runat="server" NavigateUrl='<%# string.Format("/{0}/Portfolio", DataBinder.Eval(Container.DataItem, "CONNECTION_STRING")) %>' ><%# DataBinder.Eval(Container.DataItem, "ORG_NAME") %></asp:Label>
                           </h4>
                           <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ORG_ID") %>' />
                           <%--<asp:Button ID="btnPortfolio" runat="server" class="btn btn-default btn-info btn-l" Text="View" PostBackUrl='<%# string.Format("/{0}/Home", DataBinder.Eval(Container.DataItem, "CONNECTION_STRING")) %>' /> --%>
                       </div>
                   </div>
               </div>
           </ItemTemplate>
       </asp:DataList>

        </div>
        </div>
    <br/>
    </asp:Content>

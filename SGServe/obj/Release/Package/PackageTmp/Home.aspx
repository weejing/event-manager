﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolHome.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="SGServe.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .content {
            margin: auto;
            background-color: #909090;
            width: 60%;
            padding: none;
            min-height: 100%;
        }
    </style>
    <%-- </div>--%>
    <div class="container">
        <div class="text-center">
            <div class="page-section">
                <h2>Why Join Us?</h2>

                <p class="lead text-muted">“Remember that the happiest people are not those getting more, but those giving more.”―H. Jackson Brown Jr.</p>
            </div>
            <br />

            <section class="row gridalicious" data-toggle="gridalicious" data-width="300">
                <div class="panel panel-default relative">
                    <div class="cover hover overlay margin-none">
                        <div class="overlay overlay-bg-black">
                            <div class="v-bottom">
                                <h5 class="text-h5 text-overlay margin-v-0-10 text-uppercase">Interact</h5>

                            </div>
                        </div>
                        <img src="main_css/images/homeimage3.jpg" class="img-responsive" />
                    </div>
                </div>
                <div class="panel panel-default relative">
                    <div class="cover hover overlay margin-none">
                        <div class="overlay overlay-bg-black">
                            <div class="v-bottom">
                                <h5 class="text-h5 text-overlay margin-v-0-10 text-uppercase">Bond</h5>
                            </div>
                        </div>
                        <img src="main_css/images/homeimage2.jpg" class="img-responsive" />
                    </div>
                </div>
                <div class="panel panel-default relative">
                    <div class="cover hover overlay margin-none">
                        <div class="overlay overlay-bg-black">
                            <div class="v-bottom">
                                <h5 class="text-h5 text-overlay margin-v-0-10 text-uppercase">Care</h5>

                            </div>
                        </div>
                        <img src="main_css/images/homeimage4.jpg" class="img-responsive" />
                    </div>
                </div>
            </section>
            <br />
            <div class="text-center">
                <h2>What can you do at SGServe </h2>

            </div>

            <div class="row" data-toggle="gridalicious">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-search fa-2x fa-fw pull-left text-muted"></i>

                            <div class="media-body">
                                <p class="lead margin-none">Search and sign up for events</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-child fa-2x fa-fw pull-left text-muted"></i>

                            <div class="media-body">
                                <p class="lead margin-none">Find like-minded individuals</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-user fa-2x fa-fw pull-left text-muted"></i>

                            <div class="media-body">
                                <p class="lead margin-none">Manage Volunteers</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-star fa-2x fa-fw pull-left text-muted"></i>

                            <div class="media-body">
                                <p class="lead margin-none">Get rewarded for your efforts</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-globe fa-2x fa-fw pull-left text-muted"></i>

                            <div class="media-body">
                                <p class="lead margin-none">Volunteerism community</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-group fa-2x fa-fw pull-left text-muted"></i>

                            <div class="media-body">
                                <p class="lead margin-none">Cooperate collaboration efforts</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="text-center" style ="margin-top:40px;">
                <h2>Need a Volunteer Management System?</h2>
                <h4><a href="NewSaaS_Plan.aspx">Sign up with us today!</a></h4>
            </div>

            <div class="row" data-toggle="gridalicious">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-child fa-2x fa-fw pull-left text-muted"></i>
                            <div class="media-body">
                                <p class="lead margin-none">Volunteer Management</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-calendar fa-2x fa-fw pull-left text-muted"></i>
                            <div class="media-body">
                                <p class="lead margin-none">Event Management</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <i class="fa fa-archive fa-2x fa-fw pull-left text-muted"></i>
                            <div class="media-body">
                                <p class="lead margin-none">Logistic Management</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <%-- <div class="overlay overlay-full" style="margin-top: 7%;">
        <div class="v-center v-bottom-xs">
            <div class="panel panel-overlay margin-bottom-none width-500 width-auto-xs h-center" data-toggle="panel-collapse" data-open="true">
                <div class="panel-heading panel-collapse-trigger">
         
                    <h4 class="panel-title">Volunteer with us</h4>
                </div>
              
                    <div class="form-inline">
                        <div class="form-group">
                            <label class="label-block">Beneficiary</label>
                            <asp:DropDownList ID="DropDownList3" runat="server" class="btn btn-white dropdown-toggle" Width="90%" data-toggle="dropdown">
                                <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem>Children</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label class="label-block" for="check-out">Role</label>
                            <asp:DropDownList ID="DropDownList4" runat="server" class="btn btn-white dropdown-toggle" data-toggle="dropdown" Width="90%">
                                <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem>Emcee</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-inline">
                        <div class="form-group">
                            <label class="label-block" for="check-in">From</label>
                            <input id="check-in" class="datepicker form-control width-110 width-100pc-xs" type="text" value="mm/dd/yyyy" />
                        </div>
                        <div class="form-group">
                            <label class="label-block" for="check-out">To</label>
                            <input id="check-out" class="datepicker form-control width-110 width-100pc-xs" type="text" value="mm/dd/yyyy" />
                        </div>
                    </div>

                    <br />
                    <div class="form-group">
                        <asp:Button ID="btnSearchEvt" class="btn btn-danger" runat="server" Text="Search" OnClick="btnSearchEvt_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

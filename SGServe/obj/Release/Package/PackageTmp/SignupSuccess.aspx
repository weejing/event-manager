﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="SignupSuccess.aspx.cs" Inherits="SGServe.SignupSuccess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-section">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h4 class="page-section-heading">
                           You have succesfully sign up. Please activate your account via the email we sent you.
                        </h4>
                        </div>   </div>   </div>
</asp:Content>

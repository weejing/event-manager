﻿<%@ Page Title="" Language="C#"  Async ="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="EventInvite.aspx.cs" Inherits="SGServe.EventInvite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   

    <div class ="property-meta">
        <h1 class ="text-h3"><asp:Label ID="evtName" runat="server"></asp:Label></h1>
        <div class ="row">
            <div class ="col-md-7">
                <asp:Image ID="evtImage" runat="server" ImageUrl="/main_css/images/photodune-378874-real-estate-xs.jpg" />
                <ul>
                    <li class ="property-meta-item"><i class="fa fa-clock-o fa-fw"></i>
                        <asp:Label ID="Date" runat="server"/></li>                    
                </ul>
            </div>
            <div class ="col-md-5">
                <div class ="panel panel-default">
                    <div class ="panel-heading">
                        <h4 class="panel-title">Description</h4>
                    </div>
                    <div class ="panel-body">
                        <div class ="expandable expandable-indicator-white expandable-trigger">
                            <div class ="expandable-content">
                                <p><asp:Label ID="desc" runat="server"></asp:Label></p>                    
                            </div>
                        </div>
                    </div>
                </div>                         
            </div>
        </div>
    </div>

    <asp:PlaceHolder ID="sessionDetailsHolder" runat="server"></asp:PlaceHolder>

     <asp:HiddenField ID="datesValuePlaceHolder" runat="server" />
    <asp:HiddenField ID="roleValuePlaceHolder" runat="server" />

    <asp:HiddenField ID="hiddenRoleDescriptor" runat="server" />
    <asp:HiddenField ID="HiddenPercent" runat="server" />
    <asp:HiddenField ID="hiddenDate" runat="server" />

<%--     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>--%>
    <script>
             
        function setSessionDatesPlaceHolder(dateId, numOfCompulsory)
        {

            console.log("dateId: " + dateId);
            var sessionDates = $("#date" + dateId);
            var sessionRole = $("#role" + dateId);
            console.log(sessionDates.val());

            var split = sessionDates.val().split(',');
            numOfCompulsory = parseInt(numOfCompulsory);
            console.log("split:" + split.length);
            console.log("numofCompulsory:" + numOfCompulsory);


            if (split.length < numOfCompulsory)
            {
                alert("You have to attend at least" + numOfCompulsory + "days ");
                event.preventDefault();
                return false;
            }
            console.log("sessionRole: " + sessionRole.val());
            document.getElementById("<%=datesValuePlaceHolder.ClientID%>").value = sessionDates.val();
            document.getElementById("<%=roleValuePlaceHolder.ClientID%>").value = sessionRole.val();
        }

        function roleNameOnChange(id, arrayCount)
        {

                        
            var hiddenRoleDescrip = $('#<%= hiddenRoleDescriptor.ClientID%>');
            var hiddenPercent = $('#<%= HiddenPercent.ClientID %>');


            var array = hiddenRoleDescrip.val().split('-');
            var descriptionList = array[arrayCount].split(',');

            var percentArray = hiddenPercent.val().split('-');
            var percentList = percentArray[arrayCount].split(',');

            var description = descriptionList[id.selectedIndex - 1];
            var percent = percentList[id.selectedIndex - 1];
            $('#p' + arrayCount).text(description);
            $('#w' + arrayCount).text(percent);
        }

       
    </script>
    
</asp:Content>

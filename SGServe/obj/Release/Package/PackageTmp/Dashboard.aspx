﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="SGServe.Dashboard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField ID="hdnfldVariable" runat="server" />
    <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
    <h1 class="text-h2"></h1>
    <div class="container-fluid">

        <div class="media media-grid media-clearfix-xs">
            <div class="media-left">

                <div class="width-250 width-auto-xs">
                    <div class="panel panel-default panel-info">
                        <div class="panel-heading panel-heading-info">
                            <a href="EditProfile.aspx" class="btn btn-info btn-xs pull-right"><i class="fa fa-pencil"></i></a>
                            <i class="fa fa-fw fa-info-circle"></i>Particulars
                        </div>
                        <div class="panel-body">
                            <ul class="list-unstyled profile-about margin-none">
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">Name</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">Email</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblEmail" runat="server" Style="word-wrap: break-word;" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">NRIC</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblNRIC" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">Date of Birth</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblDOB" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                                <asp:Panel ID="pnlSch" runat="server">
                                    <li class="padding-v-5">
                                        <div class="row">
                                            <div class="col-sm-4"><span class="text-muted">School</span></div>
                                            <div class="col-sm-8">
                                                <asp:Label ID="lblSchool" runat="server" Text="Label"></asp:Label>
                                            </div>
                                        </div>
                                    </li>
                                </asp:Panel>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">Phone</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">Dietary</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblDietary" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">Personality</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblPersonality" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted">Religion</span></div>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblReligion" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Panel ID="pnlPw" runat="server"><a href="ChangePassword.aspx">Change Password</a></asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default panel-info">
                        <div class="panel-heading panel-heading-gray">
                            <a href="InterestForm.aspx" class="btn btn-info btn-xs pull-right"><i class="fa fa-pencil"></i></a>
                            <i class="fa fa-gratipay"></i>&nbsp;Interest
                        </div>
                        <div class="panel-body">
                            <ul class="list-unstyled profile-about margin-none">
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted"><b>Cause</b></span></div>
                                        <div class="col-sm-8">
                                            <asp:DataList ID="dlBene" runat="server">
                                                <ItemTemplate>
                                                    <%# Eval("TERM_NAME") %>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-v-5">
                                    <div class="row">
                                        <div class="col-sm-4"><span class="text-muted"><b>Role</b></span></div>
                                        <div class="col-sm-8">
                                            <asp:DataList ID="dlRole" runat="server">
                                                <ItemTemplate>
                                                    <%# Eval("TERM_NAME") %>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </div>
                                </li>
                                <asp:Panel ID="pnlSkill" runat="server">
                                    <li class="padding-v-5">
                                        <div class="row">
                                            <div class="col-sm-4"><span class="text-muted"><b>Skill</b></span></div>
                                            <div class="col-sm-8">
                                                <asp:DataList ID="dlskill" runat="server">
                                                    <ItemTemplate>
                                                        <%# Eval("TERM_NAME") %>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </div>
                                    </li>
                                </asp:Panel>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="media-body">

                <div class="row">
                <div class="item col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-default text-center">
                  <div class="panel-body">
                    <div data-percent="85" data-size="95" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="6">
                      <div class="value text-center">
                        <span class="strong"><i class="fa-3x icon-star"></i><br /> <br /><h2 class="text-display-2 text-primary margin-none"> <asp:Label ID="lbltotalvia" runat="server" Text=""></asp:Label> </h2><h4 class="margin-none"><br />CIP Hours</h4></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                <div class="item col-md-3 col-sm-6 col-xs-12 " data-toggle="tooltip" title="View Completed Events" onclick="location.href='volunteerevents.aspx'">
                <div class="panel panel-default text-center">
                  <div class="panel-body">
                    <div data-percent="85" data-size="95" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="6">
                      <div class="value text-center">
                        <span class="strong"><i class="fa-3x icon-trophy"></i><br /> <br /><h2 class="text-display-2 text-success margin-none">  <asp:Label ID="lblcompleted" runat="server" Text=""></asp:Label> </h2><h4 class="margin-none"><br />Completed Events</h4></span>
                      </div>
                    </div>  
                  </div>
                </div>
              </div>
                <div class="item col-md-3 col-sm-6 col-xs-12" data-toggle="tooltip" title="View Ongoing Events" onclick="location.href='volunteerevents.aspx'">
                <div class="panel panel-default text-center">
                  <div class="panel-body">
                    <div data-percent="85" data-size="95" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="6">
                      <div class="value text-center">
                        <span class="strong"><i class="fa-3x icon-time-clock"></i><br /> <br /><h2 class="text-display-2 text-warning margin-none"> <asp:Label ID="lblongoing" runat="server" Text=""></asp:Label> </h2><h4 class="margin-none"><br />Ongoing Events</h4></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>                 
              <div class="item col-md-3 col-sm-6 col-xs-12" data-toggle="tooltip" title="View Upcoming Events" onclick="location.href='volunteerevents.aspx'">
                <div class="panel panel-default text-center">
                  <div class="panel-body">
                    <div data-percent="85" data-size="95" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" data-line-width="6">
                      <div class="value text-center">
                        <span class="strong"><i class="fa-3x icon-calendar-2"></i><br /> <br /><h2 class="text-display-2 text-primary margin-none"> <asp:Label ID="lblupcoming" runat="server" Text=""></asp:Label> </h2><h4 class="margin-none"><br />Upcoming Events</h4></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div> 
                                              
                    </div>

                    <%--    <asp:Timer runat="server" ID="UpdateTimer" Interval="20000" OnTick="UpdateTimer_Tick"></asp:Timer>--%>
                   
             
                <div class="row">
                    <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-gray">
                        <i class="fa fa-bookmark"></i>&nbsp;Badges
                    </div>
                    <div class="panel-body">
                        <asp:DataList ID="dlBadge" runat="server" OnItemDataBound="dlBadge_ItemDataBound" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <ItemTemplate>
                                <%--  <div class="col-sm-2">--%>
                                <asp:Image ID="imgBadge" data-toggle="tooltip" title='<%# Eval("DESCRIPTION") %>' runat="server" Width="80px" Height="80px" src='<%# Eval("PHOTO_PATH", "http://sgmgmtportal.azurewebsites.net/{0}") %>' />
                                <%--      </div>--%>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </div>
                     </div>
                 <div class="col-md-4">
                        <div class="panel panel-default" style="border-radius: 8px">
                            <div class="panel-heading panel-heading-gray">
                                Notification <i class="fa fa-fw fa-bell-o" style="color: red"></i>
                            </div>

                            <div class="panel-body">
                           <%--      <asp:UpdatePanel runat="server" ID="TimedPanel" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
                                    </Triggers>
                                    <ContentTemplate>--%>
                                        <div style="overflow-y: scroll; overflow-x: hidden; width: 100%; min-height: 400px;">
                                            <asp:Label runat="server" ID="lblNotif" />
                                        </div>
                                <%--    </ContentTemplate>
                                </asp:UpdatePanel>--%>
                            </div>
                        </div>

                    </div>
                 
                    </div>
            </div>
        </div>
    </div>

</asp:Content>

  
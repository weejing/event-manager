﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="RecommendVolunteers.aspx.cs" Inherits="SGServe.RecommendVolunteers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- content push wrapper -->
    <div class="st-pusher" id="content">

      <!-- sidebar effects INSIDE of st-pusher: -->
      <!-- st-effect-3, st-effect-6, st-effect-7, st-effect-8, st-effect-14 -->

      <!-- this is the wrapper for the content -->
      <div class="st-content">

        <!-- extra div for emulating position:fixed of the menu -->
        <div class="st-content-inner">

          <div class="cover overlay tutors height-200 height-160-m height-120-s">
              <img src="images/misc/display02.jpg" width="1700"> 
            <div class="overlay overlay-full overlay-bg-grey">
              <div class="container">
                <h1 class="text-display-1 text-overlay">Recommended Friends </h1>
                <p class="text-subhead text-overlay"> </p>
              </div>
            </div>
          </div>
          <div class="container">

    <div class="row">
        <br>
        <asp:Panel ID="Panel1" runat="server">
            <div class="panel panel-default">
                <asp:DataList ID="DataListRecommend" runat="server" RepeatColumns="3">
                      <ItemTemplate>
                            <div class="boxed">
                              <div class="media">
                                <div class="media-left">
                                  <a href="#">
                                    <img src=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> alt="" width="80" class="media-object">
                                  </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                <div class="media-body">
                                    <h4><asp:HyperLink ID="linkName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "FULL_NAME") %> ForeColor="Black" NavigateUrl='<%# string.Format("UserPortfolio.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>'></asp:HyperLink></h4>
                                    <br/>
                                    <asp:Label ID="lblMessage" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "RECOMMENDATION_FROM") %>></asp:Label><br/>
                                </div>
                              </div>
                            </div>

                        </ItemTemplate>
                  </asp:DataList>
                <asp:Label ID="lblNoRecommend" runat="server" Text="There are current no volunteers to recommend for you."></asp:Label>
            </div>
        </asp:Panel>
    </div>

          </div>

        </div>
        <!-- /st-content-inner -->

      </div>
      <!-- /st-content -->

    </div>
    <!-- /st-pusher -->
</asp:Content>

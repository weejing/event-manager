﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="SGServe.EditProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h4 class="page-section-heading">Personal Particulars</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
                <label>Email: </label>
                <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="form-group form-control-default required">
                <label>
                    Full Name (as of NRIC)
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtFullName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                <asp:TextBox ID="txtFullName" runat="server" class="form-control"></asp:TextBox>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-control-default required">
                        <label>
                            NRIC
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtNRIC" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ID="RegularExpressionValidator1" ControlToValidate="txtNRIC" runat="server" ValidationExpression="^[STFG]\d{7}[A-Z]$" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                        </label>
                        <asp:TextBox ID="txtNRIC" runat="server" class="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-control-default required">
                        <label>
                            Phone
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtPhone" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ID="RegularExpressionValidator2" ControlToValidate="txtPhone" runat="server" ValidationExpression="^[689]\d{7}$" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                        </label>
                        <asp:TextBox ID="txtPhone" runat="server" class="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-control-default">
                        <label>Personality (Myers-briggs) <a href="https://www.16personalities.com/personality-types" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                        <asp:DropDownList ID="ddlPersonality" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlSchLvl_SelectedIndexChanged">
                            <asp:ListItem>ESTP</asp:ListItem>
                            <asp:ListItem>ESTJ</asp:ListItem>
                            <asp:ListItem>ESFP</asp:ListItem>
                            <asp:ListItem>ESFJ</asp:ListItem>
                            <asp:ListItem>ENFP</asp:ListItem>
                            <asp:ListItem>ENFJ</asp:ListItem>
                            <asp:ListItem>ENTP</asp:ListItem>
                            <asp:ListItem>ENTJ</asp:ListItem>
                            <asp:ListItem>ISTJ</asp:ListItem>
                            <asp:ListItem>ISTP</asp:ListItem>
                            <asp:ListItem>ISFJ</asp:ListItem>
                            <asp:ListItem>ISFP</asp:ListItem>
                            <asp:ListItem>INFJ</asp:ListItem>
                            <asp:ListItem>INFP</asp:ListItem>
                            <asp:ListItem>INTJ</asp:ListItem>
                            <asp:ListItem>INTP</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-control-default required">
                        <label>
                            Date of Birth
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="DOB" ErrorMessage="Please select" ForeColor="Red"></asp:RequiredFieldValidator>
                        </label>
                        <asp:TextBox ID="DOB" runat="server" ReadOnly class="form-control datepicker"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-control-default required">
                        <label>Religion</label>
                        <asp:DropDownList ID="ddlReligion" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" data-toggle="dropdown" Width="90%">
                            <asp:ListItem>None</asp:ListItem>
                            <asp:ListItem>Christianity</asp:ListItem>
                            <asp:ListItem>Islam</asp:ListItem>
                            <asp:ListItem>Buddhism</asp:ListItem>
                            <asp:ListItem>Others</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-control-default required">
                        <label>Dietary</label>
                        <asp:DropDownList ID="ddlDietary" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" data-toggle="dropdown" Width="90%">
                            <asp:ListItem>None</asp:ListItem>
                            <asp:ListItem>Halal</asp:ListItem>
                            <asp:ListItem>Vegeterian</asp:ListItem>
                            <asp:ListItem>G6PD Dificiency</asp:ListItem>
                            <asp:ListItem>No Beef</asp:ListItem>
                            <asp:ListItem>No Peanut</asp:ListItem>
                            <asp:ListItem>Others</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
            <div class="form-group form-control-default required">
                <label>Current School</label>
                <asp:DropDownList ID="ddlSchLvl" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" AutoPostBack="True" OnSelectedIndexChanged="ddlSchLvl_SelectedIndexChanged">
                    <asp:ListItem>None</asp:ListItem>
                    <asp:ListItem>Primary</asp:ListItem>
                    <asp:ListItem>Secondary</asp:ListItem>
                    <asp:ListItem>Junior College</asp:ListItem>
                    <asp:ListItem>Mixed Level</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlSchool" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true">
                </asp:DropDownList>
                     </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:Button ID="btnSave" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSave_Click" /><br />
    <br />
</asp:Content>

﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="SGServe.Messages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h4 class="page-section-heading">Messages from friends</h4>
    <div class="panel panel-default">
        <div class="table-responsive">
            <asp:GridView CellPadding="50" 
        
        
        GridLines="None" Height="24px" Width="100%" CssClass="table"
                            AllowPaging="True" EmptyDataText="You have no messages received." ID="MessagesFromItemGrid" DataKeyNames="USR_COMMENT_ID" runat="server" AutoGenerateColumns="False" >
                <Columns>
                    <asp:BoundField DataField="FROM_USR_NAME" HeaderText="From"></asp:BoundField>
                    <asp:BoundField DataField="COMMENT" HeaderText="Messages"></asp:BoundField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lnkDeleteFrom" PostBackUrl='<%# string.Format("~/RemoveMessage.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_COMMENT_ID")) %>'>Delete</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle ForeColor="#0066CC" />
                <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
            </asp:GridView>
           

        </div>
    </div> 
            <br/>
        <h4 class="page-section-heading">Messages to friends</h4>
    <div class="panel panel-default">
        <div class="table-responsive">
            <asp:GridView CellPadding="50" GridLines="None" Height="24px" Width="100%" CssClass="table" AllowPaging="True" EmptyDataText="You have not written any messages. Come write some messages to show your appreciation to your friends." ID="MessagesToItemGrid" DataKeyNames="USR_COMMENT_ID" runat="server" AutoGenerateColumns="False" >
                <Columns>
                    <asp:BoundField DataField="TO_USR_NAME" HeaderText="To"></asp:BoundField>
                    <asp:BoundField DataField="COMMENT" HeaderText="Messages"></asp:BoundField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lnkDeleteTo" PostBackUrl='<%# string.Format("~/RemoveMessage.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_COMMENT_ID")) %>'>Delete</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle ForeColor="#0066CC" />
                <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
            </asp:GridView>
            </div>
    </div>
    <br/>
    <asp:Button ID="btnBack" runat="server" class="btn btn-info" Text="Back to Portfolio" OnClick="btnBack_Click" />
    <br/>
    <br/>
</asp:Content>

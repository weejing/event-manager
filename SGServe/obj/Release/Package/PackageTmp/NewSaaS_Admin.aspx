﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" Async="true" AutoEventWireup="true" CodeBehind="NewSaaS_Admin.aspx.cs" Inherits="SGServe.NewSaaS_Admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="page-section">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h4 class="page-section-heading">Create Account</h4>
                         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
          <ContentTemplate> 
                        <div class="panel panel-default">                                 
                            <div class="panel-body">
                                <div class="form-group form-control-default required">
                                    <label>
                                        Full Name
               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtFullName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="txtFullName" runat="server" class="form-control"></asp:TextBox>            
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>
                                        Email
                   
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup ="createGrp" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup ="createGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                        <asp:Label ID="lblmessage" runat="server" Text="" ForeColor="#FF3300"></asp:Label>
                                    </label>
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                
                                <div class="form-group form-control-default required">
                                    <label>
                                        Password
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtPassword"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="createGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator4" ControlToValidate="txtPassword" runat="server" ValidationExpression="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})" ErrorMessage="Must contain digit,uppercase and lowercase. Min 8 characters"></asp:RegularExpressionValidator>
                                    </label>
                                    <asp:TextBox ID="txtPassword" type="password" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group form-control-default required">
                                    <label>
                                        Repeat Password
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtPw2"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
   <asp:CompareValidator Display="Dynamic" ValidationGroup="createGrp" SetFocusOnError="true" ID="CompareValidator1" runat="server" ErrorMessage="Password does not match" ControlToCompare="txtPassword" ControlToValidate="txtPw2" ForeColor="Red"></asp:CompareValidator>
                                    </label>
                                    <asp:TextBox ID="txtPw2" type="password" runat="server" class="form-control" ControlToValidate="txtPw2"></asp:TextBox>
                                </div>
                            </div>
                        </div>
              </ContentTemplate>
    </asp:UpdatePanel>

                        <asp:Button ID="btnSave" runat="server" class="btn btn-success"  ValidationGroup ="createGrp" Text="Save" OnClick="btnSave_Click"/><br />
                        <br />
                         <br />
                    </div>
                </div>
            </div>

</asp:Content>


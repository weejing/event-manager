﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="UserFriends.aspx.cs" Inherits="SGServe.UserFriends" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="community_css/css/vendor/all.css" rel="stylesheet">
    <link href="community_css/css/app/app.css" rel="stylesheet">
    <script src="community_css/js/vendor/all.js"></script>
    <script src="community_css/js/app/app.js"></script>

          <div class="container">
            <div class="cover profile">

              <div class="wrapper">
                <div class="image" style="height:58px;">
                  <!--<img src="main_css/images/place1-wide.jpg" alt="people" />-->
                </div>
              </div>
                    
              <div class="cover-info">
                <div class="avatar">
                  <asp:Image ID="ProfilePicImg" runat="server" />
                </div>
                <div class="name">
                    <h2>
                        <asp:Label ID="lblFullName" runat="server" Text="Name" ForeColor="Black"></asp:Label>
                    </h2>
                            </div>
                <ul class="cover-nav">
                  <li><asp:HyperLink ID="linkAbout" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> About </asp:HyperLink></li>
                  <li class="active"><asp:HyperLink ID="linkFriends" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Friends </asp:HyperLink> </li>
                  <li><asp:HyperLink ID="linkFollows" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> Follows </asp:HyperLink></li>

                    <li><asp:Button ID="btnAddFriend" runat="server" class="btn btn-info" Text="Add Friend" OnClick="btnAddFriend_Click" /></li>
                    <li><asp:Label ID="lblPendingFriends" runat="server" Text="Pending friend request"></asp:Label></li>
                    <li><asp:Panel ID="PanelF" runat="server">
                    <button class="btn btn-success disabled" type="button" data-toggle="dropdown">Friend</button></asp:Panel></li>
                    <li><asp:Button ID="btnRemoveFriend" runat="server" class="btn btn-danger" Text="Remove friend" OnClick="btnRemoveFriend_Click" /></li>
                    </ul>
              </div>
                <div class="panel panel-default">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                  <div class="panel-body" >
                        <h5><asp:Label ID="lblDescription" runat="server">description</asp:Label></h5>
                  </div>
                </div>
              </div>
            </div>
           
               <br/>
              <asp:Panel ID="panelFriendRequest" runat="server">
                  <h3><asp:Label ID="lblFR" runat="server" Text="Friend Request"></asp:Label>
                  </h3>

                  <div class="table-responsive" style="padding-left: 3%; padding-right: 3%;">
                    <asp:GridView ID="FriendRequestGridView" CellPadding="50" GridLines="None" Width="100%" CssClass="table"
                        AllowPaging="True" EmptyDataText="" runat="server" AutoGenerateColumns="False" PageSize="10" 
                        DataKeyNames = "USR_ID">
                        <Columns>
                            <asp:TemplateField>
                                    <ItemTemplate>
                                       <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> Width="80" />
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FULL_NAME"/>
                            <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="btnProfile" CssClass="btn btn-info" Width="160px" Height="30px" PostBackUrl='<%# string.Format("UserPortfolio.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>'>
                                        <i class="fa fa-user"></i>Profile</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="btnAccept" CssClass="btn btn-success" Width="160px" Height="30px" OnClick="btnAcceptFriend_Click">
                                        <i class="fa fa-circle-o"></i>Accept</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="btnDelete" CssClass="btn btn-danger" Width="160px" Height="30px" OnClick="btnDeleteFriendRequest_Click">
                                        <i class="fa fa-times"></i>Remove</asp:LinkButton>
                                    </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
                                 
                  <h3>Friends</h3>
                  </asp:Panel>
              <br/>

              <div class="row" data-toggle="isotope">

              <asp:DataList ID="DataList1" runat="server" onitemcommand="DataList1_ItemCommand" RepeatColumns="3">
                    <ItemTemplate>

                  <div class="col-md-6 col-lg-4 item">
                    <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="media">
                      <div class="pull-left">
                        <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> Width="80" />
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading margin-v-5"><asp:Label ID="lblName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "FULL_NAME") %>></asp:Label></h4>
                        <h5><asp:Label ID="lnlSchool" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "SCHL_NAME") %>></asp:Label></h5>
                      </div>
                    </div>
                  </div>
                  <div class="panel-footer">
                       <asp:Button ID="btnProfile" runat="server" class="btn btn-default btn-sm" Text="Profile" CommandName="navigatePortfolio" PostBackUrl='<%# string.Format("UserPortfolio.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>'/> 
                  </div>
                </div>    
                </div> 
                    </ItemTemplate>
                </asp:DataList>
                  <asp:Label ID="lblNone" runat="server" Text="The volunteer has not added anyone as friends on SGServe."></asp:Label>
                </div>
              <br/>
              <br/>
              <br/>
    </div>
</asp:Content>

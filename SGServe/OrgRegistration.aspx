﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" Async="true" AutoEventWireup="true" CodeBehind="OrgRegistration.aspx.cs" Inherits="SGServe.OrgRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="page-section">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h4 class="page-section-heading">Organization Detail</h4>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group form-control-default required">
                                    <label>
                                        Organization Name
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtFullName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                    <asp:TextBox ID="txtFullName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group form-control-default required">
                                            <label>
                                                Email
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="signUpGrp" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="signUpGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                                <asp:Label ID="lblmessage" runat="server" Text="Email already exists" ForeColor="#FF3300"></asp:Label>
                                            </label>
                                            <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                                                            </div>
 
                                        <div class="col-md-4">
                                            <div class="form-group form-control-default required">
                                                <label>
                                                    Contact Number
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtPhone" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ValidationGroup="signUpGrp" ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ID="RegularExpressionValidator2" ControlToValidate="txtPhone" runat="server" ValidationExpression="^[689]\d{7}$" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                                                </label>
                                                <asp:TextBox ID="txtPhone" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-control-default required">
                                                <label>Postal Code
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtPostal"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPostal" Display="Dynamic"
    ErrorMessage="Numbers only" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                </label>
                                                <asp:TextBox ID="txtPostal"  runat="server" MaxLength="6" class="form-control"></asp:TextBox>
                                            </div>
                                                                                        </div>
                                            <div class="col-md-8">
                                                <div class="form-group form-control-default required">
                                                    <label>
                                                        Address
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="signUpGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtAddress"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                                    <asp:TextBox ID="txtAddress" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:Button ID="btnReg" runat="server" class="btn btn-primary" ValidationGroup="signUpGrp" Text="Submit" OnClick="btnReg_Click" /><br />
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

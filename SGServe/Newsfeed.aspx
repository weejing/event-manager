﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="Newsfeed.aspx.cs" Inherits="SGServe.Newsfeed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br/>
        <div class="row">
                <!-- LEFT COLUMN -->
              <div class="col-sm-4 col-md-4">
                 <div class="width-300 width-auto-xs">
                  <div class="panel panel-default widget-user-1 text-center">
                      <div class="avatar">
                          <br />
                        <asp:Image ID="ProfilePicImg" runat="server" Width="100" />
                        <h2><asp:Label ID="lblFullName" runat="server" Text="Name" ForeColor="Black"></asp:Label></h2>
                        <asp:Button ID="btnPortfolio" runat="server" class="btn btn-success btn-xs" Text="Portfolio" OnClick="btnPortfolio_Click" />&nbsp
                        <asp:Button ID="btnFriends" runat="server" class="btn btn-info btn-xs" Text="Friends" OnClick="btnFriend_Click" />
                      </div>
                      <br/>
                      <!--
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Notfications
                    </div>
                    <div class="panel-body">
                      some stuffs
                    </div>
                  </div>
                      -->

                      <asp:Panel ID="PanelR" runat="server">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <i class="fa fa-users fa-2x"></i><h5 class="text-center">Recommend Friends</h5>
                    <a href="RecommendVolunteers.aspx" class="btn btn-primary btn-xs pull-right">View more </a>
                        <br />
                    </div>
                    <div class="panel-body">
                        <asp:DataList ID="DataListRecommend" runat="server">
                      <ItemTemplate>
                            <div class="boxed">
                              <div class="media">
                                <div class="media-left">
                                    <img src=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> alt="" width="50" class="media-object">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                <div class="media-body">
                                    <asp:HyperLink ID="linkName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "FULL_NAME") %> ForeColor="Black" NavigateUrl='<%# string.Format("UserPortfolio.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>'></asp:HyperLink>
                                    <h6><asp:Label ID="lblMessage" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "RECOMMENDATION_FROM") %>></asp:Label></h6><br/>
                                </div>
                              </div>
                            </div>

                        </ItemTemplate>
                  </asp:DataList>  
                     </div>
                  </div>
                 </asp:Panel>

                <div class="panel panel-default">
                    <div class="panel-heading">
                     <i class="fa-2x icon-trophy"></i><h5 class="text-center">Top VIA hours</h5>
                        <asp:HyperLink ID="HyperLinkTopVIA" runat="server" CssClass="btn btn-primary btn-xs pull-right" NavigateUrl="~/TopVIAUsers.aspx">View More</asp:HyperLink>
                        <br />
                    </div>
                    <div class="panel-body">

                     <asp:DataList ID="DataListTopVIA" runat="server">
                      <ItemTemplate>

                            <div class="boxed">
                              <div class="media">
                                <div class="media-left">
                                    <img src=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> alt="" width="50" class="media-object">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                <div class="media-body">
                                    <asp:HyperLink ID="linkName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "FULL_NAME") %> ForeColor="Black" NavigateUrl='<%# string.Format("UserPortfolio.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>'></asp:HyperLink>
                                    <br/>
                                    <asp:Label ID="lblHours" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "VIA_HOURS") %>></asp:Label>&nbsp;hours<br/>
                                </div>
                              </div>
                            </div>

                        </ItemTemplate>
                  </asp:DataList>  
                        <asp:Label ID="lblNoneVIA" runat="server" Text="The volunteers with highest VIA hours will be displayed here. There are currently no volunteers who have completed event." Visible="False"></asp:Label>

                     </div>
                  </div>


                </div>
                </div>
            </div>

            <!-- RIGHT COLUMN -->

             <div class="col-sm-8 col-md-8">

                 <div class="container">
            <div class="row">
              <div class="col-md-7">
                <ul class="timeline-list">

                    <asp:DataList ID="DataList3" runat="server">
                      <ItemTemplate>


                    <li class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="user-wrapper">
                        <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "USR_IMAGE") %> Width="80" />
                        <div class="date"><asp:Label ID="lblDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ACTIVITY_DATETIME") %>'></asp:Label></div>
                      </div>
                    </div>
                    <div class="media-body">
                      <div class="media-body-wrapper">
                        <div class="panel panel-default">

                          <div class="panel-body">
                            <div class="boxed">
                              <div class="media">
                                <div class="media-body">
                                    <h4>
                                        <asp:LinkButton ID="lbUserFriend" runat="server" ForeColor="Black" Text=<%# DataBinder.Eval(Container.DataItem, "USR_FULL_NAME") %> PostBackUrl='<%# string.Format("UserPortfolio.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>'></asp:LinkButton>
                                        <asp:Label ID="lblActivityName" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "MESSAGE_DISPLAY") %>></asp:Label>
                                        <asp:LinkButton ID="lblTargetName" runat="server" ForeColor="Black" Text='<%# DataBinder.Eval(Container.DataItem, "TARGET_NAME") %>' PostBackUrl='<%# string.Format("{0}", DataBinder.Eval(Container.DataItem, "EVENT_ID")) %>'></asp:LinkButton>
                                    </h4>
                                </div>
                                <div class="media-right">
                                  <a href="#">
                                    <asp:Image ID="Image2" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "TARGET_IMAGE") %> Width="75" />
                                </div>
                              </div>
                            </div>

                          </div>

                        </div>
                      </div>
                    </div>
                  </li>


                         </ItemTemplate>
                  </asp:DataList>
                    </ul>
              </div>
               </div> </div>
              </div>


        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class RemoveMessage : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        static String currentUserID = "";
        static String navigateCommentID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                await retrieveUserDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    navigateCommentID = Request.QueryString["id"];
                    currentUserID = volunteer.VOL_ID;
                }
            }

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/removeComments?commentID=" + navigateCommentID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("Messages.aspx?id=" + currentUserID);
        }

        private async Task retrieveUserDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            if (jwtToken != null)
            {
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                // add the jwt token into the authorization header for server validation
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                volunteer = GlobalModule.getUserDetails(reply);
            }
        }
    }
}
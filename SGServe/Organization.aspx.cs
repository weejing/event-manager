﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class Organization : System.Web.UI.Page
    {
        static Volunteer volunteer = new Volunteer();
        static String currentUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["LoginType"] != null)
                {
                    if (Session["LoginType"].ToString().Equals("Organiser"))
                    {
                        generateFormWithoutLogin();
                        DataList1.Visible = false;
                        DataList2.Visible = true;
                    }
                }
                else
                {
                    await retrieveUserDetails();
                }
                   
                if (volunteer.VOL_ID == null)
                {
                    generateFormWithoutLogin();
                    DataList1.Visible = false;
                    DataList2.Visible = true;
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateForm();
                    DataList1.Visible = true;
                    DataList2.Visible = false;
                }
            }

            
        }

        private async Task retrieveUserDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            if (jwtToken != null)
            {
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                // add the jwt token into the authorization header for server validation
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                volunteer = GlobalModule.getUserDetails(reply);
            }
        }

        protected async void generateForm()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListofOrganisationsWithFollow?userID=" + currentUserID);
                HttpResponseMessage resp = await client.SendAsync(req);
                String details = await resp.Content.ReadAsStringAsync();

                List<OrganizationDetailsModel> lod = JsonConvert.DeserializeObject<List<OrganizationDetailsModel>>(details);

                foreach (OrganizationDetailsModel org in lod)
                {
                    if (org.UNFOLLOW_STATUS.Equals("unfollow"))
                    {
                        org.UEN = "btn btn-success btn-danger btn-l btn-xs";
                    }
                    else
                    {
                        org.UEN = "btn btn-success btn-success btn-l btn-xs";
                    }
                }

                DataList1.DataSource = lod;
                DataList1.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }// end of generateForm()


        protected async void generateFormWithoutLogin()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getAllListofOrganisations");
                HttpResponseMessage resp = await client.SendAsync(req);
                String details = await resp.Content.ReadAsStringAsync();

                List<OrganizationDetailsModel> lod = JsonConvert.DeserializeObject<List<OrganizationDetailsModel>>(details);

                DataList2.DataSource = lod;
                DataList2.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }// end of generateFormWithoutLogin()


        protected async void btnFollow_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            String status = button.Text;
            String orgID = button.CommandArgument;

            if (status == "follow")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + orgID + "');", true);

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/followOrganisation?userID=" + currentUserID + "&orgID=" + orgID);
                HttpResponseMessage resp = await client.SendAsync(request);

                String response = await resp.Content.ReadAsStringAsync();

                // pop up

                Response.Redirect("Organization.aspx");
            }
            else
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/unfollowOrganisation?userID=" + currentUserID + "&orgID=" + orgID);
                HttpResponseMessage resp = await client.SendAsync(request);

                String response = await resp.Content.ReadAsStringAsync();

                // pop up

                Response.Redirect("Organization.aspx");
            }

        }

        protected async void btnSearch_Click(object sender, EventArgs e)
        {
            if (currentUserID.Trim().Equals(""))
            {
                try
                {
                    Server_Connect serverConnection = new Server_Connect();
                    HttpClient client = serverConnection.getHttpClient();

                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListofOrganisationsBySearch?name=" + tbSearch.Text);
                    HttpResponseMessage resp = await client.SendAsync(req);
                    String details = await resp.Content.ReadAsStringAsync();

                    List<OrganizationDetailsModel> lod = JsonConvert.DeserializeObject<List<OrganizationDetailsModel>>(details);

                    DataList2.DataSource = lod;
                    DataList2.DataBind();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("---Issue Start---");
                    System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                    System.Diagnostics.Debug.WriteLine("---End---");
                }
            }
            else
            {
                try
                {
                    Server_Connect serverConnection = new Server_Connect();
                    HttpClient client = serverConnection.getHttpClient();

                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListofOrganisationsWithFollowBySearch?userID=" + currentUserID + "&name=" + tbSearch.Text);
                    HttpResponseMessage resp = await client.SendAsync(req);
                    String details = await resp.Content.ReadAsStringAsync();

                    List<OrganizationDetailsModel> lod = JsonConvert.DeserializeObject<List<OrganizationDetailsModel>>(details);

                    foreach (OrganizationDetailsModel org in lod)
                    {
                        if (org.UNFOLLOW_STATUS.Equals("unfollow"))
                        {
                            org.UEN = "btn btn-success btn-danger btn-l btn-xs";
                        }
                        else
                        {
                            org.UEN = "btn btn-success btn-success btn-l btn-xs";
                        }
                    }

                    DataList1.DataSource = lod;
                    DataList1.DataBind();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("---Issue Start---");
                    System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                    System.Diagnostics.Debug.WriteLine("---End---");
                }
            }
 
        }
    }//end of class
}//end of namespace
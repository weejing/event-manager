﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="InterestForm.aspx.cs" Inherits="SGServe.InterestForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <h4 class="page-section-heading"></h4>
    <div class="panel panel-default">     
        <asp:HiddenField ID="hiddenBeneVocabID" runat="server" />
        <asp:HiddenField ID="hiddenRoleVocabID" runat="server" />
        <asp:HiddenField ID="hiddenSkillVocabID" runat="server" />
        <div class="panel-body">
               <h3 class="text-h2 ribbon-heading ribbon-primary bottom-left-right">Interest Form</h3>
            <asp:Label ID="Label1" Style="font-style: italic" runat="server" Text="Interest response will be used to facilitate events matching based on your interests and skills"></asp:Label><br />
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-control-default required">
                        <div class="ribbon-mark ribbon-primary absolute right">
                            <span class="ribbon">
                                <span class="text">*</span>
                            </span>
                        </div>
                        <label>
                            Cause
                            <br>
                            <asp:CustomValidator ValidationGroup="signUpGrp" runat="server" SetFocusOnError="true" Display="Dynamic" ID="cv" ClientValidationFunction="ValidateBeneList" ErrorMessage="Please select" ForeColor="Red"></asp:CustomValidator>
                        </label>
                        <asp:CheckBoxList ID="cbBenefi" runat="server" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-control-default">
                        <div class="ribbon-mark ribbon-primary absolute right">
                            <span class="ribbon">
                                <span class="text">*</span>
                            </span>
                        </div>
                        <label>
                            Roles
                        <br>
                            <asp:CustomValidator ValidationGroup="signUpGrp" runat="server" SetFocusOnError="true" Display="Dynamic" ID="CustomValidator1" ClientValidationFunction="ValidateRoleList" ErrorMessage="Please select" ForeColor="Red"></asp:CustomValidator>
                        </label>
                        <asp:CheckBoxList ID="cbRoles" runat="server" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-control-default">
                        <div class="ribbon-mark ribbon-primary absolute right">
                            <span class="ribbon">
                                <span class="text"></span>
                            </span>
                        </div>
                        <label>Skills</label>
                        <asp:CheckBoxList ID="cbSkill" runat="server" SetFocusOnError="true" CssClass="checkbox checkbox-success" RepeatDirection="vertical">
                        </asp:CheckBoxList>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <asp:Button ID="btnSave" runat="server" ValidationGroup="signUpGrp" class="btn btn-primary" Text="Save" OnClick="btnSave_Click" /><br />
    <br />


    <script language="javascript">
        function ValidateBeneList(source, args) {
            var chkListModules = document.getElementById('<%= cbBenefi.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
        function ValidateRoleList(source, args) {
            var chkListModules = document.getElementById('<%= cbRoles.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }

    </script>
</asp:Content>

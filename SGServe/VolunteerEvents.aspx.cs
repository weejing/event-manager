﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data;
using System.Threading;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SGServe
{
    public partial class VolunteerEvents : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        List<UserPastEvents> completedEventsDetails = new List<UserPastEvents>();
        List<UserPastEvents> upcomingEventsDetails = new List<UserPastEvents>();
        List<UserPastEvents> ongoingEventsDetails = new List<UserPastEvents>();
        List<UserPastEvents> upcomingEventsDetails2 = new List<UserPastEvents>();
        protected async void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    await getCompletedEvents();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

        }

        public int generateVia()
        {
            string eventName = hdnfldVariable.Value;
            hdnfldVariable.Value = "";
            //check which organisation does the event belongs to
            getOrganisationName(eventName);
            return 0;
        }

        public void getOrganisationName(string eventName)
        {
            if (!eventName.Equals(""))
            {
                generatePDF(eventName);
            }
        }

        public void generatePDF(string eventName)
        {
            DateTime eventDate = new DateTime();
            string orgname = "";
            TimeSpan viahours = new TimeSpan();
            foreach (UserPastEvents uep in completedEventsDetails)
            {
                if (uep.EVT_NAME.Equals(eventName) && !uep.CONNECTION_STRING.Equals(""))
                {
                    eventDate = uep.EVT_DATE;
                    orgname = uep.CONNECTION_STRING;
                    viahours = uep.TIME_OUT- uep.TIME_IN;
                }
            }
           
            string filename = orgname + "_VIA_TEMPLATE.txt";
            String path = Server.MapPath("~/SaaS/VIATemplate/" + filename);
            string imagePath = "";
            if (!path.Equals(""))
            {
                //format via letter and replace @ fields with actual data
                StringBuilder sbHtmlText;
                using (System.IO.StreamReader Reader = new System.IO.StreamReader(path))
                {
                    string fileContent = Reader.ReadToEnd();
                    string[] textArr = fileContent.Split('@');
                    string replacedText = "";
                    for (int i = 0; i < textArr.Length; i++)
                    {
                        if (i == 1)
                        {
                            imagePath = textArr[i];
                            textArr[i] = "";
                        }
                        if (textArr[i].Equals("date"))
                            textArr[i] = DateTime.Now.ToString("dd MMM yyyy");
                        else if (textArr[i].Equals("eventname"))
                            textArr[i] = eventName;
                        else if (textArr[i].Equals("volunteername"))
                            textArr[i] = volunteer.FULL_NAME;
                        else if (textArr[i].Equals("viahours"))
                        {
                            string[] time = viahours.ToString(@"hh\:mm").Split(':');
                            int hour = int.Parse(time[0]);
                            textArr[i] = hour + " Hours ";
                        }
                        else if (textArr[i].Equals("volunteernric"))
                            textArr[i] = volunteer.NRIC;
                        else if (textArr[i].Equals("eventdate"))
                            textArr[i] = eventDate.ToString("dd MMM yyyy");

                        replacedText += textArr[i];
                    }
                    sbHtmlText = new StringBuilder(replacedText);
                }

                Document document = new Document();
                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(Server.MapPath("~/SaaS/VIATemplate/OrgLogo/" + imagePath));
                img.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                //pass in original height, original width, max height, max width
                ResizeImage ri = new ResizeImage(img.Height, img.Width, 150f, 550f);
                img.ScaleToFit(ri.getImageHeightFloat(), ri.getImageWidthFloat());
                PdfWriter.GetInstance(document, new FileStream(Server.MapPath("~/SaaS/VIATemplate/GeneratedVIA/" + volunteer.FULL_NAME + "_CIP.pdf"), FileMode.Create));
                //PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + "\\" + lblName.Text + "VIA.pdf", FileMode.Create));
                document.Open();
                document.Add(img);
                iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document);
                hw.Parse(new StringReader(sbHtmlText.ToString()));
                document.Close();
            }
            String filepath = Server.MapPath("~/SaaS/VIATemplate/GeneratedVIA/" + volunteer.FULL_NAME + "_CIP.pdf");
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "application/pdf";
            response.AddHeader("Content-Disposition", "attachment; filename=" + volunteer.FULL_NAME + "_CIP.pdf" + ";");
            response.TransmitFile(filepath);
            response.Flush();
            System.IO.File.Delete(filepath);
            response.End();
        }
        
        //retrieve the event id and connection string of the respective organisation of the completed events by volunteer (Console_vms DB)
        public async Task getCompletedEvents()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Dashboard/getNPO?volunteerid=" + volunteer.VOL_ID);
            HttpResponseMessage response = await client.SendAsync(request);

            String reply = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("reply: " + reply);

            //JsonArray array = (JsonArray)JsonValue.Parse(reply);
            List<string> connectionStringList = JsonConvert.DeserializeObject<List<string>>(reply);

            foreach (string connectionString in connectionStringList)
            {

                if (!connectionString.Equals(""))
                {
                    //completed
                    HttpRequestMessage completedRequest = new HttpRequestMessage(HttpMethod.Get, "/api/Dashboard/getCompletedEvents?connectionstring=" + connectionString + "&volunteerid=" + volunteer.VOL_ID);
                    HttpResponseMessage completedResponse = await client.SendAsync(completedRequest);

                    String eventListStr = await completedResponse.Content.ReadAsStringAsync();

                    JsonArray completedEventJson = (JsonArray)JsonValue.Parse(eventListStr);
                    foreach (var completeEvent in completedEventJson)
                    {
                        UserPastEvents pastevents = new UserPastEvents();
                        pastevents.EVT_ID = completeEvent["EVT_ID"].ToString().Replace("\"", "");
                        pastevents.EVT_DATE = DateTime.Parse(completeEvent["EVT_DATE"].ToString().Replace("\"", "")).Date;
                        pastevents.EVT_NAME = completeEvent["EVT_NAME"].ToString().Replace("\"", "");
                        pastevents.EVT_SESSION_ID = completeEvent["EVT_SESSION_ID"].ToString().Replace("\"", "");
                        pastevents.SESSION_NAME = completeEvent["SESSION_NAME"].ToString().Replace("\"", "");
                        pastevents.TIME_IN = DateTime.Parse(completeEvent["TIME_IN"].ToString().Replace("\"", ""));
                        pastevents.TIME_OUT = DateTime.Parse(completeEvent["TIME_OUT"].ToString().Replace("\"", ""));
                        pastevents.CONNECTION_STRING = connectionString;
                        completedEventsDetails.Add(pastevents);
                    }

                    //upcoming
                    request = new HttpRequestMessage(HttpMethod.Get, "/api/Dashboard/getUpcomingEvents?connectionstring=" + connectionString + "&volunteerid=" + volunteer.VOL_ID);
                    response = await client.SendAsync(request);
                    eventListStr = await response.Content.ReadAsStringAsync();
                    JsonArray upcomingEventJson = (JsonArray)JsonValue.Parse(eventListStr);
                    foreach (var upcomingEvent in upcomingEventJson)
                    {
                        UserPastEvents pastevents = new UserPastEvents();
                        pastevents.EVT_ID = upcomingEvent["EVT_ID"].ToString().Replace("\"", "");
                        pastevents.EVT_DATE = DateTime.Parse(upcomingEvent["EVT_DATE"].ToString().Replace("\"", "")).Date;
                        pastevents.EVT_NAME = upcomingEvent["EVT_NAME"].ToString().Replace("\"", "");
                        pastevents.EVT_SESSION_ID = upcomingEvent["EVT_SESSION_ID"].ToString().Replace("\"", "");
                        pastevents.SESSION_NAME = upcomingEvent["SESSION_NAME"].ToString().Replace("\"", "");
                        pastevents.CONNECTION_STRING = connectionString;
                        upcomingEventsDetails.Add(pastevents);
                    }
                }
            }
            countOngoingAndUpcoming();
            generateUI();
            generateVolUI();
        }

        public void generateVolUI()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < upcomingEventsDetails.Count; i++)
            {
                sb.Append("<li>");
                sb.Append("<span class=\"date\">" + upcomingEventsDetails[i].EVT_DATE.ToString("ddMMM") + "</span>");
                sb.Append("<span class=\"circle orange\">" + "</span>");
                sb.Append("<a href='EventDetail.aspx?e=" + upcomingEventsDetails[i].EVT_ID + "&o=" + upcomingEventsDetails[i].CONNECTION_STRING + "'>");
                sb.Append("<span class=\"content\">" + upcomingEventsDetails[i].EVT_NAME + "(" + upcomingEventsDetails[i].SESSION_NAME + ")" + "</span>");
                sb.Append("</a>");
                sb.Append("</li>");
            }

            if (upcomingEventsDetails.Count == 0)
            {
                lblnoupcomingevents.Visible = true;
                lblnoupcomingevents.Text = "There is currently no upcoming events!";
            }
            upcomingEvents.InnerHtml = sb.ToString();
            lblupcomingevents.Text = upcomingEventsDetails.Count.ToString();
            StringBuilder sb2 = new StringBuilder();
            for (int j = 0; j < ongoingEventsDetails.Count; j++)
            {
                sb2.Append("<li>");
                sb2.Append("<span class=\"date\">" + ongoingEventsDetails[j].EVT_DATE.ToString("ddMMM") + "</span>");
                sb2.Append("<span class=\"circle orange\">" + "</span>");
                sb2.Append("<a href='EventDetail.aspx?e=" + ongoingEventsDetails[j].EVT_ID + "&o=" + ongoingEventsDetails[j].CONNECTION_STRING + "'>");
                sb2.Append("<span class=\"content\">" + ongoingEventsDetails[j].EVT_NAME + "(" + ongoingEventsDetails[j].SESSION_NAME + ")" + "</span>");
                sb2.Append("</a>");
                sb2.Append("</li>");
            }
            if (ongoingEventsDetails.Count.ToString().Equals("0"))
            {
                lblnoongoingevents.Visible = true;
                lblnoongoingevents.Text = "There is currently no ongoing events!";
            }
            ongoingEvents.InnerHtml = sb2.ToString();
            lblongoingevents.Text = ongoingEventsDetails.Count.ToString();
        }

        public void countOngoingAndUpcoming()
        {
            List<int> positionList = new List<int>();
            for (int i = 0; i < completedEventsDetails.Count; i++)
            {
                if (upcomingEventsDetails.Contains(completedEventsDetails[i]))
                {
                    //ongoing
                    int index = upcomingEventsDetails.IndexOf(completedEventsDetails[i]);
                    ongoingEventsDetails.Add(upcomingEventsDetails[index]);
                    upcomingEventsDetails.RemoveAt(index);
                }
            }
        }

        public void generateUI()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < completedEventsDetails.Count; i++)
            {
                sb.Append("<li>");
                sb.Append("<span class=\"date\">" + completedEventsDetails[i].EVT_DATE.ToString("ddMMM") + "</span>");
                sb.Append("<span class=\"circle orange\">" + "</span>");
                sb.Append("<span class=\"content\">" + completedEventsDetails[i].EVT_NAME + "(" + completedEventsDetails[i].SESSION_NAME + ")" + "</span>");
                sb.Append("<span class=\"content\"><button class='btn btn-success btn-xs' id=\"" + completedEventsDetails[i].EVT_NAME + "\"" + "onClick=\"generateVIAButton(this.id)\" >" + "Download CIP Letter" + "</button>");
                sb.Append("</span>");
                sb.Append("</li>");
            }
            if (completedEventsDetails.Count.ToString().Equals("0"))
            {
                lblnocompletedevents.Visible = true;
                 lblnocompletedevents.Text = "There is currently no completed events!";
            }
            completedEvents.InnerHtml = sb.ToString();
            lbltotalcompletedevents.Text = completedEventsDetails.Count.ToString();
        }


    }
}
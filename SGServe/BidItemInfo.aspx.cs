﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using SGServe.Models.UserBiddingModels;
using SGServe.Models.TransactionModels;
using System.Threading.Tasks;


namespace SGServe
{
    public partial class BidItemInfo : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";
        static String bidItemID = "";
        static String itemID = "";
        static int minBidPoints = 0;
        static int currentBidAmount = 0;
        static int currentUserBidPoints = 0;

        protected async void Page_Load(object sender, EventArgs e)
        {
            bidItemID = Request.QueryString["bidItemID"];

            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateItemDetails();
                }
            }
        }


        protected async void generateItemDetails()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getBidItemDetails?bidItemID=" + bidItemID);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //get reply to bind to gridview
                    List<BidItem> items = JsonConvert.DeserializeObject<List<BidItem>>(reply);
                    itemID = items[0].SPR_ITEM_ID;
                    minBidPoints = items[0].MIN_POINT;
                    lblHeading.Text = items[0].ITEM_NAME;

                    if (System.DateTime.Compare(System.DateTime.Now, System.DateTime.Parse(items[0].END_DATE)) < 0)
                    {
                        // Panel allows bidding
                        PanelBid.Visible = true;
                    }
                    else
                    {
                        // Panel don't allow bidding
                        PanelBid.Visible = false;
                    }

                    if (items != null)
                    {
                        DateTime dtDisplay = Convert.ToDateTime(items[0].START_DATE);
                        DateTime dtDisplay2 = Convert.ToDateTime(items[0].END_DATE);
                        items[0].START_DATE = dtDisplay.ToString("d MMM yyyy h:mm tt");
                        items[0].END_DATE = dtDisplay2.ToString("d MMM yyyy h:mm tt");
                        if (items[0].STATUS.Equals("bidding"))
                        {
                            items[0].STATUS = "open for bidding";
                        }

                        ItemsGridView.DataSource = items;
                        ItemsGridView.DataBind();
                    }

                    HttpRequestMessage request1 = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getVolCurrentBidItemRecordDetails?userID=" + currentUserID + "&bidItemID=" + bidItemID);
                    HttpResponseMessage response1 = await client.SendAsync(request1);
                    String reply1 = await response1.Content.ReadAsStringAsync();

                    //get reply to bind to gridview
                    List<BidRecord> bids = JsonConvert.DeserializeObject<List<BidRecord>>(reply1);

                    if (bids.Count != 0)
                    {
                        btnPlaceBid.Visible = false;
                        btnUpdateBid.Visible = true;
                        btnWithdrawBid.Visible = true;
                        lblNonePlaced.Visible = false;

                        currentBidAmount = bids[0].BID_POINTS_AMOUNT;

                        BiddingGridView.DataSource = bids;
                        BiddingGridView.DataBind();
                    }
                    else
                    {
                        lblNonePlaced.Visible = true;
                    }

                    HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getVolunteerCurrentBidPoints?userID=" + currentUserID);
                    HttpResponseMessage response2 = await client.SendAsync(request2);
                    String reply2 = await response2.Content.ReadAsStringAsync();

                    lblPoints.Text = reply2;

                    currentUserBidPoints = Int32.Parse(reply2);
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }

        protected async void btnPlaceBid_Click(object sender, EventArgs e)
        {
            // check if user has enough points
            if (Int32.Parse(tbBidPoints.Text) > currentUserBidPoints)
            {
                MessageBox.Show("You don't have enough bid points.");
                lblExceedPoints.Visible = true;
            }
            else
            {
                lblExceedPoints.Visible = false;
                // place bid
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/VolBiddingMgmt/createPlaceBid?userID=" + currentUserID + "&bidAmount=" + tbBidPoints.Text + "&bidItemID=" + bidItemID);
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();

                Response.Redirect("BidItemInfo.aspx?bidItemID=" + bidItemID);
            }
        }

        protected async void btnUpdateBid_Click(object sender, EventArgs e)
        {
            // check if user has enough points
            if (Int32.Parse(tbBidPoints.Text)-currentBidAmount > currentUserBidPoints)
            {
                MessageBox.Show("You don't have enough bid points.");
                lblExceedPoints.Visible = true;
            }
            else
            {
                lblExceedPoints.Visible = false;
                int newBidAmount = Int32.Parse(tbBidPoints.Text);
                int updateInBidAmount = newBidAmount - currentBidAmount;

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/VolBiddingMgmt/updatePointsForBidChanges?userID=" + currentUserID + "&bidAmount=" + newBidAmount + "&changeInBidAmt=" + updateInBidAmount + "&bidItemID=" + bidItemID);
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();

                Response.Redirect("BidItemInfo.aspx?bidItemID=" + bidItemID);
            }
        }

        protected async void btnWithdrawBid_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/VolBiddingMgmt/updatePointsForWithdrawBid?userID=" + currentUserID + "&withdrawBidAmount=" + currentBidAmount + "&bidItemID=" + bidItemID);
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();

            Response.Redirect("BidItemInfo.aspx?bidItemID=" + bidItemID);
        }
    }
}
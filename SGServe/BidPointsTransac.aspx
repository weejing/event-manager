﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="BidPointsTransac.aspx.cs" Inherits="SGServe.BidPointsTransac" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <h1>Bid Points Transaction History </h1>

    <div class="row">
        <asp:Panel ID="PanelSpinWheel" runat="server">
            <h3>You have a free spin this week for bonus points !</h3>
            <asp:Button ID="ButtonGoToSpin" runat="server" Text="Spin the wheel now !!" CssClass="btn btn-info" PostBackUrl="~/SpinWheel.aspx"/>
        </asp:Panel>
        <br>
        <asp:Panel ID="Panel1" runat="server">
            <div class="panel panel-default" style="border-radius: 8px">
                <div class="table-responsive" style="padding-left: 3%; padding-right: 3%;">
                    <br />
                    <asp:GridView ID="TransGridView" CellPadding="50" GridLines="None" Width="100%" CssClass="table"
                        AllowPaging="True" EmptyDataText="There are currently no bidding transactions." runat="server" AutoGenerateColumns="False" PageSize="20" 
                        DataKeyNames = "USR_POINT_ID">
                        <Columns>
                            <asp:BoundField DataField="TRANSACTION_DATETIME" HeaderText="Date/Time"/>
                            <asp:BoundField DataField="TRANSACTION_INFO" HeaderText="Transaction Info"/>
                            <asp:BoundField DataField="CHANGES" HeaderText="Changes (points)" />
                            <asp:BoundField DataField="BALANCE" HeaderText="Balance (points)" />                            
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </div>

</asp:Content>

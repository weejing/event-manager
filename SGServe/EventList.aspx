﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="EventList.aspx.cs" Inherits="SGServe.EventList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="text-h2">Events</h1>
<%--       <div class="item col-md-12" data-toggle="tooltip" title="View My Events" onclick="location.href='volunteerevents.aspx'">
                <div class="panel panel-default text-center">
                  <div class="panel-body">
                    <div data-percent="85" data-size="95" class="easy-pie inline-block primary" data-scale-color="false" data-track-color="#efefef" >
                      <div class="value text-center">
                        <span class="strong"><i class="fa-3x icon-trophy"></i><h2 class="text-display-2 text-success margin-none"></h2><h4 class="margin-none">My Events</h4></span>
                      </div>
                    </div>  
                  </div>
                </div>
              </div>--%>
    <div class="panel panel-default" style="border-radius: 8px">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <asp:Panel ID="pnlMyEvents" runat="server">
                          <a href="volunteerevents.aspx">View all my events</a>
                    </asp:Panel>                  
                    </div></div><br/>
           <div class="row">
                <div class="col-md-1">
                    Cause
                </div>
                <div class="col-md-4">                    
                     <select id ="cause" class="selectpicker" multiple data-style="btn-white">                       
                    </select>                    
                </div>
                <div class="col-md-1">
                    Date
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtPeriod" runat="server" class="form-group daterangepicker-report"></asp:TextBox>
                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-circle btn-danger btn-sm btn-stroke" runat="server" OnClick ="searchBtn_Click"> <i class="fa fa-search"></i></asp:LinkButton>
                </div>
                <div class="col-md-1">
                    
                </div>
                <div class="col-md-2 text-right">
                    <asp:DropDownList ID="ddlSortby" AutoPostBack="True" runat="server" class="selectpicker" data-style="btn-white" data-toggle="dropdown" OnSelectedIndexChanged="ddlSortby_SelectedIndexChanged">
                        <asp:ListItem>Sort by</asp:ListItem>
                        <asp:ListItem>Organization</asp:ListItem>
                        <asp:ListItem>Recent</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                   <div style="text-align:center"> <asp:Label ID="LblCollaborative" runat="server" Text="Volunteers who have similar interest also participated in these events" Font-Bold="True"></asp:Label></div>
                    <asp:PlaceHolder ID="eventListPanel" runat="server"></asp:PlaceHolder>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <asp:HiddenField ID="causeHiddenName" runat="server" />
    <asp:HiddenField ID="causeHiddenId" runat="server" />

    <asp:HiddenField ID="searchCause" runat="server" />



    <script type="text/javascript">

        $(document).ready(function ()
        {
            var dropdown = $('#cause');
            var hiddenCauseName = $('#<%=causeHiddenName.ClientID%>');
            var hiddenCauseId = $('#<%=causeHiddenId.ClientID%>');

            var searchCause = document.getElementById("<%=searchCause.ClientID%>");

            var causeArrayName = hiddenCauseName.val().split(',');
            var causeArrayId = hiddenCauseId.val().split(',');

            for (var count = 0; count < causeArrayName.length - 1; count++)
            {
                dropdown.append('<option value =' + causeArrayId[count] + '>' + causeArrayName[count] + ' </option>');
            }

            var search = $('#<%=btnSearch.ClientID%>');


            search.click(function (e) {
                console.log("dropdowon: " + dropdown.val());

                var dateText = document.getElementById("<%=txtPeriod.ClientID%>");
                console.log(dateText.value);

                if (dateText.value == "" && dropdown.val() == null)
                {
                    alert("date text field or event cause search cannot be empty");
                    event.preventDefault();
                    return;
                }
                searchCause.value = dropdown.val();
            });
    });
    </script>
</asp:Content>





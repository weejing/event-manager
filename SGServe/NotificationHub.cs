﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using SGServe.Models.AccountModels;
using SGServe.CommonClass;

namespace SGServe
{
    [HubName("notificationHub")]
    public class NotificationHub : Hub
    {
        GlobalModule gm = new GlobalModule();
        //for this example, only this method is called.
        public void NotifyAllClients(string msg)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            context.Clients.All.displayNotification(msg);
        }
      

        //Right click on project, add new item and select signalr hub v2 class
        //This method will pass the message retrieved from server to client. You can forward to only certain client
        // by doing these

        //data structure for you to store the list of users currently connected
        public static ConcurrentDictionary<string, Volunteer> MyUsers = new ConcurrentDictionary<string, Volunteer>();


        //handle user when connected
        public override Task OnConnected()
        {

            //find user 1st. If not exist then add
            if (MyUsers.ContainsKey(HttpContext.Current.Session["vol_id"].ToString()))
            {
                Volunteer vol;
                MyUsers.TryGetValue(HttpContext.Current.Session["vol_id"].ToString(), out vol);
                vol.ConnectionIds.Add(Context.ConnectionId);
            }

            //MyUsers.TryAdd(Context.ConnectionId, new User() { ConnectionId = Context.ConnectionId });
            return base.OnConnected();
        }

        //handle user when disconnected
        public Task OnDisconnected()
        {
            Volunteer vol;
            //should get from session
            string userId = HttpContext.Current.Session["vol_id"].ToString();
            MyUsers.TryGetValue(userId, out vol);

            if (vol.ConnectionIds.Count - 1 == 0)
            {
                //remove the user totally
                Volunteer garbage;
                MyUsers.TryRemove(userId, out garbage);
            }
            else
            {
                //Still got other tabs open and active
                vol.ConnectionIds.Remove(Context.ConnectionId);
            }


            return OnDisconnected();
        }

        //forward the notification to user
        public void NotifyIndividualClient(string msg)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            //your checking condition here
            if (msg.Contains(HttpContext.Current.Session["vol_id"].ToString()))
            {
                //call the concurrent dictionary to get the userConnectionId based on the userName or userID
                Volunteer vol;
                //should get from session
                string userId = HttpContext.Current.Session["vol_id"].ToString();
                MyUsers.TryGetValue(userId, out vol);

                //notify the user
                foreach (string connectionId in vol.ConnectionIds)
                {
                    context.Clients.Client(connectionId).displayNotification(msg);
                }
            }
        }
    }
}
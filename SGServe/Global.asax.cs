﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;
using System.Collections;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Json;
using SGServe.SaaS.Models;
using System.Web.Caching;
using SGServe.SaaS;
using System.Configuration;
using SGServe.CommonClass;

namespace SGServe
{
    public class Global : System.Web.HttpApplication
    {
        private static CacheItemRemovedCallback OnCacheRemove = null;
        String check = "STRING:";

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.EnableFriendlyUrls();
            loadData2();
            //registerURL(RouteTable.Routes);
           // AddTask("DoStuff", 10);
        }



        //this is the place the system register all the virtual URL links
        async void registerURL(RouteCollection r)
        {
            List<OrganizationDetails> items = new List<OrganizationDetails>();

            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
               
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgList");
                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                //System.Diagnostics.Debug.WriteLine("reply: " + reply);

                JsonArray array = (JsonArray)JsonValue.Parse(reply);
                //String list = "SGSERVE_CONNECTION_STRING:";

                foreach (var arrayResult in array)
                {
                    OrganizationDetails newItem = new OrganizationDetails();
                    newItem.CONNECTION_STRING = arrayResult["CONNECTION_STRING"].ToString();

                    items.Add(newItem);
                    // list=list+","+ newItem.CONNECTION_STRING.Substring(1, newItem.CONNECTION_STRING.Length - 2);
                }
                //Response.Cookies["CONNECTION_STRING_LIST"].Value = list;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Global.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: registerURL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }


            //for each page of webform, please put a set of this for loop
            //Dashboard home page for SaaS
            for (int i = 0; i < items.Count; i++)
            {
                String connect = items[i].CONNECTION_STRING.Substring(1, items[i].CONNECTION_STRING.Length - 2);
                r.MapPageRoute("DashboardHome" + connect, connect, "~/SaaS/DashboardSaaS.aspx");
                //shihui
                r.MapPageRoute("Home" + connect, connect + "/home", "~/SaaS/home.aspx");
                r.MapPageRoute("Login" + connect, connect + "/login", "~/SaaS/Login.aspx");
                r.MapPageRoute("Logout" + connect, connect + "/logout", "~/SaaS/Logout.aspx");
                r.MapPageRoute("Employee Login" + connect, connect + "/ologin", "~/SaaS/oLogin.aspx");
                r.MapPageRoute("Change Password" + connect, connect + "/changepassword", "~/SaaS/changepassword.aspx");
                r.MapPageRoute("Set Password" + connect, connect + "/setpassword", "~/SaaS/setpassword.aspx");
                r.MapPageRoute("Reset Password" + connect, connect + "/resetpassword", "~/SaaS/resetpassword.aspx");
                r.MapPageRoute("Profile" + connect, connect + "/profile", "~/SaaS/profile.aspx");
                r.MapPageRoute("Redirect" + connect, connect + "/redirectpage", "~/SaaS/redirectpage.aspx");
                r.MapPageRoute("Manage Account" + connect, connect + "/manageaccount", "~/SaaS/manageaccount.aspx");
                r.MapPageRoute("Create Account" + connect, connect + "/createaccount", "~/SaaS/createaccount.aspx");
                r.MapPageRoute("Internal home" + connect, connect + "/internalhome", "~/SaaS/internalhome.aspx");
                //debbie
                r.MapPageRoute("EditInventoryItems" + connect, connect + "/EditInventory", "~/SaaS/EditInventoryItems.aspx");
                r.MapPageRoute("ManageInventory" + connect, connect + "/ManageInventory", "~/SaaS/ManageInventory.aspx");
                r.MapPageRoute("TenderStatusList" + connect,connect + "/TenderStatusList", "~/SaaS/TenderStatusList.aspx");
                r.MapPageRoute("ViewEventLogisticList" + connect, connect + "/ViewEventLogisticList", "~/SaaS/ViewEventLogisticList.aspx");
                r.MapPageRoute("ViewEventLogisticItems" + connect, connect + "/ViewEventLogisticItems", "~/SaaS/ViewEventLogisticItems.aspx");
                r.MapPageRoute("ManageExternalVolunteers" + connect, connect + "/ManageExternalVolunteers", "~/SaaS/ManageExternalVolunteers.aspx");
                r.MapPageRoute("ViewExternalVolunteer" + connect, connect + "/ViewExternalVolunteer", "~/SaaS/ViewExternalVolunteer.aspx");
                r.MapPageRoute("CreateEmailInvites" + connect, connect + "/CreateEmailInvites", "~/SaaS/CreateEmailInvites.aspx");
                r.MapPageRoute("TenderStatusList" + connect, connect + "/TenderStatusList", "~/SaaS/TenderStatusList.aspx");

                //junhau
                r.MapPageRoute("UpdateOrgInfo" + connect, connect + "/UpdateOrgInfo", "~/SaaS/UpdateOrgInfo.aspx");
                r.MapPageRoute("CreateTender" + connect, connect + "/CreateTender", "~/SaaS/CreateTender.aspx");
                r.MapPageRoute("TenderingMgmt" + connect, connect + "/TenderingMgmt", "~/SaaS/TenderingMgmt.aspx");
                r.MapPageRoute("ViewTender" + connect, connect + "/ViewTender", "~/SaaS/ViewTender.aspx");
                r.MapPageRoute("ViewBids" + connect, connect + "/ViewBids", "~/SaaS/ViewBids.aspx");

                //weejing
                r.MapPageRoute("CreateEvent" + connect, connect + "/CreateEvent", "~/SaaS/CreateEvent.aspx");
                r.MapPageRoute("CreateEventSession" + connect, connect + "/CreateEventSession", "~/SaaS/CreateEventSession.aspx");
                r.MapPageRoute("ViewEventList" + connect, connect + "/ViewEventList", "~/SaaS/ViewEventList.aspx");
                r.MapPageRoute("ViewEventDetails" + connect, connect + "/ViewEventDetails", "~/SaaS/ViewEventDetails.aspx");
                r.MapPageRoute("CreateEventLogs" + connect, connect + "/CreateEventLogs", "~/SaaS/CreateEventLogs.aspx");

                //kai fu
                r.MapPageRoute("UsersFollowed" + connect, connect + "/UsersFollowed", "~/SaaS/UsersFollowed.aspx");

                //jaslyn
                r.MapPageRoute("ManageBill" + connect, connect + "/ManageBill", "~/SaaS/ManageBill.aspx");
                r.MapPageRoute("PaymentPlans" + connect, connect + "/PaymentPlans", "~/SaaS/PaymentPlans.aspx");
                r.MapPageRoute("PaymentSuccess" + connect, connect + "/PaymentSuccess", "~/SaaS/PaymentSuccess.aspx");
                r.MapPageRoute("PaymentError" + connect, connect + "/PaymentError", "~/SaaS/PaymentError.aspx");
                r.MapPageRoute("Reporting" + connect, connect + "/Reporting", "~/SaaS/Reporting.aspx");
            }

        }


        private void AddTask(string name, int seconds)
        {
            OnCacheRemove = new CacheItemRemovedCallback(CacheItemRemoved);
            HttpRuntime.Cache.Insert(name, seconds, null,
                DateTime.Now.AddSeconds(seconds), Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, OnCacheRemove);
        }

        public void CacheItemRemoved(string k, object v, CacheItemRemovedReason r)
        {
            loadData2();
            AddTask(k, Convert.ToInt32(v));
        }

        async void loadData2()
        {
            List<OrganizationDetails> items = new List<OrganizationDetails>();

            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgList");
                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                //System.Diagnostics.Debug.WriteLine("reply: " + reply);

                JsonArray array = (JsonArray)JsonValue.Parse(reply);


                foreach (var arrayResult in array)
                {
                    OrganizationDetails newItem = new OrganizationDetails();
                    newItem.CONNECTION_STRING = arrayResult["CONNECTION_STRING"].ToString();

                    items.Add(newItem);

                }


                for (int i = 0; i < items.Count; i++)
                {
                 //   if (!check.Contains(items[i].CONNECTION_STRING.Substring(1, items[i].CONNECTION_STRING.Length - 2)))
                  //  {
                        String connect = items[i].CONNECTION_STRING.Substring(1, items[i].CONNECTION_STRING.Length - 2);
                        ConnectionStringMgmt.Add(connect);
                      //  check = check + "," + connect;
                   // }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Global.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: registerURL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }




        }
    }
}
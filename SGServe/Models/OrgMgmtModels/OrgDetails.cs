﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.OrgMgmtModels
{
    public class OrgDetails
    {
        public OrgDetails()
        {

        }
        public string CONNECTION_STRING
        {get;set;}
        public string EMAIL
        { get; set; }
        public string ORG_NAME
        { get; set; }
        public string LINK
        { get; set; }
        public string UEN
        { get; set; }
        public string ADDRESS
        { get; set; }
        public string DESCRIPTION
        { get; set; }
        public string ORG_TYPE
        { get; set; }
        public string ORG_ID
        { get; set; }
    }
}
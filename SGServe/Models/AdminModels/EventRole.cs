﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AdminModels
{
    public class EventRole
    {
        public String ROLE_NAME
        { get; set; }

        public String TRAITS
        { get; set; }

        public int ROLE_ID
        { get; set; }

        public int VOCAB_ID
        { get; set; }

        public String TERM_SKILL_IDS
        { get; set; }

        public List<Term> EVTROLESKILLLIST
        { get; set; }

        public String CONCAT_SKILL_NAME
        { get; set; }

        public String CONNECTION_STRING
        { get; set; }
    }
}
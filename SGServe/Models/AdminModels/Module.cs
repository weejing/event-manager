﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AdminModels
{
    public class Module
    {
        public String NAME
        { get; set; }

        public String ID
        { get; set; }
    }
}
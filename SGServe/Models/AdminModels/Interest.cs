﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AdminModels
{
    public class Interest
    {
        public String INTEREST_ID
        { get; set; }

        public String INTEREST_CAT_ID
        { get; set; }

        public String INTEREST
        { get; set; }

        public String INTEREST_CAT
        { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AdminModels
{
    public class Term
    {
        public int TERM_ID
        { get; set; }

        public int VOCAB_ID
        { get; set; }

        public String TERM_NAME
        { get; set; }

        public int PARENT_ID
        { get; set; }

        public DateTime CREATED_AT
        { get; set; }

        public int IS_DELETED
        { get; set; }
    }
}
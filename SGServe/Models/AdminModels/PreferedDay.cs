﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AdminModels
{
    public class PreferedDay
    {
        public String USR_PREFERED_DAY_ID
        { get; set; }

        public String USR_ID
        { get; set; }

        public String DAY
        { get; set; }
    }
}
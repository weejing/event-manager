﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AdminModels
{
    public class TermRelation
    {
        public int TERM_RELATION_ID
        { get; set; }

        public String ENTITY_ID
        { get; set; }

        public String ENTITY_TYPE
        { get; set; }

        public int TERM_ID
        { get; set; }

        public int VOCAB_ID
        { get; set; }

        public String TERM_NAME
        { get; set; }
    }
}
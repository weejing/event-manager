﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AdminModels
{
    public class Role
    {
        public String ROLE_ID
        { get; set; }

        public String ROLE_NAME
        { get; set; }

        public int IS_DELETED
        { get; set; }

        public String CONNECTION_STRING
        { get; set; }

        public List<Module> roleModList
        { get; set; }
    }
}
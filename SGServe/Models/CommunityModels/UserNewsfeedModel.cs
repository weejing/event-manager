﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.CommunityModels
{
    public class UserNewsfeedModel
    {
        public String ACTIVITY_TYPE
        { get; set; }

        public String ACTIVITY_DATETIME
        { get; set; }

        public String USR_ID
        { get; set; }

        public String USR_FULL_NAME
        { get; set; }

        public String USR_IMAGE
        { get; set; }

        public String TARGET_ID
        { get; set; }

        public String TARGET_NAME
        { get; set; }

        public String TARGET_IMAGE
        { get; set; }

        public String TARGET_USR_ID
        { get; set; }

        public String TARGET_USR_NAME
        { get; set; }

        public String COMMENTED_MESSAGE
        { get; set; }

        public String MESSAGE_DISPLAY
        { get; set; }

        public String ORG_ID
        { get; set; }

        public String ORG_NAME
        { get; set; }

        public String EVENT_ID
        { get; set; }

        public String EVENT_NAME
        { get; set; }
    }
}
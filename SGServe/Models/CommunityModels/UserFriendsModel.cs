﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.CommunityModels
{
    public class UserFriendsModel
    {
        public String OWN_USR_ID
        { get; set; }

        public List<UserPortfolio> USR_FRIENDS
        { get; set; }
    }
}
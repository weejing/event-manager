﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.CommunityModels
{
    public class UserPortfolioRecommendationModel
    {
        public String USR_ID
        { get; set; }

        public String FULL_NAME
        { get; set; }

        public String SCHL_NAME
        { get; set; }

        public String PERSONALITY_CHAR
        { get; set; }

        public String DESCRIPTION
        { get; set; }

        public List<String> userInterest
        { get; set; }

        public List<String> userSkill
        { get; set; }

        public String VIA_HOURS
        { get; set; }

        public String BID_POINTS
        { get; set; }

        public String IMAGE
        { get; set; }

        public int RECOMMENDATION_PRIORITY
        { get; set; }

        public String RECOMMENDATION_FROM
        { get; set; }
    }
}

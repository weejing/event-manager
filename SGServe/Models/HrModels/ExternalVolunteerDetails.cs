﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.HrModels
{
    public class ExternalVolunteerDetails
    {
        public string NAMELIST_ID
        {
            get;
            set;
        }
        public string NAME
        {
            get;
            set;
        }
        public string EMAIL
        {
            get;
            set;
        }
        public string PHONENO
        {
            get;
            set;
        }
        public ExternalVolunteerDetails(string Namelistid, string volName, string Email, string PhoneNo)
        {
            this.NAMELIST_ID = Namelistid;
            this.NAME = volName;
            this.EMAIL= Email;
            this.PHONENO = PhoneNo;
        }
        public string CONNECTION_STRING
        {
            get;
            set;
        }
        public ExternalVolunteerDetails( string volName, string Email, string PhoneNo)
        {
         
            this.NAME = volName;
            this.EMAIL = Email;
            this.PHONENO = PhoneNo;
        }
        public bool checkField()
        {
            if (this.NAME != "" && this.PHONENO !="" && this.EMAIL != "")
                return true;
            else
                return false;
        }

        private void getShortenURL(string longURL)
        {
            GenerateUniqueURL generate = new GenerateUniqueURL();
            generate.getShortenURL(longURL);
        }

        public ExternalVolunteerDetails()
        {

        }
    }
}
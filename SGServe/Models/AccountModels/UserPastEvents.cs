﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AccountModels
{
    public class UserPastEvents : IEquatable<UserPastEvents>
    {
        public String EVT_NAME
        { get; set; }

        public DateTime STARTDATE
        { get; set; }

        public String EVT_ID
        { get; set; }

        public String USR_EVT_ID
        { get; set; }

        public String CONNECTION_STRING
        { get; set; }

        public String EVT_SESSION_ID
        { get; set; }

        public DateTime EVT_DATE
        { get; set; }

        public String SESSIONDAY_ID
        { get; set; }

        public String SESSION_NAME
        { get; set; }

        public DateTime TIME_IN
        { get; set; }

        public DateTime TIME_OUT
        { get; set; }

        public int Completed_counter
        { get; set; }

        public int Upcoming_counter
        { get; set; }

        public int Ongoing_counter
        { get; set; }

        public int viahours
        { get; set; }

        public bool Equals(UserPastEvents other)
        {
            if (other == null) return false;
            return (this.EVT_SESSION_ID.Equals(other.EVT_SESSION_ID));
        }
    }
}
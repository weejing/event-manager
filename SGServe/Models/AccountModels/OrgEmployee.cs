﻿using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AccountModels
{
    public class OrgEmployee
    {
        public String USR_ID
        { get; set; }

        public String NAME
        { get; set; }

        public String EMAIL
        { get; set; }

        public String PASSWORD
        { get; set; }

        public DateTime REGISTRATION_DATE
        { get; set; }

        public String RESETPW_CODE
        { get; set; }

        public String CONNECTION_STRING
        { get; set; }

        public String URL
        { get; set; }

        public List<Role> roleList
        { get; set; }

        public String ORG_STATUS
        { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.AccountModels
{
    public class FbUser
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string link { get; set; }
        public string gender { get; set; }
        public string locale { get; set; }
        public string email { get; set; }
        public string user_birthday { get; set; }
        public List<string> friends { get; set; }
    }
}
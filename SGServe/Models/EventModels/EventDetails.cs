﻿using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class EventDetails
    {

        public String eventId
        { get; set; }

        public String eventName
        { get; set; }

        public String description
        { get; set; }

        public DateTime startDate
        { get; set; }

        public DateTime endDate
        { get; set; }
        
        public List<int> listOfEventCause
        { get; set; }

        public List<String> listOfEventCauseName
        { get; set; }
     

        public List<SessionDetails> listOfSession
        { get; set; }

        public int numOfSessions
        { get; set; }

        public String connectionString
        { get; set; }

        public String orgName
        { get; set; }


        public int passingWeight
        { get; set; }

        public string generateNewLink
        { get; set; }

        public string imagePath
        { get; set; }


        /*this is organised by (0) traits, (1) cause interest, (2) role interest, (3) skillsets match         
      */
        public List<int> listOfWeights
        { get; set; }

        public int logsNum
        { get; set; }

        public List<Term> listofCauseName
        { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class SessionDetails
    {
        public String sessionId
        { get; set; }

        public String sessionName
        { get; set; }

        public String sessionDescription
        { get; set; }

        public DateTime evt_Date
        { get; set; }

        public List<SessionDays> listOfSessionDays
        { get; set; }

        public DateTime startTime
        { get; set; }

        public DateTime endTime
        { get; set; }

        public String location
        { get; set; }

        public string postal
        { get; set; }
      

        public int sessionSize
        { get; set; }

        public int compulsoryDays
        { get; set; }

        public List<SessionRoles> listOfSessionRoles
        { get; set; }
       
    }
}
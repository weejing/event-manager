﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class SessionDays
    {
        public string sessionDayId
        { get; set; }

        public DateTime evt_Date
        { get; set; }

        public List<Int32> listOfRolesQuantity
        { get; set; }

        public List<EventLogs> listOfLogs
        { get; set; }

        // meaning is different for joined session days and unjoined session days
        // for joined session day, role refers to a role that is joined by a volunteer
        // for unjoined session day, role refers to a role that is avaliable to be joined for this day
        public int role
        { get; set; }


        // only for joined evnets 
        public string roleName
        { get; set; }



    }
}
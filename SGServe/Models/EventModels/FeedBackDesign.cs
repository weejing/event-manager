﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class FeedBackDesign
    {

        public string sessionId
        { get; set; }

        public string question
        { get; set; }

        public int type
        { get; set; }

        public string category
        { get; set; }

        public int number
        { get; set; }

        public string fdback_detail_id
        { get; set; }

        public String listOfTextAns
        { get; set; }

        public List<RatingCount> ratingCountList
        { get; set; }
    }
}
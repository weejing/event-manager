﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class EventCause
    {
        public string TERM_NAME
        { get; set; }

        public string TERM_ID
        { get; set; }
    }
}
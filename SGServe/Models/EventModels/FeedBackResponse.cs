﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class FeedBackResponse
    {
        public string userId
        { get; set; }

        public string connectionString
        { get; set; }

        public string sessionId
        { get; set; }

        public List<String> listOfAnswers
        { get; set; }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class VolSessionDetails
    {
        public String sessionId
        { get; set; }

        public String sessionName
        { get; set; }

        public String sessionDescription
        { get; set; }

        public List<SessionDays> listOfSessionDays
        { get; set; }

        public DateTime startTime
        { get; set; }

        public DateTime endTime
        { get; set; }

        public String location
        { get; set; }

        public int compulsory
        { get; set; }
      

        public List<SessionDays> listOfJoinedDays
        { get; set; }

        public List<SessionDays> listOfUnjoinedDays
        { get; set; }

        public List<SessionRoles> listOfEventRoles
        { get; set; }

    }
           
}
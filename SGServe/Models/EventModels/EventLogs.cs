﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class EventLogs
    {
        public String logsName
        { get; set; }

        public int qty
        { get; set;}

        public DateTime  date
        { get; set; }

    }
}
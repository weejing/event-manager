﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.EventModels
{
    public class SearchEvent
    {
        public List<int> eventCause
        { get; set; }

        public DateTime startDate
        { get; set; }

        public DateTime endDate
        { get; set; }

        public Boolean noDate
        { get; set; }

    }
}
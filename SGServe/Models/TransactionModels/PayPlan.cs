﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.TransactionModels
{
    public class PayPlan
    {
        public PayPlan()
        {

        }
        public string PLAN_TYPE_ID
        { get; set; }
        public string PLAN_NAME
        { get; set; }
        public string PLAN_AMOUNT
        { get; set; }
        public string CRM_ID
        { get; set; }
        public string DISCOUNT
        { get; set; }
        public string EXPIRED_DATE
        { get; set; }
        public string PLAN_ACTIVE
        { get; set; }
        public string PLAN_DESCRIPTION
        { get; set; }
        public string PER
        { get; set; }
        public string FREE_TRIAL
        { get; set; }
    }
}
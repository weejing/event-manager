﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.TransactionModels
{
    public class OrgPayment
    {
        public OrgPayment()
        {

        }
        public string ORG_PAYMENT_ID
        {get;set;}
        public string ORG_ID
        {get;set;}
        public string ORG_NAME
        {get;set;}
        public string AMT
        {get; set;}
        public string PAYMENT_TIME
        {get;set;}
        public string PLAN_TYPE
        { get; set; }
        public string PLAN_AMOUNT
        { get; set; }
        public string CRM_ID
        { get; set; }
        public string DISCOUNT
        { get; set; }
        public string STATUS
        { get; set; }
        public string BILL_STATUS
        { get; set; }
        public string FROM_SUBCRIPTION
        { get; set; }
        public string TO_SUBSCRIPTION
        { get; set; }
        public string BIL_ID
        { get; set; }
        public string CRM_STATUS
        { get; set; }
        public string FROM_DATE
        { get; set; }
        public string TO_DATE
        { get; set; }
        public string LINK
        { get; set; }
        public string EMAIL
        { get; set; }
        public string ADDRESS
        { get; set; }
        public string ORG_STATUS
        { get; set; }
        public string VERIFY
        { get; set; }
        public string PLAN_TYPE_ID
        { get; set; }
        public string PLAN_ACTIVE
        { get; set; }
        public string PLAN_DESCRIPTION
        { get; set; }
        public string PER
        { get; set; }
        public string FIRST_PAYMENT
        { get; set; }
        public string PAYPAL_ID
        { get; set; }
        public string PAYPAL_FEE
        { get; set; }
        public string PAYMENT_TYPE
        { get; set; }
        public string PLAN_NAME
        { get; set; }
        public string FREE_TRIAL
        { get; set; }
        public string PROFILE_ID
        { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.TransactionModels
{
    public class CRM
    {
        public CRM()
        {

        }
        public string CRM_ID
        {
            get;
            set;
        }
        public string ORG_PAYMENT_ID
        {
            get;
            set;
        }
        public string DISCOUNT
        {
            get;
            set;
        }
        public string CRM_STATUS
        {
            get;
            set;
        }
        public string IS_DELETED
        {
            get;
            set;
        }
        public string ORG_ID
        {
            get;
            set;
        }
        public string EXPIRED_DATE
        {
            get;
            set;
        }
        public string PLAN_NAME
        {
            get;
            set;
        }
        public string AMOUNT
        {
            get;
            set;
        }
        public string PLAN_TYPE_ID
        {
            get;
            set;
        }

    }
}
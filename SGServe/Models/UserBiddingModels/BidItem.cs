﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.Models.UserBiddingModels
{
    public class BidItem
    {
        public String BID_ITEM_ID
        { get; set; }

        public String SPR_ITEM_ID
        { get; set; }

        public String ITEM_NAME
        { get; set; }

        public String ITEM_DESCRIPTION
        { get; set; }

        public String SPR_ITEM_CATEGROY_ID
        { get; set; }

        public String ITEM_CATEGORY_NAME
        { get; set; }

        public int QTY
        { get; set; }

        public int NUM_BIDDERS
        { get; set; }

        public String SPONSOR_USR_ID
        { get; set; }

        public String SPONSOR_NAME
        { get; set; }

        public int MIN_POINT
        { get; set; }

        public String START_DATE
        { get; set; }

        public String END_DATE
        { get; set; }

        public String STATUS
        { get; set; }

        public String IMAGE
        { get; set; }

    }
}
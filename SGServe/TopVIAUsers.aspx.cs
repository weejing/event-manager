﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class TopVIAUsers : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateUsersList();
                }
            }
        }

        protected async void generateUsersList()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage topVIAReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getTopUsersVIA?size=10");
                    HttpResponseMessage topVIAResp = await client.SendAsync(topVIAReq);
                    var topVIA = await topVIAResp.Content.ReadAsStringAsync();

                    List<UserPortfolioModel> topVIAList = JsonConvert.DeserializeObject<List<UserPortfolioModel>>(topVIA);

                    DataListTopVIA.DataSource = topVIAList;
                    DataListTopVIA.DataBind();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
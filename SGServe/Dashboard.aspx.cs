﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data;
using System.Threading;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SGServe
{
    public partial class Dashboard : System.Web.UI.Page
    {
        List<UserPastEvents> completedList = new List<UserPastEvents>();
        List<UserPastEvents> upcomingList = new List<UserPastEvents>();

        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        //This list will contain the event name and start date of completed events by volunteers
       
        protected async void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //await getNumUnreadNotif();
                await gm.getNotification("CONSOLE_VMS", "TABLE_USR_NOTIF", lblNotif);
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    displayDetails();
                    displayEvent();
                    await displayVolInterests();
                    if (volunteer.PASSWORD == "")
                    {
                        pnlPw.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        //protected async void UpdateTimer_Tick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        await gm.getNotification("CONSOLE_VMS", "TABLE_USR_NOTIF", lblNotif);
        //        TimedPanel.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        //private async Task getNumUnreadNotif()
        //{
        //    Server_Connect serverConnection = new Server_Connect();
        //    HttpClient client = serverConnection.getHttpClient();
        //    String jwtToken = Session["Token"] as String;
        //    if (jwtToken != null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
        //        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        //        HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Get, "api/Notification/getNumUnreadNotif?connection_string=CONSOLE_VMS&tablename=TABLE_USR_NOTIF");
        //        HttpResponseMessage Resp2 = await client.SendAsync(request2);
        //        String numUnreadNotif = await Resp2.Content.ReadAsStringAsync();
        //        if (Convert.ToInt32(numUnreadNotif) >0) {
        //            lblNotiALert.Text = "<i class='fa fa-fw fa-bell-o' style='color:red'></i>";
        //        }
        //    }
        //}
        
        public async void displayEvent()
        {
            int viaHours = 0;
            int completedEventCount = 0;
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Dashboard/getNPO?volunteerid=" + volunteer.VOL_ID);
            HttpResponseMessage response = await client.SendAsync(request);

            String reply = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("reply: " + reply);

            //JsonArray array = (JsonArray)JsonValue.Parse(reply);
            List<string> connectionStringList = JsonConvert.DeserializeObject<List<string>>(reply);

            foreach (string connectionString in connectionStringList)
            {
                
                if (!connectionString.Equals(""))
                {
                    //completed
                    HttpRequestMessage completedRequest = new HttpRequestMessage(HttpMethod.Get, "/api/Dashboard/getCompletedEvents?connectionstring=" + connectionString + "&volunteerid=" + volunteer.VOL_ID);
                    HttpResponseMessage completedResponse = await client.SendAsync(completedRequest);

                    String eventListStr = await completedResponse.Content.ReadAsStringAsync();
                    
                    JsonArray completedEventJson = (JsonArray)JsonValue.Parse(eventListStr);
                    foreach (var completeEvent in completedEventJson)
                    {
                        UserPastEvents pastevents = new UserPastEvents();
                        pastevents.EVT_ID = completeEvent["EVT_ID"].ToString().Replace("\"", "");
                        pastevents.EVT_DATE = DateTime.Parse(completeEvent["EVT_DATE"].ToString().Replace("\"", "")).Date;
                        pastevents.EVT_NAME = completeEvent["EVT_NAME"].ToString().Replace("\"", "");
                        pastevents.EVT_SESSION_ID = completeEvent["EVT_SESSION_ID"].ToString().Replace("\"", "");
                        pastevents.SESSION_NAME = completeEvent["SESSION_NAME"].ToString().Replace("\"", "");
                        pastevents.TIME_IN = DateTime.Parse(completeEvent["TIME_IN"].ToString().Replace("\"", ""));
                        pastevents.TIME_OUT = DateTime.Parse(completeEvent["TIME_OUT"].ToString().Replace("\"", ""));
                        pastevents.CONNECTION_STRING = connectionString;
                        completedList.Add(pastevents);
                    }
                    completedEventCount += completedEventJson.Count;

                    //getVIAHours
                    request = new HttpRequestMessage(HttpMethod.Get, "/api/Dashboard/getCompletedVIA?connectionstring=" + connectionString + "&volunteerid=" + volunteer.VOL_ID);
                    response = await client.SendAsync(request);
                    string hoursStr = await response.Content.ReadAsStringAsync();
                    viaHours += JsonConvert.DeserializeObject<int>(hoursStr);

                    //upcoming
                    request = new HttpRequestMessage(HttpMethod.Get, "/api/Dashboard/getUpcomingEvents?connectionstring=" + connectionString + "&volunteerid=" + volunteer.VOL_ID);
                    response = await client.SendAsync(request);
                    eventListStr = await response.Content.ReadAsStringAsync();
                    JsonArray upcomingEventJson = (JsonArray)JsonValue.Parse(eventListStr);
                    foreach (var upcomingEvent in upcomingEventJson)
                    {
                        UserPastEvents pastevents = new UserPastEvents();
                        pastevents.EVT_ID = upcomingEvent["EVT_ID"].ToString().Replace("\"", "");
                        pastevents.EVT_DATE = DateTime.Parse(upcomingEvent["EVT_DATE"].ToString().Replace("\"", "")).Date;
                        pastevents.EVT_NAME = upcomingEvent["EVT_NAME"].ToString().Replace("\"", "");
                        pastevents.EVT_SESSION_ID = upcomingEvent["EVT_SESSION_ID"].ToString().Replace("\"", "");
                        pastevents.SESSION_NAME = upcomingEvent["SESSION_NAME"].ToString().Replace("\"", "");
                        pastevents.CONNECTION_STRING = connectionString;
                        upcomingList.Add(pastevents);
                    }
                }
            }
            lbltotalvia.Text = viaHours.ToString();
            lblcompleted.Text = completedEventCount.ToString();
            countOngoingAndUpcoming();
            
            if (lblcompleted.Text == "")
                lblcompleted.Text = "0";

            if (lblongoing.Text == "")
                lblongoing.Text = "0";

            if (lblupcoming.Text == "")
                lblupcoming.Text = "0";

            if (lbltotalvia.Text == "")
                lbltotalvia.Text = "0";
        }

        public void countOngoingAndUpcoming()
        {
            int ongoing = 0;
            int upcoming = 0;
            for (int i = 0; i < completedList.Count; i++)
            {
                if (upcomingList.Contains(completedList[i]))
                {
                    ongoing++;
                }
            }

            upcoming = upcomingList.Count - ongoing;
            lblupcoming.Text = upcoming.ToString();
            lblongoing.Text = ongoing.ToString();
        }

        private async Task retrieveUserDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            if (jwtToken != null)
            {
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                // add the jwt token into the authorization header for server validation
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                volunteer = GlobalModule.getUserDetails(reply);
            }
        }
        
        private void displayDetails()
        {
            lblDietary.Text = volunteer.DIETARY;
            lblDOB.Text = volunteer.DOB.ToString("d MMM yyyy");
            lblEmail.Text = volunteer.EMAIL;
            lblName.Text = volunteer.FULL_NAME;
            lblNRIC.Text = volunteer.NRIC;
            //lblGoal.Text = "X hour(s)/"+volunteer.VIA_TARGET+"hour(s)";
            lblPersonality.Text = volunteer.PERSONALITY_CHAR;

            lblPhone.Text = volunteer.PHONE_NO;
            lblReligion.Text = volunteer.RELIGION;
            if (string.IsNullOrEmpty(volunteer.SCHL_NAME)) {
                pnlSch.Visible = false;
            } else {
                pnlSch.Visible = true;
                lblSchool.Text = volunteer.SCHL_NAME;
            }
        }

        private async Task displayVolInterests()
        {   //get cause
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + volunteer.VOL_ID + "&vocab_name=cause");
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();
            var Benetable = JsonConvert.DeserializeObject<DataTable>(reply);
            dlBene.DataSource = Benetable;
            dlBene.DataBind();
            //get roles
            HttpRequestMessage rolerequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + volunteer.VOL_ID + "&vocab_name=" + "event role");
            HttpResponseMessage roleresponse = await client.SendAsync(rolerequest);
            String rolereply = await roleresponse.Content.ReadAsStringAsync();
            var Roletable = JsonConvert.DeserializeObject<DataTable>(rolereply);
            dlRole.DataSource = Roletable;
            dlRole.DataBind();
            //get skill
            HttpRequestMessage skillrequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + volunteer.VOL_ID + "&vocab_name=" + "skill");
            HttpResponseMessage skillresponse = await client.SendAsync(skillrequest);
            String skillgreply = await skillresponse.Content.ReadAsStringAsync();
            DataTable skilltable = JsonConvert.DeserializeObject<DataTable>(skillgreply);
            if (skilltable.Rows.Count==0)
            {
                pnlSkill.Visible = false;
            }
            else
            {
                pnlSkill.Visible = true;
                dlskill.DataSource = skilltable;
                dlskill.DataBind();
            }
            
            //badges
            HttpRequestMessage requestBadge = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/displayVolBadge");
            HttpResponseMessage responseBadge = await client.SendAsync(requestBadge);
            String replyBadge = await responseBadge.Content.ReadAsStringAsync();
            var BadgesTable = JsonConvert.DeserializeObject<DataTable>(replyBadge);
            dlBadge.DataSource = BadgesTable;
            dlBadge.DataBind();
        }

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Account/setVIAgoal");
            await gm.getLoginVolDetails();
            if (volunteer.VOL_ID != null)
            {
                Volunteer volunteer2 = new Volunteer { VOL_ID = volunteer.VOL_ID, VIA_TARGET = "" };
                await client.PostAsJsonAsync("api/Account/setVIAgoal", volunteer2);
                Response.Redirect("Dashboard.aspx", false);
            }
            else
            {
                Response.Redirect("login.aspx", false);
            }
        }

        protected void dlBadge_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;
            System.Web.UI.WebControls.Image image = e.Item.FindControl("imgBadge") as System.Web.UI.WebControls.Image;

            if (Convert.ToInt32(row["VOL_HAS"]) != 1)
            {
                image.Attributes.CssStyle.Add("opacity", "0.1");
            }

            }

        //        //protected void btnSetGoal_Click(object sender, EventArgs e)
        //        //{
        //        //    mpGoal.Show();
        //        //    txtGoal.Text = volunteer.VIA_TARGET;
        //        //}
    }
}
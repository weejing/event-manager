﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class UserFriends : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";
        static String navigateUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //await retrieveUserDetails();
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    navigateUserID = Request.QueryString["id"];
                    currentUserID = volunteer.VOL_ID;
                    lblFullName.Text = volunteer.FULL_NAME;
                    if (navigateUserID == currentUserID)
                    {
                        btnAddFriend.Visible = false;
                        PanelF.Visible = false;
                        btnRemoveFriend.Visible = false;
                        lblPendingFriends.Visible = false;
                        checkOwnFriendRequests();
                        lblFR.Visible = true;
                    }
                    else
                    {
                        lblFR.Visible = false;
                        checkFriend();
                    }

                    generateContent();
                    generateUserDetails();
                    linkAbout.NavigateUrl = "UserPortfolio.aspx?id=" + navigateUserID;
                    linkFriends.NavigateUrl = "UserFriends.aspx?id=" + navigateUserID;
                    linkFollows.NavigateUrl = "UserOrgFollows.aspx?id=" + navigateUserID;
                }
            }
        }

        protected async void checkOwnFriendRequests()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserFriendsRequest?userID=" + currentUserID);
                HttpResponseMessage resp = await client.SendAsync(req);
                String userDetails = await resp.Content.ReadAsStringAsync();

                List<UserPortfolioModel> upm = JsonConvert.DeserializeObject<List<UserPortfolioModel>>(userDetails);

                if (upm.Count < 1)
                {
                    lblFR.Visible = false;
                    panelFriendRequest.Visible = false;
                }
                else
                {
                    lblFR.Visible = true;
                    panelFriendRequest.Visible = true;
                }

                FriendRequestGridView.DataSource = upm;
                FriendRequestGridView.DataBind();

                //DataList2.DataSource = upm;
                //DataList2.DataBind();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected async void checkFriend()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage checkFriendsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/checkfriends?userID1=" + navigateUserID + "&userID2=" + currentUserID);
                HttpResponseMessage checkFriendsResp = await client.SendAsync(checkFriendsReq);
                String checkFriends = await checkFriendsResp.Content.ReadAsStringAsync();

                String friendStatus = JsonConvert.DeserializeObject<String>(checkFriends);
                if (friendStatus == "not friends" || friendStatus == "deleted")
                {
                    btnAddFriend.Visible = true;
                    PanelF.Visible = false;
                    btnRemoveFriend.Visible = false;
                    lblPendingFriends.Visible = false;
                }
                else if (friendStatus == "friend")
                {
                    btnAddFriend.Visible = false;
                    PanelF.Visible = true;
                    btnRemoveFriend.Visible = true;
                    lblPendingFriends.Visible = false;
                }
                else if (friendStatus == "pending")
                {
                    btnAddFriend.Visible = false;
                    PanelF.Visible = false;
                    btnRemoveFriend.Visible = false;
                    lblPendingFriends.Text = "Pending friend request";
                    lblPendingFriends.Visible = true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected async void generateContent()
        {
            try
            {
                String navigateUserID = Request.QueryString["id"].ToString();

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserFriends?userID=" + navigateUserID);
                HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
                String userDetails = await userDetailsResp.Content.ReadAsStringAsync();

                List<UserPortfolioModel> upm = JsonConvert.DeserializeObject<List<UserPortfolioModel>>(userDetails);

                if (upm.Count == 0 || upm.Equals(null))
                {
                    if (navigateUserID == currentUserID)
                    {
                        lblNone.Text = "You have not added anyone as friends yet. Kick start your SGServe volunterring journey by adding someone !";
                    }
                    else
                    {
                        lblNone.Text = "The volunteer has not added anyone as friends on SGServe.";
                    }

                    lblNone.Visible = true;
                }
                else
                {
                    lblNone.Visible = false;
                }

                DataList1.DataSource = upm;
                DataList1.DataBind();




            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
        {

        }

        protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
        {

        }

        protected async void btnAddFriend_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/addUserFriend?userID1=" + currentUserID + "&userID2=" + navigateUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserPortfolio.aspx?id=" + navigateUserID);
        }

        protected async void btnRemoveFriend_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/removeFriend?userID1=" + currentUserID + "&userID2=" + navigateUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserPortfolio.aspx?id=" + navigateUserID);
        }

        protected async void btnAcceptFriend_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            String navigateUserID = FriendRequestGridView.DataKeys[row.RowIndex].Value.ToString();

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/acceptFriendStatus?userID1=" + navigateUserID + "&userID2=" + currentUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserFriends.aspx?id=" + currentUserID);
        }

        protected async void btnDeleteFriendRequest_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            String navigateUserID = FriendRequestGridView.DataKeys[row.RowIndex].Value.ToString();

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/deleteFriend?userID1=" + currentUserID + "&userID2=" + navigateUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserFriends.aspx?id=" + currentUserID);
        }

        protected async void generateUserDetails()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserDetails?userID=" + navigateUserID);
                HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
                String userDetails = await userDetailsResp.Content.ReadAsStringAsync();

                UserPortfolioModel upm = JsonConvert.DeserializeObject<UserPortfolioModel>(userDetails);

                lblFullName.Text = upm.FULL_NAME;
                ProfilePicImg.ImageUrl = upm.IMAGE;
                lblDescription.Text = upm.DESCRIPTION;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class SearchUsers : System.Web.UI.Page
    {
        static String name;
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateRecommendation();
                }
            }
        }

        protected async void generateForm()
        {
            try
            {
                name = Session["searchtbName"].ToString();

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListofUsersBySearch?name=" + name);
                HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
                String details = await userDetailsResp.Content.ReadAsStringAsync();

                List<UserPortfolioModel> upm = JsonConvert.DeserializeObject<List<UserPortfolioModel>>(details);

                DataList1.DataSource = upm;
                DataList1.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }


        protected async void generateRecommendation()
        {
            try
            {
                List<UserPortfolioRecommendationModel> recommendList = new List<UserPortfolioRecommendationModel>();
                int countRecommend = 0;

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage recommendReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListOfRecommendedUsersByNetwork?userID=" + currentUserID);
                HttpResponseMessage recommendResp = await client.SendAsync(recommendReq);
                var recommend = await recommendResp.Content.ReadAsStringAsync();

                List<UserPortfolioRecommendationModel> recommendListNetwork = JsonConvert.DeserializeObject<List<UserPortfolioRecommendationModel>>(recommend);

                foreach (UserPortfolioRecommendationModel recommendedUser in recommendListNetwork)
                {
                    if (countRecommend > 5)
                    {
                        break;
                    }
                    else
                    {
                        countRecommend++;
                        recommendList.Add(recommendedUser);
                    }
                }

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserFacebookID?userID=" + currentUserID);
                HttpResponseMessage response = await client.SendAsync(request);
                var reply = await response.Content.ReadAsStringAsync();

                String facebookID = JsonConvert.DeserializeObject<String>(reply);

                if (!(facebookID.Trim().Equals("") || facebookID.Equals(null)))
                {
                    HttpRequestMessage fbfbRecommendReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListOfRecommendedUsersByFacebook?userID=" + currentUserID + "&facebookID=" + facebookID + "&accessToken=" + Session["fbAccessToken"].ToString());
                    HttpResponseMessage fbRecommendResp = await client.SendAsync(fbfbRecommendReq);
                    var fbRecommend = await fbRecommendResp.Content.ReadAsStringAsync();

                    List<UserPortfolioRecommendationModel> recommendListFacebook = JsonConvert.DeserializeObject<List<UserPortfolioRecommendationModel>>(fbRecommend);

                    foreach (UserPortfolioRecommendationModel recommendedUser in recommendListFacebook)
                    {
                        if (countRecommend > 9)
                        {
                            break;
                        }
                        else
                        {
                            countRecommend++;
                            recommendList.Add(recommendedUser);
                        }
                    }
                }

                HttpRequestMessage requestF = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserFriends?userID=" + currentUserID);
                HttpResponseMessage responseF = await client.SendAsync(requestF);
                var replyF = await responseF.Content.ReadAsStringAsync();

                List<UserPortfolioModel> friendsList = JsonConvert.DeserializeObject<List<UserPortfolioModel>>(replyF);

                foreach (UserPortfolioModel friendsByUser in friendsList)
                {
                    if (countRecommend > 19)
                    {
                        break;
                    }
                    else
                    {
                        countRecommend++;
                        UserPortfolioRecommendationModel friend = new UserPortfolioRecommendationModel();
                        friend.FULL_NAME = friendsByUser.FULL_NAME;
                        friend.IMAGE = friendsByUser.IMAGE;
                        friend.SCHL_NAME = friendsByUser.SCHL_NAME;
                        friend.USR_ID = friendsByUser.USR_ID;
                        recommendList.Add(friend);
                    }
                }

                if (recommendList.Count <= 0 || recommendList.Equals(null))
                {
                    lblNoRecommend.Visible = true;
                }
                else
                {
                    lblNoRecommend.Visible = false;
                }

                DataList1.DataSource = recommendList;
                DataList1.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Session["searchtbName"] = tbSearchName.Text;

            generateForm();
            //DataList1.DataBind();
            //Response.Redirect("SearchUsers.aspx");
            //DataList1.DataBind();
        }

        protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
        {
            Response.Redirect("Home.aspx");

            //if (e.CommandName == "navigatePortfolio")
            //{
            //Session["navigateUserID"] = e.Item.FindControl("USR_ID");
            //Response.Redirect("UserPortfolio.aspx?id=" + e.Item.FindControl("USR_ID"));
            //}
            //else
            //{

            //}
        }
    }
}
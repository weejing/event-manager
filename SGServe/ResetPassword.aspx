﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" Async="true" CodeBehind="ResetPassword.aspx.cs" Inherits="SGServe.ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h4 class="page-section-heading">Reset Password</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <i>An email will be sent to you to reset password.</i>
            <div class="form-group form-control-default required">
                <label>
                    Email
                     <asp:RequiredFieldValidator Display="Dynamic" SetFocusOnError="true" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator>
                </label>
                <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                <asp:Label ID="lblmessage" runat="server" Text="" ForeColor="#FF3300"></asp:Label>
            </div>
        </div>
    </div>
    <asp:Button ID="btnLookup" runat="server" class="btn btn-primary" Text="Look up" OnClick="btnLookup_Click" /><br />
    <br />
</asp:Content>

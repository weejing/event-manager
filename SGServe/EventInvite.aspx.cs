﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class EventInvite : System.Web.UI.Page
    {

        // create a list to store the ID of a sessionRole text row
        private List<String> controlList
        {
            get
            {
                if (ViewState["controls"] == null)
                {
                    ViewState["controls"] = new List<String>();
                }

                return (List<String>)ViewState["controls"];
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                displayEventDetails();
            }
            else
            {   // retain state over the button
                foreach(string button in controlList.ToList())
                {
                    Button register = defineRegisterButton(button, 0, 0);
                    sessionDetailsHolder.Controls.Add(register);
                }
            }
           
        }


        protected async void displayEventDetails()
        {
           
            String token = Session["Token"].ToString();

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/getInvitedEvent?eventId=" + Request.QueryString["eventId"]);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            EventDetails eventDetails = JsonConvert.DeserializeObject<EventDetails>(result);

            System.Diagnostics.Debug.WriteLine("result: " + result);
            Session["connectionString"] = eventDetails.connectionString;
            Session["eventID"] = eventDetails.eventId;
            evtName.Text = eventDetails.eventName + (" (" + eventDetails.connectionString + ")");
            desc.Text = eventDetails.description;
            Date.Text = eventDetails.startDate.ToShortDateString() + "-" + eventDetails.endDate.ToShortDateString();
            evtImage.ImageUrl = eventDetails.imagePath;

            hiddenRoleDescriptor.Value = "";
            HiddenPercent.Value = "";

            int count = 0;
            foreach (SessionDetails sessionDetail in eventDetails.listOfSession)
            {
                displaySessionDetails(sessionDetail, count);
                count++;
            }
        }

        private void displaySessionDetails(SessionDetails sessionDetail, int count)
        {
            addPanelOpenings();

            String time = sessionDetail.startTime.ToShortTimeString() + " - " + sessionDetail.endTime.ToShortTimeString();

            sessionDetailsHolder.Controls.Add(new LiteralControl("<h4 class='media-heading margin-v-0-10'>" + sessionDetail.sessionName + "</h4>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class ='fa fa-clock-o fa-fw'></i>" + time + "<br>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class='fa fa-map-marker fa-fw'></i>" + sessionDetail.location + "<br><br>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<p class='margin-none'>" + sessionDetail.sessionDescription + "</p><br>"));

            // create the row for session dates, event role and role description
            designSessionDetails();

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            String date = getDatesInString(sessionDetail.listOfSessionDays);
            System.Diagnostics.Debug.WriteLine("date: " + date);
            sessionDetailsHolder.Controls.Add(new LiteralControl("<input type='hidden' style='width:60%' data-toggle='select2-tags' id ='date" + count + "' data-tags ='" + date + "'/>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            defineSelectedSessionRoles(count, sessionDetail.listOfSessionRoles);
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<p id = 'p"+count+"'></p>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<p id = 'w" + count + "'></p>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));


            sessionDetailsHolder.Controls.Add(new LiteralControl("<br>"));        
            Button register = defineRegisterButton(sessionDetail.sessionId, sessionDetail.compulsoryDays, count);
            sessionDetailsHolder.Controls.Add(register);

            addPanelClosings();
        }

        private string getDatesInString(List<SessionDays> listOfSessionDays)
        {
            String dates = "";
            int count = 1;
            foreach(SessionDays day in listOfSessionDays)
            {
                if (count != listOfSessionDays.Count)
                    dates += day.evt_Date.ToString("d MMM yyyy") + ",";
                else
                    dates += day.evt_Date.ToString("d MMM yyyy");

                count++;
            }
            return dates;
        }


        protected void designSessionDetails()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Session Day"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Role"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Description"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));


            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Matched %"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));


            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

        }


        protected void addPanelOpenings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel panel-default'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media media-clearfix-xs media-clearfix-sm'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media-body'>"));
        }

        protected void addPanelClosings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
        }

       
        protected void defineSelectedSessionRoles(int count, List<SessionRoles> listOfSessionRole)
        {
            String selectId = "role" + count;
            sessionDetailsHolder.Controls.Add(new LiteralControl("<select id = '"+ selectId +"' class ='btn btn-white dropdown-toggle' onchange = 'roleNameOnChange("+selectId+", "+ count +")'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<option value='' disabled selected style='display: none;'>Select a role</option>"));

            foreach(SessionRoles role in listOfSessionRole)
            {
                sessionDetailsHolder.Controls.Add(new LiteralControl("<option value = '"+role.evt_roleId+"'>" + role.roleName +" </option>"));
                hiddenRoleDescriptor.Value += role.roleDesc + ",";
                HiddenPercent.Value += role.percent + ",";
            }
            sessionDetailsHolder.Controls.Add(new LiteralControl("</select>"));

            hiddenRoleDescriptor.Value += "-";
            HiddenPercent.Value += "-";
        }

       

        protected Button defineRegisterButton(String sessionId, int numofCompulsory, int count)
        {
            Button button = new Button();
            button.CssClass = "btn btn-danger";
            button.Click += new EventHandler(registerEventSession);
            button.ID = sessionId;
            button.Text = "Sign up";
            button.OnClientClick = "setSessionDatesPlaceHolder('" + count+ "', '"+ numofCompulsory+ "')";

            controlList.Add(sessionId);

            return button;
        }

        protected async void registerEventSession(Object sender, EventArgs e)
        {

            Button button = sender as Button;
            SignUpEvent signUp = new SignUpEvent();

            String sessionId = button.ID;
            String eventId = Session["eventID"] as String;

            signUp.sessionId = sessionId;
            signUp.eventId = Request.QueryString["eventId"];
            signUp.connectionString = Session["connectionString"] as string;

            
            signUp.evt_roleId = roleValuePlaceHolder.Value;
            signUp.eventdateList = datesValuePlaceHolder.Value;


            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String token = Session["Token"].ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpResponseMessage response = await client.PostAsJsonAsync("/api/EventMgmt/registerInvitedEvent", signUp);
            Response.Redirect("EventDetail.aspx?e=" +signUp.eventId + "&o="+signUp.connectionString, false);
        }
    }
}
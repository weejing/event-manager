﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class AcceptFriend : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        static String currentUserID = "";
        static String navigateUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                await retrieveUserDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    navigateUserID = Request.QueryString["id"];
                    currentUserID = volunteer.VOL_ID;
                }
            }

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/acceptFriendStatus?userID1=" + navigateUserID + "&userID2=" + currentUserID);
            HttpResponseMessage resp = await client.SendAsync(request);

            String response = await resp.Content.ReadAsStringAsync();

            HttpRequestMessage request2 = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/addUserActivityUser?userID=" + navigateUserID + "&activityType=ADDFRIEND" + "&targetUserID=" + currentUserID);
            HttpResponseMessage resp2 = await client.SendAsync(request2);

            String response2 = await resp.Content.ReadAsStringAsync();

            Response.Redirect("UserFriends.aspx?id=" + currentUserID);
        }

        private async Task retrieveUserDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            if (jwtToken != null)
            {
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                // add the jwt token into the authorization header for server validation
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                volunteer = GlobalModule.getUserDetails(reply);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;
using System.IO;

namespace SGServe
{
    public partial class UserPortfolioEdit : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //await retrieveUserDetails();
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    await retrieveUserPortfolioDetails();
                }
            }
        }

       

        private async Task retrieveUserPortfolioDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserDetails?userID=" + currentUserID);
            HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
            String userDetails = await userDetailsResp.Content.ReadAsStringAsync();

            UserPortfolioModel upm = JsonConvert.DeserializeObject<UserPortfolioModel>(userDetails);

            tbDescription.Text = upm.DESCRIPTION;
        }

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            /*
            if (FileUploadImage.PostedFile.ContentLength > (10 * 1024))
            {
                // pop up box
            }
            else
            { */
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                if (!tbDescription.Text.Equals(null) || !tbDescription.Text.Equals(""))
                {
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/updateUserPortfolio?userID=" + currentUserID + "&description=" + tbDescription.Text);
                    HttpResponseMessage resp = await client.SendAsync(request);
                    String response = await resp.Content.ReadAsStringAsync();
                }

                if (FileUploadImage.HasFile)
                {
                    if ((Path.GetExtension(FileUploadImage.FileName).ToLower() != ".jpg") && Path.GetExtension(FileUploadImage.FileName).ToLower() != ".png")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please upload an image');", true);
                    }

                    string guidResult = System.Guid.NewGuid().ToString("N");
                    string fn = System.IO.Path.GetFileName(FileUploadImage.PostedFile.FileName);
                    //to specify the filename
                    string filename = fn.ToLower().ToString();
                    string[] exts = filename.Split('.');
                    string ext = exts[1].ToString();
                    string finexts = guidResult + "." + ext;
                    String storedPath = Server.MapPath("images/users/" + finexts);
                    String path = "images/users/" + finexts;

                    HttpRequestMessage request1 = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/uploadImage?userID=" + currentUserID + " &imagePath=" + path);
                    HttpResponseMessage resp1 = await client.SendAsync(request1);
                    String response1 = await resp1.Content.ReadAsStringAsync();
                    //upload into folder                   
                    FileUploadImage.SaveAs(storedPath);

                }

                //String base64String = Convert.ToBase64String(FileUploadImage.FileBytes).Replace('+', '-').Replace('/', '_');
                /*
                HttpRequestMessage request1 = new HttpRequestMessage(HttpMethod.Post, "/api/CommunityMgmt/uploadImage?userID=" + currentUserID + " &imageByte=" + base64String);
                HttpResponseMessage resp1 = await client.SendAsync(request1);

                String response1 = await resp1.Content.ReadAsStringAsync();
                */
                Response.Redirect("UserPortfolio.aspx?id=" + currentUserID);
            //}

        }
    }
}
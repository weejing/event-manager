﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class RecommendVolunteers : System.Web.UI.Page
    {
        Boolean facebookLogin;
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateRecommendation();
                }
            }
        }

        protected async void generateRecommendation()
        {
            try
            {
                List<UserPortfolioRecommendationModel> recommendList = new List<UserPortfolioRecommendationModel>();
                int countRecommend = 0;

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage recommendReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListOfRecommendedUsersByNetwork?userID=" + currentUserID);
                HttpResponseMessage recommendResp = await client.SendAsync(recommendReq);
                var recommend = await recommendResp.Content.ReadAsStringAsync();

                List<UserPortfolioRecommendationModel> recommendListNetwork = JsonConvert.DeserializeObject<List<UserPortfolioRecommendationModel>>(recommend);

                foreach (UserPortfolioRecommendationModel recommendedUser in recommendListNetwork)
                {
                    if (countRecommend > 9)
                    {
                        break;
                    }
                    else
                    {
                        countRecommend++;
                        recommendList.Add(recommendedUser);
                    }
                }

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserFacebookID?userID=" + currentUserID);
                HttpResponseMessage response = await client.SendAsync(request);
                var reply = await response.Content.ReadAsStringAsync();

                String facebookID = JsonConvert.DeserializeObject<String>(reply);

                if (!(facebookID.Trim().Equals("") || facebookID.Equals(null)))
                {
                    HttpRequestMessage fbfbRecommendReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListOfRecommendedUsersByFacebook?userID=" + currentUserID + "&facebookID=" + facebookID + "&accessToken=" + Session["fbAccessToken"].ToString());
                    HttpResponseMessage fbRecommendResp = await client.SendAsync(fbfbRecommendReq);
                    var fbRecommend = await fbRecommendResp.Content.ReadAsStringAsync();

                    List<UserPortfolioRecommendationModel> recommendListFacebook = JsonConvert.DeserializeObject<List<UserPortfolioRecommendationModel>>(fbRecommend);

                    foreach (UserPortfolioRecommendationModel recommendedUser in recommendListFacebook)
                    {
                        recommendList.Add(recommendedUser);
                    }
                }

                if (recommendList.Count <= 0 || recommendList.Equals(null))
                {
                    lblNoRecommend.Visible = true;
                }
                else
                {
                    lblNoRecommend.Visible = false;
                }

                DataListRecommend.DataSource = recommendList;
                DataListRecommend.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
    }
}
﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class EventList : System.Web.UI.Page
    {

        private List<String> controlList
        {
            get
            {
                if (ViewState["controls"] == null)
                {
                    ViewState["controls"] = new List<String>();
                }

                return (List<String>)ViewState["controls"];
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
        }


        protected void Page_Load(object sender, EventArgs e)
        {            
            if(!IsPostBack)
            {
                if (Session["Token"] != null)
                {
                    if (Session["LoginType"].ToString().Equals("Organiser"))
                    {
                        getEventForPublic();
                        LblCollaborative.Visible = false;
                        pnlMyEvents.Visible = false;
                    }
                    else
                    {
                        getEventListForVol();
                        LblCollaborative.Visible = true;
                        pnlMyEvents.Visible = true;
                    }
                }
                else {
                    getEventForPublic();
                    LblCollaborative.Visible = false;
                    pnlMyEvents.Visible = false;
                }
                   

                getEventCause();
            }       
        }

        protected async void getEventListForVol()
        {
            String token = Session["Token"].ToString();
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveEventListForVol");

            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("reuslt: " + result);
            List<List<EventDetails>> listOfEventDetails = JsonConvert.DeserializeObject<List<List<EventDetails>>>(result);
            Session["eventList"] = listOfEventDetails;
            createEventList(listOfEventDetails);
        }


        /*
        * Generate a checkbox list of the existing beneficiary 
        */
        private async void getEventCause()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermList?vocab_id=" + "2");
            HttpResponseMessage response = await client.SendAsync(request);
            String reply = await response.Content.ReadAsStringAsync();

            List<EventCause> listOfEventCause = JsonConvert.DeserializeObject<List<EventCause>>(reply);

            causeHiddenName.Value = "";
            causeHiddenId.Value = "";
            foreach(EventCause cause in listOfEventCause)
            {
                causeHiddenName.Value += cause.TERM_NAME + ",";
                causeHiddenId.Value += cause.TERM_ID + ",";
            }                       
        }


        protected async void getEventForPublic()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveEventListForPublic");

            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("reuslt: " + result);
            List<List<EventDetails>> listOfEventDetails = JsonConvert.DeserializeObject<List<List<EventDetails>>>(result);
            Session["eventList"] = listOfEventDetails;
            createEventList(listOfEventDetails);

        }


        protected void createEventList(List<List<EventDetails>> listofEventCategory)
        {
            foreach (List<EventDetails> listofEventDetails in listofEventCategory)
            {
                String NPO = listofEventDetails[0].orgName;
                eventListPanel.Controls.Add(new LiteralControl("<hr>"));
                eventListPanel.Controls.Add(new LiteralControl("<H4>" + NPO + "</H4>"));
                eventListPanel.Controls.Add(new LiteralControl("<br/>"));

                eventListPanel.Controls.Add(new LiteralControl("<div class='row gridalicious' data-toggle='gridalicious' data-width='350'>"));
                System.Diagnostics.Debug.WriteLine(NPO + ":" + listofEventDetails.Count);
                foreach (EventDetails eventDetails in listofEventDetails)
                {
                    formEventPanel(eventDetails);
                }

                eventListPanel.Controls.Add(new LiteralControl("</div>"));

            }
        }

        // this method creates an event panel template
        protected void formEventPanel(EventDetails eventDetails)
        {
            String date = eventDetails.startDate.ToString("d MMM yyyy") + " - " + eventDetails.endDate.ToString("d MMM yyyy");

            eventListPanel.Controls.Add(new LiteralControl("<div class='panel panel-default relative'>"));

            // event image
            eventListPanel.Controls.Add(new LiteralControl("<div class='cover hover overlay margin-none'>"));
            eventListPanel.Controls.Add(new LiteralControl("<img src='" + eventDetails.imagePath + "' style='height:200px'/>"));

            eventListPanel.Controls.Add(new LiteralControl("<a href='EventDetail.aspx?e=" + eventDetails.eventId + "&o=" + eventDetails.connectionString + "' class='overlay overlay-full overlay-bg-black overlay-hover'>"));
            eventListPanel.Controls.Add(new LiteralControl("<span class='v-center'><span class='btn btn-circle btn-white'><i class='fa fa-eye'></i></span></span></a>"));
            eventListPanel.Controls.Add(new LiteralControl("</div>"));

            // event Details
            eventListPanel.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
            eventListPanel.Controls.Add(new LiteralControl("<h4 class='margin-v-0-5' style='overflow: hidden; height: 40px;text-overflow: ellipsis;'>" + eventDetails.eventName + "</h4>"));
            eventListPanel.Controls.Add(new LiteralControl("<span class='label label-customize-100' style='text-align:center'>" + date + "</span>"));
            eventListPanel.Controls.Add(new LiteralControl("</div>"));
            eventListPanel.Controls.Add(new LiteralControl("</div>"));
        }


      
        protected async void searchBtn_Click(object sender, EventArgs e)
        {
            eventListPanel.Controls.Clear();

            SearchEvent searchEvent = new SearchEvent();
            
            if(txtPeriod.Text.Equals("") == false)
            {
                System.Diagnostics.Debug.WriteLine("date not empty?");
                String[] dateSegement = txtPeriod.Text.Split('-');
                searchEvent.startDate= DateTime.ParseExact(dateSegement[0].Substring(0, dateSegement[0].Length - 1), "MM/dd/yyyy", null);
                searchEvent.endDate = DateTime.ParseExact(dateSegement[1].Substring(1, dateSegement[1].Length - 1), "MM/dd/yyyy", null);
                searchEvent.noDate = true;
            }


            if(searchCause.Value.Equals("") == false)
            {
                searchEvent.eventCause= searchCause.Value.Split(',').Select(Int32.Parse).ToList();
            }

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpResponseMessage response = await client.PostAsJsonAsync("/api/EventMgmt/searchEvent", searchEvent);

            String result = await response.Content.ReadAsStringAsync();
            List<List<EventDetails>> listOfEventDetails = JsonConvert.DeserializeObject<List<List<EventDetails>>>(result);
            Session["eventList"] = listOfEventDetails;
            createEventList(listOfEventDetails);
        }


        private void mergeSortDate(int startIndex, int endIndex,  List<EventDetails> listOfEvents)
        {
            System.Diagnostics.Debug.WriteLine("start index: [ {0} ] " +listOfEvents[startIndex].eventName, startIndex);
            if (endIndex - startIndex < 1)
                return;

            int median = endIndex + startIndex;

            mergeSortDate(startIndex, (median/2), listOfEvents );
            mergeSortDate(median/2+1, endIndex , listOfEvents);

            doMerge(listOfEvents, startIndex, endIndex, (median / 2) + 1);
        }


        private void doMerge(List<EventDetails> listOfEvents, int startIndex, int endIndex, int median)
        {
            int numOfElement = endIndex -startIndex + 1;
            EventDetails[] tempEventList = new EventDetails[numOfElement];
            int startIndexEnd = median - 1;
            int tempPosition = 0;
            int mergeStart = startIndex;

            // write into ta temporary array from smallest date to largest date
            while((startIndex <= startIndexEnd) && (median <= endIndex))
            {
                if(listOfEvents[startIndex].startDate <= listOfEvents[median].startDate)
                {
                    tempEventList[tempPosition] = listOfEvents[startIndex]; 
                    startIndex++;
                }
                else
                {
                    tempEventList[tempPosition] = listOfEvents[median];
                    median++;
                }
                tempPosition++;
            }

            // if start index list have a larger date
            while(startIndex <= startIndexEnd)
            {
                tempEventList[tempPosition] = listOfEvents[startIndex];
                startIndex++;
                tempPosition++;
            }

            // if end index list have a larger date
            while(median <= endIndex)
            {
                tempEventList[tempPosition] = listOfEvents[endIndex];
                median++;
                tempPosition++;
            }

            // write back the sorted array into the main event list
            for(int count = 0; count < tempEventList.Length; count++ )
            {
                listOfEvents[mergeStart] = tempEventList[count];
                mergeStart++;
            }
        }

        protected void ddlSortby_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSortby.SelectedItem.ToString() == "Organization")
            {
                eventListPanel.Controls.Clear();
                controlList.Clear();
                List<List<EventDetails>> listOfEvents = Session["eventList"] as List<List<EventDetails>>;
                createEventList(listOfEvents);
            }
            else
            {
                eventListPanel.Controls.Clear();
                controlList.Clear();
                List<List<EventDetails>> listOfEventDetails = Session["eventList"] as List<List<EventDetails>>;
                List<EventDetails> mergeList = new List<EventDetails>(listOfEventDetails[0]);


                for (int listCount = 1; listCount < listOfEventDetails.Count; listCount++)
                {
                    mergeList.AddRange(listOfEventDetails[listCount]);
                }

                mergeSortDate(0, mergeList.Count - 1, mergeList);

                eventListPanel.Controls.Add(new LiteralControl("<div class='row gridalicious' data-toggle='gridalicious' data-width='350' >"));
                DateTime currentDate = DateTime.Now;
                for (int count = 0; count < mergeList.Count; count++)
                {
                    if(mergeList[count].startDate >= currentDate )
                        formEventPanel(mergeList[count]);
                }
                eventListPanel.Controls.Add(new LiteralControl("</div>"));
            }
        }
    }
}
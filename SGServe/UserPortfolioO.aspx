﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="VolMaster.Master" AutoEventWireup="true" CodeBehind="UserPortfolioO.aspx.cs" Inherits="SGServe.UserPortfolioO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="community_css/css/vendor/all.css" rel="stylesheet">
    <link href="community_css/css/app/app.css" rel="stylesheet">
    <script src="community_css/js/vendor/all.js"></script>
    <script src="community_css/js/app/app.js"></script>

          <div class="container">
            <div class="cover profile">
                
              <div class="wrapper">
                <div class="image" style="height: 60px;">
                  <!--<img src="main_css/images/place1-wide.jpg" alt="people" />-->
                </div>
              </div>
                    
              <div class="cover-info">
                <div class="avatar">
                  <asp:Image ID="ProfilePicImg" runat="server" />
                </div>
                <div class="name">
                    <h2>
                        <asp:Label ID="lblFullName" runat="server" Text="Bill" ForeColor="Black"></asp:Label>
                    </h2>
                            </div>
                <ul class="cover-nav">

                  <li class="active"><!--<a href="UserPortfolio.aspx" id="linkAbout"><i class="fa fa-fw icon-user-1"></i> About</a>-->
                    <asp:HyperLink ID="linkAbout" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> About </asp:HyperLink></li>
                  <!--<li><a href="UserFriends.aspx" id="linkFriends"><i class="fa fa-fw fa-users"></i> Friends</a></li>
                  <li><a href="UserOrgFollows.aspx" id="linkFollows"><i class="fa fa-fw fa-building-o"></i> Follows</a></li>-->
                  <li> </li>
                  <li>

                      <li>
                      </li>
                </ul>
              </div>
            </div>
           </div>

        <div class="container">
                             <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> About
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">School</span></div>
                          <div class="col-sm-8">
                              <asp:Label ID="lblSchool" runat="server" Text="Label">my school name</asp:Label>
                            </div>
                        </div>
                      </li>
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Interests (Cause)</span></div>
                          <div class="col-sm-8">
                                    <asp:DataList ID="dlBene" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li>
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Interests (Role)</span></div>
                          <div class="col-sm-8">
                              
                                    <asp:DataList ID="dlRole" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li>
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Skills</span></div>
                          <div class="col-sm-8">
                                    <asp:DataList ID="dlskill" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li>
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Personality MBTI</span>
                              </div>
                              <div class="col-sm-8">
                              <asp:Label ID="lblPersonalityMBTI" runat="server"></asp:Label>
                                </div>
                         </div>
                      </li>
                        
                    <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Description</span>
                              </div>
                            <div class="col-sm-8">
                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                            </div>
                         </div>
                      </li>
                    </ul>
                  </div>
                <br/>
            </div>

            </div>
</asp:Content>

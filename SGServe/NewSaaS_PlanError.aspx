﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolHome.Master" AutoEventWireup="true" CodeBehind="NewSaaS_PlanError.aspx.cs" Inherits="SGServe.NewSaaS_PlanError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="panel panel-default">
        <div class="page-section" style="padding: 1%; padding-left: 5%;">
            <h2>Opps, there seems to be an error.</h2>
            <p>No transaction has been made with Paypal</p>
            <asp:Label ID ="Label1" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

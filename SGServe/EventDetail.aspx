﻿<%@ Page Title="" Language="C#" Async ="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="EventDetail.aspx.cs" Inherits="SGServe.EventDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
       <div class ="property-meta">
        <h1 class="page-section-heading" style="color: #26355b;"><asp:Label ID="evtName" runat="server"></asp:Label></h1>
        <div class ="row">
            <div class ="col-md-7">
                <asp:Image ID="evtImage" runat="server" ImageUrl="/main_css/images/photodune-378874-real-estate-xs.jpg" style="width:100%;height:300px"/>
            </div>
            <div class ="col-md-5">
                <div class ="panel panel-default">
                    <div class ="panel-heading">
                        <h4 class="panel-title"> <asp:Label ID="Date" runat="server"/><div style="float:right"><asp:PlaceHolder ID="PlaceHolderFBlink" runat="server"></asp:PlaceHolder> </div> </h4>
                    </div>
                    <div class ="panel-body">
                    <%--    <div class ="expandable expandable-indicator-white expandable-trigger">--%>
                            <div class ="expandable-content">
                                <p><asp:Label ID="desc" runat="server"></asp:Label></p>  
                                     <asp:DataList ID="dlcause" runat="server" CellSpacing="20" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <span class="label label-customize2-100"><i class="fa fa-tag" aria-hidden="true"></i>&nbsp;<%# DataBinder.Eval(Container.DataItem, "TERM_NAME") %></span>
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:Label ID="miniLink" Font-Size="Smaller" runat="server"></asp:Label><br/>
                           <asp:Label ID="lblOrg" runat="server" Text="Label" Font-Italic="True" Font-Size="Smaller"></asp:Label>                    
                            </div>
                       <%-- </div>--%>
                    </div>
                </div>        
            </div>
        </div>
    </div>
      <br/>  
    <div class =" tabbable">
        <ul class ="nav nav-tabs">
            <li class ="active"><a href ="#register" data-toggle ="tab"><i class="fa fa-fw fa-cog"></i>Register Session</a></li>
            <li><a href ="#mySession" data-toggle ="tab"><i class="fa fa-fw fa-home"></i>My Sessions</a></li>
        </ul>

        <div class =" tab-content">
            <div id ="register" class ="tab-pane active">                
              <asp:PlaceHolder ID="sessionDetailsHolder" runat="server"></asp:PlaceHolder> 
            </div>

            <div id="mySession" class ="tab-pane">
                <asp:PlaceHolder ID="mySessionsPlaceHolder" runat="server"></asp:PlaceHolder>
            </div>

        </div>

    </div>

    <asp:HiddenField ID="hiddenRoleDescriptor" runat="server" />
    <asp:HiddenField ID="hiddenDate" runat="server" />

    <asp:HiddenField ID="datesValuePlaceHolder" runat="server" />
    <asp:HiddenField ID="roleValuePlaceHolder" runat="server" />

  
 
<%--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>--%>
    <script>

        function setSessionDatesPlaceHolder(dateId, numOfCompulsory)
        {
            console.log("dateId: " + dateId);
            var sessionDates = $("#date" + dateId);
            var sessionRole = $("#role" + dateId);
            console.log(sessionDates.val());

            var split = sessionDates.val().split(',');
            numOfCompulsory = parseInt(numOfCompulsory);
            console.log("split:" + split.length);
            console.log("numofCompulsory:" + numOfCompulsory);


            if (split.length < numOfCompulsory)
            {
                alert("You have to attend at least " + numOfCompulsory + " days ");
                event.preventDefault();
                return false;
            }
            console.log("sessionRole: " + sessionRole.val());
            document.getElementById("<%=datesValuePlaceHolder.ClientID%>").value = sessionDates.val();
            document.getElementById("<%=roleValuePlaceHolder.ClientID%>").value = sessionRole.val();
        }


        function roleNameOnChange(id, arrayCount)
        {
            var dateDropDown = $('#date' + arrayCount);
            dateDropDown.val('');

            var hiddenRoleDescrip = $('#<%= hiddenRoleDescriptor.ClientID%>');
            var hiddenDateList = $('#<%= hiddenDate.ClientID%>');

            var roleArray = hiddenRoleDescrip.val().split('-');
            var descriptionList = roleArray[arrayCount].split(',');
            var description = descriptionList[id.selectedIndex - 1];
            $('#p' + arrayCount).text(description);

            var dateArray = hiddenDateList.val().split('+');
            var dateList = dateArray[arrayCount].split('-');
            var date = dateList[id.selectedIndex - 1].split(',');

            console.log(dateDropDown);

            dateDropDown.select2({
                tags: date,
                placeholder: 'Select your desired time slot'
            });

            dateDropDown.trigger("chosen:updated");
            dateDropDown.parent().show();
        }

    </script>
</asp:Content>

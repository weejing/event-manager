﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Globalization;
using SGServe.Models.AdminModels;
using SGServe.Models.TransactionModels;

namespace SGServe
{
    public partial class PaymentPlan : System.Web.UI.Page
    {
        public static string org_id = "";
        public static string connection = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                load_this_first();
            }
        }
        public async void load_this_first()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgInfo");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", urlArr[1]));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);
                //await Task.Delay(1000);
                String reply = await response.Content.ReadAsStringAsync();

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                org_id = array["ORG_ID"].ToString().Substring(1, array["ORG_ID"].ToString().Length - 2);
                connection = array["CONNECTION_STRING"].ToString().Substring(1, array["CONNECTION_STRING"].ToString().Length - 2);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

    }
}
﻿using SGServe.CommonClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmessage.Visible = false;

        }

        protected async void btnLookup_Click(object sender, EventArgs e)
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "/api/Account/reqResetPw?email=" + txtEmail.Text.ToString()+ "&url=" + baseUrl);
            HttpResponseMessage resp = await client.SendAsync(req);
            String response = await resp.Content.ReadAsStringAsync();

            if (Convert.ToBoolean(response) == true)
            {
                lblmessage.Visible = false;
                //show successmsg
                ScriptManager.RegisterStartupScript(this, this.GetType(),"alert", "alert('Reset password request success! Please check your email to reset password');window.location ='home.aspx';", true);
            }
            else
            {

                lblmessage.Visible = true;
                lblmessage.Text="Invalid email";
            }
        }
    }
}
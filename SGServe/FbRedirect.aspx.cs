﻿using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;
using System.Json;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class FbRedirect : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                // Get the Facebook code from the querystring
                if (Request.QueryString["code"] != "")
                {
                    System.Diagnostics.Debug.WriteLine("code: " + Request.QueryString["code"]);
                    await getFacebookUserData(Request.QueryString["code"]);                  
                }
            }
        }

        /*
         * This method gets the user details of a facebook user 
         */
        private async Task getFacebookUserData(String code)
        {
            // Exchange the code for an access token
            Uri targetUri = new Uri("https://graph.facebook.com/oauth/access_token?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&client_secret=" + ConfigurationManager.AppSettings["FacebookAppSecret"] + "&redirect_uri=http://" + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "/FbRedirect.aspx&code=" + code);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);

            System.IO.StreamReader str = new System.IO.StreamReader(request.GetResponse().GetResponseStream());
            string token = str.ReadToEnd().ToString().Replace("access_token=", "");

            // Split the access token and expiration from the single string
            string[] combined = token.Split('&');
            string accessToken = combined[0];

            // Request the Facebook user information
            Uri targetUserUri = new Uri("https://graph.facebook.com/me?fields=first_name,last_name,gender,locale,link,birthday&access_token=" + accessToken);
            System.Diagnostics.Debug.WriteLine("token: " + accessToken);

            HttpWebRequest user = (HttpWebRequest)HttpWebRequest.Create(targetUserUri);
            // Read the returned JSON object response
            String jsonResponse = string.Empty;
            try
            {
                StreamReader userInfo = new StreamReader(user.GetResponse().GetResponseStream());
                jsonResponse = userInfo.ReadToEnd();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception:" + e.ToString());
            }

            // Deserialize and convert the JSON object to the Facebook.User object type
            JavaScriptSerializer sr = new JavaScriptSerializer();
            string jsondata = jsonResponse;
            FbUser userDetails = sr.Deserialize<FbUser>(jsondata);
            Session["fbAccessToken"] = accessToken;
            await getJwtAccessToken(userDetails.id, accessToken);
        }

        /*
         * This methods sends a post reqeust to the vms server for an authentication check to be done 
         * Returns with a json web token if authetication succeeds
         * 
         */
        private async Task getJwtAccessToken(String userId, String accessToken)
        {

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/Authorise/Token");
            // form up the body data
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("UserName", userId));
            keyValues.Add(new KeyValuePair<string, string>("password", accessToken));
            keyValues.Add(new KeyValuePair<string, string>("LoginType", "Facebook"));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));

            request.Content = new FormUrlEncodedContent(keyValues);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            JsonObject content = (JsonObject)JsonValue.Parse(result);

            ICollection<String> listOfKeys = content.Keys;
            foreach (string key in listOfKeys)
            {
                if (key.Equals("error"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Invalid account, please register!');window.location ='signup.aspx';", true);
                }
                else
                {
                    String access = content["access_token"].ToString();
                    String token = access.Substring(1, access.Length - 2);
                    Session["Token"] = token;
                    Session.Add("LoginType", "Volunteer");
                    Response.Redirect("dashboard.aspx",false);
                }
            }   

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGServe.spinwheelcontent
{
    public class Segment
    {
        public String fillStyle;
        public String text;

        public Segment(String fillStyle, String text)
        {
            this.fillStyle = fillStyle;
            this.text = text;
        }
    }
}
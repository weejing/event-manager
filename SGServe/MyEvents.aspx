﻿<%@ Page Title="" Language="C#" Async ="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="MyEvents.aspx.cs" Inherits="SGServe.MyEvents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <h1 class="text-h2">My Events</h1>
    <div class="row gridalicious" data-toggle="gridalicious" data-width="200" >
        <asp:PlaceHolder ID="eventListPanel" runat="server"></asp:PlaceHolder>
        </div>

</asp:Content>

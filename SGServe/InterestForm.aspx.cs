﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class InterestForm : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else {
                    await generateForm();
                }         
            }
        }

        protected async Task generateForm()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermListByVocabName?vocab_name=cause");
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                HttpRequestMessage rolReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermListByVocabName?vocab_name=event role");
                HttpResponseMessage rolResp = await client.SendAsync(rolReq);
                String roles = await rolResp.Content.ReadAsStringAsync();
                HttpRequestMessage skillReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermListByVocabName?vocab_name=skill");
                HttpResponseMessage skillResp = await client.SendAsync(skillReq);
                String skills = await skillResp.Content.ReadAsStringAsync();

                var Benetable = JsonConvert.DeserializeObject<DataTable>(reply);
                var Roletable = JsonConvert.DeserializeObject<DataTable>(roles);
                var skillstable = JsonConvert.DeserializeObject<DataTable>(skills);
                hiddenBeneVocabID.Value = Benetable.Rows[0][1].ToString();
                hiddenRoleVocabID.Value = Roletable.Rows[0][1].ToString();
                hiddenSkillVocabID.Value = skillstable.Rows[0][1].ToString();

                cbBenefi.DataSource = Benetable;
                cbBenefi.DataTextField = "TERM_NAME";
                cbBenefi.DataValueField = "TERM_ID";
                cbBenefi.DataBind();

                cbRoles.DataSource = Roletable;
                cbRoles.DataTextField = "TERM_NAME";
                cbRoles.DataValueField = "TERM_ID";
                cbRoles.DataBind();

                cbSkill.DataSource = skillstable;
                cbSkill.DataTextField = "TERM_NAME";
                cbSkill.DataValueField = "TERM_ID";
                cbSkill.DataBind();

                //get & display volunteer interest form response - cause & role
               // Server_Connect vbserverConnection = new Server_Connect();
                HttpRequestMessage vbrequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id="+volunteer.VOL_ID+ "&vocab_name=cause");
                HttpResponseMessage vbresponse = await client.SendAsync(vbrequest);
                String vbreply = await vbresponse.Content.ReadAsStringAsync();
                var vbBenetable = JsonConvert.DeserializeObject<DataTable>(vbreply);
                foreach (DataRow row in vbBenetable.Rows)
                {
                    cbBenefi.Items.FindByValue(row["TERM_ID"].ToString()).Selected = true;
                }

               // Server_Connect vrserverConnection = new Server_Connect();
                HttpRequestMessage vrrequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + volunteer.VOL_ID + "&vocab_name="+ "event role");
                HttpResponseMessage vrresponse = await client.SendAsync(vrrequest);
                String vrreply = await vrresponse.Content.ReadAsStringAsync();
                var vrRoletable = JsonConvert.DeserializeObject<DataTable>(vrreply);
                foreach (DataRow row in vrRoletable.Rows)
                {
                    cbRoles.Items.FindByValue(row["TERM_ID"].ToString()).Selected = true;
                }

               // Server_Connect SserverConnection = new Server_Connect();
                HttpRequestMessage Srequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + volunteer.VOL_ID + "&vocab_name=" + "skill");
                HttpResponseMessage Sresponse = await client.SendAsync(Srequest);
                String Sreply = await Sresponse.Content.ReadAsStringAsync();
                var Stable = JsonConvert.DeserializeObject<DataTable>(Sreply);
                foreach (DataRow row in Stable.Rows)
                {
                    cbSkill.Items.FindByValue(row["TERM_ID"].ToString()).Selected = true;
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:"+ex.Message);
            }
        }

        //private async Task retrieveUserDetails()
        //{
        //    Server_Connect serverConnection = new Server_Connect();
        //    HttpClient client = serverConnection.getHttpClient();
        //    String jwtToken = Session["Token"] as String;
        //    if (jwtToken != null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
        //        // add the jwt token into the authorization header for server validation
        //        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        //        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
        //        HttpResponseMessage Resp = await client.SendAsync(request);
        //        String reply = await Resp.Content.ReadAsStringAsync();
        //        volunteer = GlobalModule.getUserDetails(reply);
        //    }
        //}

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Account/updateInterestForm");

            List<TermRelation> volInterestList = new List<TermRelation>();
            foreach (ListItem item in cbBenefi.Items)
            {
                if (item.Selected)
                {
                    TermRelation volInterest = new TermRelation { TERM_ID = Convert.ToInt32(item.Value), VOCAB_ID = Convert.ToInt32(hiddenBeneVocabID.Value) };
                    volInterestList.Add(volInterest);
                }
            }
            foreach (ListItem item in cbRoles.Items)
            {
                if (item.Selected)
                {
                    TermRelation volInterest = new TermRelation { TERM_ID = Convert.ToInt32(item.Value), VOCAB_ID = Convert.ToInt32(hiddenRoleVocabID.Value) };
                    volInterestList.Add(volInterest);
                }
            }

            foreach (ListItem item in cbSkill.Items)
            {
                if (item.Selected)
                {
                    TermRelation volInterest = new TermRelation { TERM_ID = Convert.ToInt32(item.Value), VOCAB_ID = Convert.ToInt32(hiddenSkillVocabID.Value) };
                    volInterestList.Add(volInterest);
                }
            }

            volunteer = await gm.getLoginVolDetails();
            if (volunteer.VOL_ID != null)
            {
                Volunteer volunteer2 = new Volunteer { VOL_ID = volunteer.VOL_ID, volListofInts = volInterestList};
                await client.PostAsJsonAsync("api/Account/updateInterestForm", volunteer2);
                Response.Redirect("Dashboard.aspx", false);              
            }
            else
            {
                Response.Redirect("login.aspx", false);
            }
        }
            
        }
}
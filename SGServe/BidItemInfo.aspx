﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="BidItemInfo.aspx.cs" Inherits="SGServe.BidItemInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                <h2>
                Bidding Details : <asp:Label ID="lblHeading" runat="server" Text=" "></asp:Label>
            </h2>
    <div class="row">
        <br>
        <asp:Panel ID="Panel1" runat="server">
            <div class="panel panel-default">
                <h3>Item details</h3>
                <br/>
                <div class="table-responsive" style="padding-left: 3%; padding-right: 3%;">
                    <asp:GridView ID="ItemsGridView" CellPadding="50" GridLines="None" Width="100%" CssClass="table"
                        AllowPaging="True" EmptyDataText="There are currently no items avaliable for bidding" runat="server" AutoGenerateColumns="False" PageSize="20" 
                        DataKeyNames = "BID_ITEM_ID">
                        <Columns>
                            <asp:TemplateField>
                                    <ItemTemplate>
                                       <asp:Image ID="Image1" runat="server" ImageUrl=<%# DataBinder.Eval(Container.DataItem, "IMAGE") %> Width="80" />
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ITEM_NAME" HeaderText="Item Name"/>
                            <asp:BoundField DataField="ITEM_CATEGORY_NAME" HeaderText="Category"/>
                            <asp:BoundField DataField="ITEM_DESCRIPTION" HeaderText="Item Description" />
                            <asp:BoundField DataField="QTY" HeaderText="Quantity published" />
                            <asp:BoundField DataField="NUM_BIDDERS" HeaderText="Number of Bidders" />
                            <asp:BoundField DataField="MIN_POINT" HeaderText="Minimum Bidding Points" />
                            <asp:BoundField DataField="START_DATE" HeaderText="Start Date" />
                            <asp:BoundField DataField="END_DATE" HeaderText="End Date" />
                            <asp:BoundField DataField="STATUS" HeaderText="Bidding Status" />
                            <asp:BoundField DataField="SPONSOR_NAME" HeaderText="Sponsor Name" />
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                </div>

                <br/>
                <h3>Bidding details</h3>
                <br/>
                <div class="table-responsive" style="padding-left: 3%; padding-right: 3%;">
                    <asp:GridView ID="BiddingGridView" CellPadding="50" GridLines="None" Width="100%" CssClass="table"
                        AllowPaging="True" EmptyDataText="You have not placed on this bid." runat="server" AutoGenerateColumns="False" PageSize="20" 
                        DataKeyNames = "USR_BID_ID">
                        <Columns>
                            <asp:BoundField DataField="BID_POINTS_AMOUNT" HeaderText="Bid Amount"/>
                            <asp:BoundField DataField="DATE_TIME" HeaderText="Bidding Date & Time" />
                        </Columns>
                        <HeaderStyle ForeColor="#0066CC" />
                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                    </asp:GridView>
                    <asp:Label ID="lblNonePlaced" runat="server" Text="You have not placed on this bid."></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <h3> Current Avaliable points : <asp:Label ID="lblPoints" runat="server" Text=""></asp:Label></h3>
        <br/>
          <asp:Panel ID="PanelBid" runat="server" Visible="False">
            <div class="panel-body">
                <div class="form-group form-control-default required">
                    <label>Bid Points</label>
                    <asp:Label ID="lblExceedPoints" runat="server" Text="You don't have enough bid points." Visible="False" ForeColor="#FF3300"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="bidGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="tbBidPoints" ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="bidGrp" ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ID="RegularExpressionValidator3" ControlToValidate="tbBidPoints" runat="server" ValidationExpression="^\d+$" ErrorMessage="Wrong Format. Please place bid in numerical value."></asp:RegularExpressionValidator>                    
                    <asp:TextBox ID="tbBidPoints" runat="server" class="form-control"></asp:TextBox>
                    &nbsp;
                </div>
                <asp:Button ID="btnPlaceBid" runat="server" class="btn btn-info" Text="Place Bid" ValidationGroup="bidGrp" OnClick="btnPlaceBid_Click"/>&nbsp
                <asp:Button ID="btnUpdateBid" runat="server" class="btn btn-info" Text="Update Bid" Visible="False" OnClick="btnUpdateBid_Click" />
                <asp:Button ID="btnWithdrawBid" runat="server" class="btn btn-danger" Text="Withdraw Bid" Visible="False" OnClick="btnWithdrawBid_Click" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>

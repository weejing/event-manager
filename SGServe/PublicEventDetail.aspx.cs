﻿using FastMember;
using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class PublicEventDetail : System.Web.UI.Page
    {

        private List<String> controlList
        {
            get
            {
                if (ViewState["controls"] == null)
                {
                    ViewState["controls"] = new List<String>();
                }

                return (List<String>)ViewState["controls"];
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            displayEventDetails();
            facebookSharerHolder();
        }


        protected async void displayEventDetails()
        {
            String eventId = Request.QueryString["e"];
            String connectionString = Request.QueryString["o"];

            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveSessionDetailsForPublic?eventId=" + eventId + "&connectionString=" + connectionString);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("result: " + result);
            VolEventDetail eventDetails = JsonConvert.DeserializeObject<VolEventDetail>(result);

            //display event cause
            DataTable tableCause = new DataTable();
            using (var reader = ObjectReader.Create(eventDetails.listofCauseName))
            {
                tableCause.Load(reader);
            }
            dlcause.DataSource = tableCause;
            dlcause.DataBind();

            evtName.Text = eventDetails.eventName;
            desc.Text = eventDetails.description;
            Date.Text = eventDetails.startDate.ToString("d MMM yyyy") + " - " + eventDetails.endDate.ToString("d MMM yyyy");
            evtImage.ImageUrl = eventDetails.imagePath;
            miniLink.Text = eventDetails.generateNewLink;

            hiddenRoleDescriptor.Value = "";
            hiddenDate.Value = "";


            int count = 0;
            foreach (VolSessionDetails sessionDetail in eventDetails.listOfSession)
            {
                addSessionDetail(sessionDetail, count);
                count++;                           
            }
        }


        // this method adds all unjoined sesison into the ui template
        protected void addSessionDetail(VolSessionDetails sessionDetail, int count)
        {
            addPanelOpenings();
            String time = sessionDetail.startTime.ToShortTimeString() + " - " + sessionDetail.endTime.ToShortTimeString();

            sessionDetailsHolder.Controls.Add(new LiteralControl("<h4 class='media-heading margin-v-0-10'>" + sessionDetail.sessionName + "</h4>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class ='fa fa-clock-o fa-fw'></i>" + time + "<br>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class='fa fa-map-marker fa-fw'></i>" + sessionDetail.location + "<br><br>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<p class='margin-none'>" + sessionDetail.sessionDescription + "</p><br>"));


            if(sessionDetail.listOfUnjoinedDays != null)
            {
                // create the row for session dates, event role and role description
                designSessionDetails();

                sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));

                sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
                defineSelectedSessionRoles(count, sessionDetail.listOfEventRoles, sessionDetail.listOfUnjoinedDays);
                sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

                sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
                sessionDetailsHolder.Controls.Add(new LiteralControl("<p id = 'p" + count + "'></p>"));
                sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

                sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-5'>"));
                sessionDetailsHolder.Controls.Add(new LiteralControl("<input type='hidden' style='width:100%' id ='date" + count + "'>"));
                sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

                sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            }
            else
            {
                sessionDetailsHolder.Controls.Add(new LiteralControl("<p>No available event sessions</p>"));
            }

            addPanelClosings();
        }


        protected void addPanelOpenings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel panel-default'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media media-clearfix-xs media-clearfix-sm'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media-body'>"));
        }

        protected void addPanelClosings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
        }


        protected void designSessionDetails()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-3'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Role"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Description"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-5'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Session Day"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));


            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

        }


        protected void defineSelectedSessionRoles(int count, List<SessionRoles> listOfSessionRole, List<SessionDays> listOfUnjoinedDays)
        {
            String selectId = "role" + count;
            sessionDetailsHolder.Controls.Add(new LiteralControl("<select id = '" + selectId + "' class ='btn btn-white dropdown-toggle' onchange = 'roleNameOnChange(" + selectId + ", " + count + ")'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<option value='' disabled selected style='display: none;'>Select a role</option>"));

            foreach (SessionRoles role in listOfSessionRole)
            {
                sessionDetailsHolder.Controls.Add(new LiteralControl("<option value = '" + role.evt_roleId + "'>" + role.roleName + " </option>"));
                hiddenRoleDescriptor.Value += role.roleDesc + ",";

                for (int dayCount = 0; dayCount < listOfUnjoinedDays.Count; dayCount++)
                {
                    if (listOfUnjoinedDays[dayCount].role == role.roleId)
                    {
                        hiddenDate.Value += listOfUnjoinedDays[dayCount].evt_Date.ToString("d MMM yyyy") + ",";
                    }
                }
                hiddenDate.Value += "-";

            }
            sessionDetailsHolder.Controls.Add(new LiteralControl("</select>"));

            hiddenRoleDescriptor.Value += "-";
            hiddenDate.Value += "+";
        }

        protected void facebookSharerHolder()
        {
            PlaceHolderFBlink.Controls.Add((new LiteralControl("<iframe src = 'https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fsgserveportal.azurewebsites.net%2FPublicEventDetail.aspx%3Fe%3D" + Request.QueryString["e"] + "%26o%3D" + Request.QueryString["o"] + "&layout=button&size=small&mobile_iframe=true&appId=1390650524284898&width=58&height=20' width='58' height='20' style='border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true'></iframe>")));
        }
    }
}
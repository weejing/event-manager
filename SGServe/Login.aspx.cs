﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Http;
using System.Json;
using System.Net.Http.Headers;
using SGServe.CommonClass;
using System.Threading.Tasks;
using SGServe.Models.AccountModels;

namespace SGServe
{
    public partial class Login : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        Boolean canLogin;
        GlobalModule gm = new GlobalModule();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmessage.Visible = false;

        }

        protected async void btnLogin_Click(object sender, EventArgs e)
        {

            await request_login();
            if (canLogin == true)
            {
                //check if account is activated. if no, don't allow to login
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.ACTIVATION_CODE != "")  {
                    Session.Remove("Token");
                    lblmessage.Visible = true;
                    lblmessage.Text = "Please activate your account";
                }
                else { 
                // Theres a need to set false as you do not want the session thread to be terminated
                Response.Redirect("Dashboard.aspx", false);
                 }
            }
            else {
                lblmessage.Visible = true;
            }

        }


        private async Task request_login()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();


            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/Authorise/Token");
            // form up the body data
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("UserName", txtLoginEmail.Text));
            keyValues.Add(new KeyValuePair<string, string>("password", txtLoginPassword.Text));
            keyValues.Add(new KeyValuePair<string, string>("LoginType", "Volunteer"));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));

            request.Content = new FormUrlEncodedContent(keyValues);

            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            JsonObject content = (JsonObject)JsonValue.Parse(result);
            ICollection<String> listOfKeys = content.Keys;
            foreach (string key in listOfKeys)
            {
                if (key.Equals("error"))
                {
                    canLogin = false;
                }
                else
                {
                    String access = content["access_token"].ToString();
                    String token = access.Substring(1, access.Length - 2);
                    Session.Add("Token", token);
                    Session.Add("LoginType", "Volunteer");
                    canLogin = true;
                    break;
                }
            }
        }

        protected void btnFB_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://www.facebook.com/v2.7/dialog/oauth/?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&redirect_uri=http://" + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "/FbRedirect.aspx&response_type=code&state=1&scope=email,user_birthday,user_friends"); 
        }



    }
}
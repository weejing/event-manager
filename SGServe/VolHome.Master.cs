﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class VolMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            generateMenu();
        }


        private void generateMenu()
        {
            string leftMenu = "<ul class='nav navbar-nav'>";
            string EndTag = "</ul>";
            //string rightMenu = "<ul class='nav navbar-nav navbar - right'>";
            string rightMenu = "<ul class='nav navbar-nav navbar-right'>";
            if (Session["Token"] == null ||  Session["LoginType"].ToString().Equals("Organiser"))
            {
                leftMenu = leftMenu + "<li class='dropdown'> <a href='EventList.aspx'>Events</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'> <a href='Organization.aspx'>Organizations</a> </li>";

                //right menu
                rightMenu = rightMenu + "<li class='dropdown'><a href ='Login.aspx'><i class='fa fa-fw fa-lock'></i>Login</a></li>";
                rightMenu = rightMenu + "<li class='dropdown'><a href='Signup.aspx'><i class='fa fa-fw fa-plus'></i>Sign Up </a></li>";

            }
            else
            {
                leftMenu = leftMenu + "<li class='dropdown'> <a href='Dashboard.aspx'>Dashboard</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'> <a href='EventList.aspx'>Events</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'> <a href='Organization.aspx'>Organizations</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>Community<span class='caret'></span></a>";
                leftMenu = leftMenu + "<ul class='dropdown-menu'>";
                leftMenu = leftMenu + " <li><a href='Newsfeed.aspx'>Newsfeed</a></li>";
                leftMenu = leftMenu + "<li><a href='SearchUsers.aspx'>Search User</a></li>";
                leftMenu = leftMenu + " </ul></li>";

                //right menu
                rightMenu = rightMenu + "<li class='dropdown'><a href ='Logout.aspx'><i class='fa fa-fw fa-sign-out'></i>Signout</a></li>";
            }
            leftMenu = leftMenu + EndTag;
            rightMenu = rightMenu + EndTag;
            lblleftMenu.Text = leftMenu;
            lblRightmenu.Text = rightMenu;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SGServe
{
    public partial class OrgRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmessage.Visible = false;
        }

        protected void btnReg_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
           // http://vmsserver.azurewebsites.net/
            client.BaseAddress = new Uri("http://vmsserver.azurewebsites.net/");

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgAccount/register");

        }
    }
}
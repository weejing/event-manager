﻿using FastMember;
using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class EventDetail : System.Web.UI.Page
    {

        private List<String> controlList
        {
            get
            {
                if (ViewState["controls"] == null)
                {
                    ViewState["controls"] = new List<String>();
                }

                return (List<String>)ViewState["controls"];
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Token"] != null)
                {
                    if (Session["LoginType"].ToString().Equals("Organiser"))
                    {
                        String redirect = "PublicEventDetail.aspx?e=" + Request.QueryString["e"] + "&o=" + Request.QueryString["o"];
                        Response.Redirect(redirect, false);
                    }
                    else {
                        displayEventDetails();
                    }
              }
                else
                {
                    String redirect = "PublicEventDetail.aspx?e=" + Request.QueryString["e"] + "&o=" + Request.QueryString["o"];
                    Response.Redirect(redirect, false);
                }                                                                      
            }
            else
            {
                foreach (string button in controlList.ToList())
                {
                    Button register = defineRegisterButton(button, 0, 0);
                    sessionDetailsHolder.Controls.Add(register);
                }
            }
            facebookSharerHolder();

        }


        protected async void displayEventDetails()
        {
            String token = Session["Token"].ToString();
           
            String eventId = Request.QueryString["e"];
            String connectionString = Request.QueryString["o"];


            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/retrieveSessionDetailsForVol?eventId=" + eventId + "&connectionString=" + connectionString);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("result: " + result);
            VolEventDetail eventDetails = JsonConvert.DeserializeObject<VolEventDetail>(result);

            //display event cause
            DataTable tableCause = new DataTable();
            using (var reader = ObjectReader.Create(eventDetails.listofCauseName))
            {
                tableCause.Load(reader);
            }
            dlcause.DataSource = tableCause;
            dlcause.DataBind();

            evtName.Text = eventDetails.eventName;
            lblOrg.Text = "Organized by " + connectionString;
            desc.Text = eventDetails.description;
            Date.Text = eventDetails.startDate.ToString("d MMM yyyy") + " - " + eventDetails.endDate.ToString("d MMM yyyy");
            miniLink.Text = eventDetails.generateNewLink;
            evtImage.ImageUrl = eventDetails.imagePath;

            hiddenRoleDescriptor.Value = "";
            hiddenDate.Value = "";


            int count = 0;
            int numOfSessionJoined = 0;
            Boolean joinAny = false;
            foreach (VolSessionDetails sessionDetail in eventDetails.listOfSession)
            {
                if (sessionDetail.listOfJoinedDays != null)
                {
                    addJoinedSessionDetail(sessionDetail);
                    numOfSessionJoined = sessionDetail.listOfJoinedDays.Count;
                    joinAny = true;
                }

                addUnjoinedSessionDetail(sessionDetail, count, numOfSessionJoined, eventId, connectionString);
                count++;                           
            }

            if (joinAny == false)
                mySessionsPlaceHolder.Controls.Add(new LiteralControl("You have not joined any event sessions"));
        }

        // this method adds all joined session details
        protected void addJoinedSessionDetail(VolSessionDetails sessionDetail)
        {
            addJoinedPanelOpenings();

            String time = sessionDetail.startTime.ToShortTimeString() + " - " + sessionDetail.endTime.ToShortTimeString();

            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<h4 class='media-heading margin-v-0-10'>" + sessionDetail.sessionName + "</h4>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<i class ='fa fa-clock-o fa-fw'></i>" + time + "<br>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<i class='fa fa-map-marker fa-fw'></i>" + sessionDetail.location + "<br><br>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<p class='margin-none'>" + sessionDetail.sessionDescription + "</p><br>"));

            defineTable(sessionDetail.listOfJoinedDays);

           
            addJoinedPanelClosings();
        }


        // this method adds all unjoined sesison into the ui template
        protected void addUnjoinedSessionDetail(VolSessionDetails sessionDetail, int count , int numOfSessionJoined, string eventId, string connectionString)
        {
            addPanelOpenings();
            String time = sessionDetail.startTime.ToShortTimeString() + " - " + sessionDetail.endTime.ToShortTimeString();

            sessionDetailsHolder.Controls.Add(new LiteralControl("<h4 class='media-heading margin-v-0-10'>" + sessionDetail.sessionName + "</h4>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class ='fa fa-clock-o fa-fw'></i>" + time + "<br>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<i class='fa fa-map-marker fa-fw'></i>" + sessionDetail.location + "<br><br>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<p class='margin-none'>" + sessionDetail.sessionDescription + "</p><br> <hr>"));


            if (sessionDetail.listOfUnjoinedDays == null)
            {
                sessionDetailsHolder.Controls.Add(new LiteralControl("No available event sessions"));
                addPanelClosings();
                return;
            }
                

            // create the row for session dates, event role and role description
            designSessionDetails();

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-2'>"));
            defineSelectedSessionRoles(count, sessionDetail.listOfEventRoles, sessionDetail.listOfUnjoinedDays);
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<p id = 'p" + count + "'></p>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-5'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<input type='hidden' style='width:100%' id ='date" + count + "'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

           
            int numOfCompulsory;
            if (numOfSessionJoined >= sessionDetail.compulsory)            
                numOfCompulsory = 1;           
            else            
                 numOfCompulsory = sessionDetail.compulsory - numOfSessionJoined;

            String id = eventId + "," + sessionDetail.sessionId + "," + connectionString;
         
            Button register = defineRegisterButton(id, numOfCompulsory ,count);
          
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-1'>"));
            sessionDetailsHolder.Controls.Add(register);
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            addPanelClosings();
        }


        protected void addPanelOpenings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel panel-default'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media media-clearfix-xs media-clearfix-sm'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class ='media-body'>"));
        }

        protected void addPanelClosings()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));
        }


        protected void addJoinedPanelOpenings()
        {
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<div class ='panel panel-default'>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<div class ='panel-body'>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<div class ='media media-clearfix-xs media-clearfix-sm'>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<div class ='media-body'>"));

        }


        protected void addJoinedPanelClosings()
        {
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }
       

        protected void defineTable(List<SessionDays> listOfJoinedDays)
        {
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<div class = 'table-responsive'>"));

            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<table class = 'table'>"));

            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<thead><tr> <th>Date</th> <th>Role</th></tr></thead>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("<tbody>"));

            foreach(SessionDays day in listOfJoinedDays )
            {
                mySessionsPlaceHolder.Controls.Add(new LiteralControl("<tr> <td>"+ day.evt_Date.ToString("d MMM yyyy") + "</td> <td>" + day.roleName +"</td> </tr>"));
            }

            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</tbody>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</table>"));

            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            mySessionsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }

        protected void designSessionDetails()
        {
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'row'>"));
            
            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-2'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Role"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-4'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Description"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

            sessionDetailsHolder.Controls.Add(new LiteralControl("<div class = 'col-md-5'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("Session Day"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));


            sessionDetailsHolder.Controls.Add(new LiteralControl("</div>"));

        }


        protected void defineSelectedSessionRoles(int count, List<SessionRoles> listOfSessionRole, List<SessionDays> listOfUnjoinedDays)
        {
            String selectId = "role" + count;
            sessionDetailsHolder.Controls.Add(new LiteralControl("<select id = '" + selectId + "' class ='btn btn-white dropdown-toggle' onchange = 'roleNameOnChange(" + selectId + ", " + count + ")'>"));
            sessionDetailsHolder.Controls.Add(new LiteralControl("<option value='' disabled selected style='display: none;'>Select a role</option>"));

            foreach (SessionRoles role in listOfSessionRole)
            {
                sessionDetailsHolder.Controls.Add(new LiteralControl("<option value = '" + role.evt_roleId + "'>" + role.roleName + " </option>"));
                hiddenRoleDescriptor.Value += role.roleDesc + ",";

                for(int dayCount = 0; dayCount < listOfUnjoinedDays.Count; dayCount++)
                {
                    if(listOfUnjoinedDays[dayCount].role == role.roleId)
                    {
                        hiddenDate.Value += listOfUnjoinedDays[dayCount].evt_Date.ToString("d MMM yyyy") + ",";                        
                    }                                       
                }
                hiddenDate.Value += "-";

            }
            sessionDetailsHolder.Controls.Add(new LiteralControl("</select>"));

            hiddenRoleDescriptor.Value += "-";
            hiddenDate.Value += "+";
        }



        protected Button defineRegisterButton(String id, int numOfCompulsory, int count)
        {
            Button button = new Button();
            button.CssClass = "btn btn-danger btn-sm";
            button.Click += new EventHandler(registerEventSession);
            button.ID = id;
            button.Text = "Sign up";
            button.OnClientClick = "setSessionDatesPlaceHolder('" + count + "', '" + numOfCompulsory + "')";

            controlList.Add(id);

            return button;
        }

        protected async void registerEventSession(Object sender, EventArgs e)
        {
            Button button = sender as Button;
            SignUpEvent signUp = new SignUpEvent();

            String [] idArray = button.ID.Split(',');

            signUp.eventId = idArray[0];
            signUp.sessionId = idArray[1];
            signUp.connectionString = idArray[2];


            signUp.eventdateList= datesValuePlaceHolder.Value;
            signUp.evt_roleId = roleValuePlaceHolder.Value; 
            
            
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String token = Session["Token"].ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            HttpResponseMessage response = await client.PostAsJsonAsync("/api/EventMgmt/registerEvent", signUp);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('You have successfully signed up!');window.location ='EventDetail.aspx?e=" + idArray[0]+ "&o="+ idArray[2] + "';", true);
            //String redirect = "EventDetail.aspx?e=" + idArray[0] + "&o=" + idArray[2];
            //Response.Redirect(redirect, false);
        }

        protected void facebookSharerHolder()
        {
            PlaceHolderFBlink.Controls.Add((new LiteralControl("<iframe src = 'https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fsgserveportal.azurewebsites.net%2FEventDetail.aspx%3Fe%3D" + Request.QueryString["e"] + "%26o%3D" + Request.QueryString["o"] + "&layout=button&size=small&mobile_iframe=true&appId=1390650524284898&width=58&height=20' width='58' height='20' style='border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true'></iframe>")));
        }

    }
}
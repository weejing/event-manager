﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Net;
using System.IO;

namespace SGServe
{
    public partial class Newsfeed : System.Web.UI.Page
    {
        Boolean facebookLogin;
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //await retrieveUserDetails();
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    await generateUserDetails();
                    await generateContent();
                    generateRecommendation();
                }
            }
        }

        protected async Task generateContent()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage friendsActivitiesReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserFriendsActivities?userID=" + currentUserID);
                HttpResponseMessage friendsActivitiesResp = await client.SendAsync(friendsActivitiesReq);
                var friendsActivities = await friendsActivitiesResp.Content.ReadAsStringAsync();

                List<UserNewsfeedModel> friendsActivitiesList = JsonConvert.DeserializeObject<List<UserNewsfeedModel>>(friendsActivities);

                foreach (UserNewsfeedModel feed in friendsActivitiesList)
                {
                    DateTime dtDisplay = Convert.ToDateTime(feed.ACTIVITY_DATETIME);
                    feed.ACTIVITY_DATETIME = dtDisplay.ToString("d MMM yyyy h:mm tt");
                }

                if (friendsActivitiesList.Count() <= 0)
                {
                    UserNewsfeedModel welcomeFeed = new UserNewsfeedModel();

                    welcomeFeed.MESSAGE_DISPLAY = "Welcome to SGServe. You will receive newsfeed from your friends activities here. Add some friends to kick start your journey on SGServe.";
                    welcomeFeed.USR_IMAGE = "images/bidItem/O4YIM70147834434234155.jpg";
                    friendsActivitiesList.Add(welcomeFeed);
                }

                DataList3.DataSource = friendsActivitiesList;
                DataList3.DataBind();

                /*
                HttpRequestMessage recommendReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListOfRecommendedUsersByNetwork?userID=" + currentUserID);
                HttpResponseMessage recommendResp = await client.SendAsync(recommendReq);
                var recommend = await recommendResp.Content.ReadAsStringAsync();

                List<UserPortfolioRecommendationModel> recommendList = JsonConvert.DeserializeObject<List<UserPortfolioRecommendationModel>>(recommend);

                if (recommendList.Count <= 0 || recommendList.Equals(null))
                {
                    PanelR.Visible = false;
                }
                else
                {
                    PanelR.Visible = true;
                }

                DataListRecommend.DataSource = recommendList;
                DataListRecommend.DataBind();
                */

                HttpRequestMessage topVIAReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getTopUsersVIA?size=2"); 
                HttpResponseMessage topVIAResp = await client.SendAsync(topVIAReq);
                var topVIA = await topVIAResp.Content.ReadAsStringAsync();

                List<UserPortfolioModel> topVIAList = JsonConvert.DeserializeObject<List<UserPortfolioModel>>(topVIA);

                DataListTopVIA.DataSource = topVIAList;
                DataListTopVIA.DataBind();

                if (topVIAList.Count() <= 0)
                {
                    lblNoneVIA.Visible = true;
                    HyperLinkTopVIA.Visible = false;
                }
                else
                {
                    lblNoneVIA.Visible = false;
                    HyperLinkTopVIA.Visible = true;
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }


        protected async void generateRecommendation()
        {
            try
            {
                List<UserPortfolioRecommendationModel> recommendList = new List<UserPortfolioRecommendationModel>();
                int countRecommend = 0;

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage recommendReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListOfRecommendedUsersByNetwork?userID=" + currentUserID);
                HttpResponseMessage recommendResp = await client.SendAsync(recommendReq);
                var recommend = await recommendResp.Content.ReadAsStringAsync();

                List<UserPortfolioRecommendationModel> recommendListNetwork = JsonConvert.DeserializeObject<List<UserPortfolioRecommendationModel>>(recommend);

                foreach (UserPortfolioRecommendationModel recommendedUser in recommendListNetwork)
                {
                    if (countRecommend > 2)
                    {
                        break;
                    }
                    else
                    {
                        countRecommend++;
                        recommendList.Add(recommendedUser);
                    }
                }

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserFacebookID?userID=" + currentUserID);
                HttpResponseMessage response = await client.SendAsync(request);
                var reply = await response.Content.ReadAsStringAsync();

                String facebookID = JsonConvert.DeserializeObject<String>(reply);

                if (!(facebookID.Trim().Equals("")))
                {
                    HttpRequestMessage fbfbRecommendReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getListOfRecommendedUsersByFacebook?userID=" + currentUserID + "&facebookID=" + facebookID + "&accessToken=" + Session["fbAccessToken"].ToString());
                    HttpResponseMessage fbRecommendResp = await client.SendAsync(fbfbRecommendReq);
                    var fbRecommend = await fbRecommendResp.Content.ReadAsStringAsync();

                    List<UserPortfolioRecommendationModel> recommendListFacebook = JsonConvert.DeserializeObject<List<UserPortfolioRecommendationModel>>(fbRecommend);

                    foreach (UserPortfolioRecommendationModel recommendedUser in recommendListFacebook)
                    {
                        if (countRecommend > 4)
                        {
                            break;
                        }
                        else
                        {
                            countRecommend++;
                            recommendList.Add(recommendedUser);
                        }
                    }
                }

                DataListRecommend.DataSource = recommendList;
                DataListRecommend.DataBind();

                if (recommendList.Count <= 0 || recommendList.Equals(null))
                {
                    PanelR.Visible = false;
                }
                else
                {
                    PanelR.Visible = true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected async Task generateUserDetails()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserDetails?userID=" + currentUserID);
                HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
                String userDetails = await userDetailsResp.Content.ReadAsStringAsync();

                UserPortfolioModel upm = JsonConvert.DeserializeObject<UserPortfolioModel>(userDetails);

                lblFullName.Text = volunteer.FULL_NAME;
                ProfilePicImg.ImageUrl = upm.IMAGE;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        /*
        private async Task getFacebookUserData(String code)
        {
            // Exchange the code for an access token
            Uri targetUri = new Uri("https://graph.facebook.com/oauth/access_token?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&client_secret=" + ConfigurationManager.AppSettings["FacebookAppSecret"] + "&redirect_uri=http://" + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "/Newsfeed.aspx&code=" + code);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);

            System.IO.StreamReader str = new System.IO.StreamReader(request.GetResponse().GetResponseStream());
            string token = str.ReadToEnd().ToString().Replace("access_token=", "");

            // Split the access token and expiration from the single string
            string[] combined = token.Split('&');
            string accessToken = combined[0];

            // Request the Facebook user information
            Uri targetUserUri = new Uri("https://graph.facebook.com/me?fields=first_name,last_name,friends&access_token=" + accessToken);
            System.Diagnostics.Debug.WriteLine("token: " + accessToken);

            HttpWebRequest user = (HttpWebRequest)HttpWebRequest.Create(targetUserUri);
            // Read the returned JSON object response
            String jsonResponse = string.Empty;
            try
            {
                StreamReader userInfo = new StreamReader(user.GetResponse().GetResponseStream());
                jsonResponse = userInfo.ReadToEnd();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception:" + e.ToString());
            }

            // Deserialize and convert the JSON object to the Facebook.User object type
            JavaScriptSerializer sr = new JavaScriptSerializer();
            string jsondata = jsonResponse;
            FbUser userDetails = sr.Deserialize<FbUser>(jsondata);
        }
        */

        protected void btnPortfolio_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserPortfolio.aspx?id=" + currentUserID);
        }

        protected void btnFriend_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserFriends.aspx?id=" + currentUserID);
        }

    }
}
﻿using SGServe.CommonClass;
using SGServe.SaaS.Models;
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGServe
{
    public partial class NewSaaS_Step1 : System.Web.UI.Page
    {
        List<String> items = new List<String>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //insert new organisation into database (Step 1)
        protected async void btnReg_Click(object sender, EventArgs e)
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

               
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgList");
                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                //System.Diagnostics.Debug.WriteLine("reply: " + reply);

                JsonArray array = (JsonArray)JsonValue.Parse(reply);

                foreach (var arrayResult in array)
                {
                    OrganizationDetails newItem = new OrganizationDetails();
                    newItem.CONNECTION_STRING = arrayResult["CONNECTION_STRING"].ToString();

                    items.Add(newItem.CONNECTION_STRING.Substring(1, newItem.CONNECTION_STRING.Length - 2));
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Global.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: registerURL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
           
            if (items.Contains(txtOrgShort.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Short form duplicate!');", true);
            }
            else if (CheckURLValid(txtLink.Text)==false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Insert a real website link!');", true);
            }
            else
            {
                try
                {
                    string baseurl = Request.Url.GetLeftPart(UriPartial.Authority) + "/";
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(baseurl);
                    // form up the headers data
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/insertOrganization");
                    // form up the body data
                    var keyValues = new List<KeyValuePair<string, string>>();
                    keyValues.Add(new KeyValuePair<string, string>("ORG_ID", "-"));
                    keyValues.Add(new KeyValuePair<string, string>("ORG_NAME", txtOrgName.Text));
                    keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", txtOrgShort.Text.ToUpper()));
                    keyValues.Add(new KeyValuePair<string, string>("EMAIL", txtEmail.Text));
                    keyValues.Add(new KeyValuePair<string, string>("LINK", txtLink.Text));
                    keyValues.Add(new KeyValuePair<string, string>("UEN", txtUEN.Text));
                    keyValues.Add(new KeyValuePair<string, string>("ADDRESS", txtAddress.Text));
                    keyValues.Add(new KeyValuePair<string, string>("DESCRIPTION", txtDesc.Text));
                    keyValues.Add(new KeyValuePair<string, string>("ORG_TYPE", ddType.Text));

                    request.Content = new FormUrlEncodedContent(keyValues);
                    // client.SendAsync(request);

                    HttpResponseMessage response = await client.SendAsync(request);

                    String reply = await response.Content.ReadAsStringAsync();

                    Response.Cookies["ORG_ID"].Value = reply.Substring(1, 36);
                    Response.Cookies["ORG_ID"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["CONNECTION_STRING"].Value = txtOrgShort.Text;
                    Response.Cookies["CONNECTION_STRING"].Expires = DateTime.Now.AddMinutes(5);

                    
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), reply, "alert('Record Inserted Successfully')", true);

                    Response.Redirect("NewSaaS_Step2.aspx", false);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("---Issue Start---");
                    System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                    System.Diagnostics.Debug.WriteLine("---Method: Insert New Organization Button---");
                    System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                    System.Diagnostics.Debug.WriteLine("---End---");
                }

                ////shihui
                //RouteTable.Routes.MapPageRoute("Home" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/home", "~/SaaS/home.aspx");
                //RouteTable.Routes.MapPageRoute("Login" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/login", "~/SaaS/Login.aspx");
                //RouteTable.Routes.MapPageRoute("Logout" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/logout", "~/SaaS/Logout.aspx");
                //RouteTable.Routes.MapPageRoute("Employee Login" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/ologin", "~/SaaS/oLogin.aspx");
                //RouteTable.Routes.MapPageRoute("Change Password" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/changepassword", "~/SaaS/changepassword.aspx");
                //RouteTable.Routes.MapPageRoute("Set Password" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/setpassword", "~/SaaS/setpassword.aspx");
                //RouteTable.Routes.MapPageRoute("Reset Password" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/resetpassword", "~/SaaS/resetpassword.aspx");
                //RouteTable.Routes.MapPageRoute("Profile" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/profile", "~/SaaS/profile.aspx");
                //RouteTable.Routes.MapPageRoute("Redirect" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/redirectpage", "~/SaaS/redirectpage.aspx");
                //RouteTable.Routes.MapPageRoute("Manage Account" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/manageaccount", "~/SaaS/manageaccount.aspx");
                //RouteTable.Routes.MapPageRoute("Create Account" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/createaccount", "~/SaaS/createaccount.aspx");

                ////debbie
                //RouteTable.Routes.MapPageRoute("EditInventoryItems" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/EditInventory", "~/SaaS/EditInventoryItems.aspx");
                //RouteTable.Routes.MapPageRoute("ManageInventory" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/ManageInventory", "~/SaaS/ManageInventory.aspx");
                ////String connect2 = "Console_VMS";
                //RouteTable.Routes.MapPageRoute("TenderStatusList" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/TenderStatusList", "~/SaaS/TenderStatusList.aspx");

                ////junhau
                //RouteTable.Routes.MapPageRoute("UpdateOrgInfo" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/UpdateOrgInfo", "~/SaaS/UpdateOrgInfo.aspx");
                //RouteTable.Routes.MapPageRoute("CreateTender" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/CreateTender", "~/SaaS/CreateTender.aspx");
                //RouteTable.Routes.MapPageRoute("TenderingMgmt" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/TenderList", "~/SaaS/TenderingMgmt.aspx");
                //RouteTable.Routes.MapPageRoute("ViewTender" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/ViewTender", "~/SaaS/ViewTender.aspx");
                //RouteTable.Routes.MapPageRoute("ViewBids" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/ViewBids", "~/SaaS/ViewBids.aspx");

                ////weejing
                //RouteTable.Routes.MapPageRoute("CreateEvent" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/CreateEvent", "~/SaaS/CreateEvent.aspx");
                //RouteTable.Routes.MapPageRoute("CreateSession" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/CreateEventSession", "~/SaaS/CreateEventSession.aspx");
                //RouteTable.Routes.MapPageRoute("ViewEventList" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/ViewEventList", "~/SaaS/ViewEventList.aspx");
                //RouteTable.Routes.MapPageRoute("ViewEventDetails" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/ViewEventDetails", "~/SaaS/ViewEventDetails.aspx");

                ////kaifu
                //RouteTable.Routes.MapPageRoute("UsersFollowed" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/UsersFollowed", "~/SaaS/UsersFollowed.aspx");
                //RouteTable.Routes.MapPageRoute("Portfolio" + txtOrgShort.Text.ToUpper(), txtOrgShort.Text.ToUpper() + "/Portfolio", "~/SaaS/Portfolio.aspx");

            }
        }

        public static bool CheckURLValid(string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using SGServe.Models.UserBiddingModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class BidItemsList : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //await retrieveUserDetails();
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateItemsList();
                }
            }
        }

        /*
        private async Task retrieveUserDetails()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            String jwtToken = Session["Token"] as String;
            if (jwtToken != null)
            {
                System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                // add the jwt token into the authorization header for server validation
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/Account/retrieveUserDetails");
                HttpResponseMessage Resp = await client.SendAsync(request);
                String reply = await Resp.Content.ReadAsStringAsync();
                volunteer = GlobalModule.getUserDetails(reply);
            }
        }
        */

        protected async void generateItemsList()
        {
            try
            {

                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                
                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getAllCurrentBidItems");
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //get reply to bind to gridview
                    List<BidItem> items = JsonConvert.DeserializeObject<List<BidItem>>(reply);

                    foreach (BidItem item in items)
                    {
                        DateTime dtDisplay = Convert.ToDateTime(item.START_DATE);
                        DateTime dtDisplay2 = Convert.ToDateTime(item.END_DATE);
                        item.START_DATE = dtDisplay.ToString("d MMM yyyy h:mm tt");
                        item.END_DATE = dtDisplay2.ToString("d MMM yyyy h:mm tt");
                    }

                    if (items != null)
                    {
                        ItemsGridView.DataSource = items;
                        ItemsGridView.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }

        /*
        private async void getItemCategory()
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/SponsorshipMgmt/getItemCategoryListForSearch");
            HttpResponseMessage Resp = await client.SendAsync(request);
            String reply = await Resp.Content.ReadAsStringAsync();

            List<SponsorItemCategory> records = JsonConvert.DeserializeObject<List<SponsorItemCategory>>(reply);

            if (records != null)
            {
                CategoryDDL.DataSource = records;
                CategoryDDL.DataTextField = "ITEM_CATEGORY_NAME";
                CategoryDDL.DataValueField = "SPR_ITEM_CATEGORY_ID";
                CategoryDDL.DataBind();
            }
        }
        */

        protected void btnViewBid_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            String bidItemID = ItemsGridView.DataKeys[row.RowIndex].Value.ToString();

            Response.Redirect("BidItemInfo.aspx?bidItemID=" + bidItemID);

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

        }
    }
}
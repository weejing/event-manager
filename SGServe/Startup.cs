﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(SGServe.Startup))]
namespace SGServe
{
    public class Startup
    {
        //start the signalr app. To create this class, right click on project, new item and select Owin startup class
        //under the general tab
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
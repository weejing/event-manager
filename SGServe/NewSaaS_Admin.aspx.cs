﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayPal.PayPalAPIInterfaceService.Model;
using PayPal.PayPalAPIInterfaceService;
using SGServe.Models.TransactionModels;
using System.Web.Script.Serialization;
using System.Json;
using SGServe.Models.OrgMgmtModels;
using SGServe.SaaS.Models;
using System.Web.Routing;
using System.Net.Mail;

namespace SGServe
{
    public partial class NewSaaS_Admin : System.Web.UI.Page
    {
        public static OrgDetails orgDetails = new OrgDetails();
        public static OrgPayment newTransaction = new OrgPayment();
        public static Boolean paymentMade = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            loadAllCookies();
        }

        protected void loadAllCookies()
        {
            orgDetails.CONNECTION_STRING = Request.Cookies["CONNECTION_STRING"].Value;
            orgDetails.ORG_NAME = Request.Cookies["ORG_NAME"].Value;
            orgDetails.EMAIL = Request.Cookies["EMAIL"].Value;
            orgDetails.LINK = Request.Cookies["LINK"].Value;
            orgDetails.UEN = Request.Cookies["UEN"].Value;
            orgDetails.ADDRESS = Request.Cookies["ADDRESS"].Value;
            orgDetails.DESCRIPTION = Request.Cookies["DESCRIPTION"].Value;
            orgDetails.ORG_TYPE = Request.Cookies["ORG_TYPE"].Value;


            Request.Cookies["CONNECTION_STRING"].Expires = DateTime.Now.AddDays(-1);
            Request.Cookies["ORG_NAME"].Expires = DateTime.Now.AddDays(-1);
            Request.Cookies["EMAIL"].Expires = DateTime.Now.AddDays(-1);
            Request.Cookies["LINK"].Expires = DateTime.Now.AddDays(-1);
            Request.Cookies["UEN"].Expires = DateTime.Now.AddDays(-1);
            Request.Cookies["ADDRESS"].Expires = DateTime.Now.AddDays(-1);
            Request.Cookies["DESCRIPTION"].Expires = DateTime.Now.AddDays(-1);
            Request.Cookies["ORG_TYPE"].Expires = DateTime.Now.AddDays(-1);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected async void btnSave_Click(object sender, EventArgs e)
        {
            await createNewOrg();
            
            //string message = await insertMasterAdmin();
            //ThreadPool.QueueUserWorkItem(new WaitCallback(a => insertOrgInstance()), null);

            // var org = await insertOrgInstance();
            // if (org.ToString() == "\"Success\"")
            // {
            string connect = orgDetails.CONNECTION_STRING;
            RouteTable.Routes.MapPageRoute("SaaSHome" + connect, connect, "~/SaaS/Home.aspx");
            //shihui
            RouteTable.Routes.MapPageRoute("Home" + connect, connect + "/home", "~/SaaS/home.aspx");
            RouteTable.Routes.MapPageRoute("InternalHome" + connect, connect + "/InternalHome", "~/SaaS/InternalHome.aspx");
            RouteTable.Routes.MapPageRoute("Login" + connect, connect + "/login", "~/SaaS/Login.aspx");
            RouteTable.Routes.MapPageRoute("Logout" + connect, connect + "/logout", "~/SaaS/Logout.aspx");
            RouteTable.Routes.MapPageRoute("Employee Login" + connect, connect + "/ologin", "~/SaaS/oLogin.aspx");
            RouteTable.Routes.MapPageRoute("Change Password" + connect, connect + "/changepassword", "~/SaaS/changepassword.aspx");
            RouteTable.Routes.MapPageRoute("Set Password" + connect, connect + "/setpassword", "~/SaaS/setpassword.aspx");
            RouteTable.Routes.MapPageRoute("Reset Password" + connect, connect + "/resetpassword", "~/SaaS/resetpassword.aspx");
            RouteTable.Routes.MapPageRoute("Profile" + connect, connect + "/profile", "~/SaaS/profile.aspx");
            RouteTable.Routes.MapPageRoute("Redirect" + connect, connect + "/redirectpage", "~/SaaS/redirectpage.aspx");
            RouteTable.Routes.MapPageRoute("Manage Account" + connect, connect + "/manageaccount", "~/SaaS/manageaccount.aspx");
            RouteTable.Routes.MapPageRoute("Create Account" + connect, connect + "/createaccount", "~/SaaS/createaccount.aspx");
            RouteTable.Routes.MapPageRoute("Page List" + connect, connect + "/pagelist", "~/SaaS/pagelist.aspx");
            RouteTable.Routes.MapPageRoute("Role List" + connect, connect + "/rolelist", "~/SaaS/rolelist.aspx");
            RouteTable.Routes.MapPageRoute("Notifications" + connect, connect + "/internalhome", "~/SaaS/internalhome.aspx");
            RouteTable.Routes.MapPageRoute("Event Roles" + connect, connect + "/eventroles", "~/SaaS/eventroles.aspx");
            //debbie
            RouteTable.Routes.MapPageRoute("EditInventoryItems" + connect, connect + "/EditInventoryItems", "~/SaaS/EditInventoryItems.aspx");
            RouteTable.Routes.MapPageRoute("ManageInventory" + connect, connect + "/ManageInventory", "~/SaaS/ManageInventory.aspx");
            RouteTable.Routes.MapPageRoute("TenderStatusList" + connect, connect + "/TenderStatusList", "~/SaaS/TenderStatusList.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventLogisticList" + connect, connect + "/ViewEventLogisticList", "~/SaaS/ViewEventLogisticList.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventLogisticItems" + connect, connect + "/ViewEventLogisticItems", "~/SaaS/ViewEventLogisticItems.aspx");
            RouteTable.Routes.MapPageRoute("ManageExternalVolunteers" + connect, connect + "/ManageExternalVolunteers", "~/SaaS/ManageExternalVolunteers.aspx");
            RouteTable.Routes.MapPageRoute("ViewExternalVolunteer" + connect, connect + "/ViewExternalVolunteer", "~/SaaS/ViewExternalVolunteer.aspx");
            RouteTable.Routes.MapPageRoute("CreateEmailInvites" + connect, connect + "/CreateEmailInvites", "~/SaaS/CreateEmailInvites.aspx");
            RouteTable.Routes.MapPageRoute("CreateVIATemplate" + connect, connect + "/CreateVIATemplate", "~/SaaS/CreateVIATemplate.aspx");
            //junhau
            RouteTable.Routes.MapPageRoute("UpdateOrgInfo" + connect, connect + "/UpdateOrgInfo", "~/SaaS/UpdateOrgInfo.aspx");
            RouteTable.Routes.MapPageRoute("CreateTender" + connect, connect + "/CreateTender", "~/SaaS/CreateTender.aspx");
            RouteTable.Routes.MapPageRoute("CreateTenderItem" + connect, connect + "/CreateTenderItem", "~/SaaS/CreateTenderItem.aspx");
            RouteTable.Routes.MapPageRoute("TenderingMgmt" + connect, connect + "/TenderingMgmt", "~/SaaS/TenderingMgmt.aspx");
            RouteTable.Routes.MapPageRoute("TenderingItemMgmt" + connect, connect + "/TenderingItemMgmt", "~/SaaS/TenderingItemMgmt.aspx");
            RouteTable.Routes.MapPageRoute("ViewTender" + connect, connect + "/ViewTender", "~/SaaS/ViewTender.aspx");
            RouteTable.Routes.MapPageRoute("ViewBids" + connect, connect + "/ViewBids", "~/SaaS/ViewBids.aspx");
            RouteTable.Routes.MapPageRoute("AuditSaaS" + connect, connect + "/AuditMgmtSaaS", "~/SaaS/AuditMgmtSaaS.aspx");
            RouteTable.Routes.MapPageRoute("AuditUsr" + connect, connect + "/AuditMgmtSaaSUsr", "~/SaaS/AuditMgmtSaaSUsr.aspx");

            //weejing
            RouteTable.Routes.MapPageRoute("CreateEvent" + connect, connect + "/CreateEvent", "~/SaaS/CreateEvent.aspx");
            RouteTable.Routes.MapPageRoute("CreateEventSession" + connect, connect + "/CreateEventSession", "~/SaaS/CreateEventSession.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventList" + connect, connect + "/ViewEventList", "~/SaaS/ViewEventList.aspx");
            RouteTable.Routes.MapPageRoute("ViewEventDetails" + connect, connect + "/ViewEventDetails", "~/SaaS/ViewEventDetails.aspx");
            RouteTable.Routes.MapPageRoute("CreateEventLogs" + connect, connect + "/CreateEventLogs", "~/SaaS/CreateEventLogs.aspx");
            RouteTable.Routes.MapPageRoute("CreateFeedBack" + connect, connect + "/CreateFeedBack", "~/SaaS/CreateFeedBack.aspx");
            RouteTable.Routes.MapPageRoute("SessionVol" + connect, connect + "/SessionVol", "~/SaaS/SessionVol.aspx");





            //kai fu
            RouteTable.Routes.MapPageRoute("UsersFollowed" + connect, connect + "/UsersFollowed", "~/SaaS/UsersFollowed.aspx");
            RouteTable.Routes.MapPageRoute("Portfolio" + connect, connect + "/Portfolio", "~/SaaS/Portfolio.aspx");

            //jaslyn
            RouteTable.Routes.MapPageRoute("ManageBill" + connect, connect + "/ManageBill", "~/SaaS/ManageBill.aspx");
            RouteTable.Routes.MapPageRoute("PaymentPlans" + connect, connect + "/PaymentPlans", "~/SaaS/PaymentPlans.aspx");
            RouteTable.Routes.MapPageRoute("PaymentSuccess" + connect, connect + "/PaymentSuccess", "~/SaaS/PaymentSuccess.aspx");
            RouteTable.Routes.MapPageRoute("PaymentError" + connect, connect + "/PaymentError", "~/SaaS/PaymentError.aspx");
            RouteTable.Routes.MapPageRoute("Reporting" + connect, connect + "/Reporting", "~/SaaS/Reporting.aspx");



          
            String status = Session["ManualPayment"].ToString();
            if (status.Equals("On"))
            {
                Session["ManualPayment"] = "Off";
                Response.Redirect("home.aspx",false);
            }
            else {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Account created! Check your email for more details. ');window.location ='Home';", true);
            }
            // }
        }

        protected async Task createNewOrg()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();


                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/insertOrganization");
                // form up the body data
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("ORG_ID", "-"));
                keyValues.Add(new KeyValuePair<string, string>("ORG_NAME", orgDetails.ORG_NAME));
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", orgDetails.CONNECTION_STRING));
                keyValues.Add(new KeyValuePair<string, string>("EMAIL", orgDetails.EMAIL));
                keyValues.Add(new KeyValuePair<string, string>("LINK", orgDetails.LINK));
                keyValues.Add(new KeyValuePair<string, string>("UEN", orgDetails.UEN));
                keyValues.Add(new KeyValuePair<string, string>("ADDRESS", orgDetails.ADDRESS));
                keyValues.Add(new KeyValuePair<string, string>("DESCRIPTION", orgDetails.DESCRIPTION));
                keyValues.Add(new KeyValuePair<string, string>("ORG_TYPE", orgDetails.ORG_TYPE));

                request.Content = new FormUrlEncodedContent(keyValues);
                // client.SendAsync(request);
                // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), reply, "alert('Record Inserted Successfully')", true);

                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                orgDetails.ORG_ID = jss.Deserialize<string>(reply);


                paypalGetExpressCheckoutDetails(paymentMade);

                
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Insert New Organization Button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
        public async void createTransaction()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                // form up the body data
                // form up the headers data

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Transaction/createNewTransaction");
                string url = Request.RawUrl.ToString();
                string[] urlArr = url.Split('/');
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("FIRST_PAYMENT", "1"));
                keyValues.Add(new KeyValuePair<string, string>("AMT", newTransaction.AMT));
                keyValues.Add(new KeyValuePair<string, string>("CRM_ID", newTransaction.CRM_ID));
                keyValues.Add(new KeyValuePair<string, string>("FREE_TRIAL", newTransaction.FREE_TRIAL));
                keyValues.Add(new KeyValuePair<string, string>("ORG_ID", orgDetails.ORG_ID));
                keyValues.Add(new KeyValuePair<string, string>("PAYMENT_TIME", newTransaction.PAYMENT_TIME));
                keyValues.Add(new KeyValuePair<string, string>("PAYMENT_TYPE", newTransaction.PAYMENT_TYPE));
                keyValues.Add(new KeyValuePair<string, string>("PAYPAL_FEE", newTransaction.PAYPAL_FEE));
                keyValues.Add(new KeyValuePair<string, string>("PAYPAL_ID", newTransaction.PAYPAL_ID));
                keyValues.Add(new KeyValuePair<string, string>("PER", newTransaction.PER));
                keyValues.Add(new KeyValuePair<string, string>("PLAN_AMOUNT", newTransaction.PLAN_AMOUNT));
                keyValues.Add(new KeyValuePair<string, string>("PLAN_TYPE_ID", newTransaction.PLAN_TYPE_ID));
                keyValues.Add(new KeyValuePair<string, string>("PROFILE_ID", newTransaction.PROFILE_ID));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                OrgPayment plan_list = jss.Deserialize<OrgPayment>(reply);

                JsonObject array = (JsonObject)JsonValue.Parse(reply);
                //TextBox1.Text = array["ORG_PAYMENT_ID"].ToString().Substring(1, array["ORG_PAYMENT_ID"].ToString().Length - 2);
                //TextBox2.Text = array["AMT"].ToString().Substring(1, array["AMT"].ToString().Length - 2);
                //TextBox3.Text = array["PLAN_NAME"].ToString().Substring(1, array["PLAN_NAME"].ToString().Length - 2);
                paymentMade = false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Response.Redirect("PaymentError", false);
            }
        }
        public void insertOrgInstance()
        {
            String reply = "";
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/insertOrgInstance");
                // form up the body data
                var keyValues = new List<KeyValuePair<string, string>>();

                if (Request.Cookies["CONNECTION_STRING"] == null)
                    System.Diagnostics.Debug.WriteLine("cookies is null");

                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", Request.Cookies["CONNECTION_STRING"].Value.ToString()));
                keyValues.Add(new KeyValuePair<string, string>("NAME", txtFullName.Text));
                keyValues.Add(new KeyValuePair<string, string>("EMAIL", txtEmail.Text));
                keyValues.Add(new KeyValuePair<string, string>("PASSWORD", txtPassword.Text));
                keyValues.Add(new KeyValuePair<string, string>("USR_ID", Request.Url.Scheme+"://"+Request.Url.Authority+Request.ApplicationPath.TrimEnd('/')));


                Request.Cookies["CONNECTION_STRING"].Expires = DateTime.Now.AddDays(-1);

                request.Content = new FormUrlEncodedContent(keyValues);
                client.SendAsync(request);

                //HttpResponseMessage response = await client.SendAsync(request);
                //reply = await response.Content.ReadAsStringAsync();
                //Label1.Text = reply;
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), reply, "alert('Record Inserted Successfully')", true);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertOrgInstance ---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
        public async Task<string> insertMasterAdmin()
        {
            String reply = "";
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();
                // form up the headers data
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/insertMasterAdmin");
                // form up the body data
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("NAME", txtFullName.Text));
                keyValues.Add(new KeyValuePair<string, string>("EMAIL", txtEmail.Text));
                keyValues.Add(new KeyValuePair<string, string>("CONNECTION_STRING", Request.Cookies["CONNECTION_STRING"].Value.ToString()));
                keyValues.Add(new KeyValuePair<string, string>("PASSWORD", txtPassword.Text));

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                reply = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Insert New Org User---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return reply;
        }


        //paypal methods
        //methods are loaded sequentially according to paypal developer page and also retrieve neccessary details after each paypal call
        //refer to this page https://devtools-paypal.com/guide/expresscheckout/dotnet?success=true&token=EC-4BL359488S159110S&PayerID=3FLKVMYQCSHVW
        protected void paypalGetExpressCheckoutDetails(Boolean paymentMade)
        {
            try
            {
                if (paymentMade)
                {
                    return;
                }

                string token = Request.Cookies["token"].Value;
                string payerID = Request.Cookies["PayerID"].Value;
                Request.Cookies["token"].Expires = DateTime.Now.AddDays(-1);
                Request.Cookies["PayerID"].Expires = DateTime.Now.AddDays(-1);

                GetExpressCheckoutDetailsRequestType get = new GetExpressCheckoutDetailsRequestType();
                get.Token = token;
                get.Version = "65.1";

                GetExpressCheckoutDetailsRequestType request = new GetExpressCheckoutDetailsRequestType();
                request.Version = "104.0";
                request.Token = get.Token;
                GetExpressCheckoutDetailsReq wrapper = new GetExpressCheckoutDetailsReq();
                wrapper.GetExpressCheckoutDetailsRequest = request;
                Dictionary<string, string> sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                sdkConfig.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                sdkConfig.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(sdkConfig);
                GetExpressCheckoutDetailsResponseType ecResponse = service.GetExpressCheckoutDetails(wrapper);

                string status = ecResponse.Ack.ToString();
                GetExpressCheckoutDetailsResponseDetailsType paypal_info = ecResponse.GetExpressCheckoutDetailsResponseDetails;

                if (status.Equals("SUCCESS"))
                {
                    //get payer information
                    string billing_info = paypal_info.BillingAgreementAcceptedStatus.Value.ToString();
                    getCustomerDetails(paypal_info);//see if got payment details type, PaymentInfoType
                    paypalDoExpressCheckoutPayment(token, payerID, billing_info);
                }
                else
                {
                    var query = HttpUtility.ParseQueryString(string.Empty);
                    var error_obj = ecResponse.GetExpressCheckoutDetailsResponseDetails.PaymentRequestInfo[0];
                    query["error"] = "1";
                    query["request_id"] = error_obj.PaymentRequestID;
                    query["code"] = error_obj.PaymentError.ErrorCode;
                    query["code_long"] = error_obj.PaymentError.LongMessage;
                    string queryString = query.ToString();
                    Response.Redirect("PaymentError?" + queryString, false);
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }
        protected void getCustomerDetails(GetExpressCheckoutDetailsResponseDetailsType paypal_info)
        {
            try
            {
                PayerInfoType payer_info = paypal_info.PayerInfo;
                string payer_email = payer_info.Payer; // jaslyn94-buyer@gmail.com
                string payer_name = payer_info.PayerName.FirstName + " " + payer_info.PayerName.LastName; //"test buyer"
                string payer_id = payer_info.PayerID; //"3FLKVMYQCSHVW"

                //Payment Overview
                PaymentDetailsType paymentDetails = paypal_info.PaymentDetails[0];
                newTransaction.AMT = paymentDetails.OrderTotal.value; //"7.00"

                //Item purchased
                PaymentDetailsItemType planPurchased = paymentDetails.PaymentDetailsItem[0];
                newTransaction.PLAN_TYPE_ID = planPurchased.Number;//PLAN_ID "1F013373-0F1C-4CD2-7893-1CC700F1F457"
                newTransaction.PLAN_NAME = planPurchased.Name; //"YEARLY PLAN"
                newTransaction.PER = planPurchased.Description.Split(' ')[0]; //per
                newTransaction.PLAN_AMOUNT = planPurchased.Amount.value;// "10.00"

                newTransaction.FREE_TRIAL = "0";
                //check for crm code

                if (paymentDetails.PaymentDetailsItem.Count > 1)
                {
                    PaymentDetailsItemType item = paymentDetails.PaymentDetailsItem[1];
                    if (item.Name.Equals("Discount Applied"))
                    {
                        newTransaction.CRM_ID = item.Number;//"fa229d93-7b76-4238-a0a5-1569bef4f984"
                    }
                }

                if (paypal_info.Custom != null)
                {
                    newTransaction.FREE_TRIAL = paypal_info.Custom.Split(':')[1];//"1"
                }
            }
            catch (Exception ex)
            {

            }

        }

        protected void paypalDoExpressCheckoutPayment(string token, string payerID, string billing_info)
        {
            try
            {
                //redirect link
                string baseurl = Request.Url.GetLeftPart(UriPartial.Authority) + "/";
                string link = baseurl + "/NewSaaS_Step3";

                //paypal link
                PaymentDetailsType paymentDetail = new PaymentDetailsType();
                //paymentDetail.NotifyURL = "http://localhost:53197/NewSaaS_Step3";
                paymentDetail.NotifyURL = link;
                paymentDetail.PaymentAction = (PaymentActionCodeType)EnumUtils.GetValue("Sale", typeof(PaymentActionCodeType));
                paymentDetail.OrderTotal = new BasicAmountType((CurrencyCodeType)EnumUtils.GetValue("SGD", typeof(CurrencyCodeType)), "1.00");
                List<PaymentDetailsType> paymentDetails = new List<PaymentDetailsType>();
                paymentDetails.Add(paymentDetail);

                DoExpressCheckoutPaymentRequestType request = new DoExpressCheckoutPaymentRequestType();
                request.Version = "104.0";
                DoExpressCheckoutPaymentRequestDetailsType requestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
                requestDetails.PaymentDetails = paymentDetails;
                requestDetails.Token = token;
                requestDetails.PayerID = payerID;
                request.DoExpressCheckoutPaymentRequestDetails = requestDetails;

                DoExpressCheckoutPaymentReq wrapper = new DoExpressCheckoutPaymentReq();
                wrapper.DoExpressCheckoutPaymentRequest = request;
                Dictionary<string, string> sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                sdkConfig.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                sdkConfig.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(sdkConfig);
                DoExpressCheckoutPaymentResponseType doECResponse = service.DoExpressCheckoutPayment(wrapper);

                string status = doECResponse.Ack.ToString();
                var payment_status = doECResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo;
                if (status.Equals("SUCCESS"))
                {
                    getTransactionResult(doECResponse);
                    //var payment_status_refer = doECResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0];
                    //if recurring billing, get recurring payment profile
                    if (billing_info.Equals("True"))
                    {
                        string bill_msg = "You have selected recurring payment";
                        paypalCreateRecurringPaymentsProfile(token, bill_msg);

                        
                    }
                }
                else
                {
                    var query = HttpUtility.ParseQueryString(string.Empty);
                    var error_obj = doECResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0];
                    query["error"] = "1";
                    query["request_id"] = error_obj.PaymentRequestID;
                    query["code"] = error_obj.PaymentError.ErrorCode;
                    query["code_long"] = error_obj.PaymentError.LongMessage;
                    string queryString = query.ToString();
                    Response.Redirect("PaymentError?" + queryString, false);
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }
        protected void getTransactionResult(DoExpressCheckoutPaymentResponseType doECResponse)
        {
            try
            {
                DoExpressCheckoutPaymentResponseDetailsType response = doECResponse.DoExpressCheckoutPaymentResponseDetails;
                PaymentInfoType paymentInfo = response.PaymentInfo[0];
                string paymentStatus = paymentInfo.PaymentStatus.Value.ToString(); //"COMPLETED"
                if (paymentStatus.Equals("COMPLETED")|| paymentStatus.Equals("PENDING"))
                {
                    newTransaction.PAYPAL_FEE = paymentInfo.FeeAmount.value; //"0.53"
                    newTransaction.PAYMENT_TIME = DateTime.Parse(paymentInfo.PaymentDate).ToLocalTime().ToString(); //"9/10/2016 9:26:39 PM"
                    newTransaction.PAYPAL_ID = paymentInfo.TransactionID; //"94A11958BN919320V"
                    newTransaction.PAYMENT_TYPE = "ONE OFF";
                    string paymentType = paymentInfo.PaymentType.Value.ToString(); //"INSTANT"
                    string grossAmount = paymentInfo.GrossAmount.value; //"1.00"
                    string taxAmount = paymentInfo.TaxAmount.value; //0.00

                    
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void paypalCreateRecurringPaymentsProfile(string token, string billingAgreementDescription)
        {
            try
            {
                //setting parameters
                string currency = "SGD";
                string amount = newTransaction.PLAN_AMOUNT;
                int frequency = 0;
                string frequencyPeriod;
                if (newTransaction.PER.Equals("12"))
                {
                    frequencyPeriod = "Year";
                    frequency = 1;
                }
                else
                {
                    frequencyPeriod = "Month";
                    frequency = Convert.ToInt32(newTransaction.PER);
                }
                newTransaction.PAYMENT_TYPE = "RECURRING";


                CreateRecurringPaymentsProfileRequestType createRPProfileRequest = new CreateRecurringPaymentsProfileRequestType();
                CreateRecurringPaymentsProfileRequestDetailsType createRPProfileRequestDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
                createRPProfileRequestDetails.Token = token;
                createRPProfileRequest.CreateRecurringPaymentsProfileRequestDetails = createRPProfileRequestDetails;

                //get today date to create profile
                DateTime date = DateTime.Now;
                string datetime = date.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                RecurringPaymentsProfileDetailsType profileDetails = new RecurringPaymentsProfileDetailsType(datetime);
                createRPProfileRequestDetails.RecurringPaymentsProfileDetails = profileDetails;

                BasicAmountType paymentAmount = new BasicAmountType((CurrencyCodeType)EnumUtils.GetValue(currency, typeof(CurrencyCodeType)), amount);
                BillingPeriodType period = (BillingPeriodType)EnumUtils.GetValue(frequencyPeriod, typeof(BillingPeriodType));
                BillingPeriodDetailsType paymentPeriod = new BillingPeriodDetailsType(period, frequency, paymentAmount);

                ScheduleDetailsType scheduleDetails = new ScheduleDetailsType();
                scheduleDetails.Description = billingAgreementDescription;
                scheduleDetails.PaymentPeriod = paymentPeriod;
                createRPProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails = scheduleDetails;

                CreateRecurringPaymentsProfileReq createRPProfileReq = new CreateRecurringPaymentsProfileReq();
                createRPProfileReq.CreateRecurringPaymentsProfileRequest = createRPProfileRequest;

                Dictionary<string, string> sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                sdkConfig.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                sdkConfig.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(sdkConfig);
                CreateRecurringPaymentsProfileResponseType createRPProfileResponse = service.CreateRecurringPaymentsProfile(createRPProfileReq);

                string status = createRPProfileResponse.Ack.ToString();
                if (status.Equals("SUCCESS"))
                {
                    getBuyerProfile(createRPProfileResponse);
                    insertOrgInstance();
                    createTransaction();
                }
                else
                {
                    var query = HttpUtility.ParseQueryString(string.Empty);
                    string error_obj = createRPProfileResponse.CreateRecurringPaymentsProfileResponseDetails.TransactionID;
                    query["error"] = "1";
                    query["request_id"] = error_obj;
                    query["code_long"] = "Error calling Paypal server - Recurring Profile";
                    string queryString = query.ToString();
                    Response.Redirect("PaymentError?" + queryString, false);
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("PaymentError", false);
            }
        }
        protected void getBuyerProfile(CreateRecurringPaymentsProfileResponseType createRPProfileResponse)
        {
            try
            {
                var details = createRPProfileResponse.CreateRecurringPaymentsProfileResponseDetails;
                newTransaction.PROFILE_ID = details.ProfileID; //"I-SPXFEJARC8N8"
                paymentMade = true;
            }
            catch (Exception ex)
            {

            }
        }
    }
}
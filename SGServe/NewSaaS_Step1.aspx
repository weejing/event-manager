﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VolMaster.Master" Async="true" AutoEventWireup="true" CodeBehind="NewSaaS_Step1.aspx.cs" Inherits="SGServe.NewSaaS_Step1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="page-section">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h4 class="page-section-heading">Organisation Particulars</h4>
                    
      
                        <div class="panel panel-default">                                 
                            <div class="panel-body">
                                
                                <div class="row">
                                    <div class="col-md-12">
                                         <div class="form-group form-control-default required">
                                            <label>
                                                Organisation Name
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtOrgName"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </label>
                                            <asp:TextBox ID="txtOrgName" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                         <div class="form-group form-control-default required">
                                            <label>
                                                Organisation Short Form
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtOrgShort"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </label>
                                            <asp:TextBox ID="txtOrgShort" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default">
                                            <label>Organisation Email  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtEmail"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator>    <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="createGrp" SetFocusOnError="true" ForeColor="Red" ID="RegularExpressionValidator3" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Wrong format"></asp:RegularExpressionValidator></label>
                                            <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                         <div class="form-group form-control-default">
                                            <label>Website Link <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtLink"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator> </label>
                                            <asp:TextBox ID="txtLink" runat="server"  class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default">
                                            <label>UEN<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtUEN"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                            <asp:TextBox ID="txtUEN" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default">
                                            <label>Address<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtAddress"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                            <asp:TextBox ID="txtAddress" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default">
                                            <label>Organisation Description<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="createGrp" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtDesc"
                    ErrorMessage="Please fill in" ForeColor="Red"></asp:RequiredFieldValidator></label>
                                            <asp:TextBox ID="txtDesc" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group form-control-default required">
                                            <label>Organisation Type</label>
                                            <asp:DropDownList ID="ddType" runat="server" class="selectpicker" data-style="btn-white" data-live-search="true" data-toggle="dropdown" Width="90%">
                                                <asp:ListItem>Community Club</asp:ListItem>
                                                <asp:ListItem>Charity Organisation</asp:ListItem>
                                                <asp:ListItem>More</asp:ListItem>
                                                <asp:ListItem>Type D</asp:ListItem>
                                                <asp:ListItem>Type E</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                           <asp:Button ID="btnReg" runat="server" class="btn btn-primary"  ValidationGroup="createGrp"  Text="Next Step > Select Your Plan" OnClick="btnReg_Click" /><br />
                                    </div>
                                </div>
                                <div class="row">
                                    
                                 
                                  
                               
                              
                               
                               
                            </div>
                        </div>
                      
        

                     </div>
                        <br />
                         <br />
                    </div>
                </div>
            </div>
</asp:Content>

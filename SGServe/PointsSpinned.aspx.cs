﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using SGServe.Models.UserBiddingModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class PointsSpinned : System.Web.UI.Page
    {
        static int points = 0;
        String message = "";
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            message = Request.QueryString["points"];
            points = Int32.Parse(message[0].ToString());

            if (points > 10)
            {
                points = -1;
            }

            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    registerPoints();
                    Response.Redirect("BidPointsTransac.aspx", false);
                }
            }
        }

        protected async void registerPoints()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/VolBiddingMgmt/addPointsForSpinWheel?userID=" + currentUserID + "&pointsEarned=" + points);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
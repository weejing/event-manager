﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="SearchUsers.aspx.cs" Inherits="SGServe.SearchUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h1 class="text-h2">Search for volunteers</h1>
    <div class="panel panel-default" style="border-radius: 8px">
         <div class="panel-body">
      <div class="panel panel-overlay" >
          <div class="panel-heading">
                <div class="row">
               <div class="col-md-2">
                  <h4>Volunteer Name:</h4> 
                </div>
              <div class="col-md-5">
                <div class="input-group">
                  <asp:TextBox ID="tbSearchName" class="form-control" runat="server"></asp:TextBox>
                  <span class="input-group-btn">
                     <asp:Button ID="btnSearch" class="btn btn-info" runat="server" Text="Search" OnClick="btnSearch_Click" />
                  </span>
                </div><!-- /input-group -->
              </div><!-- /.col-md-5 -->
            </div><!-- /.row -->
          </div> 
      </div>
    <br/>


            <asp:DataList ID="DataList1" runat="server" OnItemCommand="DataList1_ItemCommand" EnableViewState="False" RepeatDirection="Horizontal" RepeatLayout="Flow">
                <ItemTemplate>
                    <div class="col-sm-6 col-lg-4 col-md-9">
                        <div class="panel panel-default relative">
                             <div class="cover hover overlay margin-none">
                           
                                
                               <div class="media">
                                  <div class="pull-left">
                                     
                                        <asp:Image ID="Image1" runat="server" height="80" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "IMAGE") %>'  />
                                    </div>
                                    <div class="media-body">
                           
                                        <h4 class="media-heading margin-v-5">
                                            <asp:Label ID="lblName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FULL_NAME") %>'></asp:Label></h4>
                                        <h5>
                                            <asp:Label ID="lblSchool" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SCHL_NAME") %>'></asp:Label></h5>
                                    </div>
                                </div>
                                     <a class="overlay overlay-bg-black overlay-hover overlay-full img-responsive" href='<%# string.Format("UserPortfolio.aspx?id={0}", DataBinder.Eval(Container.DataItem, "USR_ID")) %>'><span class="v-center"><span class="btn btn-circle btn-white"><i class="fa fa-eye"></i></span></span></a>
                                        
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
             <asp:Label ID="lblNoRecommend" runat="server" Text="Search for volunteers here ! "></asp:Label>
            <br />
            <br />
         <br />
        <br />
        </div>
    </div>
</asp:Content>

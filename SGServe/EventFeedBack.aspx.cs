﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.EventModels;
using System.Net.Http;

namespace SGServe
{
    public partial class EventFeedBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                getFeedBackDetails();
            }
        }


        protected  async void getFeedBackDetails()
        {
            
            String sessionId = Request.QueryString["s"];
            String connectionString = Request.QueryString["o"];
           

            Server_Connect serverConnection = new Server_Connect();

            HttpClient client = serverConnection.getHttpClient();
            
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/EventMgmt/volGetFeedBack?sessionId=" + sessionId + "&connectionString=" + connectionString);
            HttpResponseMessage response = await client.SendAsync(request);

            String result = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("result:" + result);
            List<FeedBackDesign> listOfFeedBackDesign = JsonConvert.DeserializeObject<List<FeedBackDesign>>(result);

            int count = 0;
            hiddenType.Value = "";
            foreach(FeedBackDesign feedBack in listOfFeedBackDesign)
            {
                count++;
                String question = "<div class ='form-group'> <p>" +  feedBack.question +"</p> </div>";

                feedBackPlace.Controls.Add(new LiteralControl(question));

                String answer;
                hiddenType.Value += feedBack.type + ",";
                if(feedBack.type == 1)
                {
                    answer = "<div class = 'form-group'>  <textarea class='form-control' id ='text"+count+"' rows='4'></textarea></div>";
                }
                else
                {
                    string radio1 = " <div class='radio radio-info radio-inline'><input type ='radio' value ='1'  name = 'radio"+count+ "' id = 'radio1" + count + "' /><label for = 'radio1"+count+"'>1</label></div> ";
                    string radio2 = " <div class='radio radio-info radio-inline'><input type ='radio' value ='2'  name = 'radio" + count + "' id = 'radio2" + count + "'  /><label for = 'radio2" + count + "'>2</label></div> ";
                    string radio3 = " <div class='radio radio-info radio-inline'><input type ='radio' value ='3'  name = 'radio" + count + "' id = 'radio3" + count + "' /><label for = 'radio3" + count + "'>3</label></div> ";
                    string radio4 = " <div class='radio radio-info radio-inline'><input type ='radio' value ='4'  name = 'radio" + count + "' id = 'radio4" + count + "' /><label for = 'radio4" + count + "'>4</label></div> ";
                    string radio5 = " <div class='radio radio-info radio-inline'><input type ='radio' value ='5'  name = 'radio" + count + "' id = 'radio5" + count + "' /><label for = 'radio5" + count + "'>5</label></div> ";

                    answer = "<div class ='form-group'> <div class = 'col-md-2'><p>Lowest<p></div>  <div class ='col-md-8'>" + radio1 + radio2 + radio3 + radio4 + radio5 + "</div>  <div class ='col-md-2'><p>Highest</p></div> </div>";
                }

                feedBackPlace.Controls.Add(new LiteralControl(answer + "<hr>"));                
            }

            hiddenCount.Value = count +"";
            
        }


        public async void submitfeedBack(object sender, EventArgs e)
        {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();

            String [] responseArray = hiddenResponse.Value.Split(',');
           
            FeedBackResponse response = new FeedBackResponse();
            response.connectionString = Request.QueryString["o"];
            response.sessionId = Request.QueryString["s"];
            response.userId = Request.QueryString["v"];

            response.listOfAnswers = new List<string>();

            for(int count =0; count < responseArray.Length -1; count++)
            {
                System.Diagnostics.Debug.WriteLine(responseArray[count]);
                response.listOfAnswers.Add(responseArray[count]);
            }

            await client.PostAsJsonAsync("/api/EventMgmt/insertFeedBackResponse", response);

            Response.Redirect("Home" , false);            
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.spinwheelcontent;
using System.Web.Script.Serialization;

namespace SGServe
{
    public partial class SpinWheels : System.Web.UI.Page
    {
        //This value should be taken from database. It is needed for the initialisation of the wheel
        int size = 10;

        protected void Page_Load(object sender, EventArgs e)
        {
            generateMenu();
        }

        private void generateMenu()
        {
            string leftMenu = "<ul class='nav navbar-nav'>";
            string EndTag = "</ul>";
            //string rightMenu = "<ul class='nav navbar-nav navbar - right'>";
            string rightMenu = "<ul class='nav navbar-nav navbar-right'>";
            if (Session["Token"] == null)
            {
                leftMenu = leftMenu + "<li class='dropdown'> <a href='EventList.aspx'>Events</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'> <a href='Organization.aspx'>Organizations</a> </li>";

                //right menu
                rightMenu = rightMenu + "<li class='dropdown'><a href ='Login.aspx'><i class='fa fa-fw fa-lock'></i>Login</a></li>";
                rightMenu = rightMenu + "<li class='dropdown'><a href='Signup.aspx'><i class='fa fa-fw fa-plus'></i>Sign Up </a></li>";

            }
            else
            {
                leftMenu = leftMenu + "<li class='dropdown'> <a href='Dashboard.aspx'>Dashboard</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'> <a href='EventList.aspx'>Events</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'> <a href='Organization.aspx'>Organizations</a> </li>";
                leftMenu = leftMenu + "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>Community<span class='caret'></span></a>";
                leftMenu = leftMenu + "<ul class='dropdown-menu'>";
                leftMenu = leftMenu + " <li><a href='Newsfeed.aspx'>Newsfeed</a></li>";
                leftMenu = leftMenu + "<li><a href='SearchUsers.aspx'>Search User</a></li>";
                leftMenu = leftMenu + " </ul></li>";

                //right menu
                rightMenu = rightMenu + "<li class='dropdown'><a href ='Logout.aspx'><i class='fa fa-fw fa-sign-out'></i>Signout</a></li>";
            }
            leftMenu = leftMenu + EndTag;
            rightMenu = rightMenu + EndTag;
            lblleftMenu.Text = leftMenu;
            lblRightmenu.Text = rightMenu;
        }


        public int getCount()
        {
            return size;
        }


        public string getContent()
        {
            string[] colours = { "#eae56f", "#89f26e", "#7de6ef", "#e7706f", "#0099cc", "#e95d7a" };
            int count = 0;

            var publicationTable = new Segment[size];
            for (int i = 0; i < size; i++)
            {
                if (count > colours.Length - 1)
                    count = 0;

                count++;
            }
            publicationTable[0] = new Segment(colours[5], "2 points");
            publicationTable[1] = new Segment(colours[0], "3 points");
            publicationTable[2] = new Segment(colours[3], "6 points");
            publicationTable[3] = new Segment(colours[4], "7 points");
            publicationTable[4] = new Segment(colours[2], "5 points");
            publicationTable[5] = new Segment(colours[1], "9 points");
            publicationTable[6] = new Segment(colours[2], "5 points");
            publicationTable[7] = new Segment(colours[4], "7 points");
            publicationTable[8] = new Segment(colours[3], "6 points");
            publicationTable[9] = new Segment(colours[0], "3 points");

            return (new JavaScriptSerializer()).Serialize(publicationTable);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string winningText = hdnfldVariable.Value;
            Session["points"] = winningText;
            Response.Redirect("About.aspx");
        }
    }
}
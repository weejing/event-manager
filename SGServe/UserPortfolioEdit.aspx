﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/VolMaster.Master" AutoEventWireup="true" CodeBehind="UserPortfolioEdit.aspx.cs" Inherits="SGServe.UserPortfolioEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h4 class="page-section-heading">Update Personal Portfolio</h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
            </div>
            <div class="form-group form-control-default">
                <label>
                    Description : </label>
                <asp:TextBox ID="tbDescription" runat="server" class="form-control"></asp:TextBox>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-control-default">
                        <label>
                            Profile Image
                        </label>
                        <br/>
                        <asp:FileUpload ID="FileUploadImage" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnSave" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSave_Click" /><br />
    <br />
</asp:Content>

﻿using Newtonsoft.Json;
using SGServe.Models.AccountModels;
using SGServe.Models.AdminModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SGServe.CommonClass;
using System.Configuration;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace SGServe
{
    public partial class Signup : System.Web.UI.Page
    {

        protected async void Page_Load(object sender, EventArgs e)
        {
            lblmessage.Visible = false;
            if (IsPostBack)
            {
                DOB.Text = Request.Form[DOB.UniqueID].ToString();
            }
            else
            {
                ddlSchool.Visible = false;
                generateForm();
                fbPnl.Visible = true;
                pwPnl.Visible = true;
                if (Request.QueryString["code"] != null)
                {
                    fbPnl.Visible = false;
                    pwPnl.Visible = false;
                    await getFacebookUserData(Request.QueryString["code"]);
                }
            }
        }

        private async Task getFacebookUserData(String code)
        {
            // Exchange the code for an access token
            Uri targetUri = new Uri("https://graph.facebook.com/oauth/access_token?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&client_secret=" + ConfigurationManager.AppSettings["FacebookAppSecret"] + "&redirect_uri=http://" + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "/Signup.aspx&code=" + code);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);

            System.IO.StreamReader str = new System.IO.StreamReader(request.GetResponse().GetResponseStream());
            string token = str.ReadToEnd().ToString().Replace("access_token=", "");

            // Split the access token and expiration from the single string
            string[] combined = token.Split('&');
            string accessToken = combined[0];

            // Request the Facebook user information
            Uri targetUserUri = new Uri("https://graph.facebook.com/me?fields=first_name,last_name,gender,locale,link,email,birthday&access_token=" + accessToken);
            System.Diagnostics.Debug.WriteLine("token: " + accessToken);

            HttpWebRequest user = (HttpWebRequest)HttpWebRequest.Create(targetUserUri);
            // Read the returned JSON object response
            String jsonResponse = string.Empty;
            try
            {
                StreamReader userInfo = new StreamReader(user.GetResponse().GetResponseStream());
                jsonResponse = userInfo.ReadToEnd();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception:" + e.ToString());
            }

            // Deserialize and convert the JSON object to the Facebook.User object type
            JavaScriptSerializer sr = new JavaScriptSerializer();
            string jsondata = jsonResponse;
           FbUser userDetails = sr.Deserialize<FbUser>(jsondata);
            txtFullName.Text= userDetails.last_name + " "+ userDetails.first_name;
            DOB.Text = userDetails.user_birthday;
            txtEmail.Text = userDetails.email;
           Session["facebookid"]= userDetails.id;
        }

        protected void btnFB_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://www.facebook.com/v2.7/dialog/oauth/?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&redirect_uri=http://" + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "/Signup.aspx&response_type=code&state=1&scope=email,user_birthday,user_friends");
        }

        protected async void btnReg_Click(object sender, EventArgs e)
        {
            string baseUrl = Request.Url.ToString();
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();           
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, "/api/Account/VolcheckEmailExist?email=" + txtEmail.Text.ToString());
            HttpResponseMessage resp = await client.SendAsync(req);
            String response = await resp.Content.ReadAsStringAsync();

            if (Convert.ToBoolean(response) == true)
            {
                lblmessage.Visible = true;

            }
            else
            {
                lblmessage.Visible = false;
                registerVol(baseUrl);             
            }
        }

        protected async void generateForm() {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermListByVocabName?vocab_name=" + "cause");
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                HttpRequestMessage rolReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermListByVocabName?vocab_name=" + "event role");
                HttpResponseMessage rolResp = await client.SendAsync(rolReq);
                String roles = await rolResp.Content.ReadAsStringAsync();
                HttpRequestMessage skillReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getTermListByVocabName?vocab_name=" + "skill");
                HttpResponseMessage skillResp = await client.SendAsync(skillReq);
                String skills = await skillResp.Content.ReadAsStringAsync();

                var Benetable = JsonConvert.DeserializeObject<DataTable>(reply);
                var Roletable = JsonConvert.DeserializeObject<DataTable>(roles);
                var skillstable = JsonConvert.DeserializeObject<DataTable>(skills);

                hiddenBeneVocabID.Value = Benetable.Rows[0][1].ToString();
                cbBenefi.DataSource = Benetable;
                cbBenefi.DataTextField = "TERM_NAME";
                cbBenefi.DataValueField = "TERM_ID";
                cbBenefi.DataBind();

                hiddenRoleVocabID.Value = Roletable.Rows[0][1].ToString();
                cbRoles.DataSource = Roletable;
                cbRoles.DataTextField = "TERM_NAME";
                cbRoles.DataValueField = "TERM_ID";
                cbRoles.DataBind();

                hiddenSkillVocabID.Value = skillstable.Rows[0][1].ToString();
                cbSkill.DataSource = skillstable;
                cbSkill.DataTextField = "TERM_NAME";
                cbSkill.DataValueField = "TERM_ID";
                cbSkill.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Signup.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: Display info---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        protected async void ddlSchLvl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchLvl.SelectedValue.ToString() == "None")
            {
                ddlSchool.Visible = false;
            }
            else
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

               
                HttpRequestMessage schReq = new HttpRequestMessage(HttpMethod.Get, "/api/Admin/getSchools?sch_lvl=" + ddlSchLvl.SelectedValue.ToString());
                HttpResponseMessage schResp = await client.SendAsync(schReq);
                String schools = await schResp.Content.ReadAsStringAsync();

                var schooltable = JsonConvert.DeserializeObject<DataTable>(schools);
                ddlSchool.Visible = true;
                ddlSchool.DataSource = schooltable;
                ddlSchool.DataTextField = "SCHL_NAME";
                ddlSchool.DataValueField = "SCHL_ID";
                ddlSchool.DataBind();
            }

        }

        public void registerVol(string baseUrl) {
            Server_Connect serverConnection = new Server_Connect();
            HttpClient client = serverConnection.getHttpClient();
            string method = "";
            if (baseUrl.Contains("code"))
            {
                //fb signup
                method= "createAccFB";
            }
            else
            {
                method = "createAccNoFB";
            }
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/Account/"+ method);
            List<TermRelation> volIntList = new List<TermRelation>();
            foreach (ListItem item in cbBenefi.Items)
            {
                if (item.Selected)
                {
                    TermRelation volInt = new TermRelation { TERM_ID = Convert.ToInt32(item.Value), VOCAB_ID=Convert.ToInt32(hiddenBeneVocabID.Value) };
                    volIntList.Add(volInt);
                }
            }
            foreach (ListItem item in cbRoles.Items)
            {
                if (item.Selected)
                {
                    TermRelation volInt = new TermRelation { TERM_ID = Convert.ToInt32(item.Value), VOCAB_ID = Convert.ToInt32(hiddenRoleVocabID.Value) };
                    volIntList.Add(volInt);
                }
            }
            foreach (ListItem item in cbSkill.Items)
            {
                if (item.Selected)
                {
                    TermRelation volInt = new TermRelation { TERM_ID = Convert.ToInt32(item.Value), VOCAB_ID = Convert.ToInt32(hiddenSkillVocabID.Value) };
                    volIntList.Add(volInt);
                }
            }

            string FULL_NAME = txtFullName.Text;
            string SCHL_ID = "";
            if (ddlSchLvl.SelectedValue.ToString() != "None")
            {
                SCHL_ID= ddlSchool.SelectedValue.ToString();
            }
            string EMAIL=txtEmail.Text;
            string NRIC = txtNRIC.Text;
            string PHONE_NO = txtPhone.Text;
            string PASSWORD=txtPassword.Text;
            DateTime DateOB = DateTime.ParseExact(DOB.Text, "d MMM yyyy", null); 
            string DIETARY = ddlDietary.SelectedValue.ToString();
            string PERSONALITY_CHAR = ddlPersonality.SelectedValue.ToString();
            string RELIGION = ddlReligion.SelectedValue.ToString();
            Volunteer volunteer;
            if (Session["facebookid"] != null)
            {
                volunteer = new Volunteer { SCHL_ID = SCHL_ID, FULL_NAME = FULL_NAME, EMAIL = EMAIL, NRIC = NRIC, PHONE_NO = PHONE_NO, PASSWORD = PASSWORD, DOB = DateOB, DIETARY = DIETARY, PERSONALITY_CHAR = PERSONALITY_CHAR, RELIGION = RELIGION, volListofInts = volIntList, FACEBOOK_ID = Session["facebookid"].ToString() };
            }
            else {
                string url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
                volunteer = new Volunteer { SCHL_ID = SCHL_ID, FULL_NAME = FULL_NAME, EMAIL = EMAIL, NRIC = NRIC, PHONE_NO = PHONE_NO, PASSWORD = PASSWORD, DOB = DateOB, DIETARY = DIETARY, PERSONALITY_CHAR = PERSONALITY_CHAR, RELIGION = RELIGION, volListofInts = volIntList, URL= url };
            }            
            client.PostAsJsonAsync("api/Account/"+ method, volunteer);
            if (baseUrl.Contains("code"))
            {
                Response.Redirect("https://www.facebook.com/v2.7/dialog/oauth/?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&redirect_uri=http://" + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "/FbRedirect.aspx&response_type=code&state=1&scope=email,user_birthday,user_friends");
            }
            else
            {
                Response.Redirect("SignupSuccess.aspx", false);
            }
        }
    }
}
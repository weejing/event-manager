﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="UserPortfolioView.aspx.cs" Inherits="SGServe.UserPortfolioView" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html class="hide-sidebar top-navbar ls-bottom-footer-fixed" lang="en">

<head runat="server">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SGServe</title>

  <link href="main_css/css/vendor/all.css" rel="stylesheet">

  <link href="main_css/css/app/app.css" rel="stylesheet">
       <style type="text/css">
        .auto-style1 {
            width: 138px;
            height: 46px;
        }

        .auto-style2 {
            left: 1px;
            top: 0px;
        }
    </style>

    <script src='<%=ResolveClientUrl("~/scripts/jquery-1.6.4.min.js") %>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/scripts/jquery.signalR-2.2.1.min.js") %>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/signalr/hubs") %>' type="text/javascript"></script>
    <script>
        $(function () {
            var notify = $.connection.notificationHub;
            //this function will be called by the code behind and will pass the message over to this function
            //and display in the UI below.
            notify.client.displayNotification = function (msg) {
                $("#newData").html(msg);
            };
            //start the hub
            $.connection.hub.start();
        });
    </script>

    <link href="main_css/css/app/timeline.css" rel="stylesheet">
    <link href="main_css/css/app/cover.css" rel="stylesheet">
    <link href="main_css/css/app/essentials.css" rel="stylesheet">
    <link href="main_css/css/app/layout.css" rel="stylesheet">
    <link href="main_css/css/app/media.css" rel="stylesheet">
      <!-- Inline Script for colors and config objects; used by various external scripts; -->
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "real-estate",
      skins: {
        "default": {
          "primary-color": "#e74c3c"
        }
      }
    };
  </script>

  <script src="main_css/js/vendor/all.js"></script>


         <script language="javascript">
             function pageLoad(sender, args) {

                 $.getScript("main_css/js/app/app.js");
             }
          </script>
</head>
   <body>
     <form id="form2" runat="server">
                <asp:scriptmanager runat="server">
                </asp:scriptmanager>
    <!-- Fixed navbar -->
    <div class="navbar navbar-main navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.aspx">
                        <img src="main_css/images/sgserve.jpg" class="auto-style1" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-nav">
                <asp:Label ID="lblleftMenu" runat="server" Text=""></asp:Label>
                       <asp:Label ID="lblRightmenu" runat="server" Text=""></asp:Label> 
            </div>
            <!-- /.navbar-collapse -->

        </div>
    </div>

  <div id="content">

  
    </div>

  
      <div class="container">

          <link href="community_css/css/vendor/all.css" rel="stylesheet">
    <link href="community_css/css/app/app.css" rel="stylesheet">
    <script src="community_css/js/vendor/all.js"></script>
    <script src="community_css/js/app/app.js"></script>

          <div class="container">
            <div class="cover profile">
                
              <div class="wrapper">
                <div class="image" style="height: 60px;">
                  <!--<img src="main_css/images/place1-wide.jpg" alt="people" />-->
                </div>
              </div>
                    
              <div class="cover-info">
                <div class="avatar">
                  <asp:Image ID="ProfilePicImg" runat="server" />
                </div>
                <div class="name">
                    <h2>
                        <asp:Label ID="lblFullName" runat="server" Text="Bill" ForeColor="Black"></asp:Label>
                    </h2>
                            </div>
                <ul class="cover-nav">

                  <li class="active"><!--<a href="UserPortfolio.aspx" id="linkAbout"><i class="fa fa-fw icon-user-1"></i> About</a>-->
                    <asp:HyperLink ID="linkAbout" runat="server" ForeColor="Black"><i class="fa fa-fw icon-user-1"></i> About </asp:HyperLink></li>
                  <!--<li><a href="UserFriends.aspx" id="linkFriends"><i class="fa fa-fw fa-users"></i> Friends</a></li>
                  <li><a href="UserOrgFollows.aspx" id="linkFollows"><i class="fa fa-fw fa-building-o"></i> Follows</a></li>-->
                  <li> </li>
                  <li>

                      <li>
                      </li>
                </ul>
              </div>
            </div>
           </div>

        <div class="container">
                             <div class="panel panel-default">
                  <div class="panel-heading panel-heading-gray">
                    <i class="fa fa-fw fa-info-circle"></i> About
                  </div>
                  <div class="panel-body">
                    <ul class="list-unstyled profile-about margin-none">
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">School</span></div>
                          <div class="col-sm-8">
                              <asp:Label ID="lblSchool" runat="server" Text="Label">my school name</asp:Label>
                            </div>
                        </div>
                      </li>
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Interests (Cause)</span></div>
                          <div class="col-sm-8">
                                    <asp:DataList ID="dlBene" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li>
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Interests (Role)</span></div>
                          <div class="col-sm-8">
                              
                                    <asp:DataList ID="dlRole" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li>
                        <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4"><span class="text-muted">Skills</span></div>
                          <div class="col-sm-8">
                                    <asp:DataList ID="dlskill" runat="server">
                                        <ItemTemplate>
                                            <%# Eval("TERM_NAME") %>
                                        </ItemTemplate>
                                    </asp:DataList>
                          </div>
                        </div>
                      </li>
                      <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Personality MBTI</span>
                              </div>
                              <div class="col-sm-8">
                              <asp:Label ID="lblPersonalityMBTI" runat="server"></asp:Label>
                                </div>
                         </div>
                      </li>
                        
                    <li class="padding-v-5">
                        <div class="row">
                          <div class="col-sm-4">
                          <span class="text-muted">Description</span>
                              </div>
                            <div class="col-sm-8">
                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                            </div>
                         </div>
                      </li>
                    </ul>
                  </div>
                <br/>
            </div>

            </div>
          <span id="newData"></span>
        </div>
         <br/><br/>
      <footer class="footer">
          <strong>SGServe</strong> Copyright 2016
    </footer>
</form>
</body>
</html>
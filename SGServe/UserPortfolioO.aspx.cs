﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Data;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class UserPortfolioO : System.Web.UI.Page
    {
        OrgEmployee employee = new OrgEmployee();
        OrganizationDetailsModel org = new OrganizationDetailsModel();
        String role = "";
        static String navigateUserID = "";
        protected async void Page_Load(object sender, EventArgs e)
        {
            //await getOrgEmpDetails();
            /*
            if (employee.USR_ID == null)
            {
                Response.Redirect("login", false);
            }
            else
            {
                role = employee.ROLEE;
                */
               navigateUserID = Request.QueryString["id"];
               generateContent();
            //}
        }

        protected async void generateContent()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage userDetailsReq = new HttpRequestMessage(HttpMethod.Get, "api/CommunityMgmt/getUserDetails?userID=" + navigateUserID);
                HttpResponseMessage userDetailsResp = await client.SendAsync(userDetailsReq);
                String userDetails = await userDetailsResp.Content.ReadAsStringAsync();

                UserPortfolioModel upm = JsonConvert.DeserializeObject<UserPortfolioModel>(userDetails);

                lblFullName.Text = upm.FULL_NAME;
                lblSchool.Text = upm.SCHL_NAME;
                lblPersonalityMBTI.Text = upm.PERSONALITY_CHAR;
                lblDescription.Text = upm.DESCRIPTION;
                ProfilePicImg.ImageUrl = upm.IMAGE;

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + navigateUserID + "&vocab_name=cause");
                HttpResponseMessage response = await client.SendAsync(request);
                String reply = await response.Content.ReadAsStringAsync();
                var Benetable = JsonConvert.DeserializeObject<DataTable>(reply);
                dlBene.DataSource = Benetable;
                dlBene.DataBind();

                //get roles
                HttpRequestMessage rolerequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + navigateUserID + "&vocab_name=" + "event role");
                HttpResponseMessage roleresponse = await client.SendAsync(rolerequest);
                String rolereply = await roleresponse.Content.ReadAsStringAsync();
                var Roletable = JsonConvert.DeserializeObject<DataTable>(rolereply);
                dlRole.DataSource = Roletable;
                dlRole.DataBind();

                //get skill
                HttpRequestMessage skillrequest = new HttpRequestMessage(HttpMethod.Get, "/api/Account/getVolInterests?usr_id=" + navigateUserID + "&vocab_name=" + "skill");
                HttpResponseMessage skillresponse = await client.SendAsync(skillrequest);
                String skillgreply = await skillresponse.Content.ReadAsStringAsync();
                var skilltable = JsonConvert.DeserializeObject<DataTable>(skillgreply);
                dlskill.DataSource = skilltable;
                dlskill.DataBind();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

        }
    }
}
﻿using Newtonsoft.Json;
using SGServe.CommonClass;
using SGServe.Models.AccountModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayPal.PayPalAPIInterfaceService.Model;
using PayPal.PayPalAPIInterfaceService;
using SGServe.Models.TransactionModels;
using System.Web.Script.Serialization;
using System.Json;
using SGServe.SaaS.Models;

namespace SGServe
{
    public partial class NewSaaS_Org : System.Web.UI.Page
    {
        List<String> items = new List<String>();
        public static OrgPayment newTransaction = new OrgPayment();
        public static Boolean paymentMade = false;
        public static bool result = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cookies["token"].Value = Request.QueryString["token"];
            Response.Cookies["token"].Expires = DateTime.Now.AddMinutes(15);
            Response.Cookies["PayerID"].Value = Request.QueryString["PayerID"];
            Response.Cookies["PayerID"].Expires = DateTime.Now.AddMinutes(15);

           String status = Session["ManualPayment"].ToString();
            if (status.Equals("On")) {
                Response.Cookies["token"].Value = "176d5490-d7a2-419f-9381-699bba02258f";
                Response.Cookies["token"].Expires = DateTime.Now.AddMinutes(15);
                Response.Cookies["PayerID"].Value = "176d5490-d7a2-419f-9381-699bba02258f";
                Response.Cookies["PayerID"].Expires = DateTime.Now.AddMinutes(15);
               // Session["ManualPayment"] = "Off";
            }
        }

        protected async void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/api/OrgMgmt/getOrgList");
                HttpResponseMessage response = await client.SendAsync(request);

                String reply = await response.Content.ReadAsStringAsync();
                //System.Diagnostics.Debug.WriteLine("reply: " + reply);

                JsonArray array = (JsonArray)JsonValue.Parse(reply);

                foreach (var arrayResult in array)
                {
                    OrganizationDetails newItem = new OrganizationDetails();
                    newItem.CONNECTION_STRING = arrayResult["CONNECTION_STRING"].ToString();
                    items.Add(newItem.CONNECTION_STRING.Substring(1, newItem.CONNECTION_STRING.Length - 2));
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Global.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: registerURL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            if (items.Contains(txtOrgShort.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Short form duplicate!');", true);
            }
            else if (result == false)
            {//else if (checkUniqueOrgString(txtLink.Text) == false)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Insert a real website link!');", true);
            }
            else
            {
                try
                {
                   
                    Response.Cookies["CONNECTION_STRING"].Value = txtOrgShort.Text.ToUpper();
                    Response.Cookies["CONNECTION_STRING"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["ORG_NAME"].Value = txtOrgName.Text;
                    Response.Cookies["ORG_NAME"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["EMAIL"].Value = txtEmail.Text;
                    Response.Cookies["EMAIL"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["LINK"].Value = txtLink.Text;
                    Response.Cookies["LINK"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["UEN"].Value = txtUEN.Text;
                    Response.Cookies["UEN"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["ADDRESS"].Value = txtAddress.Text;
                    Response.Cookies["ADDRESS"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["DESCRIPTION"].Value = txtDesc.Text;
                    Response.Cookies["DESCRIPTION"].Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies["ORG_TYPE"].Value = ddType.Text;
                    Response.Cookies["ORG_TYPE"].Expires = DateTime.Now.AddMinutes(5);
                    //createTransactionCookie();
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), reply, "alert('Record Inserted Successfully')", true);

                    Response.Redirect("NewSaaS_Admin.aspx", false);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("---Issue Start---");
                    System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                    System.Diagnostics.Debug.WriteLine("---Method: Insert New Organization Button---");
                    System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                    System.Diagnostics.Debug.WriteLine("---End---");
                }
            }
        }

        public static bool checkUniqueOrgString(string orgString)
        {
            Uri uriResult;
            return Uri.TryCreate(orgString, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
        }

        /*
        protected void validateWebsite(object sender, ServerValidateEventArgs e)
        {
            string source = txtLink.Text;
            Uri uriResult;
            result = Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;

            e.IsValid = result;
        }
        */

        //create transactions over to vms
        public void createTransactionCookie()
        {
            try
            {
                Response.Cookies["FIRST_PAYMENT"].Value = "1";
                Response.Cookies["FIRST_PAYMENT"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["AMT"].Value = newTransaction.AMT;
                Response.Cookies["AMT"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["CRM_ID"].Value = newTransaction.CRM_ID;
                Response.Cookies["CRM_ID"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["FREE_TRIAL"].Value = newTransaction.FREE_TRIAL;
                Response.Cookies["FREE_TRIAL"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PAYMENT_TIME"].Value = newTransaction.PAYMENT_TIME;
                Response.Cookies["PAYMENT_TIME"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PAYMENT_TYPE"].Value = newTransaction.PAYMENT_TYPE;
                Response.Cookies["PAYMENT_TYPE"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PAYPAL_FEE"].Value = newTransaction.PAYPAL_FEE;
                Response.Cookies["PAYPAL_FEE"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PAYPAL_ID"].Value = newTransaction.PAYPAL_ID;
                Response.Cookies["PAYPAL_ID"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PER"].Value = newTransaction.PER;
                Response.Cookies["PER"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PLAN_AMOUNT"].Value = newTransaction.PLAN_AMOUNT;
                Response.Cookies["PLAN_AMOUNT"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PLAN_TYPE_ID"].Value = newTransaction.PLAN_TYPE_ID;
                Response.Cookies["PLAN_TYPE_ID"].Expires = DateTime.Now.AddMinutes(5);
                Response.Cookies["PROFILE_ID"].Value = newTransaction.PROFILE_ID;
                Response.Cookies["PROFILE_ID"].Expires = DateTime.Now.AddMinutes(5);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---WebForm1.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList button---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Response.Redirect("PaymentError", false);
            }
        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using SGServe.CommonClass;
using Newtonsoft.Json;
using SGServe.Models.CommunityModels;
using SGServe.Models.AccountModels;
using SGServe.Models.UserBiddingModels;
using System.Threading.Tasks;

namespace SGServe
{
    public partial class BidPointsTransac : System.Web.UI.Page
    {
        Volunteer volunteer = new Volunteer();
        GlobalModule gm = new GlobalModule();
        static String currentUserID = "";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                volunteer = await gm.getLoginVolDetails();
                if (volunteer.VOL_ID == null)
                {
                    Response.Redirect("login.aspx", false);
                }
                else
                {
                    currentUserID = volunteer.VOL_ID;
                    generateTransactionList();
                    checkBonusSpinWheel();
                }
            }
        }

        protected async void generateTransactionList()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/getVolBidPointsTransaction?userID=" + currentUserID);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    //get reply to bind to gridview
                    List<BidPointTransaction> items = JsonConvert.DeserializeObject<List<BidPointTransaction>>(reply);

                    if (items != null)
                    {
                        foreach (BidPointTransaction item in items)
                        {
                            DateTime dtDisplay = Convert.ToDateTime(item.TRANSACTION_DATETIME);
                            item.TRANSACTION_DATETIME = dtDisplay.ToString("d MMM yyyy h:mm tt");
                        }

                        TransGridView.DataSource = items;
                        TransGridView.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }


        protected async void checkBonusSpinWheel()
        {
            try
            {
                Server_Connect serverConnection = new Server_Connect();
                HttpClient client = serverConnection.getHttpClient();

                String jwtToken = Session["Token"] as String;

                if (jwtToken != null)
                {
                    System.Diagnostics.Debug.WriteLine("token :" + jwtToken);
                    // add the jwt token into the authorization header for server validation
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/VolBiddingMgmt/canUserSpinWheel?userID=" + currentUserID);
                    HttpResponseMessage response = await client.SendAsync(request);
                    String reply = await response.Content.ReadAsStringAsync();

                    if (reply.Contains("true"))
                    {
                        PanelSpinWheel.Visible = true;
                    }
                    else
                    {
                        PanelSpinWheel.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                Console.WriteLine(ex.Message);
            }
        }
    }
}